#ifndef FDHOLDER_H_
#define FDHOLDER_H_

#include <stdio.h>
#include <unistd.h>

class close_closer
{
public:
	void operator()(const int fd)
	{
		if (fd != -1)
			close(fd);
	}
};

class fclose_closer
{
public:
	void operator()(FILE* fd)
	{
		if (fd != NULL)
			fclose(fd);
	}
};

template <typename T, typename closer_trait=close_closer>
class FdHolder
{
public:
	FdHolder<T, closer_trait>(T fd) : fd_(fd) {}
	~FdHolder<T, closer_trait>()
	{
		closer_trait c;
		c(fd_);
	}
	
	inline operator T() const { return fd_; }
	void reset(int fd)
	{
		closer_trait c;
		c(fd_);
		fd_ = fd;
	}
	FdHolder& operator=(T fd) { reset(fd); return *this; }
	
private:
	FdHolder& operator=(const FdHolder& rhs);
	FdHolder(const FdHolder& obj);
	
	T fd_;
};

#endif /*FDHOLDER_H_*/
