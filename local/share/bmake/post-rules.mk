# Copyright 2009-2010,2012,2015 Gabriele Sales <gbrsales@gmail.com>


ifdef BMAKE_SLOTS
__BMAKE_ANNOTATION_PREFIX=+
endif

.DEFAULT_GOAL := all
.DELETE_ON_ERROR:
.PHONY: all all.phases clean clean.full layout __bmake_env __bmake_clean_workdir
.INTERMEDIATE: $(INTERMEDIATE)


# Mark private targets as local
__BMAKE_CLEAN             := $(addsuffix .__bmake_clean,$(CLEAN) $(INTERMEDIATE))
__BMAKE_CLEAN_FULL        := $(addsuffix .__bmake_clean,$(ALL) $(CLEAN_FULL))
__BMAKE_PHASES_LAYOUT     := $(addsuffix .phase,$(__BMAKE_PHASES))
__BMAKE_PHASES_MAKEFILE   := $(addsuffix /makefile,$(__BMAKE_PHASES))
__BMAKE_PHASES_ALL        := $(addsuffix .__bmake_recursive_all,$(__BMAKE_PHASES))
__BMAKE_PHASES_CLEAN      := $(addsuffix .__bmake_recursive_clean,$(__BMAKE_PHASES))
__BMAKE_PHASES_CLEAN_FULL := $(addsuffix .__bmake_recursive_clean_full,$(__BMAKE_PHASES))

$(__BMAKE_CLEAN):             SHELL:=$(LOCAL_SHELL)
$(__BMAKE_CLEAN_FULL):        SHELL:=$(LOCAL_SHELL)
__bmake_clean_workdir:        SHELL:=$(LOCAL_SHELL)
$(__BMAKE_PHASES):            SHELL:=$(LOCAL_SHELL)
$(__BMAKE_PHASES_LAYOUT):     SHELL:=$(LOCAL_SHELL)
$(__BMAKE_PHASES_MAKEFILE):   SHELL:=$(LOCAL_SHELL)
$(__BMAKE_PHASES_ALL):        SHELL:=$(LOCAL_SHELL)
$(__BMAKE_PHASES_CLEAN):      SHELL:=$(LOCAL_SHELL)
$(__BMAKE_PHASES_CLEAN_FULL): SHELL:=$(LOCAL_SHELL)


# Top level targets
all: all.phases $(ALL)
all.phases: $(__BMAKE_PHASES_ALL)
clean: $(__BMAKE_PHASES_CLEAN) $(__BMAKE_CLEAN)
clean.full: $(__BMAKE_PHASES_CLEAN_FULL) $(__BMAKE_CLEAN_FULL) clean  __bmake_clean_workdir
layout: $(addsuffix .phase,$(__BMAKE_PHASES))

__bmake_clean_workdir:
	rm -rf .bmake

.bmake/$(BIOINFO_HOST)/parallel_paths: .bmake/$(BIOINFO_HOST)/paths
	sed -r '/export /!d; s|:=|=|; s|\$$\(([^\)]*)\)|$$\1|g' $< >$@

$(__BMAKE_PHASES):
	mkdir $@

$(__BMAKE_PHASES_LAYOUT): %.phase : % %/makefile %/rules.mk
	cd $* && bmake layout __BMAKE_DO_LAYOUT=1

$(__BMAKE_PHASES_MAKEFILE): %/makefile : makefile | %
	link_install -f $< $@


.SECONDEXPANSION:
$(__BMAKE_PHASES_ALL): %.__bmake_recursive_all: layout $$(addsuffix .__bmake_recursive_all,$$(__BMAKE_PHASE_$$*_PREV))
	+cd $* && bmake all

$(__BMAKE_PHASES_CLEAN): %.__bmake_recursive_clean: layout $$(addsuffix .__bmake_recursive_clean,$$(__BMAKE_PHASE_$$*_NEXT))
	cd $* && bmake clean

$(__BMAKE_PHASES_CLEAN_FULL): %.__bmake_recursive_clean_full: layout $$(addsuffix .__bmake_recursive_clean_full,$$(__BMAKE_PHASE_$$*_NEXT))
	cd $* && bmake clean.full
