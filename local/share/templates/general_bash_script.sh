#!/bin/bash
# Argument = -t test -r server -p password -v
set -e
set -o pipefail

usage()
{
cat << EOF
usage: $0 LOAD SECONDS

This script sleep if the load averad is greather than LOAD.
The load is checked every SECOND seconds.

OPTIONS:
   -h      Show this message
   -v      Verbose
EOF
}

VERBOSE=
while getopts "ht:r:p:v" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             usage
             exit
             ;;
     esac
done

if [[ -z $TEST ]] || [[ -z $SERVER ]] || [[ -z $PASSWD ]]
then
     usage
     exit 1
fi

shift $(($OPTIND - 1))
if [ -z $2 ]
then
	echo "ERROR $0: Wrong argument number"
	usage
	exit 1
fi

echo "remaining argumetns:" $*
while [ $(awk '{printf("%d",$3)}'< /proc/loadavg) -ge $1 ]; do 
	echo alto; awk '{print $3}'< /proc/loadavg; 
	sleep $2;
done;
