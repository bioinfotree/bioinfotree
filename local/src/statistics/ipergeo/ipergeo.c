#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	if (argc == 2 && (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0))
	{
		fprintf(stderr, "Usage: %s <PARAMS\n\n", argv[0]);
		fprintf(stderr, "PARAMS contains 4 tab separated columns holding the values for\n");
		fprintf(stderr, "the N, M, n, x parameters of the hypergeometric distribution.\n\n");
		fprintf(stderr, "return the probability of obtainign x or more white bals in n drawn without replacement from an urn which contains M white balls out of N total bals.\n");
		fprintf(stderr, "analogous of R phyper(x-1, M, N-M, n, lower.tail=FALSE)\n\n");
		return 1;
	}
	else if (argc != 1)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		return 1;
	}

	setlinebuf(stdout);
	while (1)
	{
		char line[4096];
		if (!fgets(line, 4096, stdin))
		{
			if (feof(stdin))
				return 0;
			else
			{
				fprintf(stderr, "ERROR: I/O error.\n");
				return 1;
			}
		}

		unsigned int N, M, n, x;
		const int assigned = sscanf(line, "%u\t%u\t%u\t%u\n", &N, &M, &n, &x);
		if (assigned != 4)
		{
			fprintf(stderr, "ERROR: malformed input.\n");
			return 1;
		}
		
		if (!(M>=x && N>=M && n>=x && N>=n))
		{
			fprintf(stderr, "ERROR: one of the conditions M>=x, N>=M, n>=x and N>=n is not satisfied by: %u, %u, %u, %u\n", N, M, n, x);
			return 1;
		}
		// the cumulative distribution consider the starting point as escluded, this imply the -1 in x-1 in the following line
		if(x==0){
			printf("%u\t%u\t%u\t%u\t%e\n", N, M, n, x, 1.);
		}else{
			printf("%u\t%u\t%u\t%u\t%e\n", N, M, n, x, gsl_cdf_hypergeometric_Q(x-1, M, N-M, n));
		}
	}
	
	return 0;
}
