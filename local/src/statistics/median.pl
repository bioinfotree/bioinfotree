#!/usr/bin/perl

use strict;
use warnings;

my @a = sort{$a<=>$b} map {chomp;$_} <>;

my $dim=scalar(@a);
if($dim%2!=0){
	print $a[int(scalar(@a)/2)],"\n";
}else{
	my $sx = $a[(scalar(@a) / 2) - 1];
	my $dx = $a[scalar(@a) / 2];
	my $a = ($sx+$dx)/2;
	print "$a\n";
}
