#!/usr/bin/env Rscript
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>

coerceNumericVector <- function(vector, vectorDescr="Vector", allowNA=FALSE) {
  vector <- suppressWarnings(as.numeric(vector))

  if (!allowNA && sum(is.na(vector)))
    error(paste(vectorDescr, "contains non-numeric values.", sep=" "))

  vector
}
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

parseNum <- function(s, type, name="NUM") {
  if (!(type %in% c("integer", "double", "complex")))
    error(paste("invalid numeric type: ", type, sep=""))

  original <- paste(s)
  suppressWarnings(storage.mode(s) <- type)
  if (is.na(s))
    error(paste(name, " is not a valid ", type, ": ", original, sep=""))
  s
}
#
# Copyright 2009-2012 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2012 Paolo Martini <paolo.cavei@gmail.com>
bread <- function(file="stdin", ..., sep="\t", quote="\"", check.names=FALSE, comment.char="", stringsAsFactors=FALSE) {
  tryCatch(read.table(file=file, ..., sep=sep, quote=quote,
                      check.names=check.names, comment.char=comment.char,
                      stringsAsFactors=stringsAsFactors),
           error=handleEmpty)
}

handleEmpty <- function(ex) {
  msg <- geterrmessage()
  if (msg != "no lines available in input") {
    error(msg)
  } else {
    data.frame()
  }
}
# Copyright 2011-2012 Paolo Martini <paolo.cavei@gmail.com>
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>

bwrite <- function(x, file=stdout(), ..., sep="\t", quote=FALSE) {
  args <- c(list(x), list(...))
  args$file <- file
  args$sep <- sep
  args$quote <- quote

  if (!("col.names" %in% names(args)) & is.null(colnames(x))) {
    args$col.names <- FALSE
  } else {
    if (is.logical(args$'col.names') & is.null(colnames(x))) {
      if ((length(args$'col.names') == 1) & args$'col.names')
        warning("No colnames present.")
    }
  }
  
  if (!("row.names" %in% names(args)) & is.null(rownames(x))) {
    args$row.names <- FALSE
  } else {
    if (is.logical(args$'row.names') & is.null(rownames(x))){
      if ((length(args$'row.names')==1) & args$'row.names')
        warning("No rownames present.")
    }
  }
  
  do.call(write.table, args)
}


library(optparse)

opt_spec <- list(make_option(c("-w", "--with-header"), action="store_true", default=FALSE, help="take colnames from first line in  input"),
                 make_option(c("-r", "--row-names"), action="store_true", default=FALSE, help="take row names from the first input column"),
                 make_option(c("-a", "--append"), action="store_true", default=FALSE, help="preserve the original pvalues."),
                 make_option(c("-c", "--pvalue-column"), help="the column that contains pvalues." ),
                 make_option(c("-m", "--method"), default="bh", help="the correction method to be used. Default Benjamini & Hochberg (bh)"),
                 make_option(c("-n", "--ignore-na"), action="store_true", default=FALSE, help="Ignore NA values among pvalues."))

usage = paste(
  "%prog [OPTIONS] <MATRIX >CORRECTED_PVALUES",
  "",
  "The program reads a TSV file from the standard input. It extracts a",
  "column of pvalues (by default, the rightmost one) and replaces them",
  "with the corrected values.",
  "",
  "Use --pvalue-column to change the column where the program looks for",
  "pvalues; --append if you want to preserve the original values.",
  "",
  "The following correction methods are supported (bh is the default):",
  " * Bonferroni (b)",
  " * Holm (h)",
  " * Benjamini & Hochberg (bh)",
  " * Benjamini & Yekutieli (by)",
  " * density-based fdr (qvalues from fdrtool; fdr)",
  sep="\n")

parser <- OptionParser(usage=usage, option_list=opt_spec)
parsed <- parse_args(parser, positional_arguments=TRUE)
opts   <- parsed$options
args   <- parsed$args


if (length(args) != 0 )
  error("Unexpected argument number.")

method <- opts$method

if (method == "b") {
  method <- "bonferroni"
} else if (method == "h") {
  method <- "holm"
} else if (method == "bh") {
  method <- "BH"
} else if (method == "by")  {
  method <- "BY"
} else if (method == "fdr") {
  library(fdrtool); method <- "fdr"
} else { error("Unknown correction method: ", method, "\n") }

ncolNames <- NULL
if (opts$'row-names')
  ncolNames <- 1

data <- bread(header=opts$'with-header', row.names=ncolNames)

if (dim(data)[1] == 0){
  bwrite("", eol="")
  quit()
}

if (!is.null(opts$'pvalue-column')) {
  pvalue.col <- parseNum(opts$'pvalue-column', "integer", "COL")
  if (pvalue.col > dim(data)[[2]])
    error("COL is greater than the actual column number.")
} else {
  pvalue.col <- dim(data)[[2]]
}

data[,pvalue.col] <- coerceNumericVector(data[,pvalue.col],
                                         "The pvalue column",
                                         opts$'ignore-na')

if (method == "fdr") {
  pvalue.adj <- fdrtool(data[,pvalue.col], statistic="pvalue", plot=FALSE, verbose=FALSE)$qval
} else {
  pvalue.adj <- p.adjust(data[,pvalue.col], method=method)
}

if (opts$'append') {
  data <- cbind(data, pvalue.adj)
} else {
  data[,pvalue.col] = pvalue.adj
}

bwrite(data, row.names=opts$'row-names', col.names=opts$'with-header')

