#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from itertools import izip
from labels import load_labels
from optparse import OptionParser
from sys import stdin
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

def relation_value(value, lineno):
	try:
		return float(value)
	except ValueError:
		exit('Invalid token at line %d: %s' % (lineno, value))

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] X_LABELS_FILE Y_LABELS_FILE <INCIDENCE_MATRIX >XY_MAP

		Builds a map out of an incidence matrix file.
	'''))
	parser.add_option('-z', '--print-zeros', dest='print_zeros', action='store_true', default=False, help='print matrix zeros (off by default)')
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	x_labels = load_labels(args[0])
	y_labels = load_labels(args[1])

	for line_idx, line in enumerate(stdin):
		if len(x_labels) <= line_idx:
			exit('Too many matrix rows.')

		tokens = safe_rstrip(line).split('\t')
		if len(tokens) != len(y_labels):
			exit('Invalid column number at line %d.' % (line_idx+1,))

		x_label = x_labels[line_idx]
		for y_label, token in izip(y_labels, tokens):
			value = relation_value(token, line_idx+1)
			if value != 0 or options.print_zeros:
				print '%s\t%s\t%g' % (x_label, y_label, value)
	
	if line_idx != len(x_labels)-1:
		exit('Missing matrix rows.')

if __name__ == '__main__':
	main()

