__author__ = 'Alexandre Nadin'

import os
import shutil
import time
from multiprocessing import Process, Queue, Manager

from illumina_config import IlluminaConfig
from illumina_project_restructor.illumina_config import CompressedFiles
from sample_file import Step
from sample_sheet import SampleSheet
from tools import string_utils
from tools.file_utils import file_compression as fc
from tools.file_utils import file_manipulation as fm
from tools.ky_output import coloured_output as colout
from tools.pipeline.pipeline import PipelineState

DEBUG = False
class Restructer:

    def __init__(self, run_restructer):
        self._run_retructer = run_restructer
        self._run_abs_path = self._run_retructer.get_run_abs_path()
        self._projects_dict = self._run_retructer.get_projects_dict()
        self._sample_sheet_path = self._run_retructer.get_sample_sheet_path()

    def restructurate(self):


        if self._sample_sheet_path:
            print colout.notice("\n[COPY SAMPLE SHEET]")
            self.copy_sample_sheet()

        """ Restructurate Projects """
        print colout.notice("\n[RESTRUCTURATE PROJECTS]")
        for project in self._projects_dict:
            project_path = self._run_retructer.find_project_path(project)

            project_basename = os.path.basename(project_path)
            if not str(project_basename).startswith(IlluminaConfig.PROJECT_FOLDER_TAG):
                project_path_tagged = os.path.join(self._run_abs_path, IlluminaConfig.PROJECT_FOLDER_TAG + project_basename)
                print "Renaming Project Folder: ", project_basename, "->", project_path_tagged
                fm.rename_file(project_path, project_path_tagged, verbose=DEBUG)
                project_path = project_path_tagged  # Update the project path.

            project_sample_sheet_path = os.path.join(project_path
                                                  , SampleSheet.SAMPLE_SHEET_NAME + SampleSheet.SAMPLE_SHEET_EXTENSION
                                                  )

            """ Restructurate samples """
            for sample in self._projects_dict[project]:

                """ Check the sample folder exists """
                sample_dir_path = os.path.join(project_path, IlluminaConfig.SAMPLE_FOLDER_TAG + sample.get_sample_id())
                if not os.path.isdir(sample_dir_path):
                    print "Created Sample Directory: ", sample_dir_path
                    os.mkdir(sample_dir_path)
                    fm.change_file_permission(sample_dir_path, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)

                """ Prepare the sample sheets' informations. """
                sample_sample_sheet_path = os.path.join(sample_dir_path, SampleSheet.SAMPLE_SHEET_NAME\
                                       + SampleSheet.SAMPLE_SHEET_EXTENSION)  # The sample sheet in the sample directory

                data_header = SampleSheet.SAMPLE_SHEET_SEPARATOR.join(sample.get_sample_header())
                data_sample = SampleSheet.SAMPLE_SHEET_SEPARATOR.join(sample.get_sample_row())

                """ Update the Project Sample Sheet """
                if not os.path.isfile(project_sample_sheet_path):
                    self.append_sample_sheet(project_sample_sheet_path, data_sample, data_header)
                else:
                    self.append_sample_sheet(project_sample_sheet_path, data_sample)
                fm.change_file_permission(project_sample_sheet_path, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)

                """ Update to the Sample Sample Sheet """
                if not os.path.isfile(sample_sample_sheet_path):
                    self.append_sample_sheet(sample_sample_sheet_path, data_sample, data_header)
                fm.change_file_permission(sample_sample_sheet_path, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)


    def append_sample_sheet(self, sample_sheet_path, row, header=None):
        try:
            with open(sample_sheet_path, 'a') as ss:
                if header:
                    ss.write(header)
                ss.write("\n" + row)
            return 0
        except IOError as e:
            # msg = "\n[ERROR WRITING FILE] Sample Sheet " + sample_sheet_path + ": \n\t-> " + str(e.strerror)
            # self._errors.append(msg)
            return None


    def copy_sample_sheet(self):
        """
        Copies the raw sample sheet in the run directory.

        """

        print "\n" + self._sample_sheet_path, "\n->"\
            , self._run_abs_path + SampleSheet.SAMPLE_SHEET_NAME + SampleSheet.SAMPLE_SHEET_EXTENSION
        try:
            new_sample_sheet_name = self._run_abs_path + SampleSheet.SAMPLE_SHEET_NAME + SampleSheet.SAMPLE_SHEET_EXTENSION

            shutil.copy2(self._sample_sheet_path, new_sample_sheet_name)
            fm.change_file_permission(new_sample_sheet_name, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)

        except IOError as e:
            print colout.error(string_utils.stringify("\n[ERROR COPYING FILE] Sample Sheet ", self._sample_sheet_path
                                                      , ": \n\t-> ", str(e.strerror)))
            return None


class Splitter:
    MAX_SUBMIT_TRIES = 3
    PBS_SUBMIT_DELAY = 0.2  # Delay separating each submitted job.
    MAX_PBS_NODES = 30
    PROCESS_ALL_JOBS_AT_ONCE = False

    def __init__(self, projectsDict):
        self._projectsDict = projectsDict
        self._submitting_counter = 0
        self._all_file_processed = False
        self._forbidden_sample_names = ["1r_S2_L001_R1", "2R_bis_S1_L001_R1", "Hela_SCRII_S5_L002_R2"
                                        , "10_S6_L001_R", "37_S9_L001_R1", "50_S9_L001_R1"
                                        , "50_S10_L001_R1_001", "5_S10_L001_R1", "50_S10_L001_R2"
                                        ]
        self._sample_files = self.get_sample_files()


    def get_sample_files(self):
        sample_file_counter = 0
        sample_files = []
        for project in self._projectsDict:
            # print "\nProject: ", project
            for sample in self._projectsDict[project]:
                # print "  Sample:", sample.get_sample_id()
                for sample_file in sample.get_sample_files():
                    sample_file_counter += 1
                    # print "    Sample File:", sample_file.get_sample_file_name()
                    # for frag in sample_file.get_fragment_files():
                        # print "      frag", frag
                    sample_files.append(sample_file)
        return sample_files

    def split(self):
        print colout.notice("\n--[SPLITTING]--")
        ttime = time.time()

        unprocessed_count = 0
        """ Submit jobs until all sample files are processed or max number of tries is exceeded. """
        for submit_counter in range(1, Splitter.MAX_SUBMIT_TRIES+1):
            self._submitting_counter = submit_counter
            unprocessed_list = self.get_unprocessed_sample_files()
            unprocessed_count = unprocessed_list.__len__()
            if submit_counter > 1 and unprocessed_count > 0:
                print colout.error(header_only=True), unprocessed_count, "processes failed as their jobs were lost by PBS. Retrying..."
            if unprocessed_count == 0:
                break

            # raw_input("...press to process...")
            print colout.notice("\n[Submitting jobs]"), "Serie Nb.", submit_counter, ": "\
                , unprocessed_list.__len__(), "sample file(s) to process."
            self.process_sample_files(unprocessed_list)
            # self.test_sample_files(unprocessed_list)
            # unprocessed_list = self.get_unprocessed_sample_files()



        splitting_time = time.time() - ttime

        ## TODO : Try to refactor this with the chunk in the loop
        unprocessed_list = self.get_unprocessed_sample_files()
        unprocessed_count = unprocessed_list.__len__()
        if unprocessed_count > 0:
            print colout.error(header_only=True), unprocessed_count, "processes failed as their jobs were lost by PBS."

        strTime = str(splitting_time) + " sec" if splitting_time < 60 else str(splitting_time/60) + " min"

        if unprocessed_count == 0:
            print colout.valid("\n[Splitting over]"), "all the samples have been processed in", strTime, "."
            return PipelineState.SUCCESS
        else:
            print colout.error("\n[Splitting over]"), unprocessed_count, "sample(s) could not be processed. It took"\
                    , strTime, "."
            return PipelineState.FAIL

    def get_summary_sample_files(self):
        msg = colout.notice("\n[Summary Sample Files]")
        for sample_file in self._sample_files:
            msg += string_utils.stringify("\n  ", sample_file.get_sample_file_name(), "step: ." + sample_file.get_current_step() + ".", sample_file)

    def get_unprocessed_sample_files(self):
        """
        Lists the sample files that were not successfully processed.
        :param sample_files:
        :return:
        """
        print colout.notice("\n[Check unprocessed files]")
        unprocessed_list = []
        for index, a_sample_file in enumerate(self._sample_files):
            if a_sample_file.get_current_step() != Step.SPLIT:
                unprocessed_list.append((index, a_sample_file))
                print colout.error("  x"), a_sample_file.get_sample_file_name(), ":", a_sample_file.get_fragment_files()
            else:
                print colout.valid("  v"), a_sample_file.get_sample_file_name(), ":", a_sample_file.get_fragment_files()
        return unprocessed_list

    def process_sample_files(self, indexed_sample_file_tuples):
        manager = Manager()
        list_results = manager.list()  # List designed for multiprocessing -> information is not lost between processes.
        job_list = []
        counter = 0

        # print "\nTesting processing", counter_limit, "jobs in parallel."
        for position, sample_file in indexed_sample_file_tuples:
            if sample_file.reset_step() == PipelineState.FAIL: # Reset files' extension
                print colout.error("  [KO]"), "Could not reset step for sample file", sample_file.get_sample_file_name
                continue
            counter += 1
            p = Process(target=self.split_sample_file, args=(position, sample_file, list_results))
            job_list.append(p)

            """ Process all the jobs at once or sequentially? """
            if not Splitter.PROCESS_ALL_JOBS_AT_ONCE and counter >= Splitter.MAX_PBS_NODES:
                counter = 0
                if not self.process_some_jobs(job_list) == PipelineState.SUCCESS:
                    print colout.error("  [KO]"), "Jobs could not be joined."
                self.update_sample_files(list_results)
                print colout.valid("\n  samplefiles updated")

        ## Process remaining jobs
        if job_list.__len__() > 0 and not self.process_some_jobs(job_list) == PipelineState.SUCCESS:
            print colout.error("  [KO]"), "Jobs could not be joined."
        else:
            ## Update sample files
            self.update_sample_files(list_results)
            print colout.valid("\n  samplefiles updated")

        print colout.valid(string_utils.stringify("\nAll jobs processed.\n"))

    def process_some_jobs(self, jobs_list):
        print colout.notice(string_utils.stringify("\nSubmitting", jobs_list.__len__(), "jobs at a time..."))
        # raw_input("press key to proceed...")
        job_counter = 0
        for job in jobs_list:
            job_counter += 1
            job.start()
            print colout.notice(string_utils.stringify("   started job nb.", job_counter, " (pid:", job.pid
                                                       , ", alive:", job.is_alive(), ")"))
            time.sleep(Splitter.PBS_SUBMIT_DELAY)
        print colout.valid("\n    all jobs started.")
        print colout.notice("\n   trying to join the " + str(jobs_list.__len__()) + " jobs...")

        """ Join all process and empty the list at the same time """
        job_counter = 0
        while jobs_list.__len__() > 0:
            job_counter += 1
            job = jobs_list.pop(0)
            job.join()
            print colout.valid(string_utils.stringify("   joined job nb.", job_counter, " (pid:", job.pid
                                                       , ", alive:", job.is_alive(), ")"))
        print colout.valid("\n   Jobs joined.")

        return PipelineState.SUCCESS

    def split_sample_file(self, position, sample_file, list_results):
        print colout.notice("\n+"), "Splitting sample file", sample_file.get_sample_file_name()\
            , sample_file.get_fragment_files(), "(job nb.", str(position+1), ")"

        # sample_file.update_step(Step.SPLITTING)  ## REMOVE ME
        # os.listdir('/home/anadin')  ## REMOVE ME
        # time.sleep(2)
        # if 1==2:  ## REMOVE ME
        #
        msg = ""
        if sample_file.update_step(Step.SPLITTING) == PipelineState.SUCCESS:
            print string_utils.stringify("\n  ", sample_file.get_fragment_files(), "to split in"
                                         , sample_file.get_sample_destination())

            result_split = fc.split_gz_file(file_names=sample_file.get_fragment_files_paths()
                                            , common_part_name=sample_file.get_sample_file_name()
                                            , chunks_per_split=CompressedFiles.CHUNKS_PER_SPLIT
                                            , lines_per_chunk=CompressedFiles.LINES_PER_CHUNK
                                            , file_permission=IlluminaConfig.CREATED_FILES_PERMISSION
                                            , destination=sample_file.get_sample_destination()
                                            , verbose=DEBUG
                                            )

            # result_split = 1  ## REMOVE ME
            # if self._submitting_counter == 1 and sample_file.get_sample_file_name() in self._forbidden_sample_names:
            #     result_split = 0
            # elif self._submitting_counter == 2 and sample_file.get_sample_file_name() == self._forbidden_sample_names[5]:
            #     result_split = 0
            # elif sample_file.get_sample_file_name() == self._forbidden_sample_names[5]:
            # elif sample_file.get_sample_file_name() == self._forbidden_sample_names[9]:
            #     result_split = 0

            if result_split == 1:
                # sample_file.update_step(SampleFile.STEP_SPLIT)
                sample_file.update_step(Step.SPLIT)
                print colout.valid("\n [OK]"), "Sample file", sample_file.get_sample_file_name(), "succesfully split."
                # for frag in sample_file.get_fragment_files():
                #     if not os.path.isfile(os.path.join(sample_file.get_sample_destination(), frag)):
                #         os.mkdir(os.path.join(sample_file.get_sample_destination(), frag))
            else:
                print colout.error("\n [KO]"), "Sample file", sample_file.get_sample_file_name(), "could not be split."

        else:
            print colout.error("\n [KO]"), "Sample file", sample_file.get_sample_file_name(), "'s step could not be initialized."

        ## Sample file has to be updated anyway since extension changed.
        list_results.append((position, sample_file))

    def update_sample_files(self, list_results):
        """
        Updates the original sample files using their array position.
        :param position_sample_files:
        :return:
        """
        while list_results.__len__() > 0:
            position, sample_file = list_results.pop()
            self._sample_files[position] = sample_file

if __name__ == "__main__":
    pass
