__author__ = 'Alexandre Nadin'

from multiprocessing import Process, Manager
import time

def test_args(index, name, time_sleep, result_dic):
    # msg = "processed-" + str(arg) + "(pid: "
    # print msg
    time.sleep(time_sleep)
    msg = 'processed-%s-%s (pid: %d)' %(str(index), str(name), getpid())
    result_dic[index] = name
    # print msg

if __name__ == "__main__":
    max_jobs = 1000
    time_job_sleep = 10
    mgr = Manager()
    result_dict = mgr.dict()

    proc_list = []
    print "\n[STARTING JOBS]"
    print "\tProcessing jobs: ", max_jobs, "\n\ttime sleep for job: ", time_job_sleep, "s."

    for x in range(1, max_jobs):
        p = Process(target=test_args, args=(x, "job_" + str(x), time_job_sleep, result_dict), name="job_" + str(x))
        p.start()
        # print "Started", p.name, "\tpid: ", p.pid
        proc_list.append(p)
        # time.sleep(1)

    print "\n[TRACKING JOBS]"
    for p in proc_list:
        if p.is_alive():
            print "v Tracking", p.name, "\tpid: ", p.pid, "  -> alive? ", p.is_alive()
        else:
            print "x Tracking", p.name, "\tpid: ", p.pid, "  -> alive? ", p.is_alive()


    print "\n[JOINING JOBS]"
    for p in proc_list:
        print "Joining", p.name, "\tpid: ", p.pid, "  -> alive? ", p.is_alive()
        p.join()
        print " Joined", p.name, "\tpid: ", p.pid, "  -> alive? ", p.is_alive()
        # print "  -> is alive? ", p.is_alive()
        # print "\ttarget: ", p._target
        # print "\targs: ", p._args
        # print "\tkwargs: ", p._kwargs
        # print "\tauthkey: ", p._authkey
        # print "\tdic: ", p.

    print "\n[CHECK RESULTS]"
    for x in range(1, max_jobs):
        if x not in result_dict:
            print "x job No. " + str(x), "missing result."
        else:
            print "v job No. " + str(x), " -> ", result_dict[x]
