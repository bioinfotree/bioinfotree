__author__ = 'alexandre-nadin'

import multiprocessing
from multiprocessing import Process, Event, Queue
import time
from tools.ky_output import kprint
from tools.ky_output import coloured_output as colout

class ProgressBar:

    def __init__(self, max_value, queue_values=None):
        self._curr_value = 0
        self._max_value = max_value
        self._progress = ""
        self._buildProgressBar()
        self._queue_values = queue_values

    def _buildProgressBar(self):
        max_percent = 100
        curr_value = (max_percent * self._curr_value) / self._max_value\
            if self._curr_value < self._max_value else max_percent

        self._progress = "[" + curr_value * "=" + ">" + (max_percent - curr_value) * " " + "] " + str(curr_value) + "%"


    def update(self, curr=None, max=None):
        if curr is not None:
            self._curr_value = curr
        if max is not None:
            self._max_value = max
        self._buildProgressBar()

    def appendValue(self, value):
        self._curr_value += value
        self._buildProgressBar()

    def getProgress(self):
        return self._progress


class Worker(multiprocessing.Process):

    def __init__(self, value, results_queue, blocks):
        Process.__init__(self)
        self.value = value
        self.results = results_queue
        self.blocks = blocks

    def run(self):
        print 'In %s: %s blocks' % (self.name, self.blocks)
        self.results.put(self.blocks)
        return


def buildProgressBar(curr, maxi):
    max_value = 100
    curr_value = (max_value * curr) / maxi if curr < maxi else max_value
    return "[" + curr_value * "=" + ">" + (max_value - curr_value) * " " + "] " + str(curr_value) + "%"


def work(value, progress_queue):
    time.sleep(1)
    progress_queue.put(value)


def work(value, progress_queue, progress_bar):
    time.sleep(1)
    progress_queue.put(value)
    progress_bar.appendValue(value)
    kprint.replaceLine("Treated bytes: " + str(value) + colout.valid(progress_bar.getProgress()), new_line=True)


def testDef():

    values = (10, 30, 40, 100, 20)
    total = sum(values)
    print values, " -> ", total

    jobs = []
    results = Queue()
    nb_jobs = values.__len__()
    for i in range(nb_jobs):
        p = Worker(i+1, results, values[i])
        jobs.append(p)

        p.start()

    advancement = 0
    while nb_jobs:
        block = results.get()
        advancement += block
        kprint.replaceLine("Treated blocks: " + str(advancement), new_line=True)
        kprint.appendLine(str(advancement) + " blocks ")
        kprint.appendLine(kprint.validateLine(buildProgressBar(advancement, total)))
        nb_jobs -= 1

    for j in jobs:
        j.join()


def testClassQueue():

    projects = {
        1 : (20, 40, 130, 2, 45)
        , 2 : (45, 23, 90)
        , 3 : (23, 4, 356)
    }
    total_bytes = 0
    total_files = 0

    # for project in projects.keys():
    #     total_bytes += sum(projects[project])
    #     total_files += 1


    jobs = []
    results = Queue()

    for project in projects.keys():
        for value in projects[project]:
            total_bytes += value
            total_files += 1
            # p = Worker(0, results, value)
            # jobs.append(p)
            # p.start()

    progress_queue = Queue()
    pbar = ProgressBar(total_bytes)

    for project in projects.keys():
        for value in projects[project]:
            progress_queue.put(value)
            p = Process(target=work, args=(value, progress_queue, pbar))
            p.start()

    # print "total bytes: ", total_bytes
    # print "total files: ", total_files
    #
    # counter = total_files
    # advancement = 0
    # while counter:
    #     blocks = progress_queue.get()
    #     advancement += blocks
    #     # print "[BLOCK] got ", blocks, " bytes."
    #     time.sleep(0.2)
    #     pbar.appendValue(blocks)
    #     #     # kprint.replaceLine("Treated blocks: " + str(advancement), new_line=False)
    #     #     # kprint.appendLine(str(advancement) + " blocks ")
    #     kprint.replaceLine("Treated bytes: " + str(advancement) + colout.valid(pbar.getProgress()), new_line=True)
    #     counter -= 1




def testClass():

    projects = {
        1 : (20, 40, 130, 2, 45)
        , 2 : (45, 23, 90)
        , 3 : (23, 4, 356)
    }
    total_bytes = 0
    total_files = 0

    # for project in projects.keys():
    #     total_bytes += sum(projects[project])
    #     total_files += 1


    jobs = []
    results = Queue()

    for project in projects.keys():
        for value in projects[project]:
            total_bytes += value
            total_files += 1
            p = Worker(0, results, value)
            jobs.append(p)
            p.start()

    print "total bytes: ", total_bytes
    print "total files: ", total_files
    pbar = ProgressBar(total_bytes)

    advancement = 0
    while total_files:
        time.sleep(0.1)
        blocks = results.get()
        advancement += blocks
        pbar.appendValue(blocks)
        # pbar.update(curr=advancement)
        kprint.replaceLine("Treated blocks: " + str(advancement), new_line=False)
        kprint.appendLine(colout.valid(pbar.getProgress()))

        total_files -= 1

    for j in jobs:
        j.join()


def testPrint():
    print "line 0"
    kprint.replaceLine("Line 1")
    time.sleep(1)
    kprint.appendLine("line2")
    kprint.appendLine("line3")
    kprint.replaceLine("line 4")

if __name__ == '__main__':
    # testDef()
    # testClass()
    testClassQueue()
    # testPrint()
