__author__ = 'Alexandre Nadin'

import sys, os
path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../../")
sys.path.append(path)
from tools.ky_multiprocessing import function_processor as fp
from tools.file_utils import file_compression as fc
from illumina_project_restructor.illumina_config import IlluminaConfig

PROCESS_TYPES = ["series", "parallel", "read"]

def start_splitting(file_name):
    pass

def get_manual():
    return "\n\tWho: \tmass splitting.\n"\
         + "\n\tWhat: \tSplits files in series, parallel, or other features.\n"\
         + "\n\tHow: \t$ python this-script process-type files ...\n"\
         + "\n\tAvailable actions:", PROCESS_TYPES

def get_parsed_args(args):
    if args.__len__() < 3:
        print "Too few arguments provided.\n", get_manual()
        sys.exit(1)
    (script, type, files) = args.pop(0), args.pop(0), args
    if not type in PROCESS_TYPES:
        print "Please specify a correct way of splitting your files: ", PROCESS_TYPES, "\n", get_manual()
        sys.exit(2)
    for f in files:
        if not os.path.isfile(f):
            print "This is not a file:", f, "\nPlease feed me with valid files.\n", get_manual()
            sys.exit(3)
    return (script, type, files)

if __name__ == "__main__":
    (script, type, files) = get_parsed_args(sys.argv)

    print "Files to split: ", files
    ## Process in series
    if type == PROCESS_TYPES[2]:
        print "\n[Simply reading]"
        for f in files:
            print "\nReading file:", f
            print "\tOK: Found", fc.get_file_lines(f), "lines.\n"
    elif type == PROCESS_TYPES[0]:
        print "\n[Processing in series]"
        for f in files:
            print "\nSplitting file:", f
            fc.split_gz_file(file_names=f, chunks_per_split=16000000, lines_per_chunk=4, verbose=True)
            print "\tOK\n"
    ## Process in parallel
    elif type == PROCESS_TYPES[1]:
        print "\n[rocessing in parallel]"
        import functools
        proc_list = []
        for f in files:
            proc_list.append(
                functools.partial(
                    fc.split_gz_file
                    , file_name=f
                    , chunks_per_split=16000000
                    , lines_per_chunk=4
                    , file_permission=IlluminaConfig.CREATED_FILES_PERMISSION
                    , verbose=True
                    , dest="test-splitting"
                )
            )
        fp.map_partial_functions(proc_list)


