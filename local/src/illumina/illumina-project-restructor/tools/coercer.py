__author__ = 'alexandre-nadin'

def coerce2int(nb, rounded=True):
    """
    Coerce an Object to an integer.
    Tries to round to upper number if it is a float.
    :param nb:
    :return:
    """
    try:
        return int(round(float(nb))) if rounded else int(float(nb))
    except ValueError:
        return None

def coerce2ints(ints):
    n_ints = []
    for i in ints.__len__():
        n_int = coerce2int(i)
        if n_int is None:
            return None
        n_ints.append(n_int)
    return n_ints

def percentage2int(s_percentage, rounded=True):
    return coerce2int(str(s_percentage).strip().strip('%').strip(), rounded=rounded)


def tests():
    print percentage2int("  45 % ")
    print percentage2int("  45.5 % ", rounded=True)
    # print coerce2int(None, rounded=True)
    print coerce2int("001", rounded=True)
    print coerce2int("001.6", rounded=True)
    print coerce2int("001.6", rounded=False)

if __name__ == "__main__":
    tests()
