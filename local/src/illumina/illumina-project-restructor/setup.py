__author__ = 'alexandre-nadin'

from setuptools import setup
import os


PROJECT_NAME = 'illumina-project-restructor'
SCRIPTS_PATH = 'scripts/'
README_NAME = 'README.rst'

def getScriptList():
      scripts = []
      for script in os.listdir(SCRIPTS_PATH):
            scripts.append(SCRIPTS_PATH + script)
      return scripts

def getPackageNames():
      packages = ['illumina_project_restructor']
      return packages

def readme():
      with open(README_NAME) as f:
            return f.read()


setup(name=PROJECT_NAME,
      version='0.1',
      description='Useful tools in python',
      long_description=readme(),  # Will be in package/PKG-INFO file
      url=None,
      author=__author__,
      author_email='nadin.alexandre@hsr.it',
      license=None,
      packages=getPackageNames(),
      zip_safe=False,

      # Specify dependencies ('markdown')
      install_requires=[

      ],

      include_package_data=True,  # Tells to include the files specified in the MANIFEST.in

      scripts=getScriptList()
      )

