#!/usr/bin/env python
__author__ = 'Alexandre Nadin'

import sys
import os.path
import argparse
import logging
from tools.file_utils import file_manipulation as fm
from tools.ky_output import coloured_output as colout
from tools import string_utils
from illumina_project_restructor.illumina_config import IlluminaConfig, Configuration
from illumina_project_restructor.illumina_restructor import IlluminaRunRestructor

def getParser():
    """
    Parses the input arguments.
    :param argv:
    :return: The parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Restructure an Illumina output folder to the old way.")
    parser.add_argument("run_folder", help="The RUN folder to be restructured.", type=str)
    parser.add_argument("-x", "--execute", help="Execute the program once validity is checked.", action="store_true")
    # parser.add_argument("-s", "--split_gz", help="Splits the compressed files after the restructuring.", action="store_true")
    parser.add_argument("-o", "--old_sample", help="Fetches the old sample sheets, without corresponding extension."
                        , action="store_true")
    parser.add_argument("-c", "--config", help="Generates a user configuration file on the specified folder."
                        , action="store_true")
    parser.add_argument("--sample-sheet", help="Specify a custom sample sheet.", type=str)
    return parser

def set_logger():
    FORMAT = '%(asctime)s - %(levelname)s - %(name)s  - %(message)s'
    logging.basicConfig(filename="test1.log", level=logging.INFO, format=FORMAT)

def main(argv=None):
    parser = getParser()
    if argv is None:
        argv = sys.argv


    args = parser.parse_args(argv[1:])
    if args.run_folder and not os.path.exists(args.run_folder) and not os.path.isdir(args.run_folder):
        print colout.error(string_utils.stringify("\n Folder ", args.run_folder, " does not exist.\n")
                           , with_header=True)
        parser.print_help()
        return None

    ## Manage configuration file.
    cfg = Configuration()
    if args.config:
        file_path = os.path.join(args.run_folder, IlluminaConfig.CONFIG_FILE_NAME)
        cfg.writeIniConfigFile(file_path)
        print "Config file successfully written: " + file_path + "."\
                + "\nPlease check it then relaunch the command.\n"\
                + colout.warn("", with_header=True)\
                + " Note that it will overwrite any existing configuration file present in your working directory."

        return None

    ## Update Configurations
    if cfg.updateConfiguration(args.run_folder) is None:
        return None

    ## Start restructuring
    restructer = IlluminaRunRestructor(args.run_folder, args.old_sample, sample_sheet=args.sample_sheet)

    if not args.execute:
        print restructer.get_summary()
    else:
        if restructer.is_valid_run():
            # restructer.start_restructure_projects()  # Original way
            # if args.split_gz:
            #     print "asked for splitting"
            #     restructer.start_splitting_files()
            # else:
            #     print "Your files were not split. "
            restructer.start_restruct_tracking()  # New way for tracking
        else:
            print restructer.get_summary()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
    # sys.exit("RETRY")



