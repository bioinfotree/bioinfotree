#!/usr/bin/env python
# encoding: utf-8

from math import ceil, exp, floor, log10
from optparse import OptionParser
from sys import exit, maxint, stdin

class Counter(object):
	def __init__(self, fd, options):
		self.fd = fd
		self.bin_num = self._int_or_none(options.bin_num)
		self.bin_size = self._int_or_none(options.bin_size)
		self.coord_min = self._int_or_none(options.coord_min)
		self.coord_max = self._int_or_none(options.coord_max)
	
	def __iter__(self):
		bins = [0] * self.bin_num
		bin_size = self.bin_size
		coord_min = self.coord_min
		coord_max = self.coord_max
		
		for start, stop in self._value_iterator():
			if start == coord_max:
				bins[-1] += 1
			else:
				start_idx =  int(floor((start - coord_min) / bin_size))
				stop_idx = min(self.bin_num-1, int(floor((stop - coord_min) / bin_size)))
			
				for bin_idx in xrange(start_idx, stop_idx+1):
					bins[bin_idx] += 1
		
		for bin_num, bin_count in enumerate(bins):
			coord = coord_min + (bin_num * bin_size)
			yield coord, bin_count
	
	def _int_or_none(self, value):
		if value is None:
			return None
		else:
			return int(value)
	
	def _compute_bin_params(self):
		if self.bin_num is None and self.bin_size is None:
			self.bin_num = 100
		
		span = self.coord_max - self.coord_min
		if self.bin_num:
			self.bin_size = span / self.bin_num
		elif self.bin_size:
			self.bin_num = int(ceil(span / self.bin_size))

class InMemoryCounter(Counter):
	def __init__(self, fd, options):
		Counter.__init__(self, fd, options)
		self.coords = self._load_values()
		self._compute_bin_params()
	
	def _load_values(self):
		coords = []
		coord_min = 0xffffffff
		coord_max = 0
		
		for lineno, line in enumerate(self.fd):
			try:
				tokens = line.rstrip().split('\t')
				if len(tokens) != 2:
					raise ValueError
				
				start, stop = [ int(t) for t in tokens ]
				if start >= stop:
					raise ValueError
				
			except ValueError:
				exit('Invalid input at line %d.' % (lineno+1))
			
			if self.coord_min is None:
				coord_min = min(coord_min, start)
			elif start < self.coord_min:
				continue
			
			if self.coord_max is None:
				coord_max = max(coord_max, stop)
			elif stop > self.coord_max:
				continue
			
			coords.append((start, stop))
		
		if self.coord_min is None:
			self.coord_min = coord_min
		if self.coord_max is None:
			self.coord_max = coord_max
		
		return coords
	
	def _value_iterator(self):
		return self.coords.__iter__()

class OutOfMemoryCounter(Counter):
	def __init__(self, fd, options):
		Counter.__init__(self, fd, options)
		assert self.coord_min is not None
		assert self.coord_max is not None
		self._compute_bin_params()
	
	def _value_iterator(self):
		for lineno, line in enumerate(self.fd):
			try:
				tokens = line.rstrip().split('\t')
				if len(tokens) != 2:
					raise ValueError
				
				start, stop = [ int(t) for t in tokens ]
				if start >= stop:
					raise ValueError
				
			except ValueError:
				exit('Invalid input at line %d.' % (lineno+1))
			
			if start < self.coord_min or stop > self.coord_max:
				continue
			
			yield start, stop

def main():
	parser = OptionParser(usage='%prog [DATA_FILE]')
	parser.add_option('-a', '--max', dest='coord_max', type='int', default=None, help='maximum coordinate showed in the graph', metavar='MAX')
	parser.add_option('-i', '--min', dest='coord_min', type='int', default=None, help='minimum coordinate showed in the graph', metavar='MIN')
	parser.add_option('-n', '--bin-num', dest='bin_num', type='int', default=None, help='number of bins (default: 100)', metavar='BINNUM')
	parser.add_option('-m', '--out-of-memory', dest='out_of_memory', action='store_true', default=False, help='enable out of memory mode (for big datasets)')
	parser.add_option('-s', '--bin-size', dest='bin_size', type='float', default=None, help='size of each bin', metavar='BINSIZE')
	options, args = parser.parse_args()

	if len(args) > 1:
		exit('Unexpected argument number.')
	elif None not in (options.bin_num, options.bin_size):
		exit("Can't use both bin-num and bin-size options.")
	elif options.bin_num is not None and options.bin_num <= 0:
		exit('Invalid bin number value.')
	elif options.bin_size is not None and options.bin_size <= 0:
		exit('Invalid bin size value.')
	
	if len(args):
		fd = file(args[0], 'r')
	else:
		fd = stdin
	
	if options.out_of_memory:
		counter = OutOfMemoryCounter(fd, options)
	else:
		counter = InMemoryCounter(fd, options)

	for bin_start, count in counter:
		print '%f\t%d' % (bin_start, count)

if __name__ == '__main__':
	main()
