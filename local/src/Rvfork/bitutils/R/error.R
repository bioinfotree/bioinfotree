# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

error <- function(message) {
  if (interactive()) {
    stop(message, call.=FALSE)
  } else {
    cat("[ERROR] ", getProgName(), ": ", message, "\n", file=stderr(), sep="")
    quit(save="no", status=1)
  }
}
