\name{parseNum}
\alias{parseNum}
\title{Parses a string containing a numerical value}
\description{This function extracts a numerical value from a string, producing a suitable error message if the representation is malformed.}
\usage{parseNum(s, type, name="NUM")}
\arguments{
  \item{s}{The string to be parsed.}
  \item{type}{The type of the number: "integer", "double" or "complex".}
  \item{name}{A name representing the number being coverted. Used in error messages. Default "NUM"}
}
\value{The parsed number.}
\references{BioinfoTree Core Utilities \url{http://bioinfotree.org}}
\author{Gabriele Sales \email{gbrsales@gmail.com}}
