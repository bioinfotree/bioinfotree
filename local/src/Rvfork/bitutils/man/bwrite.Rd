\name{bwrite}
\alias{bwrite}
\title{Wrapper to write tables in tab delimited files.}
\description{Writer of table in tab delimited files. Quotes are not printed.}
\usage{bwrite(x, file=stdout(), ..., sep="\t", quote=FALSE)}
\arguments{
  \item{x}{the object to be written, preferably a matrix or data frame.
    If not, it is attempted to coerce \code{x} to a data frame.}
  \item{file}{the file name. Default is standard output.}
  \item{...}{argument passed directly to \code{\link{write.table}}.}
  \item{sep}{a string used to separate columns.  Default sep = "\\t" gives
    tab delimited output.}
  \item{quote}{the set of quoting characters. To disable quoting altogether,
    use quote="". See scan for the behaviour on quotes
    embedded in quotes. Quoting is only considered for columns
    read as character, which is all of them unless colClasses
    is specified.}
}

\seealso{\code{\link{write.table}}}

\references{BioinfoTree Core Utilities \url{http://bioinfotree.org}}
\author{Paolo Martini \email{paolo.cavei@gmail.com}}
