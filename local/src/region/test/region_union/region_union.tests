#!/usr/bin/env tool_test
#
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>

* Minimal columns number
  In:   region_union.in
  Fail: yes
  Cmd: cut -f-3 $1 | region_union

* Default functionality (4 columns: chr, start, stop, label)
  In:   region_union.in
  Out:  region_union_default.out
  Cmd:  cut -f-4 $1 | region_union >$@

* Default functionality (5 columns: chr, start, stop, label, label)
  In:   region_union.in
  Out:	region_union_default_5.out
  Cmd:	region_union -o 10 <$1 >$@

* Additional glue option (use the glue to separate multi-column IDs)
  In:   region_union.in
  Out:	region_union_default_region-id-glue.out
  Cmd:	region_union -o 10 --region-id-glue - <$1 >$@

* Sorted input
  In:   region_union.in
  Fail: yes
  Cmd:  cat $1 $1 | region_union

* Sorted input (assume column 1 is already sorted)
  In:   region_union.in
  Out:  region_union_already-sorted.out
  Cmd:  cat $1 $1 | cut -f-4 | region_union -s --allow-duplicates >$@

* Duplicated labels
  In:   region_union_duplicates.in
  Fail:	yes
  Cmd:	region_union <$1

* Allow duplicates in labels
  In:   region_union_duplicates.in
  Out:	region_union_duplicates.out
  Cmd:	region_union -o 10 --allow-duplicates <$1 >$@

* Glue option (glue to separate IDs of merged regions)
  In:   region_union.in
  Out:  region_union_glue.out
  Cmd:  cut -f-4 $1 | region_union -o 10 -g : >$@

* Minimal overlap to merge regions
  In:   region_union.in
  Out:  region_union_overlap-5.out
  Cmd:  cut -f-4 $1 | region_union -o 5 >$@

* Unsorted coordinates: start greater than stop
  In:   region_union.in
  Fail:	yes
  Cmd:	select_columns 1 3 2 4 <$1 | region_union

* Unsorted start in group
  In:   region_union_wrong-start.in
  Fail:	yes
  Cmd:	region_union <$1

* Invalid coordinates specification: start
  In:   region_union.in
  Fail:	yes
  Cmd:	sed 's/45/a/' $1 | region_union

* Invalid coordinates specification: end
  In:   region_union.in
  Fail:	yes
  Cmd:	sed 's/70/a/' $1 | region_union

* Invalid coordinates specification: start >= stop
  In:   region_union.in
  Fail:	yes
  Cmd:	select_columns 1 3 2 4 <$1 | region_union

* Invalid minimum overlap
  In:
  Fail: yes
  Cmd:  region_union -m -2
