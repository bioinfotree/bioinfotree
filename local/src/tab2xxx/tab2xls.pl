#!/usr/bin/perl

use strict;
use warnings;
use Spreadsheet::WriteExcel;
use Data::Dumper;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-f|fasta] < in.tab_separed > out.xls\n
	-f|fasta	one sheet per fasta block
	--header	use .META to generate the header
	-a		abbreviate the header used as sheet label by removing vocals (s/[aeiouAEIOU]+//g;) and then truncating to 31 chars
";

my $help=0;
my $fasta=0;
my $header=0;
my $abbreviate_header=0;
GetOptions (
	'h|help' => \$help,
	'f|fasta' => \$fasta,
	'header' => \$header,
	'a|abbreviate_header' => \$abbreviate_header,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $dest_book  = Spreadsheet::WriteExcel->new('/dev/stdout');
my $dest_sheet = $dest_book->addworksheet() if not $fasta;
my $i=0;
if($header){
	my $input_file=$ARGV[0];
	die("in -H mode give a filename as argument.") if not $input_file;
	my $h=`bawk -M $input_file | cut -f 2`;
	my @T=split("\n",$h);
	my $j=0;
	for(@T){
		$dest_sheet->write($i, $j,$_);
		$j++;
	}
	$i++;
}


while(<>){
	chomp;
	if($fasta and s/^>//){
		if(length>31){
			if($abbreviate_header){
				s/[aeiouAEIOU]+//g;
				$_=substr($_,0,31);
			}else{
				die("The string ($_) is too long to be used as sheet label, try the -a option.")
			}
		}
		$dest_sheet = $dest_book->addworksheet($_);
		$i=0;
		next;
	}
	my @F = split(/\t/);
	my $j=0;
	for(@F){
		$dest_sheet->write($i, $j,$_);
		$j++;
	}
	$i++;
	if($i>65536){
		die("ERROR: cannot convert files with more than 65536 rows, use tab2xlsx instead.")
	}
}


$dest_book->close();
