#!/usr/bin/perl

use strict;
use warnings;
use Excel::Writer::XLSX;
use Data::Dumper;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-f|fasta] < in.tab_separed > out.xls\n
	-f|fasta		one sheet per fasta block
	--header		use .META to generate the header
	-a			abbreviate the header used as sheet label by removing vocals (s/[aeiouAEIOU]+//g;) and then truncating to 31 chars
	-c|colors_filename 	the file contain a tab delimited list of couple (value, color), e.g.  <(echo -e \"0\\twite\\n1\\tblue\\n2\\tred\\n3\\tpurple\") 
";

my $help=0;
my $fasta=0;
my $header=0;
my $abbreviate_header=0;
my $colors_filename=undef;
GetOptions (
	'h|help' => \$help,
	'f|fasta' => \$fasta,
	'header' => \$header,
	'c|colors=s' => \$colors_filename
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $dest_book  = Excel::Writer::XLSX->new('/dev/stdout');



my %formats=();
if($colors_filename){
	open(COLORS,"<","$colors_filename") or die("Can't open colors filename ($colors_filename)");
	while(<COLORS>){
		chomp;
		my ($value, $color)=split("\t",$_);
		$formats{$value}=$dest_book->add_format(color => $color, bg_color => $color );
	}
}


my $dest_sheet = $dest_book->add_worksheet() if not $fasta;
my $i=0;
if($header){
	my $input_file=$ARGV[0];
	die("in -H mode give a filename as argument.") if not $input_file;
	my $h=`bawk -M $input_file | cut -f 2`;
	my @T=split("\n",$h);
	my $j=0;
	for(@T){
		$dest_sheet->write($i, $j,$_);
		$j++;
	}
	$i++;
}


while(<>){
	chomp;
	if($fasta and s/^>//){
		$dest_sheet = $dest_book->add_worksheet($_);
		$i=0;
		next;
	}
	my @F = split(/\t/);
	my $j=0;
	for(@F){
		my $f = undef;
		if(defined($colors_filename) and $i>0 and $j>0 and defined($formats{$_})){
			$f=$formats{$_}
		}
		if(defined($f)){
			$dest_sheet->write($i, $j,$_,$f);
		}else{
			$dest_sheet->write($i, $j,$_);
		}
		$j++;
	}
	$i++;
}


$dest_book->close();
