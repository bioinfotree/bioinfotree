#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>; 2014 Ivan Molineris <ivan.molineris@gmail.com>

from array import array
from optparse import OptionParser
from sys import stdin
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage

NA="NA"

class SparseMatrix(object):
    def __init__(self, cols_item=set()):
        self.data = {} 
        self.cols_item = cols_item
        self.cols_item_sorted = []

    def set(self, x, y, v):
        pos = self.data.get(x,{})
        if y in pos:
            exit("only one value per position allowed, duplicated value for (%s,%s)" % (x,y))
        pos[y]=v
        self.data[x]=pos
        self.cols_item.add(y)

    def rows(self):
        for x in self.data.iterkeys():
            retval = [x]
            for y in self.cols_item_sorted:
                retval.append(self.data[x].get(y,None))
            yield retval

    def head(self):
        self.cols_item_sorted = sorted(list(self.cols_item))
        return ">ROW_ID\t%s" % "\t".join(self.cols_item_sorted)

    def clean(self):
        self.data={}

def None2NA(v):
    if v is None:
        return NA
    else:
        return v;

def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS]
        
        .META: stdin
            1 id1
            2 id2
            3 val

        Builds a weighted incidence matrix out of a map file.
    '''))
    parser.add_option('-c', '--columns', type=str, dest='columns', default=None, help='space separed list of labels, if given produce a value (NA) also for label in COLUMNS, other than those found in the input file [default: %default]', metavar='COLUMNS')
    parser.add_option('-C', '--columns_from_file', type=str, dest='columns_from_file', default=None, help='same as -c but take the list from file [default: %default]', metavar='COLUMNS')
    parser.add_option('-s', '--sorted', dest='sorted', action='store_true', default=False, help='assume the input block sorted on the first column (memory efficient) [default: %default]')
    parser.add_option('-k', '--kill', dest='kill', action='store_true', default=False, help='kill columns not reported in -c or -C [default: %default]')
    parser.add_option('-i', '--assume_sorted', dest='assume_sorted', action='store_true', default=False, help='do not check for lexicografic sorting of first column [default: %default]')
    parser.add_option('-e', '--missing_values', type=str, dest='missing_values', default="NA", help='the string used in place of missing values [default: %default]')
    options, args = parser.parse_args()

    if len(args) != 0:
        exit('Unexpected argument number.')
    

    if options.sorted and not (options.columns or options.columns_from_file):
        exit("In -s mode you must specify all the columns with -c.")

    if options.columns and options.columns_from_file:
        exit("-c an -C are not compatible")

    global NA
    NA=options.missing_values
    
    columns = []
    if options.columns:
        columns=options.columns.split()
    elif options.columns_from_file:
        with file(options.columns_from_file, 'r') as fd:
            for line in fd:
                tokens = line.rstrip().split('\t')
                columns+=tokens

    if columns:
        matrix = SparseMatrix(set(columns))
    else:
        matrix = SparseMatrix()    
    
    xpre=None
    header_printed=False
    for line in stdin:
        try:
            x,y,v = line.rstrip().split("\t")
            if options.sorted:
                if y not in columns:
                    if options.kill:
                        continue
                    else:
                        exit("Unexpected column (%s)." % y)
                if xpre is not None:
                    try:
                        assert(x>=xpre) or options.assume_sorted
                    except AssertionError:
                        raise Exception("Input not sorted on column 1.")
        except ValueError:
            raise Exception("Malformed input (%s)" % line)

        ##################
        #in caso di sorted stampo al cambio di x e svuoto la matrice
        #
        if options.sorted and xpre is not None and xpre!=x:#stampo linea per linea
            if not header_printed:
                print matrix.head()
                header_printed = True

            for r in matrix.rows():
                print '\t'.join( [None2NA(i) for i in r] )#non posso usare v al posto di i perche` v e` gia` usata
            matrix.clean()
        #
        ##################
        xpre=x
        matrix.set(x, y, v)

    if not header_printed:
        print matrix.head()

    for r in matrix.rows():
        print '\t'.join( [None2NA(v) for v in r] )

if __name__ == '__main__':
    main()
