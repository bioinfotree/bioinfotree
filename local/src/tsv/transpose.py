#!/usr/bin/env python
#
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>

from sys import stdin
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import format_usage, exit, ignore_broken_pipe

def replace_none(ln, repl):
    if isinstance(ln, basestring):
        return (ln if len(ln) > 0 else repl,)
    return [ (x if x is not None else repl) for x in ln ]

def main():
    parser = OptionParser(usage=format_usage('''
             %prog [OPTIONS] <TABLE

             Prints the transpose of the input TABLE.
    '''))
    parser.add_option('-s', '--skip-missing', dest='skip', action='store_true', default=False, help='ignore different row or column lengths in input table')
    parser.add_option('-e', '--replace-empty', dest='empty', default='', help='replace all empty field with the given value (requires "-s"; default: empty string)')
    options, args = parser.parse_args()

    if len(args) != 0:
	    exit('Unexpected argument number.')
    elif options.empty != '' and not options.skip:
	    exit('Incompatible options. "--replace-empty" requires "--skip-missing".')

    mat = []
    l = None
    for line in stdin:
        tokens = safe_rstrip(line).split('\t')
        if not options.skip:
            if l is not None and len(tokens) != l:
                exit('Row lengths differ.')
        mat.append(tokens)
        l = len(tokens)

    for ln in map(None, *mat):
        ln = replace_none(ln, options.empty)
        print '\t'.join(ln)

if __name__=='__main__':
    ignore_broken_pipe(main)
