#!/usr/bin/env python
#
# Copyright 2012,2015-2016 Gabriele Sales <gbrsales@gmail.com>

from sys import stdout
from vfork.io.util import safe_rstrip
from vfork.util import ArgumentParser, exit, ignore_broken_pipe


def main():
    args = parseArgs()
    if args.merge is None:
      paste(args)
    else:
      merge(args)

def parseArgs():
  parser = ArgumentParser('''
    Writes to the standard output the columns from each FILE,
    grouping them togheter into a single row like "paste".

    Each FILE, however, is expected to have a row label in
    the first column. The label is written only once in
    output rows. For each row, the tool checks that all the
    labels are equal.

    If the "--merge" option is used, the tool takes the union of
    all labels and introduces a default value whenever a file
    lacks an entry for that label.
  ''')

  parser.add_argument('files', metavar='FILE', nargs='+',
                      help='the input FILEs')

  parser.add_argument('-m', '--merge', metavar='MISSING',
                      help='merge file with different labels, using a '
                           'user-specified MISSING value')

  parser.add_argument('-1', '--header', action='store_true',
                      help='print but otherwise ignore the header of input files')

  return parser.parse_args()

def paste(args):
  fds = [ file(a, 'r') for a in args.files ]
  lineno = 0

  if args.header:
      print '\t'.join(f.readline().rstrip() for f in fds)

  while True:
    lines = [ fd.readline() for fd in fds ]
    lineno += 1

    blanks = [ l == '' for l in lines ]
    if True in blanks:
      if all(blanks):
        break
      else:
        ended = args.files[blanks.index(True)]
        exit('Input from file ended prematurely at line %d: %s' % (lineno-1, ended))

    tokens = [ safe_rstrip(l).split('\t') for l in lines ]
    try:
      no_tokens = [ len(t) for t in tokens ].index(0)
      exit('Label missing at line %d of file: %s' % (lineno, args.files[no_tokens]))
    except ValueError:
      pass

    labels = [ t[0] for t in tokens ]
    first_label = labels[0]
    for idx, other_label in enumerate(labels[1:], 1):
      if other_label != first_label:
        exit('Labels differ at line %d between files: %s - %s' % (lineno, labels[0], labels[idx]))

    output = [first_label]
    for component in [ t[1:] for t in tokens ]:
      output += component
    print '\t'.join(output)

def merge(args):
  missing = args.merge
  labels = set()
  headers = []
  values = []

  for path in args.files:
    header, content = readContent(path, args.header)
    headers.append(header)
    values.append(content)
    labels |= set(content.iterkeys())

  if args.header:
    print '\t'.join(headers)

  for label in sorted(labels):
    out = [label] + [ v.get(label, missing) for v in values ]
    stdout.write('\t'.join(out) + '\n')

def readContent(path, has_header):
  header = None
  mapping = {}

  with open(path, 'r') as fd:
    if has_header:
      header = fd.readline().rstrip()

    for line in fd:
      label, content = safe_rstrip(line).split('\t', 1)

      if label in mapping:
        exit('Duplicate label "{}" in file: {}'.format(label, path))
      else:
        mapping[label] = content

  return header, mapping


if __name__ == '__main__':
    ignore_broken_pipe(main)
