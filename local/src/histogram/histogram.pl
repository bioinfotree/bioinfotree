#!/usr/bin/perl
#
#
#	clacola la frequenze di occorrenza del valore in seconda colonna
#
#	se la variabile istogram = 1 disegna un rudimentale istogramma
#
#

use warnings;
use strict;
use IO::File;

my $usage="
cat numbers_1_x_row | histogram.pl -c class_number [--plot] [--log]
	-c accept a single number 
	--plot generate png image
	--log  log of occurencies
	--pplot pseudoplot (for console)

	--plot and --pplot mutualy excusive

";

$ARGV[0] !~ /--help/ or die($usage);

####################
#	costants
####################

my $pplot_length=90;

####################
#	input control part
####################

my %optv;
while(my $option =shift(@ARGV)){
	if ($option =~ /^-c/){
		my $value=shift(@ARGV) or die($usage);
		$optv{$option}=$value;
	}elsif($option =~ /^--plot/){
		$optv{'--plot'}=1;
	}elsif($option =~ /^--pplot/){
		$optv{'--pplot'}=1;
	}elsif($option =~ /^--log/){
		$optv{'--log'}=1;
	}elsif($option =~ /^--dxlabel/){
		$optv{'--dxlabel'}=1;
	}elsif($option =~ /^--sxlabel/){
		$optv{'--sxlabel'}=1;
	}else{
		die($usage);
	}
}

$optv{'-c'} or die($usage.join("\n",%optv));
my $class_num=0;
my @bins=undef;
if($optv{'-c'} =~/^\d+$/){
	$class_num=$optv{'-c'};
}else{
	@bins=split(/\s+/,$optv{'-c'});
	map{$_=~/\d+.?\d*/ or die ("ERROR: histogram.pl: -c value must format $_ not supported\n")} @bins;
	@bins = sort {$a<=>$b} @bins;
}
$optv{'--pplot'} and $optv{'--plot'} and die($usage);

my $fh = *STDOUT;
my $rm_temp_file=0;
if($optv{'--plot'}){
	-r "data.plot" and die("ERROR: temp file \"data.plot\" required by --plot already exist\n");
	$fh = IO::File->new(">data.plot") or die "can't write on data.plot\n";
	$rm_temp_file=1;
}
	
############################################



my $class_width=0;

my %class;
my @val;


$_=<STDIN>;
chomp $_;
my $max=$_;
my $min=$_;
push(@val,$_);

if(!($min =~ /\d+/ or $min =~ /\d+\.\d+/ or $min =~ /\d+e-\d+/)){
		die("ERROR: NaN in input\n");
}

#my $zero_log_warn=0;

while(<STDIN>){
	chomp;
	if($optv{'--log'}){
#		if($_>0){
			$_=log($_)/log(10);
#		}else{
#			warn "data <= 0 in data log scale" if !$zero_log_warn;
#			$zero_log_warn=1;
#		}
	}
	if($_>$max){
		$max=$_;
	}
	if($_<$min){
		$min=$_;
	}
	push(@val,$_);
}

$class_width = ($max-$min)/($class_num);

my $c_max=0;
my $c_min=$class_num;
for(@val){
	#Ymy $c=sprintf '%i', ($_/$class_width);
	my $c=int($_/$class_width);
#	if($c==$_/$class_width){	#se $_/$class_width e` un intero significa che il dato sta esattamente sul bordo
#					#di una classe, se fosse il massimo int($_/$class_width) genererebbe la
#					#$class_num+1 esima classe
#					#ricolloco quindi a forza il valore nella classe precedente 
#					#cioe`, se il valor cade esattamente sulla separazione tra le classi
#					#(questo succede sicuramente per il massimo o per l minimo)
#					#lo colloco nella classe precedente
#					#a meno che sia il minimo (il che genererebbe la classe -1)
#		
#		$c-- if $c>0;
#	}
	if($_==$max){
		$c--;
	}
	$c_min < $c or $c_min=$c;
	$c_max > $c or $c_max=$c;
	
	$class{$c}++;
}

for(my $i=$c_min; $i<=$c_max; $i++){
	
	if(!$class{$i}){
		$class{$i}=0;
	}
	
	my $x=undef;
	if($optv{'--sxlabel'}){
		$x=$i*$class_width;
	}elsif($optv{'--dxlabel'}){
		$x=($i+1)*$class_width;
	}else{
		$x=$i*$class_width+($class_width/2);
	}

	if($optv{'--log'}){
		$x=10**$x;
	}

	my $y=$class{$i};
	
	print $fh sprintf "%.3f\t%.3f", $x, $y;
	print $fh "\n";
	
}






if($optv{'--plot'}){
	my $dir=$ENV{BIOINFO_ROOT};
	if($optv{'--log'}){
		print `gnuplot $dir/local/src/histogram/plot_log.gnp`;
		#print `gnuplot $dir/local/src/histogram/plot_log_y.gnp`;
	}else{
		print `gnuplot $dir/local/src/histogram/plot.gnp`;
	}
}

if($rm_temp_file){
	unlink('data.plot');
}
