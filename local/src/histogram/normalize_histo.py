#!/usr/bin/env python
from __future__ import with_statement
from __future__ import division

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from subprocess import Popen, PIPE
#from collections import defaultdict

def main():
	usage = format_usage('''
		%prog < STDIN
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	sum = 0
	val = []
	for line in stdin:
		x , y = safe_rstrip(line).split('\t')
		try:
			y = int(y)
		except ValueError:
			y = float(y)
		sum += y
		val.append((x,y))
	
	for x, y in val:
		print "%s\t%g" % (x,y/sum)



if __name__ == '__main__':
	main()

