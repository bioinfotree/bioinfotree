#!/usr/bin/env python
# encoding: utf-8

from __future__ import with_statement

from optparse import OptionParser
from sys import exit, stdin
from vfork.geometry.region import Index as RegionIndex
from vfork.io.colreader import Reader
from vfork.io.util import safe_rstrip

def load_blocks(filename):
	blocks = []
	
	with file(filename, 'r') as fd:
		fd.readline() # skip header
		
		reader = Reader(fd, '0u,1u,2u', False)
		while True:
			tokens = reader.readline()
			if tokens is None:
				break
			
			if tokens[1] <= tokens[0]:
				exit('Invalid coordinates at line %d of file %s' % (reader.lineno()+1, filename))
			
			blocks.append(tokens)
	
	return RegionIndex(blocks)

def parse_uint(repr):
	try:
		value = int(repr)
		if value < 0:
			raise ValueError
		return value
	except ValueError:
		raise ValueError

def parse_cols(cols):
	try:
		start_col = parse_uint(cols[0])
		if start_col == 0:
			raise ValueError
	except ValueError:
		exit('Invalid start coordinate column index.')
	
	try:
		stop_col = parse_uint(cols[1])
		if stop_col == 0 or stop_col == start_col:
			raise ValueError
	except ValueError:
		exit('Invalid stop coordinate column index.')
	
	return start_col-1, stop_col-1

def main():
	parser = OptionParser(usage='%prog BLOCKS_FILE START_COL STOP_COL <REGIONS')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	
	start_col, stop_col = parse_cols(args[1:])
	block_index = load_blocks(args[0])
	
	for lineno, line in enumerate(stdin):
		tokens = safe_rstrip(line).split('\t')
		if len(tokens) <= max(start_col, stop_col):
			exit('Insufficient token number at line %d' % (lineno+1,))
		
		try:
			start = parse_uint(tokens[start_col])
		except ValueError:
			exit('Invalid start coordinate at line %d.' % (lineno+1,))
		
		try:
			stop = parse_uint(tokens[stop_col])
			if stop <= start:
				raise ValueError
		except ValueError:
			exit('Invalid stop coordinate at line %d.' % (lineno+1,))
		
		overlapping = block_index.get_overlapping(start, stop)
		if len(overlapping) != 1:
			continue
		
		block = overlapping[0]
		if block[0] > start or block[1] < stop:
			continue
		
		offset = start - block[0]
		tokens[start_col] = str(start + block[2])
		tokens[stop_col] = str(stop + block[2])
		
		print '\t'.join(tokens)

if __name__ == '__main__':
	main()
