#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $help=0;
my %words=();
my $verbose = 0;
my $mismatch = 0;
my @char=('A','T','G','C');

my $usage="$0 [-v] probe_file < nucloetides
	the prob_file is in the form
ID1	sequence1
ID2	sequence2
...

";


GetOptions (
	'h|help' => \$help,
	'v|verbose'  => \$verbose,
	'm|mismatch=i' => \$mismatch
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

die("More than 1 mismatch not allowed") if $mismatch > 1;

my $probe_file = shift;

die($usage) if !defined($probe_file);

open(PROB,$probe_file) or die("Can't open file ($probe_file)");

my %probe=();
my $k=undef;#prob length
while(<PROB>){
	chomp;
	die("Malformed probe_file") if m/\W/ or m/\s/;
	$probe{$_}=1;
	if(!defined($k)){
		$k=length($_);
	}elsif(length($_)!=$k){
		die("Pobes of different size found.");
	}
}

print STDERR "probes letti - ". `date`   if $verbose;
 
my $pos=-1;
$_=<>;
chomp;
die("Header fasta not found") if $_!~/^>/;

my $seqid = $_;
$seqid =~ s/^>//;
$_="";

while( 1 ){
	
	if(length($_) < $k){
		my $new_row=<>;
		#print STDERR length($_),$new_row;
		if(defined($new_row)){
			chomp $new_row;
			if($new_row =~ /^>/){
				#print STDERR "hash costruito - ". `date`  if $verbose;
				#&print_coords();
				%words=();
				$seqid = $new_row;
				$seqid =~ s/^>//;
				$pos=0;
				$_="";
				next;
			}
			$_.=$new_row;
		}else{
			last;
		}
	}
	
	my $w=substr($_,0,$k);
	$pos+=1;
	substr($_,0,1,"");
	next if $w!~/^[ACGT]+$/;
	
	if(defined $probe{$w}){
		print $w, 'P',$seqid, '+', $pos;
		delete($probe{$w});
	}

	my $wr = reverse $w;
	$wr =~ tr/ACGT/TGCA/; #la parola trovata viene rev-com; siamo virtualmente sullo strand - e cerchiamo lo stesso seed!

	if(defined $probe{$wr}){
		print $wr,'P', $seqid,'-', $pos;
		delete($probe{$wr});
	}

	if($mismatch){
		for(&neighbour2($w)){
			#print $w,$_;
			if(defined $probe{$_}){
				print $w,'Pm',$seqid, '+', $pos;
				delete($probe{$_});
			}

			$_ = reverse $_;
			$_ =~ tr/ACGT/TGCA/; #la parola trovata viene rev-com; siamo virtualmente sullo strand - e cerchiamo lo stesso seed!

			if(defined $probe{$_}){
				print $wr,'Pm', $seqid, '-', $pos;
				delete($probe{$_});
			}
			
		}
	}
}

while (my $key = each %probe) {
	print "A",$key;
}


sub neighbour
{
	my $w=shift @_;
	my @out;
	push @out,$w;
	my @ws = split(//, $w);

	my $k=length($w); 
	for(my $i=0; $i<$k; $i++){
		foreach my $c (@char){
			next if $ws[$i] eq $c;
			my @w1=@ws;
			$w1[$i]=$c;
			push @out,join('',@w1);
		}
	}
	return @out;
}

sub neighbour2
{
	my $w=shift @_;
	my @out;
	push @out,$w;
	my $ws = $w;

	my $k=length($w); 
	for(my $i=0; $i<$k; $i++){
		$w = $ws;
		substr ($w,$i,1,'A');
		push @out,$w if $ws ne $w;
		$w = $ws;
		substr ($w,$i,1,'C');
		push @out,$w if $ws ne $w;
		$w = $ws;
		substr ($w,$i,1,'G');
		push @out,$w if $ws ne $w;
		$w = $ws;
		substr ($w,$i,1,'T');
		push @out,$w if $ws ne $w;
	}
	return @out;
}
