#!/bin/bash
#
# Copyright 2015-2016 Gabriele Sales <gbrsales@gmail.com>

set -eu -o pipefail

bam=""
tmp=""
out=""

main() {
  parseArgs "$@"

  trap cleanup EXIT
  tmp="$(mktemp --tmpdir="$PWD" bam2xz.XXXXXXXXXX)"

  compress
  check

  mv "$tmp" "$out"
  rm "$bam"
}

parseArgs() {
  [ $# -eq 1 ] || usage

  bam="$1"
  out="$1.xz"

  [ ! -e "$out" ] || error "Compressed BAM already exists."
}

usage() {
  echo "Usage: ${0##*/} BAM"
  echo
  echo "Compress a BAM file with the XZ algorithm."
  echo
  echo "Set the THREADNUM variable to limit the number of"
  echo "threads used for compression."
  exit 1
}

error() {
  echo "[${0##*/}] $1" >&2
  exit 1
}

cleanup() {
  if [ -e "$tmp" ]; then
    rm -f "$tmp"
  fi
}

compress() {
  if [ -n "${THREADNUM:-}" ]; then
    opts="-p $THREADNUM"
  fi

  samtools view -u "$bam" | pixz -9e ${opts:-} >"$tmp"
}

check() {
  if [ -n "${THREADNUM:-}" ]; then
    decompr=$(($THREADNUM-2))
    if [ $decompr -lt 1 ]; then
      decompr=1
    fi

    opts="-p $decompr"
  fi

  cmp <(samtools view -u "$bam") <(pixz -d ${opts:-} <"$tmp") || \
    error "Mismatch between the original BAM file and the compressed copy."
}


main "$@"
