#!/bin/bash
# Argument = -t test -r server -p password -v
set -e
set -o pipefail

usage()
{
cat << EOF
usage: $0 file.bam 

This script verify that the EOF of the bab file is correct. 

OPTIONS:
   -h      Show this message
EOF
}
verbose=0

while getopts "hv" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
	 v)
	      verbose=1
	      ;;
         ?)
             usage
             exit
             ;;
     esac
done
shift $(($OPTIND - 1))

BGZF_EOF=1F8B08040000000000FF0600424302001B00030000000000*
for bamfile in $@; do
	[[ -s $bamfile ]] || (echo "ERROR: $1 not found or 0 byte" >&2 | exit 2)
	size=`stat -c "%s" $bamfile`
	seek=$(( $size - 28 ))
	BAM_EOF=`hexdump -e  '4/1 "%02X"' -s $seek ${bamfile}`
	if [[ $BAM_EOF == $BGZF_EOF ]]
	then
		if [ $verbose ]; then
			echo $bamfile ok
		fi
		continue
	else
		echo "ERROR: $1 unexpected end of file" >&2
		if [ $verbose ]; then
			continue
		fi
		exit 1
	fi
done
