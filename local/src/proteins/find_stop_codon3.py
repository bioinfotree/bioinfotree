#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from Bio.Seq import Seq
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
stop_codons = ["TAG", "TAA", "TGA"]
kmer_to_define_reading_frame = None

def process(seq, options, reverse=False):
	global stop_codons, kmer_to_define_reading_frame
	myindex = seq.find(kmer_to_define_reading_frame)
	seq_cut = None
	if  myindex == -1:
		if not reverse and options.both_strand:
			return process(Seq(seq).reverse_complement().tostring(), options, reverse=True)
		if not options.sequence:
			print "-1" 
		#else:
		# do nothing	
		return None
	else:
		seq_cut=seq[myindex:]
		myframe = myindex % 3
		if options.sequence:
			print seq_cut
			return

	#print "DEBUG - Frame is: " + str(myframe)

		stop_lst = []
		mylen = len(seq)

		for stop in stop_codons:
			#myoffset = 0
			myoffset = myindex
			#print "DBG: seq length is: " + str(mylen)
			while myoffset < mylen:
				#print "DBG: offset is: " + str(myoffset)
				stop_pos = seq.find(stop, myoffset)
				#print "DBG: " + str(myoffset) + " - " + str(stop_pos)
				if stop_pos != -1:
					if (stop_pos - myindex) % 3 == 0:
						stop_lst.append(stop_pos)
					myoffset = stop_pos + 2
				else:
					myoffset = mylen

		if options.verbose == False:
			print len(stop_lst)
		else:
			stop_lst.sort()
			print seq
			print myframe
			print len(stop_lst)
			print stop_lst

def main():
	global kmer_to_define_reading_frame
	usage = format_usage('''
		%prog PARAM1 KMER_TO_DEFINE_READING_FRAME < STDIN
		retrun the number of stop condon for each sequence bla bla

		STDIN
		must contain only sequences, one per line

		STDOUT
		one number for each line of input
		-1 -> not found KMER_TO_DEFINE_READING_FRAME
		0  -> not found any stop codon in the reading frame
		N  -> found N stop codon in the reading frame

	''')
	parser = OptionParser(usage=usage)
	
	#parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='verbose debugging mode [default: %default]')
	parser.add_option('-s', '--sequence', dest='sequence', action='store_true', default=False, help='return	sequence from the occurred kmer [default: %default]')
	parser.add_option('-b', '--both_strand', dest='both_strand', action='store_true', default=False, help='if reference sequence not found try to revcomp the input sequence [default: %default]')

	options, args = parser.parse_args()

	# to change in tuple
	
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	kmer_to_define_reading_frame = args[0]
	#for id, sample, raw, norm in Reader(stdin, '0u,1s,2i,3f', False):
	for seq in stdin:
		seq = seq.rstrip()
		process(seq, options)

if __name__ == '__main__':
	main()
