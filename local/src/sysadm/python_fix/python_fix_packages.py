#!/usr/bin/env python
#
# Copyright 2015 Gabriele Sales <gbrsales@gmail.com>

from os import environ, makedirs
from os.path import isdir, join
from pkg_resources import WorkingSet
from sys import argv, exit, stderr, stdout, version_info
import errno
import site
import subprocess

root = None
host = None


def main():
  if len(argv) != 1:
    usage()

  global root, host
  root = lookup_var('BIOINFO_ROOT')
  host = lookup_var('BIOINFO_HOST')

  lib_dir, affected = affected_packages()
  update_paths()
  reinstall_vfork()
  upgrade_instructions(lib_dir, affected - set(['pyvfork']))

def usage():
  stderr.write('Usage: python_fix\n\n')
  stderr.write('Fix Python modules inside BioinfoTree.\n')
  exit(1)

def affected_packages():
  lib_dir = join(root, 'binary', host, 'local', 'lib', 'python')
  ws = WorkingSet([lib_dir])
  return lib_dir, set(ws.entry_keys.get(lib_dir, []))

def update_paths():
  site_dir = site.getusersitepackages()
  if not isdir(site_dir):
    makedirs_safe(site_dir)

  with open(join(site_dir, 'bioinfotree.pth'), 'w') as o:
    o.write('import os;'
            'import site;'
            'import sys;'
            '__bit_root=os.environ.get("BIOINFO_ROOT");'
            '__bit_host=os.environ.get("BIOINFO_HOST");'
            '__bit_pythonv="python%d.%d" % (sys.version_info.major, sys.version_info.minor);'
            'site.addsitedir(os.path.join(__bit_root, "binary", __bit_host, "local",'
            '                             "lib", __bit_pythonv, "site-packages"))'
            '  if __bit_root and __bit_host else None;'
            'del __bit_root;'
            'del __bit_host\n')

  bit_dir = join(root, "binary", host, "local", "lib",
                 "python%d.%d" % (version_info.major, version_info.minor),
                 "site-packages")
  makedirs_safe(bit_dir)

def lookup_var(name):
  try:
    return environ[name]
  except KeyError:
    exit('[ERROR] ' + name + ' is not defined.')

def makedirs_safe(path):
  try:
    makedirs(path)
  except OSError, e:
    if e.errno != errno.EEXIST:
      raise

def reinstall_vfork():
  vfork_dir = join(root, 'local', 'src', 'pyvfork')
  code = subprocess.call(['binary_install'], cwd=vfork_dir)
  if code != 0:
    exit('[ERROR] The installation of pyvfork failed.')

def upgrade_instructions(lib_dir, affected_pkgs):
  stdout.write('''\

_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_

Upgrade COMPLETED.

The search path of Python modules has been modified.

This unfortunately means that packages previously installed inside BioinfoTree
are no longer visibile. Please reinstall them manually.
''')

  if len(affected_pkgs):
    stdout.write('The following is a list of the packages that could be affected:\n\n')

    for name in sorted(affected_pkgs):
      stdout.write(' * ' + name + '\n')

    stdout.write('\n')

  if isdir(lib_dir):
    stdout.write('When you are satisfied, you can delete the following directory:\n\n')
    stdout.write('  ' + lib_dir + '\n\n')


if __name__ == '__main__':
  main()
