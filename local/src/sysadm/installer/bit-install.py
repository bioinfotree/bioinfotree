#!/usr/bin/env python
#
# Copyright 2010-2013,2015 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010-2011 Paolo Martini <paolo.cavei@gmail.com>

from distutils.sysconfig import get_python_inc
from optparse import OptionParser
from os import chdir, environ, getuid, makedirs
from os.path import abspath, exists, isdir, join, relpath, split
from pwd import getpwuid
from socket import getfqdn
from subprocess import CalledProcessError, check_output, Popen, PIPE
from sys import exit, modules, stderr, version_info
import re
import site

try:
    from pkg_resources import get_distribution, DistributionNotFound
except ImportError:
    exit('Python module "setuptools" is missing. Please install it.')


welcome_msg = '''
### Welcome to the BioinfoTree installer ###

This script will perform the guided installation of the
BioinfoTree software suite on your computer.

The installer  will check your system looking  for some
software needed by  BioinfoTree.  Then it will download
all BioinfoTree files and will compile some utilities.

Finally it will optionally update  your bash profile in
order to enable BioinfoTree tools.

Please press ENTER to continue or CTRL+C to abort.
'''

ending_msg = '''
The  BioinfoTree core was succesfully installed on your
system.

=======================================================
*                                                     *
*            BioinfoTree is (almost) ready.           *
*                                                     *
=======================================================

To complete the installation,  please add the following
lines:

[[ -z $BIOINFO_ROOT ]] && export BIOINFO_ROOT=%s
export BIOINFO_HOST=%s
source $BIOINFO_ROOT/local/share/bash/bashrc

to the top of $HOME/.bash_profile or $HOME/.bashrc

Please be aware that such commands must be executed both
in interactive and in non-interactive sessions.
This means that they must be placed before checks like:

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

For more help please visit:

http://bioinfotree.org

Thank you and enjoy the BioinfoTree!
'''

name_msg = 'Your name [%s]: '

email_msg = 'Your email address [%s]: '

installation_path_msg = 'Installation path [%s]: '

hostname_msg = 'Unique host name [%s]: '

deps = {
    'standalone' : [ ('bash', '3'),
                     ('git', '1.7'),
                     ('gawk', '3.1'),
                     ('sed', '4.2'),
                     ('wget', None),
                     ('perl', '5.10'),
                     ('python', '2.7'),
                     ('R', '2.10'),
                     ('cython', '0.11') ],

    'python'     : [ ('numpy', '1.3'),
                     ('scipy', '0.7') ]
}

installable = { 'pyvfork library'     : 'local/src/pyvfork',
                'R support libraries' : 'local/src/Rvfork',
                'GMSL'                : 'local/src/redist/gmsl' }

def get_input(msg, default=None):
    while 1:
        reply = raw_input(msg)
        print

        if len(reply) == 0:
            if default is None:
                continue
            else:
                return default
        else:
            return reply

def personal_data(user_info):
    default_name = user_info.pw_gecos.rstrip(',')
    name = get_input(name_msg % default_name, default_name)

    try:
        out = check_output(['git', 'config', '--get', 'user.email'],
                           shell=False).strip()
        default_email = out if len(out) else ''
    except CalledProcessError:
        default_email = ''

    email = get_input(email_msg % default_email, default_email)

    return name, email

def check_version(expected, found):
    for e, f in zip(expected, found):
        if e < f:
            return True
        elif e > f:
            return False
    return True


version_rx = re.compile(r'(\d+)\.(\d+)')

def check_standalone(lst):
    for prog, version in lst:
        try:
            proc = Popen([prog, '--version'], stdout=PIPE, stderr=PIPE,
                         close_fds=True, shell=False)
            out, err = proc.communicate()
            if proc.returncode != 0:
                exit('Error checking "%s" version.' % prog)

            lines = (out+err).split('\n')
            while len(lines) and len(lines[0]) == 0:
                del lines[0]

            if len(lines) == 0:
                exit('Error checking "%s" version.' % prog)

            if version is not None:
                m = version_rx.search(lines[0])
                if m is None:
                    exit('Error checking version of "%s".' % prog)

                min_version = tuple( int(n) for n in version.split('.') )
                found_version = tuple( int(n) for n in m.groups() )
                if not check_version(min_version, found_version):
                    exit('Program "%s" is too old. Please install version %s '
                         'or higher.' % (prog,version))

        except OSError:
            exit('Program "%s" not found. Please install version %s '
                 'or higher.' % (prog,version))

def check_python(lst):
    python_header = join(get_python_inc(), 'Python.h')
    if not exists(python_header):
        exit('Python header files are missing. Please install them.')

    try:
        import sqlite3
    except ImportError:
        exit('This Python installation does not provide the "sqlite3" module. '
             'Please install it.')

    for name, version in lst:
        try:
            distrib = get_distribution(name)
        except DistributionNotFound:
            exit('Python module "%s" not found. Please install version %s '
                 'or higher.' % (name,version))

        m = version_rx.search(distrib.version)
        if m is None:
            exit('Error checking version of Python module "%s".' % name)

        min_version = tuple( int(n) for n in version.split('.') )
        found_version = tuple( int(n) for n in m.groups() )
        if not check_version(min_version, found_version):
            exit('Python module "%s" is too old. Please install version %s '
                 'or higher.' % (name,version))


def git_clone(repository, branch, root):
    proc = Popen(['git', 'clone', '--branch', branch, repository, root],
                 shell=False)
    if proc.wait() != 0:
        exit('ERROR: Cannot clone remote BioinfoTree repository '
             'at "%s".' % repository)
    print

def setup_R(root, hostname):
    print '* Setting up R library site'
    libdir = join(root, "binary", hostname, "local", "lib", "R")
    makedirs(libdir)
    environ['R_LIBS_USER'] = libdir

def install_R_pkg(name):
    print '* Installing R package: ' + name

    exprs = 'options(error=function(e){q(status=1)});' \
            'install.packages("%(name)s",' \
            '                 repos="http://cran.us.r-project.org",' \
            '                 quiet=TRUE);' \
            'packageVersion("%(name)s")' % locals()

    proc = Popen(['Rscript', '-e', exprs], shell=False)
    if proc.wait() != 0:
        exit('Cannot install R package: ' + name)


def setup_python(root, hostname):
    print '* Setting up Python site directory'

    site_dir = site.getusersitepackages()
    if not isdir(site_dir):
        makedirs(site_dir)

    with open(join(site_dir, 'bioinfotree.pth'), 'w') as o:
        o.write('import os;'
                'import site;'
                'import sys;'
                '__bit_root=os.environ.get("BIOINFO_ROOT");'
                '__bit_host=os.environ.get("BIOINFO_HOST");'
                '__bit_pythonv="python%d.%d" % (sys.version_info.major, sys.version_info.minor);'
                'site.addsitedir(os.path.join(__bit_root, "binary", __bit_host, "local",'
                '                             "lib", __bit_pythonv, "site-packages"))'
                '  if __bit_root and __bit_host else None;'
                'del __bit_root;'
                'del __bit_host\n')

    bit_dir = join(root, "binary", hostname, "local", "lib",
                   "python%d.%d" % (version_info.major, version_info.minor),
                   "site-packages")
    makedirs(bit_dir)


def install_pkgs(root, pkgs):
    cmd = 'source $BIOINFO_ROOT/local/share/bash/bashrc; binary_install'

    for name, path in pkgs.iteritems():
        proc = Popen(['/bin/bash', '-c', cmd],
                     shell=False,
                     cwd=join(root, path))
        if proc.wait() != 0:
            exit('ERROR: Cannot install %s.' % name)
    print


def main():
    parser = OptionParser(usage='%prog [OPTIONS]')
    parser.add_option('-r', '--repository', dest='repository',
                      default='bitro@bioinfotree.org:bioinfotree.git',
                      help='define the BioinfoTree repository (default: %default)')
    parser.add_option('-b', '--branch', dest='branch',
                      default='master',
                      help='use the given git branch (default: %default)')
    options, args = parser.parse_args()

    if len(args) != 0:
        exit('Unexpected argument number.')

    check_standalone(deps['standalone'])
    check_python(deps['python'])

    user_info = getpwuid(getuid())

    get_input(welcome_msg, '')

    default_root = join(user_info.pw_dir, 'bioinfotree')
    root = get_input(installation_path_msg % default_root, default_root)
    root = abspath(root)
    #  if exists(root):
    #      exit('ERROR: The path "%s" already exists.' % root)

    root_prefix, root_name = split(root)
    if not isdir(root_prefix):
        exit('ERROR: The directory "%s" does not exist.' % root_prefix)

    default_hostname = getfqdn()
    hostname = get_input(hostname_msg % default_hostname, default_hostname)

    try:
        chdir(root_prefix)
    except:
        exit('ERROR: Cannot enter directory "%s".' % root_prefix)
    git_clone(options.repository, options.branch, root_name)

    try:
        chdir(root)
    except:
        exit('ERROR: Cannot enter directory "%s".' % root)

    environ['BIOINFO_ROOT'] = root
    environ['BIOINFO_HOST'] = hostname

    setup_R(root, hostname)
    install_R_pkg('optparse')
    setup_python(root, hostname)
    install_pkgs(root, installable)

    print ending_msg % (root, hostname)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit('\nInstallation aborted.')
