#!/bin/bash
#
# Copyright 2008,2015 Gabriele Sales <gbrsales@gmail.com>
set -eu -o pipefail

function main {
	if [ $# -ne 1 ]; then
		error "Unexpected argument number."
	elif [ $1 == "-h" -o $1 == "--help" ]; then
		print_help "$0"
	elif [ -z $BIOINFO_ROOT ]; then
		error "BIOINFO_ROOT is not defined."
	elif [ -z $BIOINFO_HOST ]; then
		error "BIOINFO_HOST is not defined."
	fi

	install_dir_base="$BIOINFO_ROOT/binary/$BIOINFO_HOST/"
	if [ ! -d "$install_dir_base" ]; then
		error "Missing directory: $install_dir_base"
	fi

	install_dir="$install_dir_base/local/lib/ensembl/$1"
	[[ -e "$install_dir" ]] && error "Installation directory ($install_dir) already exists; please remove it and rerun this script."
	mkdir -p "$install_dir" || error "Cannot create directory: $install_dir"
	touch "$(dirname "$install_dir")/.do_not_clean"
	cd "$install_dir" || error "Cannot change directory to: $install_dir"

	if ! wget -O- 'https://cpan.metacpan.org/authors/id/C/CJ/CJFIELDS/BioPerl-1.6.1.tar.gz' | tar -xz; then
		error "Cannot retrieve BioPerl."
	fi
	mv BioPerl-1.6.1 bioperl || error "Cannot setup BioPerl."

	clone_ensembl_repo ensembl $1
	clone_ensembl_repo ensembl-variation $1
	clone_ensembl_repo ensembl-funcgen $1
	clone_ensembl_repo ensembl-compara $1
	clone_ensembl_repo ensembl-io $1
}

function error {
	echo "[install_ensembl_api] $1" >&2
	exit 1
}

function print_help {
	echo "Usage: $1 API_VERSION"
	echo
	echo "Fetches and installs the required version of Ensembl APIs."
	exit 1
}

function clone_ensembl_repo {
	# $1: package
	# $2: version
	git clone \
	  --branch "release/$2" \
	  --depth 1 \
	  "https://github.com/Ensembl/$1.git" || error "Cannot clone $1 repository."
}


main "$@"
