# Copyright 2012,2014 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from os.path import join
import sqlite3


class PackageDb(object):
    def __init__(self, path):
        self.conn = sqlite3.connect(join(path, 'packages.db'))
        self._init()

    def list_all(self):
        c = self.conn.cursor()
        c.execute('SELECT path, stow, version FROM packages')

        for path, use_stow, version in c:
            yield path, bool(use_stow), version

    def register(self, path, use_stow, version):
        c = self.conn.cursor()
        c.execute('INSERT OR REPLACE INTO packages (path, stow, version) VALUES (?,?,?)', (path, int(use_stow), version))
        self.conn.commit()

    def clear(self):
        c = self.conn.cursor()
        c.execute('DELETE FROM packages')
        c.commit()

    def forget(self, path):
        c = self.conn.cursor()
        c.execute('DELETE FROM packages WHERE path=?', (path,))
        self.conn.commit()
        return c.rowcount > 0

    def _init(self):
        c = self.conn.cursor()
        c.execute(''' CREATE TABLE IF NOT EXISTS packages
                        ( path TEXT PRIMARY KEY
                        , stow INTEGER NOT NULL
                        , version TEXT NOT NULL ) ''')
