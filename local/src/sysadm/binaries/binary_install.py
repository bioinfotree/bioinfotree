#!/usr/bin/env python
#
# Copyright 2008-2015 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from argparse import ArgumentParser, RawDescriptionHelpFormatter
from bioinfotree import BioinfoTree
from link import resolve_link
from os import environ, getcwd, listdir, makedirs, rmdir, stat, unlink, walk
from os.path import abspath, basename, dirname, join
from package import init_package
from sys import argv, exit, stderr


def main():
    args = parse_args()
    args.handler(args, makeBit(args))

def parse_args():
    parser = ArgumentParser(description='''\
Utility to manage the installation of binary packages into BioinfoTree.''')

    sub = parser.add_subparsers()
    parse_install(sub)
    parse_list(sub)
    parse_upgrade(sub)
    parse_forget(sub)

    return parser.parse_args(fakeDefaultCommand(sub))

def parse_install(sub):
    parser = sub.add_parser('install',
                            formatter_class=RawDescriptionHelpFormatter,
                            description='''\
Install binaries.

The software scans the subtree rooted in the working directory looking for
software sources; it compiles and installs them in the /binary tree.''')

    parser.add_argument('-a', '--all', action='store_true',
      help='install all binaries in the tree, even those '
           'outside of the working directory')

    parser.add_argument('-f', '--force', action='store_true',
      help='force recompilation')

    parser.add_argument('-i', '--ignore', nargs='+', default=[], metavar='DIR',
      help='do not enter into DIR when looking for sources')

    parser.add_argument('-r', '--remove-old', action='store_true',
      help='remove old binaries before installing new ones')

    parser.add_argument('-s', '--stow', action='store_true',
      help='use stow to manage standalone packages')

    parser.add_argument('-x', '--external', action='store_true',
      help="install the package contained in the current directory, even if "
           "it's outside of BIOINFO_ROOT")

    parser.add_argument('--ghc-options', default=None, metavar='OPTS',
      help='options to pass to the GHC compiler')

    parser.set_defaults(handler=install_packages)

def parse_list(sub):
    parser = sub.add_parser('list',
               description='List installed packages.')

    parser.set_defaults(handler=list_packages)

def parse_upgrade(sub):
    parser = sub.add_parser('upgrade',
               description='Upgrade installed packages.')

    parser.add_argument('-f', '--force', action='store_true',
      help='force recompilation')

    parser.set_defaults(handler=upgrade_packages)

def parse_forget(sub):
    parser = sub.add_parser('forget',
               description='Forget some of the installed packages.')

    parser.add_argument('pkgs', metavar='PKG', nargs='+',
                        help='the path of the package you want to forget')

    parser.set_defaults(handler=forget_packages)

def fakeDefaultCommand(sub):
    commands = sub.choices.keys()

    args = argv[1:]
    if len(args) == 0 or \
       (args[0] not in ('-h', '--help') and args[0][:1] == '-'):
        args.insert(0, commands[0])

    return args

def makeBit(args):
    vs = vars(args)
    external = vs.get('external', False)
    ghc_options = vs.get('ghc_options', None)
    return BioinfoTree(external, ghc_options)


def install_packages(args, bit):

    if args.external:
        has_errors = install_external(args, bit)
    else:
        has_errors = install_internal(args, bit)

    if has_errors:
        exit('[binary_install] Some installs have errors.')

def install_external(args, bit):

    if args.ignore and args.external:
        exit('[binary_install] --ignore is not compatible with --external.')

    path = getcwd()
    pkg = init_package(path, listdir(path), bit)
    if pkg is None:
        stderr.write("[binary_install] Don't known how to build this package.\n")
        return True
    else:
        return not pkg.build(args.force, args.stow)

def install_internal(args, bit):

    if args.remove_old and not args.all:
        exit('[binary_intall] --remove-old requires --all.')

    if args.remove_old:
        clean_binaries(bit.binary_dir())
        bit.clear_pkgs()

    if args.all:
        cwd = bit.root
    else:
        cwd = get_workdir()
        if bit.is_outside(cwd):
            exit('[binary_install] Cannot run outside of BIOINFO_ROOT: '
                 'consider using --external.')

    ignores = [ abspath(i)+'/' for i in args.ignore ]

    has_errors = False

    for pkg in iter_srcs(cwd, bit):
        if any(pkg.path.startswith(i) for i in ignores):
            continue

        print '>', pkg.path
        if not pkg.build(args.force, args.stow):
            has_errors = True

    return has_errors

def get_workdir():
    # Stolen from
    # http://bugs.python.org/issue1154351#msg61185

    cwd = getcwd()

    try:
        pwd = environ["PWD"]
    except KeyError:
        return cwd

    cwd_stat, pwd_stat = map(stat, [cwd, pwd])

    if cwd_stat.st_dev == pwd_stat.st_dev and \
       cwd_stat.st_ino == pwd_stat.st_ino:
        return pwd
    else:
        return cwd

def clean_binaries(root):
    for path in clean_files(root):
        while len(path) >= len(root):
            try:
                rmdir(path)
            except OSError:
                break
            else:
                path = dirname(path)

def clean_files(root):
    empty_dirs = []
    for dirpath, dirnames, filenames in walk(root):
        if '.do_not_clean' in filenames:
            dirnames[:] = []
            continue

        try:
            dirnames.remove('stow')
        except ValueError:
            pass

        n = len(filenames)
        for filename in filenames:
            path = resolve_link(join(dirpath, filename))
            if not (outside_of(path, root) or '/stow/' in link_path):
                unlink(path)
                n -= 1

        if n == 0 and len(dirnames) == 0:
            empty_dirs.append(dirpath)

    return empty_dirs

def iter_srcs(root, bit):
    for dirpath, dirnames, filenames in walk(root):
        if '/local/src/' in (dirpath+'/'):
            pkg = init_package(dirpath, filenames, bit)
            if pkg is not None:
                yield pkg


def list_packages(args, bit):
    pkgs = list(bit.list_pkgs())
    if len(pkgs) == 0:
        print 'No packages installed.'

    else:
        max_len = max(len(p[0]) for p in pkgs)

        fmt = '%%- %ds (at %%s)' % max_len
        for name, version in pkgs:
            print fmt % (name, version)


def upgrade_packages(args, bit):
    has_errors = False

    for path, use_stow in bit.outdated_pkgs():
        print '>', path

        pkg = init_package(path, listdir(path), bit)
        if pkg is None:
            stderr.write("[binary_install] don't know how to build this package.\n")
            return True

        else:
            if not pkg.build(args.force, use_stow):
                has_errors = True

    return has_errors


def forget_packages(args, bit):
    for pkg in args.pkgs:
        if not bit.forget_pkg(pkg):
            print >>stderr, "[binary_install] No such package: " + pkg


if __name__ == '__main__':
    main()
