# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from os import readlink
from os.path import dirname, islink
from sys import exit


def resolve_link(path):
    depth = 20
    while depth > 0 and islink(path):
        path = join(dirname(path), readlink(path))
        depth -= 1

    if depth <= 0 and islink(path):
        exit('[binary_install] Detected link cycle at: ' + path)

    return path
