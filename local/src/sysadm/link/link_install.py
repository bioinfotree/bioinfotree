#!/usr/bin/env python
#
# Copyright 2008,2012 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from vfork.path import link_relative
from vfork.util import exit, format_usage
from os.path import basename
import errno


def main():
    parser = OptionParser(usage=format_usage('''
        %prog SRC DEST

        Create a relative symbolic link DEST pointing to SRC.
    '''))
    parser.add_option('-f', '--force', dest='force', action='store_true', default=False, help='silently overwrite existing links')
    options, args = parser.parse_args()
    if len(args) < 2:
        exit('Unexpected argument number.')


    if len(args) == 2:
        sources=[args[0],]
        dests=[args[1],]
    else: #args>2
        dest_dir=args.pop()
        sources=args
        dests = ["%s/%s" % (dest_dir, basename(s) ) for s in sources]

    for s,d in zip(sources,dests):
        try:
            link_relative(s, d, force=options.force)
        except IOError, e:
            if e.errno == errno.EEXIST:
                exit('Link already exists.')
            else:
                exit(str(e))


if __name__ == '__main__':
    main()
