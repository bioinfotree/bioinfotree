# cut -f1 GOBPALLMm.txt | tr "~" "\t" > GOBPALLMm_term2name_file_in

gsea.rda: GOBPALLMm_term2name_file_in GOBPALLMm_term2gene_file_in gene_value_file_in
	condactivate;\	
	../term_enrichment.R -P 10 -m 10 -M 500 -c 1 gsea $< $^2 $^3 > $@;\
	condeactivate
	
enricher.rda: terms/UxUNIVERSE.txt GOBPALLMm_term2name_file_in GOBPALLMm_term2gene_file_in terms/UxDIFFERENTIAL.txt
	condactivate;\	
	../term_enrichment.R -P 10 -m 10 -M 500 -c 1 -u $< enricher $^2 $^3 $^4 > $@;\
	condeactivate

enricher.txt: enricher.rda
	condactivate;\	
	R --slave --vanilla -e 'suppressMessages(library(clusterProfiler));out<-get(load("$<"));write.table(summary(out), quote=FALSE, sep="\t", row.names=FALSE)' > $@;\
	condeactivate

gsea_table.txt: gsea.rda
	condactivate;\	
	R --slave --vanilla -e 'suppressMessages(library(clusterProfiler));out<-get(load("$<"));write.table(summary(out), quote=FALSE, sep="\t", row.names=FALSE)' > $@;\
	condeactivate

#not working
gsea_table.xlsx: gsea.rda
	library(xlsx)
	write.xlsx2(out.table, file="summary.table.xlsx", row.names=FALSE)

enrichr_table.txt: enrichr.rda
	#R --slave --vanilla -e 'suppressMessages(library(clusterProfiler));load("$<");write.table(summary(gsea.fromtable))' > $@

test:
	echo -e "pythonpath: $$PYTHONPATH \npythonhome: $$PYTHONHOME \nr-libs: $$R_LIBS_USER"
	echo -e "conda pref: $$CONDA_PREFIX"
	condactivate;\
	R --vanilla -e "library(clusterProfiler)";\
	r-libpaths;\
	echo "r libs: $$R_LIBS_USER";\
	echo -e "pythonpath: $$PYTHONPATH \npythonhome: $$PYTHONHOME \nr-libs: $$R_LIBS_USER" ;\
	echo -e "conda pref: $$CONDA_PREFIX"

testdisplay:
	@echo "DISPLAY: $$DISPLAY" 
	echo -e "PYTHONPATH: $$PYTHONPATH\n PYTHONHOME:$$PYTHONHOME\n R_LIBS_USER: $$R_LIBS_USER\n PERL5LIB: $$PERL5LIB\n R_LIBS_PATH: $$(r-libpaths | tail -n 5)" 
	condactivate;\
	echo -e "PYTHONPATH: $$PYTHONPATH\n PYTHONHOME:$$PYTHONHOME\n R_LIBS_USER: $$R_LIBS_USER\n PERL5LIB: $$PERL5LIB\n R_LIBS_PATH: $$(r-libpaths | tail -n 5)";\
	condeactivate

testrpaths:
	condactivate;\
        echo -e "\nR-LIBPATHS:" ;\
        r-libpaths | tail -n5 ;\
	echo -e "\nCONDA PATH: $$USR_CONDA_PATH" ;\
	echo -e "\nR_LIBS_USER: \n.$$R_LIBS_USER." ;\
	echo -e "\nR_LIBS_SITE: \n.$$R_LIBS_SITE." ;\
        echo -e "\nR-VERSION:" ;\
        R --version;\
        echo -e "WHICH-R:";\
        which R;\
        condeactivate && unset R_LIBS

enricher.html: term_enrichment_enricher.Rmd
	condactivate;\
	rmd2html $< -o $@;\
	condeactivate

