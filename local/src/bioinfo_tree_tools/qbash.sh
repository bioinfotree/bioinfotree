#/bin/bash
#set -e #do not use set -e because we ned to handle the fact that qsub can exit whit an error
#set -o pipefail

STDOUT_PROLOGUE_LEN=12
STDOUT_EPILOGUE_LEN=18
LOG_PATH="$PWD/.bmake/log/PBS_jobs"

usage()
{
cat << EOF
usage: $0 -c "CMD"

This script run CMD in qsub, wait for the job to be completed and exit with the same exit status of the job.
"set -eo pipefail" is enforced in the shell running the job.

The path indicated in CMD are relative to the path in which qbash is called and not to the user home as per default in qsub.

Qbash translate "echo PBS" in "#PBS" in the first row of the script to be executed before to submit it to qsub.
This allow to write PBS directives as "echo PBS someting" in the first line of a rule.

If -N SOMENAME is given as a directive, then qbash eventually shorten the SOMENAME since qsub require that the name is shorter than 15 character. 
The last 15 character are keep if SOMENAME has to be shortened.

OPTIONS:
   -h      Show this message
   -c CMD  The command to be executed
   -p N    Skip the first N rows from the standard output (PBS prologue) to be reported (default $STDOUT_PROLOGUE_LEN)
   -e N    Skip the last N rows from the standard output (PBS elipogue) to be reported (default $STDOUT_EPILOGUE_LEN)
EOF
}

CMD=
while getopts "hc:p:e:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         c)
             CMD=$OPTARG
             ;;
         p)
             STDOUT_PROLOGUE_LEN=$OPTARG
             ;;
         e)
             STDOUT_EPILOGUE_LEN=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [[ -z $CMD ]] 
then
     usage
     exit 1
fi

#directives in makefiles are expressed as "echo PBS someting" insteand of "#PBS someting", we have to translate "echo PBS" -> "PBS" in the first line of the rule.
#Moreover names have to be shorter that 15 char, so we eventually shorten the name
PBS_DIRECTIVES=$(printf '%s' "$CMD" \
	| perl -le '$_ = <>; $PBS=""; $PBS=$1 if $_ =~ m/\s*echo\s+(PBS[^;]+)/; print "#$PBS"')
CMD=$(printf '%s' "$CMD" | perl -ne '$.==1 and s|\s*echo\s+(PBS\s[^;]+);\s*||; print')

mkdir -p $LOG_PATH
#echo "PBS_DIRECTIVES: $PBS_DIRECTIVES";
#echo "CMD: $CMD";
PID=$BASHPID
SHELL_FILE="$LOG_PATH/$PID.sh"
OUT_FILE="$LOG_PATH/$PID.OU"
ERR_FILE="$LOG_PATH/$PID.ER"

printf '%s\nset -eo pipefail;\ncd $PBS_O_WORKDIR;\n%s\n' "$PBS_DIRECTIVES" "$CMD" > $SHELL_FILE

JOB_ID=$(qsub -W block=true -e $ERR_FILE -o $OUT_FILE $SHELL_FILE)
QSUB_EXIT_STATUS=$?

#REPORT THE OUT AND ERR TO THE CONSOLE

#output and error files can be generated some time after qsub exit
while  [ ! -f $OUT_FILE ];
do
	sleep 0.1
done
let EMPTY_OUTPUT=$STDOUT_PROLOGUE_LEN+$STDOUT_EPILOGUE_LEN
if [ `wc -l <  $OUT_FILE` -gt $EMPTY_OUTPUT ]; then
	if [ -t 1 ] ; then echo ">$PID STDOUT"; fi
	#https://stackoverflow.com/questions/911168/how-to-detect-if-my-shell-script-is-running-through-a-pipe
	
	tail -n +$(($STDOUT_PROLOGUE_LEN+1)) $OUT_FILE | head -n -$STDOUT_EPILOGUE_LEN
fi

while ! [ -f $ERR_FILE ];
do
	sleep 0.1
done
if [ -s $ERR_FILE ]; then
	RED='\033[0;31m'
	RESET='\e[0m'
	echo -e "$RED>$PID STERR"
	cat $ERR_FILE >&2
	echo -e "$RESET"
fi

exit $QSUB_EXIT_STATUS
