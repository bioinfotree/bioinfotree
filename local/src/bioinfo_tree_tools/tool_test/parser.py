# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

import re


class Test(object):
    def __init__(self, title):
        self.title     = title
        self.in_files  = None
        self.out_files = None
        self.fail      = None
        self.cmd       = None
        self.errlog    = None

    def incomplete(self):
        return None in (self.in_files, self.cmd) or (self.out_files is None and self.fail is None)


def iter_tests(filename):
    header_rx     = re.compile(r'^\*\s+(.+)\s*$')
    definition_rx = re.compile(r'^\s+(\w+):\s*(.*)$')

    for block in iter_blocks(filename):
        hline   = block[0]
        header = header_rx.match(hline.content)
        if header is None:
            test_definition_error('Expected header', hline)

        test = Test(header.group(1))

        for line in block[1:]:
            definition = definition_rx.match(line.content)
            if definition is None:
                test_definition_error('Expected definition', line)

            name  = definition.group(1).lower()
            value = definition.group(2)

            if name == 'in':
                if test.in_files is not None:
                    test_definition_error('Redefinition of "In"', line)
                test.in_files = value.split()

            elif name == 'out':
                if test.out_files is not None:
                    test_definition_error('Redefinition of "Out"', line)
                test.out_files = value.split()

            elif name == 'fail':
                if test.fail is not None:
                    test_definition_error('Redefinition of the "Fail" flag', line)
                test.fail = True

            elif name == 'cmd':
                if test.cmd is not None:
                    test_definition_error('Redefinition of "Cmd"', line)
                test.cmd = value

            else:
                test_definition_error('Unexpected definition of "%s"' % name, line)

        if test.incomplete():
            test_definition_error('Incomplete test definition', hline)

        yield test

def test_definition_error(msg, line):
    exit('%s at line %d of file: %s' % (msg, line.lineno, line.filename))

def iter_blocks(filename):
    block = []
    for line in iter_lines(filename):
        is_header = line.content[0] not in " \t"
        if is_header:
            if len(block):
                yield block
                block = []

        block.append(line)

    if len(block):
        yield block


class Line(object):
    def __init__(self, filename, lineno, content):
        self.filename = filename
        self.lineno   = lineno
        self.content  = content

    def append(self, more):
        self.content += more

    def __repr__(self):
        return '<%s:%d> %s' % (self.filename, self.lineno, self.content)


def iter_lines(filename):
    with file(filename) as fd:
        accum = None
        for idx, line in enumerate(fd):
            line = line.rstrip()

            if len(line) == 0 or line.lstrip()[:1] == '#':
                if accum is not None:
                    yield accum
                    accum = None
                continue

            continued = line[-1:] == '\\'
            if continued:
                line = line[:-1]

            if accum is None:
                accum = Line(filename, idx+1, line)
            else:
                accum.append(line)

            if not continued:
                yield accum
                accum = None

        if accum is not None:
            yield accum
