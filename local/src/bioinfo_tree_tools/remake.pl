#!/usr/bin/perl
use warnings;
use strict;
use POSIX;
use Getopt::Long;
use Pod::Usage;
use File::Temp qw/ tempfile/;
use lib $ENV{'BIOINFO_ROOT'}.'/local/lib/perl';
use BioinfoUtils;



$SIG{__WARN__} = sub {die @_};


my $help=0;
my $dry_run=0;
my $required_flags="";
GetOptions (
    'h|help'    => \$help,
    'n|dry_run' => \$dry_run,
    'f|flags=s' => \$required_flags,
    );
unless (scalar(@ARGV)>0) {pod2usage({-exitval => 1, -verbose => 1, -output => \*STDERR});}
pod2usage({-exitval => 0, -verbose => 99, -output => \*STDOUT}) if $help;

=head1 NAME

remake

=head1 SYNOPSIS

remake [-h | --help] [-n | --dry_run] [-f | --flags 'FLAGS'] file1 file2 ...

=head1 DESCRIPTION

Remake the files passed as argument using bmake.

If a newly produced file is identical to previous, remake maintain the time-stamp of 
the previous file in order to avoid running the downstream pipeline.

The -n flag shows the recipe to generate the files indicated as arguments
in a dry run fashion even if the files are already present.

FLAGS after -f are directly passed to bmake.

=head1 OPTIONS

=over 8

=item B<--help> | B<-h>

Prints the help message and exits.

=item B<--dry_run> | B<-n>

Shows the recipe and exits.

=item B<--flags> | B<-f>

Flags to be passed to bmake.

=back

=head1 AUTHOR

Ivan Molineris, E<lt>ivan.molineris@gmail.comE<gt>.

Michele Vidotto, E<lt>michele.vidotto@gmail.comE<gt>.

=head1 COPYRIGHT

This program is part of the bioinfotree framework

=cut


$\="\n";
$,="\t";

my $vars = join(" ",grep( /=/,@ARGV));
@ARGV = grep(!/=/,@ARGV);

for(@ARGV)
{
	my $tmp_file = undef;
	if(-e $_)
	{
	    (my $fh, $tmp_file) = tempfile(DIR=>'.');
	    if( m/\.gz$/)
	    {
		unlink($tmp_file);
		$tmp_file.=".gz" if m/\.gz$/;
	    }
	    rename($_, $tmp_file) or warn "Couldn't rename $_ to $tmp_file: $!\n";
	}

	my $flags=$required_flags;
	$flags .= " -n" if $dry_run;
	my $bmake_errors = &cmd_exec_system("bmake $flags $vars '$_'");

	if($dry_run && defined($tmp_file))
	{
	    rename($tmp_file,$_) or  warn "Couldn't rename $tmp_file to $_: $!\n";
	    exit(0);
	}
	
	if(defined($tmp_file))
	{ # i.e. the file $_ exists
	    if( not $bmake_errors )
	    {
		my $differ = &cmd_exec_nofail("zcmp -s $_ $tmp_file");
		if($differ == 0)
		{
		    rename($tmp_file,$_) or  warn "Couldn't rename $tmp_file to $_: $!\n";
		    print STDERR "[remake] remaked file identical to previous file";
		}
		else
		{
		    #print STDERR "differ: $differ\n";
		    unlink($tmp_file);
		}
	    }
	    else
	    {
		rename($tmp_file,$_) or  warn "Couldn't rename $tmp_file to $_: $!\n";
		print STDERR "[remake] remake failed, revert to previous file\n";
	    }
	}
}
