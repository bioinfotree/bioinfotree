#!/usr/bin/env python
#
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2011 Paolo Martini <paolo.cavei@gmail.com>

from optparse import OptionParser
from os import _exit, execvp, fork, kill, waitpid, WIFEXITED, WEXITSTATUS, WIFSIGNALED, WTERMSIG
from signal import alarm, signal, siginterrupt, SIGALRM, SIGTERM, SIGKILL, SIG_IGN
from vfork.util import exit, format_usage
from vfork.io.util import parse_int
import errno
import sys


def time_bounded(timeout, f):
    def ignore(*args):
        pass

    signal(SIGALRM, ignore)
    siginterrupt(SIGALRM, True)
    alarm(timeout)

    try:
        return f()
    except OSError, e:
        if e.errno == errno.EINTR:
            return 'interrupted'
        else:
            raise

def kill_gradually(pid):
    kill(pid, SIGTERM)

    res = time_bounded(5, lambda: waitpid(pid, 0))
    if res == 'interrupted':
        kill(pid, SIGKILL)


def main():
    parser = OptionParser(usage=format_usage('''
	%prog [OPTIONS] TIMEOUT -- CMD ARGS

	Runs CMD. If it doesn't completed within
        TIMEOUT seconds, kills the process and
        exits with an error code.
    '''))

    parser.add_option('-c', '--timeout-code', dest='timeout_code', type='int', default=1, help='sets the exit code when TIMEOUT is reached. Default: %default.')

    options, args = parser.parse_args()

    if len(args) < 2:
        exit('Unexpected argument number.')

    timeout = parse_int(args[0], 'TIMEOUT', 'strict_positive')

    pid = fork()
    if pid == 0:
        execvp(args[1], args[1:])
        _exit(1)
    else:
        def wait_child():
            code = waitpid(pid, 0)[1]
            if WIFEXITED(code):
                sys.exit(WEXITSTATUS(code))
            elif WIFSIGNALED(code):
                sys.exit(-WTERMSIG(code))
            else:
                sys.exit(1)

        res = time_bounded(timeout, wait_child)
        if res == 'interrupted':
            kill_gradually(pid)
            sys.exit(options.timeout_code)


if __name__ == '__main__':
    main()
