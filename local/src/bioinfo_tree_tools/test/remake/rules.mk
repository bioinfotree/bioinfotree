# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

static_part1.txt:
	printf """\
	Nel mezzo del cammin di nostra vita\n\
	mi ritrovai per una selva oscura,\n\
	che la diritta via era smarrita.\n\
	\n\
	""" \
	>$@

static_part2.txt:
	printf """\
	Ahi quanto a dir qual era e' cosa dura\n\
	esta selva selvaggia e aspra e forte\n\
	che nel pensier rinova la paura!\n\
	\n\
	Tant'e' amara che poco e' piu' morte;\n\
	ma per trattar del ben ch'i' vi trovai,\n\
	diro' de l'altre cose ch'i' v' ho scorte.\
	""" \
	>$@

static.txt: static_part1.txt static_part2.txt
	cat $^ >$@

random.txt:
	>$@; \
	for i in `seq 1 100`; do \
	date +%s | sha256sum | base64 | head -c 32 >>$@; \   * generate random text *
	done

with_errors.txt:
	echo "changed" >$@
	exit 1

with_errors2.txt: unknown.txt
	echo "changed" >$@


ALL += random.txt static.txt

INTERMEDIATE += static_part1.txt static_part2.txt

CLEAN += random.txt static.txt
