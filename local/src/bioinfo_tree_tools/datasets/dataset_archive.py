#!/usr/bin/env python
#
# Copyright 2014,2016 Gabriele Sales <gbrsales@gmail.com>

from argparse import ArgumentTypeError
from os import chdir, getcwd, walk
from os.path import exists, join, split
from shutil import rmtree
from subprocess import CalledProcessError, check_call
from sys import stderr
from vfork.util import ArgumentParser, exit


def main():
  args = parseArgs()

  cwd = getcwd()
  for path in candidateDatasets(args.name):
    archive(path, args.threads)
    chdir(cwd)

def parseArgs():
  parser = ArgumentParser(description='''
    This program automates the archival of old datasets.

    Given a NAME, it recursively searches the working directory for datasets
    with that name.

    Every time it finds a matching dataset, it performs the following steps:

    - expands all compressed files (this will make the later compression more
      effective);
    - creates a tar archive with all dataset contents;
    - compresses the archive with pixz at the maximum compression level;
    - checks the archive integrity against the original files;
    - deletes the dataset.
  ''')

  parser.add_argument('name', metavar='NAME',
                      help='the name of the datasets to be archived')

  parser.add_argument('-t', '--threads', metavar='THREADS',
                      type=positiveInt, default=0,
                      help='use this number of THREADS to compress data (default: use all cores)')

  return parser.parse_args()

def positiveInt(arg):
  try:
    n = int(arg)
    if n <= 0: raise ValueError()
    return n
  except ValueError:
    raise ArgumentTypeError('THREADS must be a positive integer: %s' % arg)

def candidateDatasets(name):
  for root, dirs, files in walk('.'):
    for d in dirs:
      if d == name:
        yield join(root, d)

def archive(path, threads):
  dir, name = split(path)
  chdir(dir)

  arch = name + '.tpxz'
  if exists(arch):
    stderr.write('Archive already exists, leaving it alone: ' +
                 join(dir, arch) + '\n')
    return

  decompress(name, dir)
  tar(name, arch, dir, threads)
  check(arch, dir, threads)
  remove(name)

def decompress(name, dir):
  for root, dirs, files in walk(name):
    for f in files:
      if f.endswith('.gz'):
        decompressWith('gzip', join(root, f), dir)
      elif f.endswith('.bz2'):
        decompressWith('bzip2', join(root, f), dir)
      elif f.endswith('.xz') or f.endswith('.tpxz'):
        decompressWith('pixz', join(root, f), dir)

def decompressWith(prog, path, root):
  try:
    check_call([prog, '-d', path])
  except CalledProcessError:
    exit('Error decompressing: ' + join(root, path))

def tar(name, arch, dir, threads):
  try:
    check_call(['tar', '-c', '-f', arch, '-I', pixz_cmd('-9e', threads), name])
  except CalledProcessError:
    exit('Error creating the archive: ' + join(dir, arch))

def pixz_cmd(opts, threads):
  pixz = 'pixz ' + opts
  if threads >= 1:
    pixz += ' -p %d' % threads
  return pixz

def check(arch, dir, threads):
  try:
    check_call(['tar', '-d', '-f', arch, '-I', pixz_cmd('', threads)])
  except CalledProcessError:
    exit('Error checking the archive: ' + join(dir, arch))

def remove(name):
  rmtree(name)


if __name__ == '__main__':
  main()
