#!/bin/bash

export LC_ALL=C
exec join -t $'\t' --check-order $@
