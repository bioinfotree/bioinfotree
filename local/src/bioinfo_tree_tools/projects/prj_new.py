#!/usr/bin/env python
#
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import chdir, getcwd, makedirs, sep
from os.path import exists, join
from subprocess import call
from vfork.path import bit_root, subdir_of
from vfork.util import exit, format_usage


def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] NAME

        Creates the skeleton of a new project.
    '''))
    parser.add_option('-g', '--git-repo', dest='git', default=False, action='store_true', help='initialize a new git repository for the project')
    options, args = parser.parse_args()
    if len(args) != 1:
        exit('Unexpected argument number.')

    if sep in args[0]:
        exit('Invalid project name: ' + args[0])
    
    root = bit_root()
    work_dir = getcwd()
    if not subdir_of(work_dir, root):
        exit('Cannot work outside of BIOINFO_ROOT: ' + root)

    prj_dir = join(work_dir, args[0])
    if exists(prj_dir):
        exit('The required project already exists.')

    for d in ['local/share/makefiles', 'local/share/rules',
              'local/bin', 'local/src',
              'dataset']:
        makedirs(join(prj_dir, d))

    if options.git:
        chdir(prj_dir)

        if call(['git', 'init']) != 0:
            exit('Error running git init.')

        with file('.gitignore', 'w') as fd:
            fd.write('dataset\n')

        if call(['git', 'add', '.gitignore']) != 0 or \
           call(['git', 'commit', '-m', 'Ignore datasets.']) != 0:
           exit('Error making the first git commit.')


if __name__ == '__main__':
    main()
