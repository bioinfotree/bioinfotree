# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from commands import getstatusoutput
from subprocess import Popen, PIPE
from time import sleep
import queue
import re

class Queue(object):
	job_id_rx = re.compile(r'Job <(\d+)> is submitted')
	job_status_rx = re.compile('\w+\s+\w+\s+(\w+)')
	
	def __init__(self, config):
		self.config = config
	
	def submit(self, job, wait=False):
		args = [
			'bsub',
			'-cwd', job.work_dir,
			'-e', job.name + '.err',
			'-o', job.name + '.out',
			'-J', job.name
		]
		if wait:
			args.append('-K')
		
		try:
			proc = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=False, close_fds=True)
			out, err = proc.communicate(job.command)
		except OSError:
			raise queue.QueueError('cannot queue job', 'you are probably missing the bsub command')
			
		if proc.wait() != 0:
			raise queue.QueueError('cannot queue job', err)
		
		if not wait:
			m = self.job_id_rx.search(out)
			if m is None:
				raise queue.QueueError('cannot retrieve job ID', out)
			else:
				job.queue_id = m.group(1)
	
	def wait(self, job):
		if job.queue_id is None:
			raise ValueError, 'invalid job'
		
		while True:
			status, output = getstatusoutput('bjobs ' + job.queue_id)
			if status != 0:
				raise queue.QueueError('cannot stat job', output)
			
			lines = output.split('\n')
			if len(lines) != 2:
				raise queue.QueueError('canot stat job', output)
			
			m = self.job_status_rx.match(lines[0])
			if m is None or m.group(1).lower() != 'stat':
				raise queue.QueueError('cannot stat job', output)
			
			m = self.job_status_rx.match(lines[1])
			if m is None:
				raise queue.QueueError('cannot stat job', output)
			elif m.group(1).lower() == 'done':
				break
			else:
				sleep(self.config.poll_interval)
