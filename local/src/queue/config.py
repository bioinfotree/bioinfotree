# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement

class Config(object):
	@classmethod
	def from_file(klass, filename):
		inst = klass()
		ConfigReader([
			('type', 'string'),
			('poll_interval', 'positive_integer')
		]).load(inst, filename)
		return inst

class ConfigReader(object):
	def __init__(self, entries):
		self.converters = {
			'string' : None,
			'positive_integer' : self._positive_integer
		}
		self.entries = self._prepare_entries(entries)
	
	def load(self, config, filename):
		try:
			cfg = self._file_to_dict(filename)
		except IOError, e:
			raise ConfigError, 'cannot read file: %s' % e.args[1]
		
		for name, converter in self.entries:
			try:
				value = cfg.pop(name)
			except KeyError:
				raise ConfigError, 'missing entry %s' % name
			
			if converter:
				value = converter(name, value)
			
			setattr(config, name, value)
		
		if len(cfg):
			raise ConfigError, 'unexpected entry %s' % cfg.keys()[0]
	
	def _prepare_entries(self, entries):
		res = []
		
		for name, converter in entries:
			try:
				converter = self.converters[converter]
			except KeyError:
				raise ValueError, 'invalid converter %s' % converter
			else:
				res.append((name, converter))
		
		return res
	
	def _file_to_dict(self, filename):
		cfg = {}
		
		with file(filename, 'r') as fd:
			for lineidx, line in enumerate(fd):
				idx = line.find('#')
				if idx >= 0:
					line = line[:idx]
				
				line = line.strip()
				if len(line) == 0:
					continue
				
				idx = line.find('=')
				if idx < 0:
					raise ConfigError, 'syntax error at line %d of file %s' % (lineidx+1, filename)
				
				name, value = line[:idx].rstrip(), line[idx+1:].lstrip()
				cfg[name] = value
		
		return cfg
	
	def _positive_integer(self, name, value):
		try:
			i = int(value)
			if i <= 0:
				raise ValueError
			return i
		except ValueError:
			raise ConfigError, 'invalid %s value: %s' % (name, value)

class ConfigError(Exception):
	''' An error in the configuration. '''
