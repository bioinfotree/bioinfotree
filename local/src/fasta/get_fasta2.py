#!/usr/bin/env python

from __future__ import with_statement

from optparse import OptionParser
from sys import exit, stdout

class FastaIndex(object):
	def __init__(self, filename):
		self._load(filename)
	
	def get_label_index(self, label):
		return self.labels.index(key)
	
	def get_by_label(self, label):
		return self.label_map[label]
	
	def get_by_idx(self, idx):
		return self.label_map[self.labels[idx]]
	
	def _load(self, filename):
		self.labels = []
		self.label_map = {}
		
		with file(filename, 'r') as fd:
			for lineno, line in enumerate(fd):
				tokens = line.rstrip().split('\t')
				try:
					if len(tokens) != 4:
						raise ValueError
					
					label = tokens[0]
					start = int(tokens[2])
					size = int(tokens[3])
				except ValueError:
					exit('Invalid index entry at line %d of file %s: %s' % (lineno+1, filename, line))
				
				self.labels.append(label)
				self.label_map[label] = (start, size)

def read_block(fd, start, size):
	fd.seek(start)
	
	while size > 0:
		chunk_size = min(size, 4096)
		size -= chunk_size
		
		chunk = fd.read(chunk_size)
		stdout.write(chunk)

def main():
	parser = OptionParser(usage='%prog (LABEL|@INDEX) FASTA INDEX_FILE')
	parser.add_option('-n', '--number', dest='number', type='int', default=1, help='output NUM blocks', metavar='NUM')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	elif options.number < 1:
		exit('Invalid block number.')
	
	fasta_index = FastaIndex(args[2])
	
	label = args[0]
	if label[0] == '@':
		try:
			idx = int(label[1:]) - 1
			if idx < 0:
				raise ValueError
		except ValueError:
			exit('Invalid index: %s' % idx)
	elif options.number == 1:
		try:
			idx = None
			start, size = fasta_index.get_by_label(label)
		except KeyError:
			exit('The label %s is not present in the file %s' % (label, args[1]))
	else:
		try:
			idx = index.get_label_index(label)
		except KeyError:
			exit('The label %s is not present in the file %s' % (label, args[1]))
	
	with file(args[1], 'r') as fd:
		if idx is None:
			read_block(fd, start, size)
		else:
			try:
				for idx in xrange(idx, idx+options.number):
					start, size = fasta_index.get_by_idx(idx)
					read_block(fd, start, size)
			except IndexError:
				exit('The file %s has less than %d blocks.' % (args[1], idx))

if __name__ == '__main__':
	main()
