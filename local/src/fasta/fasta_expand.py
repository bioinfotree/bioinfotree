#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from array import array
from optparse import OptionParser
from random import shuffle
from sys import stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter
from vfork.util import exit, format_usage

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <FASTA >EXPANDED_FASTA

		Each time a FASTA header contains more than one label,
		repeats block content for each label separately. Single
		entries are copied without modifications.

		Example input:

		>A;B
		SEQUENCE

		Example output:

		>A
		SEQUENCE
		>B
		SEQUENCE
	'''))
	parser.add_option('-s', '--separator', dest='separator', default=';', help='the character separating labels (default: ;)')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')

	for header, lines in MultipleBlockStreamingReader(stdin, join_lines=False):
		lines = list(lines)
		for label in header.split(options.separator):
			print '>%s' % label
			for line in lines:
				print line
	
if __name__ == '__main__':
	main()

