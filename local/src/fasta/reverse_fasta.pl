#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {my @loc = caller(1); CORE::die "WARNING generated at line $loc[2] in $loc[1]: ", @_};

my $usage = "$0 [-a|all] [COLL] < fasta\n
	reverse each sequence having a filed in the COLL position of the header =~ m/^-/
";

my $help=0;
my $all=0;
GetOptions (
	'h|help' => \$help,
	'a|all' => \$all,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $STRAND_COLUMN = undef;
if(not $all){
	$STRAND_COLUMN = shift;
	die($usage) if $STRAND_COLUMN !~ /\d+/;
	$STRAND_COLUMN--;
}

my $id=undef;
my @rows=();
while(<>){
	chomp;
	next if length==0 ;
	if(/^>/){
		my $post_id=$_;
		
		&reverse_if_needed($id,join('',@rows)) if @rows;
		
		$id=$post_id;
		@rows=();
		next;
	}
	push @rows, $_;
}
&reverse_if_needed($id,join('',@rows)) if @rows;

sub reverse_if_needed
{
	my $id=shift;
	my $seq=shift;
	my @F=split /\t/,$id;

	die("strand column not defined") if not $all  and !defined($F[$STRAND_COLUMN]);

	if($all or $F[$STRAND_COLUMN] =~ /^-/){
		die("invalid characters in sequence ($id)\n\n\n\n($seq)\n\n\n\n") if $seq !~ m/^[ACGTNacgtn]+$/;
		$seq = reverse $seq;
		$seq =~ tr/ACGTNacgtn/TGCANtgcan/;
	}

	print $id;
	while(length($seq)){
		print substr($seq,0,120,'');
	}
}
