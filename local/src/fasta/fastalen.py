#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from sys import stdin
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.util import exit, format_usage
import re

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <FASTA >LEN

		Prints the length of all FASTA blocks in input.
	'''))
	parser.add_option('-b', '--biggest-unmasked', dest='biggest_unmasked_only', action='store_true', default=False, help='measure the biggest unmasked subsequence size only')
	parser.add_option('-u', '--unmasked', dest='unmasked_only', action='store_true', default=False, help='count unmasked bases only')
	options, args = parser.parse_args()

	if len(args) > 1:
		exit('Unexpected argument number.')
	elif options.biggest_unmasked_only and options.unmasked_only:
		exit('--biggest-unmasked and --unmasked are mutually exclusive.')
	
	# for backward compatibility
	if len(args):
		fd = file(args[0], 'r')
	else:
		fd = stdin

	if options.biggest_unmasked_only:
		biggest_unmasked_rx = re.compile(r'[ACGT]+')
	elif options.unmasked_only:
		unmasked_table = ''.join(chr(i) for i in xrange(256))
	
	for header, block in MultipleBlockStreamingReader(fd, join_lines=True):
		if options.biggest_unmasked_only:
			subsequence_sizes = [ len(s) for s in biggest_unmasked_rx.findall(block) ]
			if len(subsequence_sizes):
				size = max(subsequence_sizes)
			else:
				size = 0
		elif options.unmasked_only:
			size = len(block.translate(unmasked_table, 'acgtnN'))
		else:
			size = len(block)
		
		print '%s\t%d' % (header, size)

if __name__ == '__main__':
	main()
