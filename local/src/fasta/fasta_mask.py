#!/usr/bin/env python
#
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>

from itertools import groupby
from operator import itemgetter
from optparse import OptionParser
from sys import stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage, ignore_broken_pipe


def iter_regions(filename):
    with file(filename, 'r') as fd:
        reader = Reader(fd, '0s,1u,2u')
        last_label = None

        for region in reader:
            label, start, stop = region
            if last_label is not None and label < last_label:
                exit('Disorder found in column 1, line %d of file %s.' % (reader.lineno(), filename))
            last_label = label

            if stop < start:
                exit('Invalid stop coordinate at line %d of file %s.' % (reader.lineno(), filename))
            
            yield region

def merge_overlapped(regions):
    if len(regions) == 0:
        return

    open_start, open_stop = regions[0]
    for start, stop in regions[1:]:
        if start > open_stop:
            yield open_start, open_stop
            open_start = start
        
        open_stop = stop

    yield open_start, open_stop

def load_regions(filename):
    res = {}
    for id, regions in groupby(iter_regions(filename), itemgetter(0)):
        res[id] = list(merge_overlapped(sorted(r[1:] for r in regions)))
    return res

def mask_soft(seq):
    return seq.lower()

def mask_hard(seq):
    return 'N' * len(seq)


def main():
    parser = OptionParser(usage=format_usage('''
      Usage: %prog [OPTIONS] REGIONS <FASTA

      Transforms the FASTA sequences provided as input masking
      the regions corresponding to the given coordinates.

      REGIONS must have the following format:
      1) sequence ID
      2) start
      3) stop

      By default, masking is applied by replacing the actual
      sequence with strings of N characters. The "--soft" mode,
      on the other hand, replaces the original sequence with a
      lowercase version.
    '''))
    parser.add_option('-l', '--soft', dest='soft', action='store_true', default=False, help='mask using lowercase letters')
    options, args = parser.parse_args()

    if len(args) != 1:
        exit('Unexpected argument number.')

    regions = load_regions(args[0])
    if options.soft:
        mask = mask_soft
    else:
        mask = mask_hard
    
    w = MultipleBlockWriter(stdout)
    for label, seq in MultipleBlockStreamingReader(stdin, join_lines=True):
        w.write_header(label)
        
        base = 0
        for start, stop in regions.get(label, []):
            if stop > len(seq):
                exit('Invalid region coordinates (out of bounds): %s %d %d.' % (label, start, stop))

            w.write_sequence(seq[base:start])
            w.write_sequence(mask(seq[start:stop]))

            base = stop

        w.write_sequence(seq[base:])

    w.flush()


if __name__ == '__main__':
    ignore_broken_pipe(main)
