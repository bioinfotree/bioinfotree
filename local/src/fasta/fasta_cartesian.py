#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from optparse import OptionParser
from sys import stdout, stderr
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.util import exit, format_usage
import re

def parse_partition_size(v):
	assert len(v) > 0
	
	unit = v[-1].lower()
	if unit in 'km':
		if unit == 'k':
			scale = 1024
		elif unit == 'm':
			scale = 1024 * 1024
		v2 = v[:-1]
	else:
		scale = 1
		v2 = v
	
	try:
		size = int(v2)
		if size <= 0:
			raise ValueError
	except ValueError:
		exit('Invalid macroblock size: %s' % v)
	else:
		return size * scale

def identity_macroblock(reader):
	idx = 0
	for header, content in reader:
		yield idx, idx+1
		idx += 1

def single_block(reader):
	blocks = 0
	for header, content in reader:
		blocks += 1
	yield 0, blocks

def split_by_blocks(reader, blocks):
	idx = start = 0
	for header, content in reader:
		idx += 1
		if idx - start == blocks:
			yield start, idx
			start = idx
	
	if start != idx:
		yield start, idx

def content_size(content):
	return sum(len(l) for l in content)

def split_by_size(reader, max_size):
	start = idx = size = 0
	for header, content in reader:
		cs = content_size(content)
		size += cs
		if size > max_size:
			if start == idx:
				print >>stderr, '[WARNING] Block \'%s\' is too big to respect the required maximum macroblock size.' % header
				yield start, idx+1
				start = idx + 1
			else:
				yield start, idx
				start = idx
			size = cs
		idx += 1
	
	if start != idx:
		yield start, idx

def build_partitions(reader, blocks, size):
	if blocks is None and size is None:
		ps = identity_macroblock(reader)
	elif blocks is not None:
		if blocks == 0:
			ps = single_block(reader)
		else:
			ps = split_by_blocks(reader, blocks)
	else:
		ps = split_by_size(reader, size)
	return ps

class Pattern(object):
	def __init__(self, pattern):
		self.pattern = pattern
		self.reference_rx = re.compile(r'(?<!%)%([1234])')
	
	def __call__(self, *vars):
		def expand_ref(m):
			pos = int(m.group(1))
			return str(vars[pos-1])
		return self.reference_rx.sub(expand_ref, self.pattern)
	
def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] LEFT_FASTA RIGHT_FASTA TARGET_NAME_PATTERN CMD_PATTERN >RULES
		
		Computes the cartesian product between the blocks in the two FASTA files and
		emits a make rule for each resulting pair.
		
		The -i, -j, -m and -n switches make the program partition the input files into
		macroblocks (corresponding each to one or more of the original blocks); the
		cartesian product is then computed among such macroblocs. If any cardinality (-i
		or -j) is set to 0, the corresponding input file is made into a single partition.
		
		The following variable references are expanded when encountered in the patterns:
		- %1: the start index of the left (macro)block;
		- %2: the stop index of the left (macro)block;
		- %3: the start index of the right (macro)block;
		- %4: the stop index of the right (macro)block.
		
		All indexes are 0 based. When no macro-blocking is requested, %2=%1+1 and
		%4=%3+1. References may be escaped by using a double % sign.
	'''))
	parser.add_option('-i', '--left-macroblock-cardinality', dest='left_macroblock_cardinality', type='int', default=None, help='the maximum number of blocks from the left file to put into a single macroblock', metavar='I')
	parser.add_option('-j', '--right-macroblock-cardinality', dest='right_macroblock_cardinality', type='int', default=None, help='the maximum number of blocks from the right file to put into a single macroblock', metavar='J')
	parser.add_option('-m', '--left-macroblock-size', dest='left_macroblock_size', default=None, help='the maximum size of each macroblock of the left file', metavar='M')
	parser.add_option('-n', '--right-macroblock-size', dest='right_macroblock_size', default=None, help='the maximum size of each macroblock of the right file', metavar='N')
	parser.add_option('-d', '--dependencies', dest='dependencies', default=None, help='additional dependencies for the emitted targets', metavar='PATTERN')
	parser.add_option('-l', '--target-list', dest='target_list', default=None, help='write a make variable holding the names of all the generated targets', metavar='NAME')
	options, args = parser.parse_args()
	
	if len(args) != 4:
		exit('Unexpected argument number.')
	elif options.left_macroblock_cardinality is not None and options.left_macroblock_size is not None:
		exit('Cannot use --left-macroblock-cardinality and --left-macroblock-size together.')
	elif options.right_macroblock_cardinality is not None and options.right_macroblock_size is not None:
		exit('Cannot use --right-macroblock-cardinality and --right-macroblock-size together.')
	
	if options.left_macroblock_cardinality is not None and options.left_macroblock_cardinality < 0:
		exit('Invalid left macroblock cardinality: %s' % options.left_macroblock_cardinality)
	if options.right_macroblock_cardinality is not None and options.right_macroblock_cardinality < 0:
		exit('Invalid right macroblock cardinality: %s' % options.right_macroblock_cardinality)
	if options.left_macroblock_size is not None:
		options.left_macroblock_size = parse_partition_size(options.left_macroblock_size)
	if options.right_macroblock_size is not None:
		options.right_macroblock_size = parse_partition_size(options.right_macroblock_size)
	
	target_pattern = Pattern(args[2])
	command_pattern = Pattern(args[3])
	if options.dependencies:
		dependency_pattern = Pattern(options.dependencies)
	
	targets = []
	with file(args[0], 'r') as left_fd:
		with file(args[1], 'r') as right_fd:
			left_reader = MultipleBlockStreamingReader(left_fd, join_lines=False)
			right_reader = MultipleBlockStreamingReader(right_fd, join_lines=False)
			
			left_partitions = build_partitions(left_reader, options.left_macroblock_cardinality, options.left_macroblock_size)
			right_partitions = list(build_partitions(right_reader, options.right_macroblock_cardinality, options.right_macroblock_size))
			
			for lp in left_partitions:
				for rp in right_partitions:
					target = target_pattern(lp[0], lp[1], rp[0], rp[1])
					command = command_pattern(lp[0], lp[1], rp[0], rp[1])
					dependencies = dependency_pattern(lp[0], lp[1], rp[0], rp[1]) if options.dependencies else ''
					stdout.write('%s: %s %s %s\n\t%s\n' % (target, args[0], args[1], dependencies, command))
					if options.target_list: targets.append(target)
	
	if options.target_list:
		stdout.write('%s:=%s\n' % (options.target_list, ' '.join(targets)))

if __name__ == '__main__':
	main()
