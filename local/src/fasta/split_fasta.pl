#!/usr/bin/perl
use POSIX;
use File::Basename;
use Getopt::Long;
use strict;
use warnings;

my $usage="usage: $0 [-e] input_filename n output_dir
produce n fasta file in the output_dir 
the files are named filename.1 ... filename.n, each containing a slice of the original filename

	-e|each_block 			produce a output file for each fasta block, ignore the n param
	-f|out_filename_extension 	in -e mode add the given extension to the generated files, e.g. -f .fa [default: \"\"]
	-n|enumerate			use number inshead of fasta ide for the file names
	-p|print_header			report the heder in each file
	-o|print_header_only_id		if -p ther report the heder in each file, but only the first word of the header
";

my $each_block = 0;
my $print_header = 0;
my $print_header_only_id = 0;
my $enumerate = 0;
my $help=0;
my $out_filename_extension="";

GetOptions (
	'h|help' => \$help,
	'e|each_block' => \$each_block,
	'f|out_filename_extension=s' => \$out_filename_extension,
	'n|enumerate' => \$enumerate,
	'p|print_header' => \$print_header,
	'o|print_header_only_id' => \$print_header_only_id,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $filename=shift @ARGV;
open FH, $filename or die("Can't open file ($filename)\n$usage");
$filename=basename($filename);

my $n=shift @ARGV;
die("ERROR: second argument must be a valid number\n$usage") if $n !~/\d+/ and not $each_block;

my $out_dir=shift @ARGV;
die("ERROR: invalid output directory ($out_dir)\n$usage") if !-d $out_dir;


my $header_per_splice =1;
if(not $each_block){
	my $header_n=`grep '>' $filename | wc -l`;
	chomp($header_n);
	$header_n =~ s/^\s+//;
	$header_per_splice = ceil($header_n/$n);
}


my $out_filename=undef;
my $j=0;
my $i=0;
while(<FH>){
	if(m/>/){
	
		$i++;
		if($i>=$header_per_splice or !defined($out_filename)){
			$i=0;
			$j++;
			
			close OUT if defined($out_filename);
			
			if(not $each_block){
				$out_filename =  $filename;
				$out_filename =~ s/\.fa$//;
				$out_filename .= ".$j.fa";
			}else{
				m/^>([^\s]+)/;
				if($enumerate){
					$out_filename = "$out_dir/$j".$out_filename_extension;
				}else{
					$out_filename = "$out_dir/$1".$out_filename_extension;
				}
			}

			die("ERROR: $out_filename allready exist") if(-e $out_filename);

			open OUT, ">$out_filename" or die("Can't write file ($out_filename)");
			next if $each_block and not $print_header;# in questo caso non scrivo l'header
			s/\s.*/\n/ if $print_header and $each_block and $print_header_only_id;
		}
	}

	die("ERROR: empty line before firs header") if !defined($out_filename);
	
	print OUT;
	
}
