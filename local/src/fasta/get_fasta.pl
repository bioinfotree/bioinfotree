#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

my $usage="$0 [-r|-i] [-w] [-n] [-f] id file.fa";

my $regex=0;
my $word_regexp=0;
my $id_only=0;
my $id_from_file=0;
my $no_header=0;
my @ids=();
my %ids_hash=();
my $invert = 0;

GetOptions (
	'w' => \$word_regexp,
	'r' => \$regex,
	'i' => \$id_only,
	'f' => \$id_from_file,
	'n' => \$no_header,
	'v' => \$invert,
)or die($usage);

die($usage) if $id_only and $regex;

my $id = shift @ARGV;
($id =~ /\w+/) or die($usage);
if($id_from_file){
	open (FH, $id) or die("Can't open file $id\n");
	while(<FH>){
		chomp;
		if($id_only){
			$ids_hash{$_}=1;
		}else{
			push @ids, $_;
		}
	}
}else{
	@ids=($id);
	if($id_only){
		$ids_hash{$id}=1;
	}
}



my $print=0;
while(<>){
	if($print){
		if (m/^>/){
			if(!&match($_)){
				$print=0;
				next;
			}else{
				next if $no_header;
			}
		}
		print;
		$print=1;
	}else{
		if(m/^>/ and &match($_)){
			$print=1;
			if($no_header){
				next;
			}else{
				print;
			}
		}
	}
}

sub match{
	if($invert){
		return 1 if not &sub_match($_)
	}else{
		return &sub_match($_)
	}
}

sub sub_match
{
	
	my $a=shift;
	if($id_only and !$id_from_file){
		return 1 if $a=~m/^>$id_only\s/;
	}elsif($id_only){
		$a=~m/^>([\w-]+)/;
		return 1 if defined($ids_hash{$1});
	}elsif($regex){
		for(@ids){
			return 1 if $a=~/$_/;
		}
	}elsif($word_regexp){
		for(@ids){
			return 1 if $a=~/\W$_\W/;
		}
	}
}
