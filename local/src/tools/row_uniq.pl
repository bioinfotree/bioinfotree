#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {my @loc = caller(1); CORE::die "WARNING generated at line $loc[1] in $loc[0]: ", @_};

my $usage = "$0 [-s|--separator SEP] COLUMN\n";

my $separator=";";
GetOptions (
	's|separator=s' => \$separator,
) or die($usage);

my $col_n = shift;
die($usage) if $col_n !~/\d+/;
$col_n--;

while(<>){
	chomp;
	my @F = split /\t/;
	my @G = split(/$separator/,$F[$col_n]);
	my %tmp=();
	for(@G){
		$tmp{$_}++
	}
	$F[$col_n] = join($separator, keys(%tmp));

	print @F;
}
