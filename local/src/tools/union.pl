#!/usr/bin/perl

use warnings;
use strict;
use constant CHR_COL   => 0;
use constant B_COL     => 1;
use constant E_COL     => 2;
use constant ID_COL    => 3;
#use Getopt::Long;



my $usage = "cat chr_b_e_id | $0";



#
# 1. inizializzazione
#
my @union=();
my $last_chr=undef;
my $last_b=undef;
while(<>){
	chomp;
	my ($chr, $b, $e, @all) = split /\t/;
	die("Error: undefined chr") if !defined($chr) or !length($chr);
	die("Error: undefined b")   if !defined($b)   or !length($b);
	die("Error: undefined e")   if !defined($e)   or !length($e);
	die("Error: disorder found" ) if defined($last_chr) and $chr lt $last_chr;
	die("Error: disorder found" ) if defined($last_b) and $chr eq $last_chr  and $b < $last_b;

	if(!@union){
		@union=($chr, $b, $e, \@all);
	}elsif(	$chr ne $union[0]
			or
		$b > $union[2]
	){
		my_print(@union);
		@union=($chr, $b, $e, \@all);
	}else{
		$union[2] = $e > $union[2] ? $e : $union[2];
		push @union, \@all; 
	}
	$last_chr=$chr;
	$last_b=$b;
}

my_print(@union);



sub my_print
{
	print shift,"\t";
	print shift,"\t";
	print shift;
	my $first=1;
	for(@_){
		next if !@{$_};
		if($first){
			print "\t";
			$first=0;
		}else{
			print ';';
		}
		print join(',',@{$_});
	}
	print "\n";
}
