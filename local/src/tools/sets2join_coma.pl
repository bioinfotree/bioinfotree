#!/usr/bin/perl

use warnings;
use strict;

$,="\t";
$\="\n";


while(<>){
	chomp;
	my @F=split /\t/;
	my $set=pop @F;
	for (split(/;/, $set)) {
		print @F,$_;
	}
}
