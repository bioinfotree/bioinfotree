#!/usr/bin/perl
use warnings;
use strict;
$,="\t";

use constant ID_COL => 0;


my $i=0;
my @data=();
my %id=();
my @num_col=();


if($ARGV[0] eq '-'){
	for(<STDIN>){
		chomp;
		next if !length;
		my @F=split;
		$data[$i]{$F[ID_COL]}=\@F;
		$id{$F[ID_COL]}=1;
		$num_col[$i] = scalar(@F) if !defined $num_col[$i];
	}
	$i++;
	shift(@ARGV);
}

for(@ARGV){
	open FH, $_ or die("Can't open file ($_)\n");
	while(<FH>){
		chomp;
	        next if !length;
	        my @F=split;
		$data[$i]{$F[ID_COL]}=\@F;
		$id{$F[ID_COL]}=1;
		$num_col[$i] = scalar(@F) if !defined $num_col[$i];
	}
	close FH;
	$i++;
}

for(keys(%id)){
	for(my $j=0; $j<$i; $j++){
		if(defined($data[$j]{$_})){
			print @{$data[$j]{$_}};
		}else{
			for(2..$num_col[$j]){
				print "\t";
			}
		}
		if($j<$i - 1){
			print "\t";
		}
	}
	print "\n";
}
