#include "RandomGenerator.h"

/*! \brief Generatore di numeri casuali (gsl)
 *	
 *	Questa classe e` utile per generare numeri casuali interi o double,
 *	uniformemente distribuiti in un intervallo, oppure per mescolare array
 *	di qualunque tipo
 *
 *  */ 
using namespace std;
RandomGenerator::RandomGenerator(bool _verbose){
	
	verbose=_verbose;
	
	// inizializzo il generatore di numeri casuali,
	const gsl_rng_type * T;

	gsl_rng_env_setup();

	T = gsl_rng_default;
	GslRng = gsl_rng_alloc (T);
	gsl_rng_set(GslRng,get_seed());
	
	if(verbose){
		printf ("generator type: %s\n", gsl_rng_name (GslRng));
		printf ("default seed = %lu (il firts value cambia\n\tanche se il seed sembra\n\trimanere a 0 nonos-\n\ttante la chiamata\n\t a gsl_rng_set)\n", gsl_rng_default_seed);
		printf ("first value = %lu\n", gsl_rng_get (GslRng));
		//exit(-1);
	}
}	


double RandomGenerator::get_double(){
	if(!dmin_set or !dmax_set){
		cerr<<"RandomGenerator::get: dmin or dmax not set"<<endl;
		exit(-1);
	}
	double rand_num=gsl_rng_uniform(GslRng);
	return (rand_num*(dmax - dmin) + dmin);
}

int RandomGenerator::get_int(){
	if(!imin_set or !imax_set){
		cerr<<"RandomGenerator::get: imin or imax not set"<<endl;
		exit(-1);
	}

	return imin + gsl_rng_uniform_int(GslRng, (imax - imin) + 1);
	//+1 in modo tale che il massimo sia incluso:
	//http://www.gnu.org/software/gsl/manual/gsl-ref_17.html#SEC266
}

unsigned long int RandomGenerator::get_seed(){
	unsigned long int seed=0;	
	struct timeval time;
	struct timezone timez;
    	int rc;
    	rc=gettimeofday(&time, &timez);
	seed=time.tv_sec*1000000+time.tv_usec;
	
	if(verbose){
		cerr<<"seed: " << seed<<endl;
		//exit(-1);
	}
	return(seed);
}
template<class Type>
void RandomGenerator::shuffle(Type array[], unsigned int a_size){
	int n_switch = (int)((a_size + 1)* log((double)a_size));

	set_max((int)(a_size -1));
	set_min(0);
	
	for(int i=0; i<n_switch; i++){
		int a=get_int();
		int b=get_int();
		Type tmp = array[a];
		array[a]=array[b];
		array[b]=tmp;
	}
}
