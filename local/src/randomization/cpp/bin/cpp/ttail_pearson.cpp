#include <iostream>
#include <stdlib.h>
#include "gsl/gsl_cdf.h"

using namespace std;

int main( int argc, char* argv[] ){
	if(argc!=3){
		cerr<<"usage: ttail_pearson value degre_of_fredom"<<endl;
		exit(-1);
	}
	float p=atof(argv[1]);
	int n = atoi(argv[2]);
	p=p*::sqrt((n-2)/(1-p*p));
	cout << p << "\t" << gsl_cdf_tdist_Q(p,n) << endl;
	return(0);
}
