#ifndef CONFIGMAP_H_
#define CONFIGMAP_H_

#include <ext/hash_map>

namespace config {
	// private stuff
	namespace
	{
		struct StringEquality
		{
			bool operator()(const std::string& s1, const std::string& s2) const
			{
				return s1.compare(s2) == 0;
			}
		};
		
		struct string_hash
		{
			size_t operator()(const std::string& s) const
			{
				return __gnu_cxx::hash<const char*>()(s.c_str());
			}
		};
	}
	
	class ConfigMap
	{
	public:
		ConfigMap() {}
		
		ConfigMap* load(const char* path);
		bool has_key(const std::string& option) const;
		const std::string& operator[](const std::string& option) const;
		
	protected:
		char* strip(char* str);
	
		__gnu_cxx::hash_map<const std::string, std::string, string_hash, StringEquality> options_;
	
	private:
		//ConfigMap(const ConfigMap& obj);
		ConfigMap& operator=(const ConfigMap& rhs);
	};
	
	class IOError {};
	class KeyError {};
}

#endif /*CONFIGMAP_H_*/
