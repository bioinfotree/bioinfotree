#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

while(<>){
	chomp;
	my ($id, $i_b, $i_e, $a_b, $a_e, $b_b, $b_e, @all) = split;
	my $u = &max($a_e,$b_e) - &min($a_b, $b_b);
	my $i = $i_e - $i_b;
	my $a = $a_e - $a_b;
	my $b = $b_e - $b_b;
	print 	$id, $i_b, $i_e, $a_b, $a_e, $b_b, $b_e,
		$i/$a, $i/$b, 1 - $i/$u, 
		@all;
}

sub min
{
	my ($a,$b)=@_;
	return $a > $b ? $b : $a;
}

sub max
{
	my ($a,$b)=@_;
	return $a < $b ? $b : $a;
}
