#!/usr/bin/env python

from optparse import OptionParser
from sys import stdin
from vfork.util import exit, format_usage

def iter_lines(fd):
	for idx, line in enumerate(fd):
		tokens = line.split(None)
		
		try:
			yield [ int(t) for t in tokens ]
		except ValueError:
			exit('Invalid numeric value at line %d:\n%s' % (idx+1, line.rstrip()))

def main():
	usage = format_usage('''
		%prog <TSV
		
		Reads a tab-delimited file and prints the
		sum of all the values on each line.
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	for nums in iter_lines(stdin):
		print sum(nums)

if __name__ == '__main__':
	main()
