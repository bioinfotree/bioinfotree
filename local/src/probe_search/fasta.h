// Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

#ifndef FASTA
#define FASTA

#include <string>

struct FastaBlock
{
	FastaBlock(const std::string& label, const std::string& sequence) : label(label), sequence(sequence) {}
	
	const std::string label;
	const std::string sequence;
};

class FastaReader
{
public:
	FastaReader(const std::string& filename);
	~FastaReader();
	const FastaBlock* get_block();

private:
	bool read_line(char* buf, const int size);
	
	const std::string _filename;
	FILE* _fd;
};

#endif //FASTA
