// Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

#ifndef SEEDHASH_H
#define SEEDHASH_H

class SeedHash
{
public:
	SeedHash(const unsigned int size) : _size(size), _mask(compute_mask(size)), _value(-1) {}
	
	bool compute(const char* s, const unsigned int start)
	{
		_value = 0;
		
		unsigned int pos = 0;
		while (pos < _size)
		{
			_value *= 4;
			
			switch(s[start+pos])
			{
			case 'A':
				break;
			case 'C':
				_value += 1;
				break;
			case 'G':
				_value += 2;
				break;
			case 'T':
				_value += 3;
				break;
			default:
				_value = -1;
				return false;
			}
			
			pos++;
		}
		
		return true;
	}
	
	bool update(const char* s, const unsigned int start)
	{
		if (_value < 0)
			return compute(s, start);
		
		_value = (_value * 4) & _mask;
		
		switch (s[start + _size - 1])
		{
		case 'A':
			break;
		case 'C':
			_value += 1;
			break;
		case 'G':
			_value += 2;
			break;
		case 'T':
			_value += 3;
			break;
		default:
			_value = -1;
			return false;
		}
		
		return true;
	}
	
	inline operator int() const
	{
		return _value;
	}

private:
	static int compute_mask(const unsigned int size)
	{
		int mask = 0;
		for (unsigned int i = 0; i < size; i++)
			mask = (mask << 2) | 3;
		return mask;
	}
	
	const unsigned int _size;
	const int _mask;
	int _value;
};

#endif //SEEDHASH_H
