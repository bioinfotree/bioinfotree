#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import networkx as NX
#from subprocess import Popen, PIPE
#from collections import defaultdict

def main():
	usage = format_usage('''
		%prog < STDIN
			.META STDIN
				1	id1
				2	id2

			each row in the STDIN is a edge

			.META STDOUT
				1	clique_id
				2	vertex_id

			a vertex may belong to many different cliques
	''')
	parser = OptionParser(usage=usage)
	
	#parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF', metavar='CUTOFF')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE', metavar='FILE')
	#parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help')

	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	Graph = NX.Graph()
	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		#tokens = line.rstrip().split('\t')
		assert len(tokens) == 2
		Graph.add_edge(tokens[1],tokens[0])
	for i,cq in enumerate(NX.find_cliques(Graph)):
		for v in cq:
			print "%i\t%s" % (i,v) 
		

if __name__ == '__main__':
	main()

