#!/usr/bin/perl 
use warnings;
use strict;
$,="\t";
$\="\n";

my $usage="$0 connectivity_file < conn_comp_file\n

conn_comp_file:
id_vertex	id_conn_comp

connectivity_file:
id_vertex	connectivity
";

my $connectivity_filename = shift;

die($usage) if !$connectivity_filename;

my %connectivity=();
my %conn_comp=();

while(<>){
	my @F = split;
	push @{$conn_comp{$F[1]}}, $F[0];
}

open FH, $connectivity_filename or die("Can't open file ($connectivity_filename)");

while(<FH>){
	my ($k,$v) = split;
	$connectivity{$k}=$v;

}

for(keys %conn_comp){
	my $n=scalar(@{$conn_comp{$_}});
	my $m=0;
	
	foreach my $k (@{$conn_comp{$_}}){
		$m+=$connectivity{$k}
	}
	$m/=2;

	print 	$_,
		$n,
		$m,
		$m / ($n * ($n - 1) / 2);
}
