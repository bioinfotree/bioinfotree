#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, argv
import atexit
from optparse import OptionParser
from vfork.util import exit
from collections import defaultdict
from gzip import GzipFile
import networkx as NX

script_name = argv[0]

def main():
	parser = OptionParser(usage='''%prog root_id''') 
	
	parser.add_option('-d', '--directed', dest='directed', action='store_true', default=False, help='the graph given in stdin is considered as a directed graph, ancestors in the first column (A --> B), see --reverse_edge')
	parser.add_option('-f', '--from_file', dest='from_file', action='store_true', default=False, help='the first argument is not a root but a file with a list node used indipendently form caclculate shortest path')
	parser.add_option('-r', '--reverse_edge', dest='reverse_edge', action='store_true', default=False, help='reverse edge direction')
	
	options, args = parser.parse_args()

	if options.reverse_edge and not options.directed:
		exit("Option -r is meaningless without option -d");
	
	if len(args) != 1:
		exit('Unexpected argument number.')

	roots = []

	if not options.from_file:
		roots.append(args[0])
	else:
		with file(args[0], 'r') as fd:
			for line in fd:
				tokens = line.rstrip().split('\t')
				assert len(tokens) == 1
				roots.append(tokens[0])
	
	if not options.directed:
		Graph = NX.Graph()
	else:
		Graph = NX.DiGraph()

	for line in stdin:
		tokens = line.rstrip().split('\t')
		assert len(tokens) == 2
		if not options.reverse_edge:
			Graph.add_edge(tuple(tokens))
		else:
			Graph.add_edge(tokens[1],tokens[0])

	for root in roots:
		shortest_path = NX.single_source_shortest_path(Graph, root)

		for id, path in shortest_path.iteritems():
			print "%s\t%s\t%d" % (root, id, len(path))

if __name__ == '__main__':
	main()

