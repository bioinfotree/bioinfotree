#!/usr/bin/env python
#
# Copyright 2007-2008 Ivan Molineris <ivan.molineris@gmail.com>
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage
from sys import stdin

def main():
	parser = OptionParser(usage=format_usage('''
		%prog <NETWORK_FILE
		
		Computes the connected components of the input network.

		The output contains the following two columns:
		* node ID
		* connected component ID
	'''))
	parser.add_option('-n', '--numeric-ids', dest='numeric_ids', action='store_true', default=False, help='generate numeric IDs for connected components')
	options, args = parser.parse_args()

	conn_comp = {}
	id = 0
	for a, b in Reader(stdin, '0s,1s', False):
		conn_a = conn_comp.get(a, None)
		conn_b = conn_comp.get(b, None)

		if conn_a is not None and conn_b is not None:
			for k, v in conn_comp.iteritems():
				if v == conn_b:
					conn_comp[k] = conn_a
		elif conn_a is not None:
				conn_comp[b] = conn_a
		elif conn_b is not None:
				conn_comp[a] = conn_b
		else:
			if options.numeric_ids:
				k = id
				id += 1
			else:
				k = a

			conn_comp[a] = k
			conn_comp[b] = k

	for k, v in conn_comp.iteritems():
		print '%s\t%s' % (k, v)

if __name__ == '__main__':
	main()
