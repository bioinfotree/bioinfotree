#!/usr/bin/env python
from optparse import OptionParser
from os import environ, readlink, unlink, walk
from os.path import basename, dirname, isdir, islink, join, normpath
from shutil import copy2, copytree, rmtree
from subprocess import Popen, PIPE
from tempfile import mkdtemp
from vfork.util import exit

def get_platform_path():
	try:
		root = environ['BIOINFO_ROOT']
	except KeyError:
		exit('Undefined BIOINFO_ROOT.')
	
	try:
		host = environ['BIOINFO_HOST']
	except KeyError:
		exit('Undefined BIOINFO_HOST')
	
	path = join(root, 'platform', host)
	if not isdir(path):
		exit('Missing platform directory: %s' % path)
	
	return path

def display_path(path):
	s = '/platform/'
	return path[path.rindex(s)+len(s):]

def list_svn_unknown(root):
	proc = Popen(['svn', 'st'], stdout=PIPE, cwd=root)
	out, err = proc.communicate()
	if proc.returncode != 0:
		exit('Error running svn st.')
	
	paths = []
	for line in out.split('\n'):
		line = line.strip()
		if len(line) == 0:
			continue
		
		tokens = line.split(None)
		if len(tokens) != 2:
			exit('Invalid svn st output.')
		elif tokens[0] == '?':
			paths.append(tokens[1])
	
	return paths

def iter_svn_roots(root):
	for dirpath, dirnames, filenames in walk(root):
		if '.svn' in dirnames:
			dirnames[:] = []
			yield dirpath

def resolve_link(path):
	n = 0
	while True:
		n += 1
		if n > 100:
			raise RuntimeError, 'too many levels of symbolic links'

		if not islink(path):
			return path
		else:
			path = normpath(join(dirname(path), readlink(path)))

def iter_svn_links(root, svn_root):
	for dirpath, dirnames, filenames in walk(root):
		for name in dirnames + filenames:
			try:
				pointed = resolve_link(join(dirpath, name))
				if (pointed + '/').startswith(svn_root + '/'):
					if isdir(pointed):
						yield pointed
					else:
						yield dirname(pointed)
			except OSError:
				pass

def iter_build_dirs(platform_path, svn_root):
	for head in ('bin', 'lib'):
		for svn_path in iter_svn_links(join(platform_path, head), svn_root):
			for dirpath, dirnames, filenames in walk(svn_path):
				if 'makefile' in (f.lower() for f in filenames):
					dirnames[:] = []
					yield dirpath

def run_in_place(cmd, path):
	proc = Popen(cmd, shell=True, cwd=path, close_fds=True)
	return proc.wait() == 0

def rebuild(path):
	tmpdir = mkdtemp()
	try:
		build_dir = join(tmpdir, 'work')
		copytree(path, build_dir)
		
		if not run_in_place('make clean && make', build_dir):
			exit('*** Error rebuilding %s' % display_path(path))
		
		if not run_in_place('make clean', path):
			exit('*** Error cleaning %s' % display_path(path))
		
		unknowns = list_svn_unknown(build_dir)
		for unknown in unknowns:
			src_path = join(build_dir, unknown)
			dst_path = join(path, dirname(unknown))
			if isdir(src_path):
				exit('Unexpected directory while building %s: %s' % (display_path(path), unknown))
			else:
				copy2(src_path, dst_path)
	finally:
		rmtree(tmpdir)

def main():
	parser = OptionParser(usage='%prog')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	platform_path = get_platform_path()
	for svn_root in iter_svn_roots(platform_path):
		print '*** Updating %s' % display_path(svn_root)
		if not run_in_place('svn up', svn_root):
			exit('*** Error updating %s' % display_path(svn_root))
		print '*** Done updating %s' % display_path(svn_root)
		
		build_paths = []
		for build_dir in iter_build_dirs(platform_path, svn_root):
			print '*** Rebuilding %s' % display_path(build_dir)
			rebuild(build_dir)
			print '*** Done rebuilding %s' % display_path(build_dir)
			build_paths.append(build_dir + '/')
		
		unknowns = list_svn_unknown(svn_root)
		for unknown in unknowns:
			unknown = join(svn_root, unknown)
			
			clean = True
			for build_path in build_paths:
				if unknown.startswith(build_path):
					clean = False
					break
			
			if clean:
				if isdir(unknown):
					exit('Unexpected directory: %s' % unknown)
				else:
					print '*** Unlinking', display_path(unknown)
					unlink(unknown)

if __name__ == '__main__':
	main()

