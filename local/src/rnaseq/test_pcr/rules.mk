contrast?="0 0 0 0 1 1 1 1"
rnaseq_contrast?="(KO.ND-WT.ND)"
housekeeping?=Tbp
qpcr.pdf: qPCR1.txt limma.RData
	qPCRvsRNASeq -c $(contrast) -r $(rnaseq_contrast) -k $(housekeeping) $< $^2 $@ 
	cmp $@.cfr $@
