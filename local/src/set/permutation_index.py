#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from optparse import OptionParser
from sys import stdin
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

def read_labels(filename):
	labels = []
	with file(filename, 'r') as fd:
		for line in fd:
			labels.append(safe_rstrip(line))
	return labels

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] LABELS <PERMUTATIONS
	
		Given a set of LABELS, computes the order in which they appear for each
		line of the input file.

		For example, given the labels:

		  A B C

		and the permutation file:

		  A B C
		  C B A

		the program prints:

		  0 1 2
		  2 1 0

		When a label is missing an empty string is written in place of the index.
	'''))
	parser.add_option('-f', '--from-file', dest='from_file', help='read labels from file', metavar='PATH')
	options, args = parser.parse_args()
	
	if (options.from_file is not None and len(args) != 0) or (options.from_file is None and len(args) == 0):
		exit('Unexpected argument number.')
	
	if options.from_file is None:
		labels = args
	else:
		labels = read_labels(options.from_file)
	
	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		
		idxs = []
		for label in labels:
			try:
				idxs.append(str(tokens.index(label)))
			except ValueError:
				idxs.append('')

		print '\t'.join(idxs)

if __name__ == '__main__':
	main()
