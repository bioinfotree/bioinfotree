#include <fcntl.h>
#include <stdexcept>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include "io.h"

using namespace std;

Sequence::Sequence(const char* filename)
{
	fh_.reset(new FileHandle(open(filename, O_RDONLY, 0)));
	if (*fh_ == -1)
		throw runtime_error("open() error");
	
	mm_.reset(new MemoryMap(*fh_, measure_size()));
}

const char* Sequence::content() const
{
	return reinterpret_cast<const char*>(mm_->addr());
}

const size_t Sequence::length() const
{
	return mm_->length();
}

off_t Sequence::measure_size() const
{
	struct stat stat_info;
	
	if (fstat(*fh_, &stat_info) == -1)
		throw runtime_error("fstat() error");
	
	return stat_info.st_size;
}

FileHandle::FileHandle(const int fd) : fd_(fd)
{
}

FileHandle::~FileHandle()
{
	if (fd_ != -1)
	{
		close(fd_);
		fd_ = -1;
	}
}

FileHandle::operator const int() const
{
	return fd_;
}

MemoryMap::MemoryMap(const FileHandle& fd, size_t length) : addr_(mmap(NULL, length, PROT_READ, MAP_FILE, fd, 0)), length_(length)
{
	if (addr_ == (void*)-1)
		throw runtime_error("mmap() failure");
}

MemoryMap::~MemoryMap()
{
	if (munmap(addr_, length_) == -1)
		throw runtime_error("munmap() failure");
}

const void* MemoryMap::addr() const
{
	return addr_;
}

size_t MemoryMap::length() const
{
	return length_;
}
