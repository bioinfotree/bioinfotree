#include <iostream>
#include "io.h"
#include "seed.h"

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		exit(1);
	}
	
	Sequence seq1(argv[1]);
	Sequence seq2(argv[2]);

	const size_t seed_size = 100;
	const size_t skip_size = 9900;	
	SeedIterator seed_iterator(seq1, seed_size);
	const char* seed_ptr;
	
	char seed[seed_size+1];
	seed[seed_size] = 0;
	
	while (seed_iterator.next())
	{
		memcpy(seed, seed_iterator.seed(), seed_size);
				
		const char* match;
		size_t search_offset = 0;
		while (true)
		{
			match = strstr(seq2.content() + search_offset, seed);
			if (match != NULL)
			{
				size_t match_offset = match - seq2.content();
				cout << seed_iterator.offset() << "\t" << match_offset << endl;
				search_offset = match_offset + seed_size;
			}
			else
				break;
		}
		
		seed_iterator.skip(skip_size);
	}
	
	return 0;
}
