#ifndef IO_H
#define IO_H

#include <memory>
#include <sys/types.h>

class FileHandle;
class MemoryMap;

class Sequence
{
public:
	Sequence(const char* filename);
	
	const char* content() const;
	const size_t length() const;

private:
	off_t measure_size() const;

	std::auto_ptr<FileHandle> fh_;
	std::auto_ptr<MemoryMap> mm_;
};

class FileHandle
{
public:
	FileHandle(const int fd);
	~FileHandle();
	operator const int() const;

private:
	FileHandle(const FileHandle& obj);
	FileHandle& operator=(const FileHandle& rhs);

	int fd_;
};

class MemoryMap
{
public:
	MemoryMap(const FileHandle& fd, size_t length);
	~MemoryMap();
	
	const void* addr() const;
	size_t length() const;
	
private:
	MemoryMap(const MemoryMap& obj);
	FileHandle& operator=(const MemoryMap& rhs);
	
	void* addr_;
	size_t length_;
};

#endif //IO_H
