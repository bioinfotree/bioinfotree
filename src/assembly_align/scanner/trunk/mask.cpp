#include "io.h"
#include "mask.h"

using namespace std;

SequenceMask::const_iterator::const_iterator(const SequenceMask::const_iterator& obj) :
	masks_(obj.masks_), bof_(obj.bof_), iterator_(obj.iterator_)
{
}

SequenceMask::const_iterator& SequenceMask::const_iterator::operator=(const SequenceMask::const_iterator& rhs)
{
	if (&rhs != this)
	{
		masks_ = rhs.masks_;
		bof_ = rhs.bof_;
		iterator_ = rhs.iterator_;
	}
	
	return *this;
}

bool SequenceMask::const_iterator::next()
{
	if (bof_)
		bof_ = false;
	else
		++iterator_;
	
	return iterator_ != masks_->end();
}

const Region& SequenceMask::const_iterator::mask() const
{
	return *iterator_;
}

SequenceMask::const_iterator::const_iterator(const std::list<Region>* masks, std::list<Region>::const_iterator iterator) :
	masks_(masks), bof_(true), iterator_(iterator)
{
}

SequenceMask::SequenceMask(const Sequence& sequence, const size_t collapse_threshold) :
	sequence_length_(sequence.length()), collapse_threshold_(collapse_threshold)
{
}

void SequenceMask::add(const Region& mask)
{
	list<Region>::iterator it = masks_.begin(), end = masks_.end();
	for (; it != end; ++it)
	{
		if (it->start > mask.start)
			break;
	}
	
	if (it != masks_.begin())
	{
		list<Region>::iterator last(it);
		--last;
		
		if (last->stop - mask.start < collapse_threshold_)
		{
			last->stop = mask.stop;
			return;
		}
	}
	
	masks_.insert(it, mask);
}

SequenceMask::const_iterator SequenceMask::iterator() const
{
	return const_iterator(&masks_, masks_.begin());
}
