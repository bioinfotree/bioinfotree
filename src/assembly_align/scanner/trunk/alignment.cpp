#include <string.h>
#include "alignment.h"
#include "io.h"

// DEBUG
#include <iostream>
using namespace std;

Alignment::Alignment(const Sequence& query, const Sequence& target) :
	query(query), query_start(0), query_stop(0),
	target(target), target_start(0), target_stop(0)
{
}

Alignment::Alignment(const Alignment& obj) :
	query(obj.query), query_start(obj.query_start), query_stop(obj.query_stop),
	target(obj.target), target_start(obj.target_start), target_stop(obj.target_stop)
{
}

ExtendableAlignment::ExtendableAlignment(const Alignment& alignment) :
	Alignment(alignment),
	query_lower_bound_(0), query_upper_bound_(0), target_lower_bound_(0), target_upper_bound_(0)
{
}

void ExtendableAlignment::set_query_lower_bound(const size_t offset)
{
	query_lower_bound_ = offset;
}

void ExtendableAlignment::set_query_upper_bound(const size_t offset)
{
	query_upper_bound_ = offset;
}

void ExtendableAlignment::set_target_lower_bound(const size_t offset)
{
	target_lower_bound_ = offset;
}

void ExtendableAlignment::set_target_upper_bound(const size_t offset)
{
	target_upper_bound_ = offset;
}

void ExtendableAlignment::extend()
{
	if (query_upper_bound_ == 0)
		query_upper_bound_ = query.length();
	if (target_upper_bound_ == 0)
		target_upper_bound_ = target.length();
	
	while (query_stop < query_upper_bound_ &&
	       target_stop < target_upper_bound_ &&
	       query.content()[query_stop] == target.content()[target_stop])
	{
		++query_stop;
		++target_stop;
	}
	
	while (query_start > query_lower_bound_ &&
	       target_start > target_lower_bound_)
	{
		--query_start;
		--target_start;
		if (query.content()[query_start] != target.content()[target_start])
		{
			++query_start;
			++target_start;
			break;
		}
	}
}

SeedAlignmentIterator::SeedAlignmentIterator(const Sequence& query, const size_t seed_offset, const size_t seed_size, const Sequence& target, const SequenceMask& target_mask) :
	query_(query), seed_offset_(seed_offset), seed_size_(seed_size), target_(target), target_mask_iterator_(target_mask.iterator()),
	search_pos_(target.length()), last_mask_stop_(0), end_of_mask_(false), alignment_(query, target)
{
	alignment_.query_start = seed_offset;
	alignment_.query_stop = seed_offset + seed_size;
	unmasked_target_region_.start = 0;
	unmasked_target_region_.stop = 0;
}

bool SeedAlignmentIterator::next()
{
	size_t match_offset;
	
	while (true)
	{
		while (search_pos_ > unmasked_target_region_.stop || unmasked_target_region_.stop - search_pos_ < seed_size_)
		{
			if (end_of_mask_)
				return false;
			
			find_next_unmasked_region();
			search_pos_ = unmasked_target_region_.start;
		}
		
		search_pos_ = match_seed(search_pos_, unmasked_target_region_.stop);
		if (search_pos_ != target_.length())
			break;
	}
	
	alignment_.target_start = search_pos_++;
	alignment_.target_stop = alignment_.target_start + seed_size_;
	
	return true;
}

const Alignment& SeedAlignmentIterator::alignment() const
{
	return alignment_;
}

const Region& SeedAlignmentIterator::unmasked_target_region() const
{
	return unmasked_target_region_;
}

void SeedAlignmentIterator::find_next_unmasked_region()
{	
	while (true)
	{
		unmasked_target_region_.start = last_mask_stop_;
	
		if (!target_mask_iterator_.next())
		{
			unmasked_target_region_.stop = target_.length();
			end_of_mask_ = true;
			break;
		}
		
		const bool back2back = target_mask_iterator_.mask().start == last_mask_stop_;
		last_mask_stop_ = target_mask_iterator_.mask().stop;
		if (!back2back)
		{
			unmasked_target_region_.stop = target_mask_iterator_.mask().start;
			break;
		}
	}
}

size_t SeedAlignmentIterator::match_seed(const size_t search_start, const size_t search_end) const
{
	const char* seed = query_.content() + seed_offset_;
	size_t match_size = 0;
	
	for (size_t i = search_start; i < search_end - seed_size_; ++i)
	{
		if (seed[match_size] == target_.content()[i])
		{
			++match_size;
			if (match_size == seed_size_)
				return i + 1 - seed_size_;
		}
		else
			match_size = 0;
	}
	
	return target_.length();
}
