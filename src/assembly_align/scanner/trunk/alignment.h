#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include <list>
#include <sys/types.h>
#include "mask.h"

class Alignment
{
public:
	Alignment(const Sequence& query, const Sequence& target);
	Alignment(const Alignment& obj);
	virtual ~Alignment() {}

	const Sequence& query;
	size_t query_start;
	size_t query_stop;
	
	const Sequence& target;
	size_t target_start;
	size_t target_stop;

private:
	const Alignment& operator=(const Alignment& rhs);
};

class ExtendableAlignment : public Alignment
{
public:
	ExtendableAlignment(const Alignment& alignment);
	
	void set_query_lower_bound(const size_t offset);
	void set_query_upper_bound(const size_t offset);
	void set_target_lower_bound(const size_t offset);
	void set_target_upper_bound(const size_t offset);
	void extend();

private:
	ExtendableAlignment(const ExtendableAlignment& obj);
	const ExtendableAlignment& operator=(const ExtendableAlignment& rhs);

	size_t query_lower_bound_;
	size_t query_upper_bound_;
	size_t target_lower_bound_;
	size_t target_upper_bound_;
};

class SeedAlignmentIterator
{
public:
	SeedAlignmentIterator(const Sequence& query, const size_t seed_offset, const size_t seed_size, const Sequence& target, const SequenceMask& target_mask);
	
	bool next();
	const Alignment& alignment() const;
	const Region& unmasked_target_region() const;

private:
	SeedAlignmentIterator(const SeedAlignmentIterator& obj);
	SeedAlignmentIterator& operator=(const SeedAlignmentIterator& rhs);
	void find_next_unmasked_region();
	size_t match_seed(const size_t search_start, const size_t search_end) const;
	
	const Sequence& query_;
	const size_t seed_offset_;
	const size_t seed_size_;
	const Sequence& target_;
	SequenceMask::const_iterator target_mask_iterator_;
	size_t search_pos_;
	size_t last_mask_stop_;
	bool end_of_mask_;
	Alignment alignment_;
	Region unmasked_target_region_;
};

#endif //ALIGNMENT_H
