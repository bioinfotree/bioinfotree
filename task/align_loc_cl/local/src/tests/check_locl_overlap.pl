#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $usage = "zcat qualocosa.loc_cl.gz | $0 -c";

my $option_count = undef;
GetOptions(
	'count|c' => \$option_count
) or die $usage;


my @list = ();
my $locl_id = undef;
my $count = 0;
my $count_all = 0;
while (<>){
	chomp;
	if ($_ =~ /^>/){
		if (defined $locl_id) {
			&overlap(\@list,$locl_id);
			@list = ();
		}
		$locl_id = $';
		next;
	}
	
	my @F = split /\t/;
	my @G = ($F[1],$F[2],$F[5],$F[6]);
	push @list,\@G;
	$count_all++;
}

&overlap(\@list,$locl_id) if (defined $locl_id);
my $overlap_perc = 2 * $count / $count_all * 100;
print "$count\t$count_all\t$overlap_perc\n" if defined $option_count;
#print "number of overlaps between hsps: $count, total: $count_all, percent: $overlap_perc" if defined $option_count;

sub overlap
{
	my $block_ref = shift;
	my $block_id = shift;

	my $block_len = scalar @{$block_ref};
	for (my $i=0; $i<$block_len - 1; $i++) {
		my @hsp1 = @{@{$block_ref}[$i]};
		for (my $j=$i + 1; $j<$block_len; $j++){
			my @hsp2 = @{@{$block_ref}[$j]};
			next if ( ($hsp1[1] < $hsp2[0]) or ($hsp2[1] < $hsp1[0]) );
			next if ( ($hsp1[3] < $hsp2[2]) or ($hsp2[3] < $hsp1[2]) );
			die "overlap between hsp$i and hsp$j in locl $block_id" if !defined $option_count;
			$count ++;
		}
	}
}
