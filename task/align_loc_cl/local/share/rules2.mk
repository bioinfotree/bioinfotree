include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

ALIGN_DIR            ?= $(BIOINFO_ROOT)/task/alignments/inhouse/dataset/mouse_mouse_ensembl40_wublast
SPECIES1             ?= mmusculus
SPECIES2             ?= mmusculus
CHRS1                ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 X Y
CHRS2                ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 X Y
ALIGN_SCORE_MIN      ?= 0
LOC_CL_MIN_ALIGN_NUM ?= 1
LOC_CL_MAX_GAP       ?= 22000

define list_pairs
	if [ $(SPECIES1) == $(SPECIES2) ]; then \
		chrs=( $(CHRS1) ); \
		for ((i=0; i<$${#chrs[*]}; i=$$i+1)); do \
			for ((j=i; j<$${#chrs[*]}; j=$$j+1)); do \
				echo "chr$${chrs[$$i]}_chr$${chrs[$$j]}"; \
			done \
		done \
	else \
		for chr1 in $(CHRS1); do \
			for chr2 in $(CHRS2); do \
				echo "chr$${chr1}_chr$${chr2}"; \
			done \
		done \
	fi
endef

BIN_DIR       := $(BIOINFO_ROOT)/task/align_loc_cl/local/bin
ALL_CHR_PAIRS := $(shell $(list_pairs))
ALL_LOC_CL    := $(addsuffix .loc_cl.gz,$(ALL_CHR_PAIRS))

all: $(ALL_LOC_CL)
clean:
	rm -f $(addsuffix .fusion,$(ALL_LOC_CL))
clean.full: clean
	rm -f $(ALL_LOC_CL)

ALL_LOC_CL_ACCODA:=$(addsuffix .accoda,$(ALL_LOC_CL))
.PHONY: all.accoda $(ALL_LOC_CL_ACCODA)
all.accoda: $(addsuffix .accoda,$(ALL_LOC_CL))
%.loc_cl.gz.accoda:
	accoda --name $(patsubst .%,,$<) -- make $<

ALL_LOC_CL_CHECK_COORDS:=$(addsuffix .check.coords,$(ALL_LOC_CL))
.PHONY: check.coords
check.coords: $(ALL_LOC_CL_CHECK_COORDS)
%.loc_cl.gz.check.coords: %.loc_cl.gz
	@set -e; \
	zcat $< \
	| $(BIN_DIR)/check_locl_coords

ALL_LOC_CL_CHECK_OVERLAP:=$(addsuffix .check.overlap,$(ALL_LOC_CL))
.PHONY: check.overlap
check.overlap: $(ALL_LOC_CL_CHECK_OVERLAP)
%.loc_cl.gz.check.overlap: %.loc_cl.gz
	@set -e; \
	zcat $< \
	| $(BIN_DIR)/check_locl_overlap $(LOC_CL_MAX_GAP)

.SECONDEXPANSION:
%.loc_cl.gz.fusion: $$(or $$(wildcard $(ALIGN_DIR)/$(SPECIES1)_$$(word 1,$$(subst _, ,$$(word 1,$$(subst ., ,$$@))))_$(SPECIES2)_$$(word 2,$$(subst _, ,$$(word 1,$$(subst ., ,$$@)))).*.pdb.gz),alignments_missing)
	set -e; \
	zcat $< \
	| awk '$$9>=$(ALIGN_SCORE_MIN)' \
	| cut -f 2,3,6,7 \
	| $(BIN_DIR)/locl_fusion -g $(LOC_CL_MAX_GAP) >$@

%.loc_cl.gz: %.loc_cl.gz.fusion $$(or $$(wildcard $(ALIGN_DIR)/$(SPECIES1)_$$(word 1,$$(subst _, ,$$(word 1,$$(subst ., ,$$@))))_$(SPECIES2)_$$(word 2,$$(subst _, ,$$(word 1,$$(subst ., ,$$@)))).*.pdb.gz),alignments_missing)
	set -e; \
	zcat $(word 2,$^) \
	| awk '$$9>=$(ALIGN_SCORE_MIN)' \
	| $(BIN_DIR)/pdb2locl $< \
	| gzip >$@

