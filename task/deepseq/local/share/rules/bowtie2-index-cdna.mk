# Copyright 2014 Gabriele Sales <gbrsales@gmail.com>

CDNA_FA ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/74_nrm/cdna.fa

extern $(CDNA_FA)


cdna.fa: $(CDNA_FA)
	link_install $< $@

cdna: cdna.fa
	bowtie2-build -f $< $@
	touch $@


ALL   += cdna
CLEAN += $(wildcard cdna.*.bt2) cdna.fa
