# Copyright 2011,2014 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2012 Davide Risso <risso.davide@gmail.com>

GENOME_DIR ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/74_nrm/

param chromosomes as CHRS from $(GENOME_DIR)
extern $(foreach c,$(CHRS),$(GENOME_DIR)/chr$(c).fa) as CHRS_FA

ifdef NON_CHROMOSOMAL
extern $(GENOME_DIR)/nonchromosomal.fa as NON_CHR
CHRS_FA += $(NON_CHR)
endif

genome.fa: $(CHRS_FA)
	cat $^ >$@

genome: genome.fa
	bowtie2-build -f $< $@
	touch $@


ALL   += genome
CLEAN += $(wildcard genome.*.bt2) genome.fa
