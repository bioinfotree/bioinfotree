VERSION=$(shell basename `pwd`)

ENSEMBL_VERSION = 86
ENSEMBL_DIR=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/$(ENSEMBL_VERSION)
ENSEMBL_MMUSCULUS_DIR=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl/mmusculus/$(ENSEMBL_VERSION)

SETS_CONTAINING_ONLY_HUMAN_GENES = Achilles_fitness_decrease Achilles_fitness_increase BioCarta_2013 BioCarta_2015 BioCarta_2016 Cancer_Cell_Line_Encyclopedia Chromosome_Location Disease_Signatures_from_GEO_down_2014 Disease_Signatures_from_GEO_up_2014 ENCODE_Histone_Modifications_2013 ENCODE_TF_ChIP-seq_2014 ENCODE_and_ChEA_Consensus_TFs_from_ChIP-X Epigenomics_Roadmap_HM_ChIP-seq GO_Biological_Process_2013 GO_Biological_Process_2015 GO_Cellular_Component_2013 GO_Cellular_Component_2015 GO_Molecular_Function_2013 GTEx_Tissue_Sample_Gene_Expression_Profiles_up Genome_Browser_PWMs HMDB_Metabolites HomoloGene Human_Gene_Atlas Human_Phenotype_Ontology KEA_2013 KEGG_2013 KEGG_2015 KEGG_2016 Kinase_Perturbations_from_GEO_down Kinase_Perturbations_from_GEO_up LINCS_L1000_Kinase_Perturbations_down LINCS_L1000_Kinase_Perturbations_up LINCS_L1000_Ligand_Perturbations_down LINCS_L1000_Ligand_Perturbations_up MSigDB_Computational MSigDB_Oncogenic_Signatures NCI-60_Cancer_Cell_Lines NCI-Nature_2016 NURSA_Human_Endogenous_Complexome OMIM_Disease OMIM_Expanded Old_CMAP_down Old_CMAP_up Panther_2015 Panther_2016 Pfam_InterPro_Domains Phosphatase_Substrates_from_DEPOD Reactome_2013 Reactome_2015 Reactome_2016 TargetScan_microRNA Tissue_Protein_Expression_from_Human_Proteome_Map Tissue_Protein_Expression_from_ProteomicsDB Transcription_Factor_PPIs VirusMINT Virus_Perturbations_from_GEO_down Virus_Perturbations_from_GEO_up WikiPathways_2013 WikiPathways_2015 WikiPathways_2016 dbGaP
#zgrep -iL Trp53 ../../local/share/data/20161117/*  |  cut -d "/" -f 7 | transpose | tr "\t" " " |  sed 's/.txt.gz//g'
#removed Allen brain atlas
#removed HumanCyc_2015 Humancyc_2016 LINCS_L1000_Chem_Pert_down MCF7_Perturbations_from_GEO_down NCI-Nature_2015 becouse of ; present in set names

SETS_CONTAINING_ALSO_MOUSE_GENES = Aging_Perturbations_from_GEO_down Aging_Perturbations_from_GEO_up CORUM ChEA_2013 ChEA_2015 ChEA_2016 Disease_Perturbations_from_GEO_down Disease_Perturbations_from_GEO_up DrugMatrix Drug_Perturbations_from_GEO_2014 Drug_Perturbations_from_GEO_down Drug_Perturbations_from_GEO_up ENCODE_Histone_Modifications_2015 ENCODE_TF_ChIP-seq_2015 ESCAPE GeneSigDB Genes_Associated_with_NIH_Grants KEA_2015 Ligand_Perturbations_from_GEO_down Ligand_Perturbations_from_GEO_up MGI_Mammalian_Phenotype_2013 MGI_Mammalian_Phenotype_Level_3 MGI_Mammalian_Phenotype_Level_4 Microbe_Perturbations_from_GEO_down Microbe_Perturbations_from_GEO_up Mouse_Gene_Atlas PPI_Hub_Proteins SILAC_Phosphoproteomics Single_Gene_Perturbations_from_GEO_down Single_Gene_Perturbations_from_GEO_up TF-LOF_Expression_from_GEO TRANSFAC_and_JASPAR_PWMs
#zgrep -il Trp53 ../../local/share/data/20161117/* | cut -d "/" -f 7 | sed 's/.txt.gz//'

SETS_CONTAINING_ONLY_MOUSE_GENES = MGI_Mammalian_Phenotype_2013 MGI_Mammalian_Phenotype_Level_3 MGI_Mammalian_Phenotype_Level_4 Mouse_Gene_Atlas
#(for i in `zgrep -il Trp53 ../../local/share/data/20161117/* `; do zgrep -L TP53 $i; done;) |  cut -d "/" -f 7 | sed 's/.txt.gz//' 


extern $(ENSEMBL_DIR)/mmusculus.mart.homolog.one2one_through_gene_name.gz as HOMOLOG_MAP
extern $(ENSEMBL_DIR)/gene-name.map.gz as ENSG2NAME_HSAPIENS
extern $(ENSEMBL_MMUSCULUS_DIR)/gene-name.map.gz as ENSG2NAME_MMUSCULUS

ALL+=$(addsuffix .unweighted.tab.ensg_duplicate.mmusculus.gz, $(SETS_CONTAINING_ONLY_HUMAN_GENES))
ALL+=$(addsuffix .gmt.gz, $(SELECTED_SETS))
ALL+=$(addsuffix .mmusculus.gmt.gz, $(SETS_CONTAINING_ONLY_HUMAN_GENES))
ALL+=$(addsuffix .unweighted.tab.gz, $(SETS_CONTAINING_ONLY_MOUSE_GENES))

%.unweighted.gz: $(PRJ_ROOT)/local/share/data/$(VERSION)/%.txt.gz
	zcat $< | perl -lpe 's/\s+$$/\n/; s/,[01].\d+([\t\n])/\1/g' | cut -f 1,3- | gzip > $@

%.unweighted.tab.gz: %.unweighted.gz
	zcat $< | append_each_row -B -F '' '>' | tr "\t" "\n"| fasta2tab | gzip > $@

$(addsuffix .unweighted.tab.ensg_duplicate.gz, $(SETS_CONTAINING_ONLY_HUMAN_GENES)): %.unweighted.tab.ensg_duplicate.gz: %.unweighted.tab.gz $(ENSG2NAME_HSAPIENS)
	zcat $< | translate -c -a -d -v -e "NA" <(bawk '{print $$2,$$1}' $^2) 2 | gzip > $@

.META: *.ensg_duplicate.gz
	1	set_id			Transcriptional misregulation in cancer_Homo sapiens_hsa05202
	2	gene_name		TP53
	3	ensg			ENSG00000141510;ENSG00000534510		COULD BE ESMUS

$(addsuffix .unweighted.tab.ensg_duplicate.mmusculus.gz, $(SETS_CONTAINING_ONLY_HUMAN_GENES)): %.ensg_duplicate.mmusculus.gz: %.ensg_duplicate.gz $(HOMOLOG_MAP) $(ENSG2NAME_MMUSCULUS)
	zcat $< \
	| translate -a -k <(bawk '{print $$ensembl_gene,$$homolog_gene}' $^2) 3 \
	| translate -a <(zcat $^3) 4 \
	| gzip > $@

.META: *.ensg_duplicate.mmusculus.gz
	1	set_id			Transcriptional misregulation in cancer_Homo sapiens_hsa05202
	2	gene_hsapiens		TP53
	3	ensg			ENSG00000141510
	4	ensmusg			ENSMUSG00000059552
	5	gene_name_mmusculus	Trp53 

%.gmt.gz: %.unweighted.tab.gz
	zcat $< \
	| perl -pe 'die("Found invalid characdter (;)") if m/;/' \
	| collapsesets -g ";" 2 | tr ";" "\t" \
	| bawk '{$$1=$$1 "\tNA"; print}' | gzip > $@

%.mmusculus.gmt.gz: %.unweighted.tab.ensg_duplicate.mmusculus.gz
	bawk '{print $$set_id,$$gene_name_mmusculus}' $< \
	| perl -pe 's/;/_/g' \
	| collapsesets -g ";" 2 | tr ";" "\t" \
	| bawk '{$$1=$$1 "\tNA"; print}' | gzip > $@

dataset_containign_mouse_genes:
	(zgrep -il Trp53 ../../local/share/data/20161117/*)
	
$(addsuffix .mmusculus.set.gz, $(SETS_CONTAINING_ONLY_HUMAN_GENES)): %.mmusculus.set.gz: %.unweighted.tab.ensg_duplicate.mmusculus.gz
	bawk '{print $$set_id, $$gene_name_mmusculus}' $< | gzip > $@
$(addsuffix .mmusculus.set.gz, $(SETS_CONTAINING_ONLY_MOUSE_GENES)): %.mmusculus.set.gz: %.unweighted.tab.gz
	cp $< $@
ALL_SETS.mmusculus.gz: $(addsuffix .mmusculus.set.gz, $(SETS_CONTAINING_ONLY_MOUSE_GENES)) $(addsuffix .mmusculus.set.gz, $(SETS_CONTAINING_ONLY_HUMAN_GENES))
	(for i in $^; do\
	        zcat $$i | append_each_row -B $${i%%.mmusculus.set.gz};\
	done;) | bsort -k1,1 -S5% | gzip > $@

ALL_SETS.hsapiens.gz: $(addsuffix .unweighted.tab.gz, $(SETS_CONTAINING_ONLY_HUMAN_GENES))
	(for i in $^; do\
	        zcat $$i | append_each_row -B $${i%%.unweighted.tab.gz};\
	done;) | bsort -k1,1 -S5% | gzip > $@

