#!/usr/bin/perl

use warnings;
use strict;

my $usage = "
config.pl specis 
";



$ARGV[0] or die($usage);
my $specis=$ARGV[0];

-e $specis or mkdir $specis;

open INI,$specis.".ini" or die "Can't read $specis.ini\n";	

my %ini_data=();
my $section="makefile";
while (my $line=<INI>) {
	chomp $line;
	if ( (!$line) or ($line=~/^;/) ) {
		next;
	} elsif ($line=~/\[/) {
		$line=~s/\[//;
		$line=~s/\]//;
		$section=$line;
	} elsif ($line=~/=/){
		my ($var,$val)=split(/\s*=\s*/,$line);
		$ini_data{$section}{$var}=$val;
	}
	else {
		die "ERRORE nel file $specis.ini\n\n";
	}
}



#-e $specis."/makefile" or &sost_val_in_file($specis,"makefile");
&sost_val_in_file($specis,"makefile");

sub sost_val_in_file
{
	my $dir=shift(@_);
	my $filename=shift(@_);

	open IN,$filename or die "Can't open $filename\n\n";
	open OUT,">$dir/$filename" or die "Can't write $dir/$filename\n\n";
	my $sostituisci=1;
	while (my $line=<IN>) {
		chomp $line;
		if ($line=~/\s*#+\s*END_OF_CONFIG/) {
			$sostituisci=0;
		}
		if(!$sostituisci){
			print OUT $line,"\n";
			next;
		}
		if($line!~/./ or $line=~/^\s*#/){
			print OUT $line,"\n";
			next;
		}
		if ($line=~/\s*\??:?=\s*/) {
			chomp $line;
			my @tmp=split(/\s*\??:?=\s*/,$line);
			if(!defined($ini_data{$filename}{$tmp[0]})){
				die("ERROR: value not given in $specis.ini file: section [$filename], variable $tmp[0]\n");
			}else{
				print OUT "$tmp[0] := ".$ini_data{$filename}{$tmp[0]}."\n";
			}
		}else {
			print OUT $line."\n";
		}
	}
	close OUT;
}

