# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009 Alessandro Coppe <alexcoppe@gmail.com>

REFSEQ_HOST    ?= ftp.ncbi.nlm.nih.gov
REFSEQ_PATH    ?= /refseq/release/
REFSEQ_CLASS   ?= vertebrate_mammalian
REFSEQ_VERSION ?= 33

FASTA_SUFFIX   := .rna.fna.gz

define do_list
tr ";" "\n" <<<"user anonymous nothing; cd $(1); ls -l" \
| ftp -p -n $(REFSEQ_HOST)
endef

fasta.list:
	( \
		set -e; \
		echo -n "ALL_FASTA:="; \
		$(call do_list,$(REFSEQ_PATH)/$(REFSEQ_CLASS)) \
		| sed -r 's|.* +||' \
		| grep -F '$(FASTA_SUFFIX)' \
		| tr "\n" " " \
	) >$@

include fasta.list
CLEAN += fasta.list

INTERMEDIATE += check.version
check.version:
	@set -e; \
	found="$$($(call do_list,$(REFSEQ_PATH)/release-notes) | sed -r 's|.* +RefSeq-release([0-9]+).*|\1|;t;d')"; \
	if [[ $$found  != $(REFSEQ_VERSION) ]]; then \
		echo "*** Version mismatch: expected $(REFSEQ_VERSION), found $$found" >&2; \
		exit 1; \
	fi

ALL += cdna.fa.gz
cdna.fa.gz: $(ALL_FASTA)
	zcat $^ \
	| gzip >$@


INTERMEDIATE += $(ALL_FASTA)
$(ALL_FASTA): check.version
	wget 'ftp://$(REFSEQ_HOST)/$(REFSEQ_PATH)/$(REFSEQ_CLASS)/$@'
