BASE_URL := http://hgdownload.soe.ucsc.edu/goldenPath/$(VERSION)

genome.fa.gz.bwt: genome.fa.gz
	bwa index $<

genome.fa.gz:
	wget -c -O $@ $(BASE_URL)/bigZips/$(VERSION).fa.gz

genome.fa.gz.bwt: genome.fa.gz
	bwa index $<

genome.fa.gz.amb: genome.fa.gz.bwt
	echo done
genome.fa.gz.ann: genome.fa.gz.bwt
	echo done
genome.fa.gz.pac: genome.fa.gz.bwt
	echo done
genome.fa.gz.sa: genome.fa.gz.bwt
	echo done
