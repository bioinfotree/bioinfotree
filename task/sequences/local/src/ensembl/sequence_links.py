#!/usr/bin/env python
#
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from urllib import urlopen
from vfork.util import exit, format_usage, ignore_broken_pipe, safe_import

with safe_import('beautifulsoup'):
    from bs4 import BeautifulSoup


def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] URL SPECIES VERSION

        Downloads from URL the HTML table containing the links
        to the sequences of SPECIES.

        Checks they match the given VERSION.

        Prints a table with the following format:
         * type of sequence
         * URL
    '''))
    options, args = parser.parse_args()
    if len(args) != 3:
        exit('Unexpected argument number.')

    expected_version = 'release-%s' % args[2]

    content = retrieve_list(args[0])
    for info in parse_list(content, args[1]):
        if expected_version not in info[1]:
            exit('Cannot find the required version: ' + args[2])
        else:
            print '%s\t%s' % info

def retrieve_list(url):
    return urlopen(url).read()

def parse_list(content, species):
    soup = BeautifulSoup(content)
    tbl = find_species_table(soup)
    headers = gather_headers(tbl)

    for row in tbl.findAll('tr'):
        cols = row.findAll('td')
        if len(cols) == 0: continue

        for c in cols:
            label = ' '.join(c.findAll(text=True))
            if species in label: break
        else:
            continue

        info = zip_headers(headers, cols)
        for header, td in info[2:]:
            anchor = td.a
            if anchor is None: continue
            href = anchor['href']

            link_label = ''.join(td.findAll(text=True))
            if header is not None:
                link_label = header + ' ' + link_label
            yield link_label, href

        break

    else:
        exit('Cannot find the required species: ' + species)

def find_species_table(soup):
    for tbl in soup.findAll('table'):
        thead = tbl.find('thead', recursive=False)
        if thead is None:
            thead = tbl

        tr = thead.find('tr', recursive=False)
        if tr is None: continue

        for th in tr.findAll('th'):
            label = th.string
            if label is None: continue

            if label.lower() == 'species':
                return tbl

    exit('Cannot find the species table.')

def gather_headers(tbl):
    headers = []
    for th in tbl.findAll('th'):
        headers.append(''.join(th.findAll(text=True)))
    return headers

def zip_headers(headers, tds):
    if len(headers) != len(tds):
        headers = [None] * len(tds)
    return zip(headers, tds)


if __name__ == '__main__':
    ignore_broken_pipe(main)
