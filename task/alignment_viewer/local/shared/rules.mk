BIN_DIR:=$(BIOINFO_ROOT)/task/alignment_viewer/local/bin
SCHEMA_DIR:=$(BIOINFO_ROOT)/task/alignment_viewer/local/src/database
ANNOTATION_DIR?=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl40/mmusculus
SINTENY_DIR?=$(BIOINFO_ROOT)/task/sinteny/dataset/mouse_wublast40/
SPECIES?=mmusculus
ENS_VERSION?=40
DATABASE_HOST=localhost
DATABASE_USER=molineri
DATABASE_PREFIX:=cgv_

.PHONY: emptydb.myisam emptydb.innodb switch.production regions segments repeats masks features alignments diagonals spgaps

DATABASE_NAME=$(DATABASE_PREFIX)$(SPECIES)_ensembl$(ENS_VERSION)

chrs.combo: $(SINTENY_DIR)/makefile
	set -o pipefail; \
	set -e; \
	( \
		echo -n 'ALL_CHRS:='; \
		grep '^ALL_CHR *[:=]' $(SINTENY_DIR)/makefile \
		| sed 's|^ALL_CHR *[:=]*||'; \
		echo -n 'ALL_CHR_PAIRS:='; \
		grep '^ALL_CHR_COUPLE *[:=]' $(SINTENY_DIR)/chromosomes_couple.mk \
		| sed 's|^ALL_CHR_COUPLE *[:=]*||'; \
	) >$@
				
include chrs.combo
ALL_ANNOTATION_PREFIXES=$(addprefix $(ANNOTATION_DIR)/chr,$(ALL_CHRS))
ALL_SINTENY_PREFIXES=$(addprefix $(SINTENY_DIR)/chr,$(ALL_CHRS))
ALL_SEGMENTS_PREFIXES=$(addprefix $(SINTENY_DIR)/,$(ALL_CHR_PAIRS))

all : regions segments
emptydb.myisam emptydb.innodb:
	target=$@; \
	engine=$${target#emptydb.}; \
	mysql -p -u $(USER) -h $(DATABASE_HOST) $(DATABASE_NAME) <$(SCHEMA_DIR)/schema-$$engine.sql

switch.production:
	name=$$(hostname); \
	if [ $$(awk -F. '{print NF}' <<<$$name) -ne 3 ]; then \
		name=$$(hostname -f); \
	fi; \
	if [ $$name != $(DATABASE_HOST) ]; then \
		echo "This operation must be performed on $(DATABASE_HOST)." >&2; \
		exit 1; \
	fi; \
	set -o pipefail; \
	set -e; \
	sed 's|DATABASE_NAME|$(DATABASE_NAME)|g;s|PRODUCTION_DATABASE|$(PRODUCTION_DATABASE)|g' $(SCHEMA_DIR)/switch_production.sql \
	| mysql -u $$USER -p $(PRODUCTION_DATABASE)

regions: repeats masks features
segments: alignments diagonals spgaps

repeats: $(addsuffix .repeats,$(ALL_ANNOTATION_PREFIXES))
	set -o pipefail; \
	set -e; \
	for i in $^; do \
		chr_name=$$(basename $$i); \
		chr_name=$${chr_name%%.repeats}; \
		awk '{print $$1 "\t" $$2 "\trepeats\t" $$3}' <$$i \
		| $(BIN_DIR)/load_regions -u $$USER -H $(DATABASE_HOST) -d $(DATABASE_NAME) $(SPECIES) $${chr_name##chr}; \
	done

masks: $(addsuffix .masks,$(ALL_ANNOTATION_PREFIXES))
	set -o pipefail; \
	set -e; \
	for i in $^; do \
		chr_name=$$(basename $$i); \
		chr_name=$${chr_name%%.masks}; \
		awk '{print $$1 "\t" $$2 "\tmasks\t" $$3}' <$$i \
		| $(BIN_DIR)/load_regions -u $$USER -H $(DATABASE_HOST) -d $(DATABASE_NAME) $(SPECIES) $${chr_name##chr}; \
	done

features: $(addprefix $(ANNOTATION_DIR)/coords_nonredundant.,$(ALL_CHRS))
	set -o pipefail; \
	set -e; \
	for i in $^; do\
		chr_name=$$(basename $$i); \
		chr_name=$${chr_name##coords_nonredundant.}; \
		awk '{print $$2 "\t" $$3 "\tfeatures\t" $$4}' <$$i \
		| $(BIN_DIR)/load_regions -u $$USER -H $(DATABASE_HOST) -d $(DATABASE_NAME) $(SPECIES) $$chr_name; \
	done

alignments: $(addsuffix .loc_cl,$(ALL_SEGMENTS_PREFIXES))
	set -e; \
	for i in $^; do \
		params="$$(echo $$(basename $$i) | tr '_.' ' ' | sed 's|chr||g' \
		| awk '{print "$(SPECIES) " $$1 " $(SPECIES) " $$2}')"; \
		grep -v '>' $$i \
		| cut -f -4 \
		| awk '{print $$0 "\talignments\tblast" }' \
		| $(BIN_DIR)/load_segments -u $$USER -H $(DATABASE_HOST) -d $(DATABASE_NAME) $$params; \
	done

diagonals: $(addsuffix .loc_cl.diag,$(ALL_SEGMENTS_PREFIXES))
	set -e; \
	for i in $^; do\
		params="$$(echo $$(basename $$i) \
		| tr '_.' ' ' \
		| sed 's|chr||g' \
		| awk '{print "$(SPECIES) " $$1 " $(SPECIES) " $$2}')"; \
		grep -v '>' $$i \
		| tr "," "\t" \
		| grep . \
		| awk '{print $$1 "\t" $$(NF-2) "\t" $$3 "\t" $$NF "\tdiagonals\tblast"}' \
		| $(BIN_DIR)/load_segments -u $$USER -H $(DATABASE_HOST) -d $(DATABASE_NAME) $$params; \
	done

spgaps: $(addsuffix .loc_cl.diag.spgaps,$(ALL_SEGMENTS_PREFIXES))
	set -e; \
	for i in $^; do\
		params="$$(echo $$(basename $$i) \
		| tr '_.' ' ' \
		| sed 's|chr||g' \
		| awk '{print "$(SPECIES) " $$1 " $(SPECIES) " $$2}')"; \
		grep -v '>' $$i \
		| cut -f 3 \
		| tr "," "\t" \
		| grep . \
		| awk '{print $$0 "\tspgaps\tblast"}' \
		| $(BIN_DIR)/load_segments -u $$USER -H $(DATABASE_HOST) -d $(DATABASE_NAME) $$params; \
	done

