#!/usr/bin/perl

use strict;
use warnings;

$\="\n";
$,="\t";

my $usage = "$0 < qualcosa.commty";

my $conn_comp_pre = undef;
my $community_pre = undef;
my @commty = ();
while (<>) {
	chomp;
	if (m/^>(\d+)$/) {
		print ">$community_pre"."_".$conn_comp_pre, @commty if (scalar @commty);
		$community_pre = $1;
		$conn_comp_pre = undef;
		next;
	}
	next if m/^#/;

	my @F = split /\t/;

	if (!defined $conn_comp_pre) {
		$conn_comp_pre = $F[1];
		@commty = ($F[0]);
		next;
	}

	if ($F[1] == $conn_comp_pre) {
		push @commty, $F[0];
	} else {
		print ">$community_pre"."_".$conn_comp_pre, @commty;
		@commty = ($F[0]);
		$conn_comp_pre = $F[1];
	}
}

print ">$community_pre"."_".$conn_comp_pre, @commty if (scalar @commty);
