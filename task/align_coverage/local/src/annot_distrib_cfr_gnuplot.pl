#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$\="\n";

my $usage="$0 \$@ -l label_index -r random -t triggered -c whole_chr";

my $label_idx = undef;
my $random = undef;
my $triggered = undef;
my $whole_chr = undef;

GetOptions(
	'label|l=s'      => \$label_idx,
	'random|r=s'     => \$random,
	'triggered|t=s'  => \$triggered,
	'whole_chr|c=s'  => \$whole_chr
) or die $usage;

die $usage if (!defined $label_idx or !defined $random or !defined $triggered or !defined $whole_chr);


my $random_tmp = $random.".tmp";
my $triggered_tmp = $triggered.".tmp";
my $whole_chr_tmp = $whole_chr.".tmp";


`get_fasta $label_idx $random | grep -v '>' > $random_tmp`;


my $tr_sum = `cut -f $label_idx $triggered`;
chomp $tr_sum;
my $tr_n_lines = `cat $triggered | wc -l`;
chomp $tr_n_lines;
$tr_sum /= $tr_n_lines;
`echo -e "$tr_sum\t0.1" > $triggered_tmp`;


my $chr_length = `cut -f 3 $whole_chr`;
my $col = $label_idx + 3;
my $chr_sum = `cut -f $col $whole_chr`;
$chr_sum /= $chr_length;
`echo -e "$chr_sum\t0.1" > $whole_chr_tmp`;


my $style = "w steps";

print "set terminal postscript enhanced color \"Helvetica\" 10;";
print "set grid mytics ytics xtics;";
print "set logscale y;";
print "set yrange [0.001:1.1];";
print "plot \\
'$random_tmp' w histep title 'random',          \\
'$triggered_tmp' w p pt 6 ps 1 title 'triggered',  \\
'$whole_chr_tmp' w p pt 6 ps 1 title 'whole chr'";
