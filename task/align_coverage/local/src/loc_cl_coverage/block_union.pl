#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;


#$,="\t";
#$\="\n";


my $usage = "$0 [-hc 2] -b blocks_number input_file
	foreach block, calculates the union and then prints out the number of regions obtained\n";


my $header_cols = 0;
my $blocks_number = undef;

GetOptions(
	'blocks_number|b=i' => \$blocks_number,
	'header_cols|hc=i'  => \$header_cols
);

$blocks_number or die ($usage);

my $input_file = shift @ARGV;
(-s $input_file) or die $usage;


my $n_lines = `wc -l $input_file`;
($n_lines) = ($n_lines =~ /^\s*(\d+)\s+\w+/);
die ("ERROR: $0: \$n_lines $n_lines isn't an integer multiple of \$blocks_number $blocks_number") if ($n_lines % $blocks_number > 0);
my $block_l = $n_lines / $blocks_number;

open FILE, $input_file or die ("Can't open file $input_file!");


for (my $k=0; $k<$blocks_number; $k++) {

	my $tmp = "tmp";
	open TMP,"> tmp" or die "Can't write on file $tmp\n";

	for (my $j = 0; $j < $block_l; $j++) {
		my $line = <FILE>;
		print TMP $line;
	}

	my $n = `cat $tmp | sort -k2,2n | union | wc -l`;

	print $n;

	close TMP;
}
