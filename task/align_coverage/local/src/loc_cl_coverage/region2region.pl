#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use constant CHR_SX        => 0;
use constant ALIGN_SX_B    => 1;
use constant ALIGN_SX_E    => 2;
use constant ALIGN_STRAND  => 3;
use constant CHR_DX        => 4;
use constant ALIGN_DX_B    => 5;
use constant ALIGN_DX_E    => 6;
use constant ALIGN_SX_GAPS => 10;
use constant ALIGN_DX_GAPS => 11;


$,="\t";
$\="\n";
$| = 1;


my $usage = "$0 -f loc_cl_file -r_sx chr_b_e -r_dx chr_b_e";
my $loc_cl_file = undef;
my $region_sx_file = undef;
my $region_dx_file = undef;

GetOptions (
	'r_sx_file|r_sx=s' => \$region_sx_file,
	'r_dx_file|r_dx=s' => \$region_dx_file,
	'loc_cl_file|f=s'  => \$loc_cl_file
) or die ($usage);

die $usage if (!defined $region_sx_file or !defined $region_dx_file or !defined $loc_cl_file);

open LOC_CL_FILE, $loc_cl_file or die ("ERROR: $0: Can't open file ($loc_cl_file)");
open REGION_SX_FILE, $region_sx_file or die ("ERROR: $0: Can't open file ($region_sx_file)");
open REGION_DX_FILE, $region_dx_file or die ("ERROR: $0: Can't open file ($region_dx_file)");

################################################
#
#	carico in memoria gli header del loc_cl
#

my @loc_cl_header = ();
#my $is_left  = $side eq 'sx' ? 1 : 0;
#my $is_right = $side eq 'dx' ? 1 : 0;
#if ($chr_1 eq $chr_2) {
#	$is_left = $is_right = 1;
#}

while (my $line = <LOC_CL_FILE>) {
	next if (!($line =~ /^>/));

	my ($start1,$stop1,$start2,$stop2) = ($line =~ /^>(\d+)_(\d+)_(\d+)_(\d+)\s*$/);
	my $offset = tell LOC_CL_FILE;
	die $offset if ( (!$start1) || (!$stop1) );
	die $offset if ( (!$start2) || (!$stop2) );

	my @temp = ($start1,$stop1,$start2,$stop2,$offset);
	push @loc_cl_header, \@temp;
}

@loc_cl_header = sort { ${$a}[0] <=> ${$b}[0] } @loc_cl_header;
my $loc_cl_header_idx = 0;
my $loc_cl_header_idx_max = scalar(@loc_cl_header);

################################################
#
#	carico in memoria le regioni 
#

my @region_sx=();
my $region_sx_idx_max=undef;
while(<REGION_SX_FILE>){
	chomp;
	my @F = split;
	push @region_sx, \@F;
}
$region_sx_idx_max=scalar(@region_sx);
@region_sx = sort { ${$a}[0] <=> ${$b}[0] } @region_sx;

my @region_dx=();
my $region_dx_idx_max=undef;
while(<REGION_DX_FILE>){
	chomp;
	my @F = split;
	push @region_dx, \@F;
}
$region_dx_idx_max=scalar(@region_dx);
@region_dx = sort { ${$a}[0] <=> ${$b}[0] } @region_dx;

################################################
#
#	associo a ciascuna regione i loc_cl che si sovrappongono
#

my @loc_cl_in_region_sx=();
my @loc_cl_in_region_dx=();

for(my $j=0; $j<$region_sx_idx_max; $j++){
	my ($chr, $r_b, $r_e) = @{$region_sx[$j]};
	for(my $i=0; $i<$loc_cl_header_idx_max; $i++){
		my ($b1,$e1,$b2,$e2,$offset) = @{$loc_cl_header[$i]};
		next if $e1 <= $r_b;
		next if $b1 >= $r_e; # sono ordinate SOLO sugli starts
				# se le regioni sono disgiunte, posso mettere last
#		last if $b1 >= $r_e; #posso mettere last perche sono ordinate
		push @{$loc_cl_in_region_sx[$j]}, $i;
	}
}
for(my $j=0; $j<$region_dx_idx_max; $j++){
	my ($chr, $r_b, $r_e) = @{$region_dx[$j]};
	for(my $i=0; $i<$loc_cl_header_idx_max; $i++){
		my ($b1,$e1,$b2,$e2,$offset) = @{$loc_cl_header[$i]};
		next if $e2 <= $r_b;
		next if $b2 >= $r_e;
		push @{$loc_cl_in_region_dx[$j]}, $i;
	}
}

################################################
#
#	per ogni coppia di regioni prendo gli allineamenti in comune
#

for(my $i=0; $i<$region_sx_idx_max; $i++){
	next if (!defined $loc_cl_in_region_sx[$i]);
	my @loc_cl_sx=@{$loc_cl_in_region_sx[$i]};
	for(my $j=0; $j<$region_dx_idx_max; $j++){
		next if (!defined $loc_cl_in_region_dx[$j]);
		my @loc_cl_dx=@{$loc_cl_in_region_dx[$j]};
		my %union;
		my %isect;
		for(@loc_cl_sx, @loc_cl_dx){ $union{$_}++ && $isect{$_}++ }
		my @common_loc_cl = keys %isect;
		&compute_weight($i,$j,\@common_loc_cl) if scalar(@common_loc_cl);
	}
}

sub compute_weight
{
	my $reg_sx_idx=shift;
	my $reg_dx_idx=shift;
	my $common_loc_cl_ref=shift;

	#####################################
	#
	#	recupero allineamenti dai location clusters
	#
	for(@{$common_loc_cl_ref}){
		my $offset=${$loc_cl_header[$_]}[4];
		seek LOC_CL_FILE,$offset,0;
		my $align = <LOC_CL_FILE>;
		while ( (defined $align) and ($align !~ /^>/) ) {
			chomp $align;
			my @F = split /\t/, $align;
			my @reg_sx = @{$region_sx[$reg_sx_idx]};
			my @reg_dx = @{$region_dx[$reg_dx_idx]};
			
			$align = <LOC_CL_FILE>;
			
			next if $reg_sx[2] <= $F[ALIGN_SX_B]; #reg_sx_e <= alignment_sx_b
			next if $reg_sx[1] >= $F[ALIGN_SX_E]; #reg_sx_b >= alignment_sx_e
			next if $reg_dx[2] <= $F[ALIGN_DX_B]; #reg_dx_e <= alignment_dx_b
			next if $reg_dx[1] >= $F[ALIGN_DX_E]; #reg_dx_b >= alignment_dx_e
		
			# coordinate assolute dell'intersezione tra regione e allineamento
			my $int_sx_b = $F[ALIGN_SX_B] < $reg_sx[1] ? $reg_sx[1] : $F[ALIGN_SX_B]; 
			my $int_sx_e = $F[ALIGN_SX_E] < $reg_sx[2] ? $F[ALIGN_SX_E] : $reg_sx[2]; 
			my $int_dx_b = $F[ALIGN_DX_B] < $reg_dx[1] ? $reg_dx[1] : $F[ALIGN_DX_B]; 
			my $int_dx_e = $F[ALIGN_DX_E] < $reg_dx[2] ? $F[ALIGN_DX_E] : $reg_dx[2]; 

			#print $F[CHR_SX],$int_sx_b,$int_sx_e,$F[CHR_DX],$int_dx_b,$int_dx_e;

			my $int_reg_abs_ref = &crop_align($int_sx_b, $int_sx_e, $int_dx_b, $int_dx_e,\@F);

			my @out = @{$int_reg_abs_ref};
			unshift @out,$F[CHR_SX];
			splice @out,3,0,$F[ALIGN_STRAND],$F[CHR_DX];
			my @gaps_start_sx = ();
			my @gaps_start_dx = ();
			my @gaps_length_sx = ();
			my @gaps_length_dx = ();
			@gaps_start_sx  = ($F[ALIGN_SX_GAPS] =~ /(\d+),\d+;*/g) if defined $F[ALIGN_SX_GAPS];
			@gaps_length_sx = ($F[ALIGN_SX_GAPS] =~ /\d+,(\d+);*/g) if defined $F[ALIGN_SX_GAPS];
			@gaps_start_dx  = ($F[ALIGN_DX_GAPS] =~ /(\d+),\d+;*/g) if defined $F[ALIGN_DX_GAPS];
			@gaps_length_dx = ($F[ALIGN_DX_GAPS] =~ /\d+,(\d+);*/g) if defined $F[ALIGN_DX_GAPS];
			@gaps_start_sx = map {$_-= ($int_sx_b - $F[ALIGN_SX_B])} @gaps_start_sx;
			@gaps_start_dx = map {$_-= ($int_dx_b - $F[ALIGN_DX_B])} @gaps_start_dx;

			for (my $i=$#gaps_start_sx; $i>=0; $i--){
				if($gaps_start_sx[$i]<0){
					splice @gaps_start_sx,$i,1;
					splice @gaps_length_sx,$i,1;
				}
			}
			
			for (my $i=$#gaps_start_dx; $i>=0; $i--){
				if($gaps_start_dx[$i]<0){
					splice @gaps_start_dx,$i,1;
					splice @gaps_length_dx,$i,1;
				}
			}

			for (my $i=0; $i<scalar(@gaps_start_sx); $i++){
				$gaps_start_sx[$i].=','.$gaps_length_sx[$i];
			}
			for (my $i=0; $i<scalar(@gaps_start_dx); $i++){
				$gaps_start_dx[$i].=','.$gaps_length_dx[$i];
			}

			push @out,('','','',join(';',@gaps_start_sx));
			push @out, join(';',@gaps_start_dx);

			print @F;
			print @out;

			return;
		
		}
	}

	
}

sub crop_align
{
	my $int1_sx_b_abs = shift;
	my $int1_sx_e_abs = shift;
	my $int2_dx_b_abs = shift;
	my $int2_dx_e_abs = shift;
	my $align_ref    = shift;

	my @F=@{$align_ref};


	########################################
	#
	#	carico i gap
	#
	my @gaps_start_sx=();
	my @gaps_length_sx = ();
	if($F[ALIGN_SX_GAPS]){

		@gaps_start_sx  = ($F[ALIGN_SX_GAPS] =~ /(\d+),\d+;*/g);
		@gaps_length_sx = ($F[ALIGN_SX_GAPS] =~ /\d+,(\d+);*/g);

		my $tmp = &correct_gaps(\@gaps_start_sx,\@gaps_length_sx);
		@gaps_start_sx = @{$tmp};#ora i gap sono in coordinate genomiche (con origine in align_start)

		if($F[ALIGN_STRAND] eq '-'){
			@gaps_length_sx = reverse @gaps_start_sx;
			@gaps_start_sx  = reverse map{$_ = $F[ALIGN_SX_E] - $_ } @gaps_start_sx;
		}
	}
	
	my @gaps_start_dx=();
	my @gaps_length_dx=();
	if($F[ALIGN_DX_GAPS]){

		@gaps_start_dx  = ($F[ALIGN_DX_GAPS] =~ /(\d+),\d+;*/g);
		@gaps_length_dx = ($F[ALIGN_DX_GAPS] =~ /\d+,(\d+);*/g);

		my $tmp  = &correct_gaps(\@gaps_start_dx,\@gaps_length_dx);
		@gaps_start_dx = @{$tmp};
	}	


		
	########################################
	#
	#	a partire dal cromosoma sx trovo l'intersezione corrispondente sul cromosoma dx
	#
	# coordinate di allineamento sul cromosoma sx
	my $int1_sx_b_align;
	my $int1_sx_e_align;
	my $int_length_align;
	my $align_length = $F[ALIGN_SX_E] - $F[ALIGN_SX_B];

	if (scalar(@gaps_start_sx)) {
		$int1_sx_b_align = 
			&genomic2align (\@gaps_start_sx,\@gaps_length_sx, $F[ALIGN_SX_B], $int1_sx_b_abs); 
		$int1_sx_e_align =
			&genomic2align (\@gaps_start_sx,\@gaps_length_sx, $F[ALIGN_SX_B], $int1_sx_e_abs); 
		$align_length = 
			&genomic2align (\@gaps_start_sx,\@gaps_length_sx, $F[ALIGN_SX_B], $F[ALIGN_SX_E]) - $F[ALIGN_SX_B]; 
	} else {
		$int1_sx_b_align = $int1_sx_b_abs; 
		$int1_sx_e_align = $int1_sx_e_abs;
	}
	my $int_align_length = $int1_sx_e_align - $int1_sx_b_align;

	# coordinate di allineamento sul cromosoma dx
#	print STDERR $int1_sx_b_align;
	my $dist1_align = $int1_sx_b_align - $F[ALIGN_SX_B];
#	print STDERR $dist1_align;
	#my $dist1_align = &genomic2align (\@gaps_start_sx,\@gaps_length_sx, $F[ALIGN_SX_B], $dist1_align);
	my $int1_dx_b_align = $F[ALIGN_DX_B] + $dist1_align;
	my $int1_dx_e_align  = $int1_dx_b_align + $int_align_length;

	# coordinate assolute sul cromosoma dx
	my $int1_dx_b_abs;
	my $int1_dx_e_abs;

	if (@gaps_start_dx) {
		$int1_dx_b_abs = 
			&align2genomic (\@gaps_start_dx, \@gaps_length_dx, $F[ALIGN_DX_B], $int1_dx_b_align); 
		$int1_dx_e_abs =
			&align2genomic (\@gaps_start_dx, \@gaps_length_dx, $F[ALIGN_DX_B], $int1_dx_e_align); 
	} else {
		$int1_dx_b_abs = $int1_dx_b_align;
		$int1_dx_e_abs = $int1_dx_e_align;
	}	

		
	########################################
	#
	#	a partire dal cromosoma dx trovo l'intersezione corrispondente sul cromosoma sx
	#
	# coordinate di allineamento sul cromosoma dx
	my $int2_dx_b_align;
	my $int2_dx_e_align;
	my $int2_align_length;
#	my $align2_length = $F[ALIGN_DX_E] - $F[ALIGN_DX_B];

	if (scalar(@gaps_start_dx)) {
		$int2_dx_b_align = 
			&genomic2align (\@gaps_start_dx,\@gaps_length_dx, $F[ALIGN_DX_B], $int2_dx_b_abs); 
		$int2_dx_e_align =
			&genomic2align (\@gaps_start_dx,\@gaps_length_dx, $F[ALIGN_DX_B], $int2_dx_e_abs); 
#		$align2_length = 
#			&genomic2align (\@gaps_start_dx,\@gaps_length_dx, $F[ALIGN_DX_B], $F[ALIGN_DX_E]) - $F[ALIGN_DX_B]; 
	} else {
		$int2_dx_b_align = $int2_dx_b_abs; 
		$int2_dx_e_align = $int2_dx_e_abs;
	}
	$int2_align_length = $int2_dx_e_align - $int2_dx_b_align;


	# coordinate di allineamento sul cromosoma sx
	my $dist2_align = $int2_dx_b_align - $F[ALIGN_DX_B];
	#my $dist2_align = &genomic2align (\@gaps_start_dx,\@gaps_length_dx, $F[ALIGN_DX_B], $dist2_abs);
	my $int2_sx_b_align = $F[ALIGN_SX_B] + $dist2_align;
	my $int2_sx_e_align = $int2_sx_b_align + $int2_align_length;

	# coordinate assolute sul cromosoma sx
	my $int2_sx_b_abs;
	my $int2_sx_e_abs;

	if (@gaps_start_sx) {
		$int2_sx_b_abs = 
			&align2genomic (\@gaps_start_sx, \@gaps_length_sx, $F[ALIGN_SX_B], $int2_sx_b_align); 
		$int2_sx_e_abs =
			&align2genomic (\@gaps_start_sx, \@gaps_length_sx, $F[ALIGN_SX_B], $int2_sx_e_align); 
	} else {
		$int2_sx_b_abs = $int2_sx_b_align;
		$int2_sx_e_abs = $int2_sx_e_align;
	}	

		
	########################################
	#
	#	su entrambi i cromosomi prendo l'intersezione delle sequenze coperte da allineamenti
	#
	my $int_reg_sx_b_abs = $int1_sx_b_abs < $int2_sx_b_abs ? $int2_sx_b_abs : $int1_sx_b_abs; 
	my $int_reg_sx_e_abs = $int1_sx_e_abs < $int2_sx_e_abs ? $int1_sx_e_abs : $int2_sx_e_abs; 
	my $int_reg_dx_b_abs = $int1_dx_b_abs < $int2_dx_b_abs ? $int2_dx_b_abs : $int1_dx_b_abs; 
	my $int_reg_dx_e_abs = $int1_dx_e_abs < $int2_dx_e_abs ? $int1_dx_e_abs : $int2_dx_e_abs;

	my @out = ($int_reg_sx_b_abs,$int_reg_sx_e_abs,$int_reg_dx_b_abs,$int_reg_dx_e_abs);

	return \@out;

}



sub correct_gaps
{
	my @s=@{$_[0]};
	my @l=@{$_[1]};
	my $max_i=scalar(@s);
	for(my $i=0; $i<$max_i; $i++){
		my $offset=0;
		for(my $j=0; $j<$i; $j++){
			$offset+=$l[$j];	
		}
		$s[$i]-=$offset;
	}
	return \@s;
}



sub genomic2align
{
	my $gaps_start_ref = shift @_;
	my $gaps_length_ref = shift @_;
	my $align_start = shift @_;
	my $x = shift @_;

	my @gaps_start = @{$gaps_start_ref};
	my @gaps_length = @{$gaps_length_ref};
	my $l = scalar @gaps_start;

	my $offset = 0;
	for (my $i=0; (($i < $l) and ($align_start + $gaps_start[$i] < $x)) ; $i++) {
		$offset += $gaps_length[$i];
	}
	$x += $offset;
	
	return $x;
}



sub align2genomic
{
	my $gaps_start_ref = shift @_;
	my $gaps_length_ref = shift @_;
	my $align_start = shift @_;
	my $x = shift @_;

	my @gaps_start = @{$gaps_start_ref};
	my @gaps_length = @{$gaps_length_ref};
	my $l = scalar @gaps_start;

#	print @gaps_start;
#	print @gaps_length;

	if ($x < $align_start + $gaps_start[0]) {
		return $x;
	}

	my $c = $align_start + $gaps_start[0] + $gaps_length[0];
	my $offset = $gaps_length[0];
	for (my $i=1; ($i<$l) and ($c + ($gaps_start[$i] - $gaps_start[$i - 1]) + $gaps_length[$i] < $x) ; $i++) {
		$c += ($gaps_start[$i] - $gaps_start[$i - 1]) + $gaps_length[$i];
		$offset += $gaps_length[$i];
	}

	$offset -= ($c - $x) if ($c > $x);
	$x -= $offset;

	return $x;

}
