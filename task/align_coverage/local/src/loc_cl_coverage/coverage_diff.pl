#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my $usage = "$0 covfile1 covfile2";

my $file_cov_1=shift(@ARGV);
my $file_cov_2=shift(@ARGV);

die($usage) if !defined($file_cov_1);

open(FH1,$file_cov_1) or die("ERROR: $0: can't open file ($file_cov_1)");
open(FH2,$file_cov_2) or die("ERROR: $0: can't open file ($file_cov_2)");

my ($x1,$y1)=split(/\s+/,<FH1>);
my ($x2,$y2)=split(/\s+/,<FH2>);
my $y1_pre=0;
my $y2_pre=0;
my $x=0;
my $y=0;

my $end_1=0;
my $end_2=0;

while(!$end_1 or !$end_2){
	if($x1<$x2 and !$end_1){
		$x=$x1;
		$y=$y1-$y2_pre;
	}elsif($x2<$x1 and !$end_2){
		$x=$x2;
		$y=$y1_pre-$y2;
	}else{
		$x=$x1;
		$y=$y1-$y2;
		
	}
	print $x,$y;
	
	if($x1 < $x2 and !$end_1){
		$_=<FH1>;
		if(!defined($_)){
			$end_1=1;		
		}else{
			$y1_pre=$y1;
			($x1,$y1)=split /\s+/;
		}
	}elsif($x1 > $x2 and !$end_2){
		$_=<FH2>;
		if(!defined($_)){
			$end_2=1;		
		}else{
			$y2_pre=$y2;
			($x2,$y2)=split /\s+/;
		}
	}else{#uguaglainza
		$_=<FH1>;
		if(!defined($_)){
			$end_1=1;		
		}else{
			$y1_pre=$y1;
			($x1,$y1)=split /\s+/;
		}
		$_=<FH2>;
		if(!defined($_)){
			$end_2=1;		
		}else{
			$y2_pre=$y2;
			($x2,$y2)=split /\s+/;
		}
	}
}


