#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$\="\n";

die "Correggere in modo che lavori sui files cons_intersection.summary.summary";


my $usage="$0 annote_vettorini_....
	il file deve essere gia' stato trasposto";

my $input_file = shift @ARGV;
die ("ERROR: no input file") if !defined $input_file;

print "set terminal postscript enhanced color \"Helvetica\" 8
set multiplot
set size 0.5,0.45
set origin 0,0.5
set grid mytics ytics xtics
set key spacing 1 # in legend
set xlabel \"cromosomi\"
set ylabel \"\" 1.8,0";

my $point_style="w p pt 7 ps 0.5";
print "plot \\
\"$input_file\" using 1:2 title \"C\" $point_style,		\\
\"$input_file\" using 1:3 title \"E\" $point_style,		\\
\"$input_file\" using 1:4 title \"5\" $point_style,		\\
\"$input_file\" using 1:5 title \"3\" $point_style,		\\
\"$input_file\" using 1:6 title \"I\" $point_style,		\\
\"$input_file\" using 1:7 title \"U\" $point_style,		\\
\"$input_file\" using 1:8 title \"N\" $point_style;";
