#!/usr/bin/perl

use strict;
use warnings;

$\="\n";
$,="\t";

my $usage = "$0 < simm.score";

my %best = ();
while (<>) {
	# 5504:19,15057906,15058124       8928:20,37778206,37778424       1       +       1.01376146788991        1.01376146788991
	chomp;
	my @F = split /\t/;

	if (!defined $best{"$F[1]\t$F[0]"}) {
		my @tmp = (\@F);

