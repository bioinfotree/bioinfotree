#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$\="\n";

my $usage="$0 \$@ -l label_index -chr 'all_chr'";

my $label_idx = undef;
my $chr_list = undef;

GetOptions(
	'label|l=s'      => \$label_idx,
	'chr_list|chr=s' => \$chr_list
) or die $usage;

die $usage if (!defined $label_idx or !defined $chr_list);

my $input_file = shift @ARGV;
die ("ERROR: no input file") if !defined $input_file;


my @chrs = split /\s+/,$chr_list;
my @blocks = ();

#my $style = "w linespoints pt 7 ps 0.6 lw 0.5";
my $style = "w steps";

print "set terminal postscript enhanced color \"Helvetica\" 10;";
print "set grid mytics ytics xtics;";
print "set title 'Annotations distribution';";
for (my $j=0; $j<scalar @chrs; $j++) {
	push @blocks,"'".$input_file.'.tmp.'.$label_idx."'".' index '.$j.' title '."'".$chrs[$j]."' ".$style;
}
print 'plot ',join ", \\\n",@blocks;
	
exit;
