#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use IO::Handle;


my $label = undef;
my $input_file = undef;

my $usage="$0 -l C -in annote_vettorini_\$(ANNOT_LABEL).summary.summary.gnuplot \$(ALL_ANNOT_RATIO_CUTOFF)
	-l annotation_label\n";

GetOptions (
        'label|l=s'  => \$label,
        'input|in=s' => \$input_file
) or die ($usage);


my @cutoffs = @ARGV;
die ("ERROR: no input file") if !@cutoffs;



print "set terminal postscript enhanced color \"Helvetica\" 10
set grid mytics ytics xtics
set key spacing 1 # in legend
set xlabel \"cromosomi\"
set ylabel \"\" 1.8,0
#set label \"$label\" at 0.06,30 font \"Helvetica,12\"\n";

my $point_style="w p pt 7 ps 0.8";
my $command =  "plot \\\n";
my @tmp = ();
for (my $i=0; $i<scalar (@cutoffs); $i++) {
	my $col = $i + 2;
	push @tmp, "'$input_file' using 1:$col title \"$cutoffs[$i]\" $point_style";
}
$command .= join ", \\\n", @tmp;
$command .= "\n";

print $command;
