#!/usr/bin/perl

use strict;
use warnings;

my $usage = "$0 10_1_50.twin_peaks\n";

my $twin_peaks_file = shift @ARGV;
die $usage if (!defined $twin_peaks_file);

my $regions = `cat $twin_peaks_file | cut -f 4,5 | sort -n | uniq`;
chomp $regions;

my @lines = split /\n/,$regions;

foreach my $line (@lines) {
	my ($start, $stop) = split /\t/,$line; 
	print $line."\t".`cat $twin_peaks_file | awk '\$4==$start, \$5==$stop {print \$6}' | sort | uniq | wc -l`; 
}

