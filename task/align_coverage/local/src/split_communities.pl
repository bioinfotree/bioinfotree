#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "$0 -s 'stringa inizio comunita' communities_file";

my $string = undef;
GetOptions (
	'string|s=s' => \$string
) or die ($usage);
die $usage if !defined $string;


while (my $line = <>) {
	next if (!($line =~ /^$string/));

	chomp $line;
	my @community = split /\t/,$line;
	print '>'.shift @community;

	foreach my $region (@community) {
		my ($chr,$b,$e) = ($region =~ /^\d+:(\w+),(\d+),(\d+)$/);
		print $chr,$b,$e;
	}
}
