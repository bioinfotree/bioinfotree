#!/usr/bin/env python
# encoding: utf-8
from __future__ import division

from optparse import OptionParser
from sys import exit, stdin
from vfork.io.util import safe_rstrip
import networkx as nx

from sys import stderr

def parse_peak(label, lineno):
	tokens = label.split(',')
	try:
		if len(tokens) != 3:
			raise ValueError
		return label, int(tokens[1]), int(tokens[2])
	
	except ValueError:
		exit('Invalid peak label at line %d.' % (lineno+1,))

def iter_peaks_and_chr(fd):
	last_peak = None
	last_chr = None
	last_group = None
	
	for lineno, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t')
		if len(tokens) != 14:
			exit('Invalid token number at line %d.' % (lineno+1,))
		
		if tokens[0] != last_peak or tokens[5] != last_chr:
			if last_group:
				last_group.sort()
				yield parse_peak(last_peak, lineno), last_group
			
			last_peak = tokens[0]
			last_chr = tokens[5]
			last_group = []
		
		try:
			last_group.append(tuple(int(tokens[c]) for c in (6,7,2,3)))
		except ValueError:
			exit('Invalid HSP coordinates at line %d.' % (lineno+1,))
	
	if last_group:
		last_group.sort()
		yield parse_peak(last_peak, lineno), last_group

def main():
	parser = OptionParser(usage='%prog <HSP_BY_PEAK')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	last_peak_label = None
	last_peak_width = None
	last_peak_uncov = None
	
	for (peak_label, peak_start, peak_stop), hsps in iter_peaks_and_chr(stdin):
		if peak_label != last_peak_label:
			if last_peak_uncov is not None:
				print '%s\t%f' % (last_peak_label, last_peak_uncov / last_peak_width)
			
			last_peak_label = peak_label
			last_peak_width = peak_stop - peak_start
			last_peak_uncov = 0

		if len(hsps) > 2500:
			print >>stderr, "[WARNING] %d HSPs in peak %s, skipping..." % (len(hsps), peak_label)
			continue

		g = nx.Graph()
		g.add_nodes_from(xrange(len(hsps)))
		
		width_limit = 2*last_peak_width
		for i in xrange(len(hsps)-1):
			for j in xrange(i+1, len(hsps)):
				dist = max(0, hsps[j][0] - hsps[i][1])
				if dist < width_limit:
					g.add_edge(i, j)
	
		clusters = nx.connected_component_subgraphs(g)
		for cluster in clusters:
			cluster_uncov = last_peak_width
			for node in cluster:
				hsp = hsps[node]
				cluster_uncov -= min(peak_stop, hsp[3]) - max(peak_start, hsp[2])

			last_peak_uncov += max(0, cluster_uncov)
	
	if last_peak_uncov is not None:
		print '%s\t%f' % (last_peak_label, last_peak_uncov / last_peak_width)

if __name__ == "__main__":
	main()

