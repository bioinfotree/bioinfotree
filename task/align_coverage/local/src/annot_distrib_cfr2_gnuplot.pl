#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$\="\n";

my $usage="$0 \$@ -l label_index -r1 random1 -t1 triggered1 -c1 whole_chr1 -r2 random2 -t2 triggered2 -c2 whole_chr2";

my $label_idx = undef;
my $random1 = undef;
my $triggered1 = undef;
my $whole_chr1 = undef;
my $random2 = undef;
my $triggered2 = undef;
my $whole_chr2 = undef;

GetOptions(
	'label|l=s'        => \$label_idx,
	'random1|r1=s'     => \$random1,
	'triggered1|t1=s'  => \$triggered1,
	'whole_chr1|c1=s'  => \$whole_chr1,
	'random2|r2=s'     => \$random2,
	'triggered2|t2=s'  => \$triggered2,
	'whole_chr2|c2=s'  => \$whole_chr2
) or die $usage;

die $usage if (!defined $label_idx);
die $usage if (!defined $random1 or !defined $triggered1 or !defined $whole_chr1);
die $usage if (!defined $random2 or !defined $triggered2 or !defined $whole_chr2);


my $random1_tmp = $random1.".tmp.$label_idx";
my $triggered1_tmp = $triggered1.".tmp.$label_idx.";
my $whole_chr1_tmp = $whole_chr1.".tmp.$label_idx";
my ($chr1) = ($random1 =~ /(\d+)_*/);

my $random2_tmp = $random2.".tmp.$label_idx";
my $triggered2_tmp = $triggered2.".tmp.$label_idx";
my $whole_chr2_tmp = $whole_chr2.".tmp.$label_idx";
my ($chr2) = ($random2 =~ /(\d+)_*/);


`get_fasta $label_idx $random1 | grep -v '>' > $random1_tmp`;
`get_fasta $label_idx $random2 | grep -v '>' > $random2_tmp`;


my $tr_sum1 = `cut -f $label_idx $triggered1`;
chomp $tr_sum1;
my $tr_n_lines1 = `cat $triggered1 | wc -l`;
chomp $tr_n_lines1;
$tr_sum1 /= $tr_n_lines1;
`echo -e "$tr_sum1\t0.1" > $triggered1_tmp`;

my $tr_sum2 = `cut -f $label_idx $triggered2`;
chomp $tr_sum2;
my $tr_n_lines2 = `cat $triggered2 | wc -l`;
chomp $tr_n_lines2;
$tr_sum2 /= $tr_n_lines2;
`echo -e "$tr_sum2\t0.1" > $triggered2_tmp`;


my $chr1_length = `cut -f 3 $whole_chr1`;
my $col = $label_idx + 3;
my $chr1_sum = `cut -f $col $whole_chr1`;
$chr1_sum /= $chr1_length;
`echo -e "$chr1_sum\t0.1" > $whole_chr1_tmp`;

my $chr2_length = `cut -f 3 $whole_chr2`;
my $chr2_sum = `cut -f $col $whole_chr2`;
$chr2_sum /= $chr2_length;
`echo -e "$chr2_sum\t0.1" > $whole_chr2_tmp`;


my $style = "w steps";

print "set terminal postscript enhanced color \"Helvetica\" 10;";
print "set grid mytics ytics xtics;";
#print "set logscale y;";
print "plot \\
'$random1_tmp' w histep title '$chr1 random',          \\
'$triggered1_tmp' w p pt 6 ps 1 title '$chr1 triggered',  \\
'$whole_chr1_tmp' w p pt 6 ps 1 title '$chr1 whole chr',  \\
'$random2_tmp' w histep title '$chr2 random',          \\
'$triggered2_tmp' w p pt 6 ps 1 title '$chr2 triggered',  \\
'$whole_chr2_tmp' w p pt 6 ps 1 title '$chr2 whole chr'";
