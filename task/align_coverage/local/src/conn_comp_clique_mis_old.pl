#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";


my $usage = " $0 qualcosa.conn_comp < qualcosa.score
	-id if ids in file qualcosa.conn_comp don't contain chr,b,e";

my $id_only = undef;
GetOptions (
	'id_only|id' => \$id_only
) or die ($usage);

my $conn_comp_file = shift @ARGV;
die $usage if !$conn_comp_file;

open FH,$conn_comp_file or die("Can't open file ($conn_comp_file)");


my $conn_comp_id=0;
my %conn_comp=();
my @all_conn_comp_id=();
my %n=();
while (my $line = <FH>) {
	chomp $line;
	my @F = split /\t/,$line;
	
	for(@F){
		$conn_comp{$_} = $conn_comp_id;
	}
	$n{$conn_comp_id} = scalar @F;
	push @all_conn_comp_id, $conn_comp_id;
	$conn_comp_id++;
}


my %edges=();
while(<>){
	chomp;
	my @F=split;
	if ($id_only) {
		$F[0] =~ s/:.*//g;
		$F[1] =~ s/:.*//g;
	}

	next if (!defined $conn_comp{$F[0]} or !defined $conn_comp{$F[1]});

	die ("ERROR: edge between nodes from different connected components!!") if ($conn_comp{$F[0]} != $conn_comp{$F[1]});

	$edges{$conn_comp{$F[0]}}=0 if (!defined $edges{$conn_comp{$F[0]}});
	$edges{$conn_comp{$F[0]}}++;
}

for(@all_conn_comp_id){
	my $n_clique = ($n{$_} / 2) * ($n{$_} - 1);
	my $ratio = $edges{$_} / $n_clique;
	print $_, $n{$_}, $edges{$_}, $n_clique, $ratio;
}
