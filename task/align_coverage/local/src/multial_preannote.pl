#!/usr/bin/perl
use warnings;
use strict;

$,="\t";
$\="\n";

my $usage = "$0 < qualcosa.multialign";


my $chr =undef;
my $b = undef;
my $e = undef;

$_ = <>;
($chr,$b,$e) = ($_=~/^#\d+_(\w+)_(\d+)_(\d+)\s+/);

die('MANCA GESTIONE SOTTOBLOCCHI');

while (my $seq_ref = &get_sequence) {
	my $seq = shift @{$seq_ref};

	my ($seq_b_gaps) = ($seq =~ /^(-+)\w+/);
	$b += length $seq_b_gaps if (defined $seq_b_gaps);
	
	$seq = $';
	my $e_offset = ($seq =~ tr/-//);
	
	if ($e < $b) {
		my $tmp = $e;
		$e = $b;
		$b = $tmp;
	}
	
	$e -= $e_offset;

	print $chr, $b, $e;

	$chr = shift @{$seq_ref};
	$b = shift @{$seq_ref};
	$e = shift @{$seq_ref};

	last if (!defined $chr or !defined $b or !defined $e);
}





sub get_sequence 
{
	my $sequence = '';
	my $new_chr = undef;
	my $new_b = undef;
	my $new_e = undef;

	my $line = <>;
	while ($line and $line !~ /^#/) {
		chomp $line;
		$sequence .= $line;
		$line = <>;
	}

	if (defined $line and ($line =~ /^#/) ) {
		($new_chr,$new_b,$new_e) = ($line=~/^#\d+_(\w+)_(\d+)_(\d+)\s+/);
	}
		
	my @out = ($sequence, $new_chr, $new_b, $new_e);
	return \@out;
}
