# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

import inhouse-prediction-common

define targetscan_scan 
	+targetscan <(get_fasta @$1:$2 <$< | fasta2tab -s | append_each_row -A 1) <(fasta2tab -s $(word 3,$^) | bawk '{print $$1,"1",$$2}') <<<"yes" /dev/stdout 2>/dev/null \
	| unhead \
	| gzip >$@
endef

ALL += targets.gz


targets.gz: $(ALL_PREDICTION)
	zcat $^ \
	| tr ':' '\t' \
	| select_columns 2 1 3 4 5 6 7 8 9 10 11 12 13 \
	| gzip >$@

.META: targets.gz
	1   miRNA_family_ID                            hsa-miR-199a-5p
	2   Gene_ID                                    ENST00000000233
	3   species_ID                                 1
	4   MSA_start                                  365
	5   MSA_end                                    371
	6   UTR_start                                  365
	7   UTR_end                                    371
	8   Group_num                                  1
	9   Site_type                                  1a
	10  miRNA in this species                      x
	11  Group_type                                 1a
	12  Species_in_this_group                      1
	13  Species_in_this_group_with_this_site_type  
