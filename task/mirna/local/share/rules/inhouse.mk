# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

ALGORITHM ?= miranda

phase sequences using inhouse-sequences
phase predictions using inhouse-$(ALGORITHM)
phase transcripts using inhouse-transcripts
