#!/usr/bin/perl
use strict;
use warnings;
$,="\t";
$\="\n";

my $usage="usage: $0 homology_sp1_sp2 macroregions_sp1 macroregios_sp2\n";

my $filename=shift @ARGV;
die($usage) if !defined($filename);
open HOM, $filename or die("Can't open filename ($filename)");

my $filename1=shift @ARGV;
die($usage) if !defined($filename1);
open SP1, $filename1 or die("Can't open filename ($filename1)");

my $filename2=shift @ARGV;
die($usage) if !defined($filename2);
open SP2, $filename2 or die("Can't open filename ($filename2)");

my @hom;
while(<HOM>){
	chomp;
	my @tmp = split;
	push @hom,\@tmp;
}

my %sp1;
while(<SP1>){
	chomp;
	my @F = split;
	my @ENS=split /;/,$F[4];
	for(@ENS){
		$sp1{$_}=$F[0];
	}
}

my %sp2;
while(<SP2>){
	chomp;
	my @F = split;
	my @ENS=split /;/,$F[4];
	for(@ENS){
		$sp2{$_}=$F[0];
	}
}

foreach(@hom){
	my ($ENS1,$ENS2)=@{$_};
	
	warn "WARNING: no macroregion with $ENS1 in $filename1" if !defined($sp1{$ENS1});
	warn "WARNING: no macroregion with $ENS2 in $filename2" if !defined($sp2{$ENS2});
	next if !defined($sp1{$ENS1}) or !defined($sp2{$ENS2});
	print $sp1{$ENS1},$sp2{$ENS2};
}
