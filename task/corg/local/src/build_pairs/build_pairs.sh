#!/bin/bash

BIN_DIR="$BIOINFO_ROOT/task/alignments/inhouse/local/bin"

if [ "$1" == "-h" -o "$1" == "--help" ]; then
	echo "usage: $0 SEQ_PAIR_LIST SEQ_DIR1 SEQ_DIR2 BLOCK_NUM OUTPUT_DIR" >&2
	exit 0
elif [ $# -ne 5 ]; then
	echo "Unexpected argument number." >&2
	exit 1
fi

set -o pipefail;
set -e;

seqpairlist="$1"
seqdir1="$2"
seqdir2="$3"
block_num="$4"
output_dir="$5"

cut -f 1-4 "$seqpairlist" | "$BIN_DIR"/seqlist2fasta "$seqdir1" >"$output_dir/left.fa"
cut -f 5-8 "$seqpairlist" | "$BIN_DIR"/seqlist2fasta "$seqdir2" >"$output_dir/right.fa"

cd "$output_dir"
(
	left_blocks=( $(split_fasta left.fa $block_num .) )
	right_blocks=( $(split_fasta right.fa $block_num .) )
	
	for ((i=0; i<${#left_blocks[*]}; i+=1)); do
		echo -e "${left_blocks[$i]}\t${right_blocks[$i]}"
	done
) > pairs
