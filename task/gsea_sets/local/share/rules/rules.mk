VERSION?=v5.1
ENSEMBL_VERSION = 86
SET_TYPES?=c1.all c2.all c2.cgp c2.cp.biocarta c2.cp.kegg c2.cp.reactome c2.cp c3.all c3.mir c3.tft c4.all c4.cgn c4.cm c5.all c5.bp c5.cc c5.mf c6.all c7.all h.all

ENSEMBL_DIR=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/$(ENSEMBL_VERSION)

extern $(ENSEMBL_DIR)/ensg2entrez.gz as GENE_ENTREZ_MAP
extern $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/$(ENSEMBL_VERSION)/gene-name.map.gz as HSA_GENE_SYMBOL_MAP
extern $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/mmusculus/$(ENSEMBL_VERSION)/gene-name.map.gz as MMU_GENE_SYMBOL_MAP
extern $(ENSEMBL_DIR)/mmusculus.mart.homolog.one2one_through_gene_name.gz as HOMOLOG_MAP



ALL+=$(addsuffix .$(VERSION).entrez.ensg.clean.mmusculus.symbols.gmt.gz, $(SET_TYPES))
ALL+=$(addsuffix .$(VERSION).symbols.gmt.gz, $(SET_TYPES))

#%.entrez.ensg.clean.mmusculus.symbols.gmt.gz: %.entrez.ensg.clean.mmusculus.gz $(GENE_SYMBOL_MAP)
%.entrez.ensg.clean.mmusculus.symbols.gmt.gz: %.entrez.ensg.clean.mmusculus.gz $(MMU_GENE_SYMBOL_MAP) %.entrez.gmt.gz
	zcat $< \
	| translate <(zcat $^2) 2\
	| bsort | uniq \                                     * esistono symbol associati a piu` di un ensmusg *
	| tab2fasta | fasta2oneline  -t \
	| translate -a <(zcat $^3 | cut -f -2) 1\
	| gzip > $@


$(addsuffix .$(VERSION).symbols.gmt.gz,$(SET_TYPES)): %.gmt.gz: $(PRJ_ROOT)/local/share/data/$(VERSION)/%.gmt.gz
	ln -s $< $@
$(addsuffix .$(VERSION).entrez.gmt.gz,$(SET_TYPES)): %.gmt.gz: $(PRJ_ROOT)/local/share/data/$(VERSION)/%.gmt.gz
	ln -s $< $@

%.fa.gz: %.gmt.gz
	zcat $< | append_each_row -B -F '' '>' | cut -f 1,3- | tr "\t" "\n" | uniq | gzip > $@

%.ensg.gz: %.fa.gz $(GENE_ENTREZ_MAP)
	zcat $< | translate -p '^>' -a -d -j -v -e "NA" <(bawk '$$entrez {print $$entrez, $$ensg}' $^2 | sort | uniq) 1 | gzip > $@

all.gmt.gz: msigdb.$(VERSION).entrez.gmt.gz
	ln -s $< $@

%.ensg.clean.gz: %.ensg.gz
	 zcat $< | fasta2tab | bawk '$$3!="NA" {print $$1,$$3}' | sort | uniq | gzip > $@

%.ensg.clean.mmusculus.gz: %.ensg.clean.gz $(HOMOLOG_MAP)
	zcat $< | translate -k <(bawk '{print $$ensembl_gene,$$homolog_gene}' $^2) 2 \
	| gzip > $@
	
.META: *.ensg.clean.gz *.ensg.clean.mmusculus.gz
	1	GO
	2	gene

%.entrez.ensg.clean.mmusculus.gmt.gz: %.entrez.ensg.clean.mmusculus.gz
	zcat $< | tab2fasta | fasta2oneline -t | bawk '{$$2="NA\t" $$2; print}' | gzip > $@


# added by simo:
%.entrez.ensg.clean.hsapiens.symbols.gmt.gz: %.entrez.ensg.clean.gz $(HSA_GENE_SYMBOL_MAP) %.entrez.gmt.gz
	zcat $< \
	| translate <(zcat $^2) 2\
	| bsort | uniq \                                     * Ip che esistano symbol associati a piu` di un ensg come per gli ensmusg *
	| tab2fasta | fasta2oneline -t \
	| translate -a <(zcat $^3 | cut -f -2) 1\
	| gzip > $@


