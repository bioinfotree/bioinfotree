#!/usr/bin/env python
#
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>


from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import stdin
from vfork.microarray.genepix import ATF
from vfork.util import exit, format_usage
import errno
import re


class Literal(object):
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return '<Literal: %s>' % repr(self.value)

    def instance(self, field_serials):
        return self.value


class Field(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Field: %s>' % repr(self.name)

    def instance(self, field_serials):
        return '$%d' % field_serials[self.name]


class Expr(object):
    col_rx = re.compile(r'\$([a-zA-Z][a-zA-Z0-9]*|\{[^\}]+\})')

    def __init__(self, expr, known_fields):
        self.parts = []
        while len(expr):
            m = self.col_rx.search(expr)
            if m is None:
                self._literal(expr)
                break

            start, end = m.span()

            label = m.group(1)
            extended = label[0] == '{'
            if extended: label = label[1:-1]

            if label not in known_fields:
                if extended:
                    raise ValueError('unknown field: ' + label)
                else:
                    self._literal(expr[:end])
            else:
                self._literal(expr[:start])
                self.parts.append(Field(label))
            
            expr = expr[end:]

    def __repr__(self):
        out  = ['<Expression with %d parts:' % len(self.parts)]
        out += [ ' ' + repr(p) for p in self.parts ]
        out.append('>')
        return '\n'.join(out)

    def referenced_fields(self):
        fs = set()
        for p in self.parts:
            if isinstance(p, Field):
                fs.add(p.name)
        return fs

    def instance_fields(self, field_serials):
        return ''.join(p.instance(field_serials) for p in self.parts)

    def _literal(self, s):
        if len(s):
            self.parts.append(Literal(s))


class Bawk(object):
    max_bufsize = 1000

    def __init__(self, expr):
        self.proc = Popen(['bawk', expr], stdin=PIPE)
        self.buf = []

    def feed(self, fields):
        self.buf.append('\t'.join(fields))
        if len(self.buf) >= self.max_bufsize:
            self._flush()

    def close(self):
        self._flush()
        self.proc.stdin.close()

        if self.proc.wait() != 0:
            exit('bawk exited with an error.')

    def _flush(self):
        try:
            self.proc.stdin.write('\n'.join(self.buf))
        except IOError, e:
            if e.errno != errno.EPIPE:
                raise

        self.buf = []


def print_info(atf):
    headers = atf.headers
    for h in sorted(headers.keys()):
        print '%s\t%s' % (h, headers[h])

def print_fields(atf):
    for c in atf.table.columns:
        print c

def process_expr(atf, expr):
    try:
        expr = Expr(expr, atf.table.columns)
    except ValueError, e:
        exit('Malformed expression: ' + str(e))

    rf = list(expr.referenced_fields())
    ei = expr.instance_fields(dict( (f,i) for i,f in enumerate(rf,1) ))
    tb = atf.table.ix[:,rf]

    bawk = Bawk(ei)
    for i in xrange(tb.shape[0]):
        bawk.feed(tuple(tb.ix[i]))
    bawk.close()


def main():
    parser = OptionParser(format_usage('''
      Usage: %prog [OPTIONS] CMD ... <ATF

      Process a GenePix Axon Text File.

      CMD determines how the data is processed:
       - INFO: prints the table of optional headers
       - FIELDS: prints the names of data fields
       - EXPR: evaluates an awk-like expression over the data records
    '''))

    options, args = parser.parse_args()
    if len(args) < 1:
        exit('Unexpected argument number.')

    try:
        atf = ATF(stdin)
    except ValueError, e:
        exit('Malformed ATF file: ' + str(e))

    cmd = args[0].upper()
    if cmd == 'INFO':
        print_info(atf)
    elif cmd == 'FIELDS':
        print_fields(atf)
    elif cmd == 'EXPR':
        if len(args) < 2:
            exit('Missing expression.')
        process_expr(atf, args[1])
    else:
        exit('Invalid command.')


if __name__ == '__main__':
    main()
