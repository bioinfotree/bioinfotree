#!/usr/bin/env python
#
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2011 Enrica Calura <enrica.calura@gmail.com>
# Copyright 2011 Paolo Martini <paolo.cavei@gmail.com>

from optparse import OptionParser
from sys import stdin
from vfork.util import exit, format_usage

base_url = 'ftp://ftp.ebi.ac.uk/'

def read_table(fd):
    rows = []
    for line in fd:
        line = line.rstrip()
        if len(line) == 0: continue
        rows.append(line.split('\t'))

    return rows[0], rows[1:]

def get_column_pair(header, label, rows):
    try:
        idx = header.index(label)
    except ValueError:
        idx = -1
    if idx == -1 or idx+1 >= len(header):
        return []

    ps = set()
    for row in rows:
        url = row[idx+1]
        if not url.startswith(base_url):
            exit('Malformed SDRF file.')
        ps.add((row[idx],url))

    return ps

def print_calls(ps):
    for url in set(p[1] for p in ps):
        cname = url[url.rindex('/')+1:]
        print "$(call archive,%s,%s)" % (cname, url)

    for fname, url in ps:
        cname = url[url.rindex('/')+1:]
        print "$(call experiment,%s,%s)" % (fname, cname)


def main():
    parser = OptionParser(usage=format_usage('''
      Usage: %prog <SDRF >RULES

      Reads an SDRF file and produces rules to extract
      the expression files.
    '''))
    options, args = parser.parse_args()
    if len(args) != 0:
        exit('Unexpected argument number.')

    header, rows = read_table(stdin)
    print_calls(get_column_pair(header, 'Array Data File', rows))
    print_calls(get_column_pair(header, 'Derived Array Data File', rows))
    print_calls(get_column_pair(header, 'Array Data Matrix File', rows))
    print_calls(get_column_pair(header, 'Derived Array Data Matrix File', rows))


if __name__ == '__main__':
    main()
