#!/usr/bin/env python
# coding: utf-8

from itertools import izip
from optparse import OptionParser
from os.path import join
from sys import exit
from vfork.fasta.reader import SingleBlockReader

def read_regions(filename):
	fd = file(filename, 'r')
	seq_ids = set()
	regions = []
	
	for line in fd:
		tokens = line.rstrip().split('\t')
		assert len(tokens) == 4
		
		seq_ids.add(tokens[0])
		start = int(tokens[2])
		stop = int(tokens[3])
		regions.append((tokens[0], tokens[1], start, stop))
	
	return seq_ids, regions

def simmetrize(regions, valid_ids):
	aux = []
	for idx, region in enumerate(regions):
		if region[0] not in valid_ids:
			aux.append(idx)
	
	aux.reverse()
	for idx in aux:
		del regions[idx]

def main():
	parser = OptionParser(usage='%prog OLD_REGIONS OLD_GENOME NEW_REGIONS NEW_GENOME')
	options, args = parser.parse_args()

	if len(args) != 4:
		exit('Unexpected argument number.')
	
	old_regions = read_regions(args[0])[1]
	seq_ids, new_regions = read_regions(args[2])
	simmetrize(old_regions, seq_ids)
	assert len(old_regions) == len(new_regions)
	old_regions.sort()
	new_regions.sort()
	
	last_old_chr = None 
	old_reader = None
	last_new_chr = None
	new_reader = None
	for (old_id, old_chr, old_start, old_stop), (new_id, new_chr, new_start, new_stop) in izip(old_regions, new_regions):
		if old_chr != last_old_chr:
			old_reader = SingleBlockReader(join(args[1], 'chr%s.fa' % old_chr))
		if new_chr != last_new_chr:
			new_reader = SingleBlockReader(join(args[3], 'chr%s.fa' % new_chr))
		
		old_seq = old_reader.get(old_start, old_stop - old_start)
		new_seq = new_reader.get(new_start, new_stop - new_start)
		if old_seq != new_seq:
			print 'Differing sequences:'
			print 'chr:   %s vs %s' % (old_chr, new_chr)
			print 'start: % 10d vs % 10d' % (old_start, new_start)
			print 'stop:  % 10d vs % 10d' % (old_stop, new_stop)
			exit(1)
	
	print '%d sequences matching.' % len(old_regions)

if __name__ == '__main__':
	main()
