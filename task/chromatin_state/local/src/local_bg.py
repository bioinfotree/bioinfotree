#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from random import randint
#from subprocess import Popen, PIPE
from collections import defaultdict
#from vfork.io.colreader import Reader

def intersect(region,masking):
	m=masking[region[0]]
	for i in m:
		if region[1] > i[1]:
			continue
		if region[2] < i[0]:
			continue
		return True
	return False

def main():
	usage = format_usage('''
		%prog masking  < input_regions
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-s', '--step', type=int, dest='windows_step', default=1000, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	parser.add_option('-w', '--window', type=int, dest='window_width', default=None, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	#parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	#for id, sample, raw, norm in Reader(stdin, '0u,1s,2i,3f', False):
	masking=defaultdict(list)
	with file(args[0], 'r') as fd:
		for line in fd:
			(chr,b,e) = safe_rstrip(line).split('\t')
			masking[chr].append((int(b),int(e)))
	
	for line in stdin:
		(chr,b,e,id) = safe_rstrip(line).split('\t')
		b=int(b)
		e=int(e)
		if randint(0,1) == 0:
			s=+1
		else:
			s=-1

		tentativo=0
		w_b=None
		w_e=None
		if options.window_width is None:
			W=w_e-w_b
		else:
			W=options.window_width
			
		while(tentativo==0 or intersect((chr,w_b,w_e),masking)==True):
			if s>0:
				w_b = e+1 + options.windows_step*tentativo
				w_e = w_b + W
			else:
				w_e = b-1 - options.windows_step*tentativo
				w_b = w_e - W
			tentativo+=1
		print "\t".join([str(i) for i in (chr,b,e,id,w_b,w_e,s)])

		

if __name__ == '__main__':
	main()

