PLOT_DIR = ~/bioinfotree/task/chromatin_state/local/ucsc/h19/

ALL_CELLS = Gm12878 H1hesc K562 Helas3 Hepg2 Huvec Nhek

.PHONY: download
download: $(addsuffix .bed.gz, $(ALL_CELLS))
	@echo -e '\033[1;32m All done.\033[0m'

$(addsuffix .bed.gz, $(ALL_CELLS)): %.bed.gz:
	wget -c -O $*.bed.gz http://hgdownload-test.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeOpenChromSynth/wgEncodeOpenChromSynth$*Pk.bed.gz

%.bed: %.bed.gz
	zcat $*.bed.gz | bawk '{print $$1,$$2,$$3,$$4,$$21}' | bsort -k1,1 -k2,2n | union \
	> $@

%.bed-valid:%.bed.gz
	zcat $*.bed.gz | bawk '$$21==1 {print $$1,$$2,$$3,$$4,$$21}' | bsort -k1,1 -k2,2n | union \
	> $@

%.bed-DNase:%.bed.gz
	zcat $*.bed.gz | bawk '$$21==2 {print $$1,$$2,$$3,$$4,$$21}' | bsort -k1,1 -k2,2n | union \
	> $@

%.bed-FAIRE:%.bed.gz
	zcat $*.bed.gz | bawk '$$21==3 {print $$1,$$2,$$3,$$4,$$21}' | bsort -k1,1 -k2,2n | union \
	> $@

%.bed-ChSeq:%.bed.gz
	zcat $*.bed.gz | bawk '$$21==4 {print $$1,$$2,$$3,$$4,$$21}' | bsort -k1,1 -k2,2n | union \
	> $@

%.bin_max50: %
	cat $< | awk '{print $$3 - $$2}' | awk '$$1<=50' | binner -n 25 \
	> $@

%.bin_5cent: %
	NR=$$(cat $< | wc -l) && \
	CUTOFF_ROW=$$(perl -le "print int($$NR - $$NR * 0.05)") && \
	CUTOFF=$$(cat $< | bawk '{print $$3-$$2}' | bsort -k1,1n | more | head -n $$CUTOFF_ROW | tail -1) && \
	cat $< | awk '{print $$3 - $$2}' | awk -v CUTOFF=$$CUTOFF '$$1<=CUTOFF' | binner -n 25 \
	> $@

%.bin_logsc: %
	cat $< | awk '{print $$3 - $$2}' | binner -n 50 -l \
	> $@

.PHONY: bins
bins: $(addsuffix .bed.bin_max50, $(ALL_CELLS)) $(addsuffix .bed.bin_5cent, $(ALL_CELLS)) $(addsuffix .bed.bin_logsc, $(ALL_CELLS))
	@echo -e '\033[1;32m Whole BIN files done.\033[0m'

.PHONY: valid
valid: $(addsuffix .bed-valid.bin_max50, $(ALL_CELLS)) $(addsuffix .bed-valid.bin_5cent, $(ALL_CELLS)) $(addsuffix .bed-valid.bin_logsc, $(ALL_CELLS))
	@echo -e '\033[1;32m Validated BIN files done.\033[0m'

.PHONY: dnase
dnase: $(addsuffix .bed-DNase.bin_max50, $(ALL_CELLS)) $(addsuffix .bed-DNase.bin_5cent, $(ALL_CELLS)) $(addsuffix .bed-DNase.bin_logsc, $(ALL_CELLS))
	@echo -e '\033[1;32m DNase BIN done.\033[0m'

.PHONY: faire
faire: $(addsuffix .bed-FAIRE.bin_max50, $(ALL_CELLS)) $(addsuffix .bed-FAIRE.bin_5cent, $(ALL_CELLS)) $(addsuffix .bed-FAIRE.bin_logsc, $(ALL_CELLS))
	@echo -e '\033[1;32m FAIRE BIN files done.\033[0m'

.PHONY: chseq
chseq: $(addsuffix .bed-ChSeq.bin_max50, $(ALL_CELLS)) $(addsuffix .bed-ChSeq.bin_5cent, $(ALL_CELLS)) $(addsuffix .bed-ChSeq.bin_logsc, $(ALL_CELLS))
	@echo -e '\033[1;32m ChipSeq BIN files done.\033[0m'

.PHONY: all
all: bins valid dnase faire chseq
	@echo -e '\033[1;32m All done.\033[0m'

.PHONY: plots
plots: plot_%
	@echo -e '\033[1;32m Plots done.\033[0m'

plot_%: all
	gnuplot $(PLOT_DIR)plot_whole
	gnuplot $(PLOT_DIR)plot_valid
	gnuplot $(PLOT_DIR)plot_DNase
	gnuplot $(PLOT_DIR)plot_FAIRE
	gnuplot $(PLOT_DIR)plot_ChSeq

.PHONY: clean
clean:
	rm *.bin*

