extern $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/hsapiens/hg19_131122/gencodeCompV17_genes.tss as TSS_MAP

PERCENTILE_CUTOFF_ON_LENGTH = 0.01
.PHONY: download
download:
	wget_dir -f narrowPeak ftp://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeOpenChromDnase/
	for i in wgEncodeOpenChromDnase*; do mv $$i `perl -pe 's|wgEncodeOpenChromDnase||; s|Pk.narrowPeak.gz|.narrowPeak.gz|'<<<$$i`; done

%.narrowPeak.gz:
	wget -c -O $*.narrowPeak.gz http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeOpenChromDnase/wgEncodeOpenChromDnase$*Pk.narrowPeak.gz

.META: *.narrowPeak.gz
	1	chrom - Name of the chromosome (or contig, scaffold, etc.).
	2	chromStart - The starting position of the feature in the chromosome or scaffold. The first base in a chromosome is numbered 0.
	3	chromEnd - The ending position of the feature in the chromosome or scaffold. The chromEnd base is not included in the display of the feature. For example, the first 100 bases of a chromosome are defined as chromStart=0, chromEnd=100, and span the bases numbered 0-99. If all scores were '0' when the data were submitted to the DCC, the DCC assigned scores 1-1000 based on signal value. Ideally the average signalValue per base spread is between 100-1000.
	4	name - Name given to a region (preferably unique). Use '.' if no name is assigned.
	5	score - Indicates how dark the peak will be displayed in the browser (0-1000).
	6	strand - +/- to denote strand or orientation (whenever applicable). Use '.' if no orientation is assigned.
	7	signalValue - Measurement of overall (usually, average) enrichment for the region.
	8	pValue - Measurement of statistical significance (-log10). Use -1 if no pValue is assigned.
	9	qValue - Measurement of statistical significance using false discovery rate (-log10). Use -1 if no qValue is assigned.

%.open.bed.gz: %.narrowPeak.gz
	zcat $< \
	| bawk '{print $$1,$$2,$$3,"open"}' | sed 's|^chr||' | enumerate_rows -r \
	| gzip > $@

.PHONY: ALL.open.bed.gz
ALL.open.bed.gz: $(shell ls *.narrowPeak.gz | sed 's/.narrowPeak.gz/.open.bed.gz/')
	@echo done

%.open-no_outlier.bed.gz: %.open.bed.gz
	bawk '{print $$1~5,$$3-$$2}' $< | bsort -k5,5n \
	| enumerate_rows -n \
	| bawk -v cutoff=$(PERCENTILE_CUTOFF_ON_LENGTH) '$$1 > cutoff && $$1<1-cutoff {print $$2~6}' \
	| gzip > $@

.PHONY: ALL.open-no_outlier.bed.gz
ALL.open-no_outlier.bed.gz: $(shell ls *.narrowPeak.gz | sed 's/.narrowPeak.gz/.open-no_outlier.bed.gz/')
	@echo done

#restituisce il bed diviso in prossimali e distali a seconda che overlappino o no con almeno un TSS
%.open-proximal-distal.bed.gz: %.open-no_outlier.bed.gz $(TSS_MAP)
	intersection -l >(bawk '{print $$1,$$2,$$3,"op_distal",$$5}' > $@.missing.tmp) <(zcat $< | bsort -k1,1 -k2,2n):1:2:3 <(bawk '{print $$2,$$3,$$3+1}' $^2 | bsort -k1,1 -k2,2n | uniq):1:2:3 \
	| bawk '{print $$1,$$4,$$5,"op_proximal",$$9}'> $@.tmp
	(zcat $<; cat $@.tmp $@.missing.tmp) | bsort -k1,1 -k2,2n -S 10% | uniq | gzip > $@
	rm $@.tmp $@.missing.tmp

.PHONY: ALL.open-no_outlier.bed.gz
ALL.open-proximal-distal.bed.gz: $(shell ls *.narrowPeak.gz | sed 's/.narrowPeak.gz/.open-proximal-distal.bed.gz/')
	@echo done
