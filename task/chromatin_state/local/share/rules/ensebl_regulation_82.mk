SPECIES?=homo_sapiens
TRAKS=annotated_feature external_feature mirna_target_feature motif_feature regulatory_feature segmentation_feature

ALL+=$(addsuffix .raw.gz,$(TRAKS))

AnnotatedFeatures.gff.gz:
	wget ftp://ftp.ensembl.org/pub/release-84/regulation/$(SPECIES)/AnnotatedFeatures.gff.gz


%.raw.gz:
	(echo -n "query="; cat ../../../../local/share/queries/hsapiens_$*_87.xml) >$@.tmp
	wget -q -O - --post-file $@.tmp http://$(MART_VERSION).ensembl.org/biomart/martservice | gzip > $@
	rm $@.tmp

.META: annotated_feature.raw.gz
	1	chromosome_name
	2	chromosome_start
	3	chromosome_end
	4	feature_type_name
	5	feature_type_class
	6	feature_type_description
	7	cell_type_name
	8	efo_id
	9	so_name
	10	archive_id
	11	so_accession
	12	project_name
	13	cell_type_description

.META: external_feature.raw.gz
	1	chromosome_name
	2	chromosome_start
	3	chromosome_end
	4	binding_matrix_id
	5	score
	6	feature_type_name
	7	chromosome_strand
	8	display_label
	9	so_accession
	10	so_name

.META: mirna_target_feature.raw.gz
	1	chromosome_name
	2	chromosome_start
	3	chromosome_end
	4	feature_type
	5	feature_type_class
	6	chromosome_strand
	7	feature_type_description
	8	accession
	9	display_label
	10	so_accession
	11	so_name
	12	evidence
	13	dbprimary_acc
	14	linkage_annotation
	15	db_display_name
	16	xref_display_label

.META: motif_feature.raw.gz
	1	chromosome_name
	2	chromosome_start
	3	chromosome_end
	4	binding_matrix_id
	5	score
	6	feature_type_name
	7	chromosome_strand
	8	display_label
	9	so_accession
	10	so_name

.META: regulatory_feature.raw.gz
	1	chromosome_name
	2	chromosome_start
	3	chromosome_end
	4	feature_type_name
	5	bound_seq_region_start
	6	bound_seq_region_end
	7	feature_type_description
	8	cell_type_name
	9	has_evidence
	10	so_accession
	11	so_name
	12	efo_id
	13	regulatory_stable_id
	14	cell_type_description

.META: segmentation_feature.raw.gz
	1	chromosome_name
	2	chromosome_start
	3	chromosome_end
	4	feature_type_name
	5	cell_type_name
	6	so_accession
	7	so_name
	8	efo_id
	9	feature_type_description

