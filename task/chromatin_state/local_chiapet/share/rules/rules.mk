INTERESTIONG_REGIONS = $(BIOINFO_ROOT)/prj/affinity_matrix/dataset/jaspar_axt_refseq_hg19/hsapiens/regions/interesting_regions
ERNST_PATH = $(BIOINFO_ROOT)/task/chromatin_state/dataset/Enrst2011


edges.gz: ChiaPet.net.gz
	zcat $< | sed 's/chr//g' | cut -f -6 | enumerate_rows -r | bawk '{print $$1~3,$$7,"+"; print $$4~7,"-"}'\
	| bsort -k1,1 -k2,2n -S30% \
	| gzip > $@

regions.%.gz: $(ERNST_PATH)/$(CELL_LINE).gz
	bawk '$$4~/$*/ {print $$1~4}' $< \
	| union -m -1 \
	| gzip > $@

edges-regions.%.isec.gz: regions.%.gz edges.gz
	intersection <(zcat $<) <(zcat $^2) \
	| gzip > $@    *siccome gli edges sono overlappanti alla fin fine associo a ir ogni edges che si sovrappone anche solo di una base*

edges-ir.isec.gz: $(INTERESTIONG_REGIONS) edges.gz
	intersection <(cut -f -4 $< | bsort -k2,2 -k3,3n):2:3:4 <(zcat $^2) \
	| gzip > $@    *siccome gli edges sono overlappanti alla fin fine associo a ir ogni edges che si sovrappone anche solo di una base*

.META: edges-tss.isec.gz
	1	tss_id
	2	edge_id

ir-regions.%.assoc.gz: edges-ir.isec.gz edges-regions.%.isec.gz
	zcat $< | translate -k -a -d -j -f 9 <(zcat $^2) 9 \
	| bawk '$$18!=$$19' \
	| gzip > $@

.META: ir-regions.*.assoc.gz
	1	chr_ir	1
	2	ir_edge_int_b	752943
	3	ir_edge_int_e	753426
	4	ir_b	751426
	5	ir_e	753426
	6	edge_b	752943
	7	edge_e	757282
	8	ir_id	NR#015368#1
	9	edge_id	2
	10	chr_region	1
	11	region_edge_int_b	761210
	12	region_edge_int_e	761537
	13	region_b		760337
	14	region_e	761537
	15	edge_b		761210
	16	edge_e		764625
	17	region_types	4_Strong_Enhancer
	18	edge_side_ir	+
	19	edge_side_region	-

########################
# STATISTICA
########################

ChiaPet.net.distanza.bin: ChiaPet.net.gz
	bawk '{d1=$$5-$$3; d2=$$6-$$2; if(d1>d2){print d1}else{print d2}}' $< | binner -n 100 > $@

ChiaPet.net.distanza_%.bin: ChiaPet.net.gz
	bawk '{print $$5-$$3}' $< | sed 's/^-//' | bawk '$$1<$*' | binner -n 100 > $@

ChiaPet.net.distanza.png: ChiaPet.net.distanza.bin ChiaPet.net.distanza_10000.bin
	gnuplot <<<"\
		set terminal png size 1042,768 font ',8';\
		set multiplot;\
		set xlabel \"bp\";\
		set ylabel \"numero di coppie di segmenti\";\
		set logscale y;\
		plot '$<' w l ti \"distribuzione delle distanze tra chiapet interagenti\";\
		set xtic rotate by -90;\
		unset logscale y;\
		set grid;\
		set size 0.6,0.5;\
		set origin 0.35,0.45;\
		plot '$^2' w l ti \"zoom in 0-10k\";\
	" > $@

%.ir_distanza.bin: ir-regions.%.assoc.gz
	bawk '{d1=$$ir_e-$$region_b; d2=$$region_e-$$ir_b; if(d1>d2){print d1}else{print d2}}' $< | binner -n 100 > $@

%.ir_distanza_50000.bin: ir-regions.%.assoc.gz
	bawk '{d1=$$ir_e-$$region_b; d2=$$region_e-$$ir_b; if(d1>d2){print d1}else{print d2}}' $< | bawk '$$1<50000' | binner -n 100 > $@

%.bin.norm: %.bin
	normalize_histo < $< >$@

%.cfr_distanza-enhancer_general.png: %.ir_distanza.bin.norm ChiaPet.net.distanza.bin.norm
	gnuplot <<<"\
		set terminal png size 1042,768 font ',8';\
		set xlabel \"bp\";\
		set ylabel \"frazione di coppie di segmenti\";\
		set logscale y;\
		plot '$<' w l ti \"enhancer\", '$^2' w l ti \"generale\";\
	" > $@

%.cfr_distanza_50000-enhancer_general.png: %.ir_distanza_50000.bin.norm ChiaPet.net.distanza_50000.bin.norm
	gnuplot <<<"\
		set terminal png size 1042,768 font ',8';\
		set xlabel \"bp\";\
		set ylabel \"frazione di coppie di segmenti\";\
		set logscale y;\
		plot '$<' w l ti \"enhancer\", '$^2' w l ti \"generale\";\
	" > $@
