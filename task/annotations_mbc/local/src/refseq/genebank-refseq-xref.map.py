#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from optparse import OptionParser
from sys import stdin, stderr
from vfork.util import exit, format_usage, safe_import
import re

with safe_import('BioPython (http://biopython.org)'):
	from Bio.GenBank import Iterator, RecordParser

def print_info(record):
	for feature in record.features:
		if feature.key != 'gene':
			continue
		
		for qualifier in feature.qualifiers:
			if qualifier.key != '/db_xref=':
				continue
			
			id = qualifier.value.replace('"', '')
			#if not id.startswith('GeneID:'):
			#	continue

			print '\t'.join([record.version, id])
	
def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <GENBANK >TRANSCRIPT-GENE

		Extract transcript to gene relations from RefSeq files.
	'''))
	parser.add_option('-s', '--species', dest='species', help='keep records for SPECIES only', metavar='SPECIES')
	options, args = parser.parse_args()
	if len(args) != 0:
		exit('Unexpected argument number.')

	if options.species is not None:
		options.species = options.species.lower()
	
	for record in Iterator(stdin, RecordParser()):
		if options.species is not None:
			if record.organism.lower() != options.species:
				continue

		print_info(record)

if __name__ == '__main__':
	main()
