#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from operator import itemgetter
from optparse import OptionParser
from sys import stdin
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage

def parse_coords(coords):
	ns = [ int(n) for n in coords.split(',') if len(n) ]
	for n in ns:
		if n < 0: raise ValueError
	return ns

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <TRANSCRIPT_INFO >TRANSCRIPT_STRUCTURE

		Extracts the transcript structure from the transcript.info
		dump file.
	'''))
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	reader = Reader(stdin, '0s,2s,5s,7u,8u,9s,10s', False)
	for name, chrom, strand, coding_start, coding_stop, exon_starts, exon_stops in reader:
		if len(strand) != 1 or strand not in '+-':
			exit('Invalid strand at line %d: %s' % (reader.lineno(), strand))

		if coding_start > coding_stop:
			exit('Invalid coding region coordinates at line %d.' % reader.lineno())
		elif coding_start == coding_stop:
			coding_start = coding_stop = ''

		try:
			exon_coords = zip(parse_coords(exon_starts), parse_coords(exon_stops), range(len(exon_starts)))
		except ValueError:
			exit('Invalid exon coordinates at line %d.' % reader.lineno())

		exon_coords.sort(key=itemgetter(0), reverse=strand=='-')
		
		print '>%s\t%s\t%s\t%s\t%s' % (name, chrom, str(coding_start), str(coding_stop), strand)
		for start, stop, serial in exon_coords:
			print '%s_%d\t%d\t%d' % (name, serial+1, start, stop)

if __name__ == '__main__':
	main()

