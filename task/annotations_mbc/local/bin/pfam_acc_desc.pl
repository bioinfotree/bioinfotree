#!/usr/bin/perl

use warnings;
use strict;

$\="\n";
$,="\t";

my $line;

while ($line = <>) {

	chomp $line;
	next if $line !~ /^ACC\s+/;

	my $acc = $line;
	$acc =~ s/^ACC\s+//;
	$acc =~ s/\.\d+//;
	
	$line = <>;
	next if !$line;

	chomp $line;
	next if $line !~ /^DESC\s+/;

	my $desc = $line;
	$desc =~ s/^DESC\s+//;

	print $acc,$desc;
}

