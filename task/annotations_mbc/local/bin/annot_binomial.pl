#!/usr/bin/perl

use strict;
use warnings;

$,="\t";
$\="\n";

my $usage = "$0 total_sum_col_file < cluster_sum_col_file";

my $tot_file = shift @ARGV;
die $usage if !defined $tot_file;

open TOT,$tot_file or die "Can't open file ($tot_file)";
chomp (my $line = <TOT>);
close TOT;

my @P = split /\t/,$line;

chomp ($line = <>);
my @K = split /\t/,$line;
my $n = shift @K;

my @prob = ();
for (my $i=0; $i<scalar @P; $i++) {
	my $tmp = `binomial -p $P[$i] -k $K[$i] -n $n`;
	push @prob, $tmp;
}

@prob = map{$_ = sprintf('%e',$_) } @prob;
print @prob;

