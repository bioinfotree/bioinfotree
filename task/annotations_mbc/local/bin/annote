#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$Getopt::Long::ignorecase=0;
$,="\t";
$\="\n";

my $usage="$0 
-annotations|a nonredundant_annotation1 
[-a nonredundant_annotation2 -a ...]
[-repeats|-r chr1.repeats 
[-r|repeats chr2.repeats -r ../]
[-allready-sorted|as] 
[-do-sorting|ds] 
[-cut|f 1,4,5 ] 
[-repeat_on_gap_only|g]
input_file >out\n";

######## controllo input

my %optctl = ();
&GetOptions ( \%optctl, 
	"annotations|a=s@",
	"repeats|r=s@",
	"repeat_on_gap_only|g",
	"cut|f=s",
	"allready-sorted|as",
	"do-sorted|ds"
);

my $filename=shift(@ARGV);
$filename or die($usage);
defined($optctl{'annotations'}) or die($usage);
scalar(@{$optctl{'annotations'}}) or die($usage);

my $pipe="cat $filename";

if($optctl{'cut'}){
	$pipe .= " | cut -f $optctl{'cut'}";
}

die($usage) if($optctl{'allready-sorted'} and $optctl{'do-sorting'});

if($optctl{'do-sorting'}){
	$pipe.=" | sort -k 1,1 -k 2,2n";
}

my $sorted_input=0;
$sorted_input=1 if($optctl{'allready-sorted'} or $optctl{'do-sorting'});

my $opt_repeats=0;
$opt_repeats=1 if defined($optctl{'repeats'}) and scalar(@{$optctl{'repeats'}});

########################



#### load annotations
my %annotations=();
for(@{$optctl{'annotations'}}){
	open FH,"$_" or die("can't open file ($_)\n");
	while(<FH>){
		chomp;
		my @F=split;
		my $chr=shift(@F);
		$chr=~s/^chr//;
		$chr=uc($chr);
		# now F is start,stop,type
		push @{$annotations{$chr}}, \@F;
	}
	close FH;
}

my %imin=();
my %imax=();

for(keys(%annotations)){
	$imin{$_}=0;
	$imax{$_}=scalar(@{$annotations{$_}});
}

my @keys=('C','E','5','3','I','U','N');
#####################



#### load repeats
my %repeats=();
for(@{$optctl{'repeats'}}){
	open FH,"$_" or die("can't open file ($_)\n");
	while(<FH>){
		chomp;
		my @F=split;
		my $chr=shift(@F);
		$chr=~s/^chr//;
		$chr=uc($chr);
		# now F is start,stop,type
		push @{$repeats{$chr}}, \@F;
	}
	close FH;
}

my %rep_start_offset;
my %rep_max;
for(keys(%repeats)){
	$rep_start_offset{$_}=0;
	$rep_max{$_}=scalar(@{$repeats{$_}});
}


#####################

open FH, "$pipe |" or die("Can't open pipe ($pipe)");



while(<FH>){
	chomp;
	if(m/^>/ or !length){
		print;
		next;
	}
	my %sum=();
	for(@keys){
		$sum{$_}=0;
	}


	my ($chr, $start, $stop, @altro)=split;

	$chr=~s/chr//;
	$chr=uc($chr);
	$chr or die $_;

	
	my $take_segments=0;

	my $ann=$annotations{$chr};
	defined $ann or die("ERROR: there is no annotation for chromosome $chr (". join("\n",@{$optctl{'annotations'}}). ")\n");

	if($start > $ann->[-1]->[1]){
		$sum{'N'}=$stop-$start;
		my @tmp=();
		for(@keys){
			push @tmp, $sum{$_};
		}
		
		&add_repeats($chr,$start,$stop,\@altro,\@tmp) if($opt_repeats);
		
		print $chr,$start,$stop,@altro,@tmp;
		next;
	}

	for(my $i=$imin{$chr}; $i<$imax{$chr}; $i++){
		my $seg_ref=$ann->[$i];
		
#		if(!defined($seg_ref)){
#			print STDERR $i,$imax{$chr},$chr;
#			for(sort {$2<=>$b} keys(%imax)){
#				print $_,$imax{$_};
#			}
#			die;
#		}
		
		if($start >= $seg_ref->[0] and $start <= $seg_ref->[1]){
			if($stop >= $seg_ref->[0] and $stop <= $seg_ref->[1]){
				$sum{$seg_ref->[2]} += $stop - $start;

				#stampa	
				my @tmp=();
				for(@keys){
					push @tmp, $sum{$_};
				}
				
				&add_repeats($chr,$start,$stop,\@altro,\@tmp) if($opt_repeats);
				
				print $chr,$start,$stop,@altro,@tmp;
				#########################

				last;
			}else{
				$sum{$seg_ref->[2]} += $seg_ref->[1]-$start;
				if($sorted_input){
					$imin{$chr}=$i;	# tutti i segmenti in input successivi
							# avranno uno start >= a quello di adesso e quindi 
							# e` inutile che vada a vedermi le annotazioni precedenti
				}
			}
		}
		if($stop>=$seg_ref->[0] and $stop <= $seg_ref->[1]){
			$sum{$seg_ref->[2]} += $stop - $seg_ref->[0];



			#stampa	
			my @tmp=();
			for(@keys){
				push @tmp, $sum{$_};
			}

			&add_repeats($chr,$start,$stop,\@altro,\@tmp) if($opt_repeats);
			
			print $chr,$start,$stop,@altro,@tmp;
			#########################
			
			last;
		}elsif($start < $seg_ref->[0] and $stop > $seg_ref->[1]){ # 
			$sum{$seg_ref->[2]} += $seg_ref->[1]-$seg_ref->[0];
		}
	}
}




sub add_repeats
{
	my $chr=shift;
	my $start=shift;
	my $stop=shift;
	my $altro_ref=shift;
	my $to_print_ref=shift;

	if($optctl{'repeat_on_gap_only'} and !grep{$_ eq 'g'} @{$altro_ref}){
		push @{$to_print_ref}, 'ND';
		return;
	}
	
	my $sum=0;
	
	my $rep=$repeats{$chr};
	
	for(my $i=$rep_start_offset{$chr}; $i<$rep_max{$chr}; $i++){
		my $seg_ref=$rep->[$i];
		if($start >= $seg_ref->[0] and $start < $seg_ref->[1]){
			if($stop <= $seg_ref->[1]){
				$sum = $stop - $start;
				last;
			}else{
				$sum += $seg_ref->[1]-$start;
				if($sorted_input){
					$rep_start_offset{$chr}=$i;	# tutti i segmenti in input successivi
									# avranno uno start >= a quello di adesso e quindi 
									# e` inutile che vada a vedermi le annotazioni precedenti
				}
			}
		}elsif($stop >= $seg_ref->[0] and $stop < $seg_ref->[1]){
			$sum += $stop - $seg_ref->[0];
			last;
		}elsif($start < $seg_ref->[0] and $stop > $seg_ref->[1]){ # 
			$sum += $seg_ref->[1]-$seg_ref->[0];
		}
	}
	
	push @{$to_print_ref}, $sum;
}
