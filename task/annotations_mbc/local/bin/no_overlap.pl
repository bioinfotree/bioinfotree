#!/usr/bin/perl

use warnings;
use strict;

my $pre_e=0;
my $pre_r=undef;
while(<>){
	my ($chr,$b,$e,@all)=split;
	if($b>$pre_e){
		print $pre_r if defined($pre_r);
		$pre_r=$_;
		$pre_e=$e;
	}else{
		$pre_r=<>;
		($chr,$b,$e,@all)=split;
		$pre_e=$e;
	}
}
