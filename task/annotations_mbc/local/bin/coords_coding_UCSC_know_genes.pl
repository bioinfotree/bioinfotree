#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

use constant NAME_COL 	=> 0;
use constant CHR_COL 	=> 1;
use constant CDS_B_COL 	=> 5;
use constant CDS_E_COL 	=> 6;
use constant EXONS_B_COL=> 8;
use constant EXONS_E_COL=> 9;
use constant PROTEIN_ID => 10;

while(<>){
	chomp;
	my @F = split;
	#print	$F[NAME_COL],$F[CHR_COL],$F[CDS_B_COL],$F[CDS_E_COL],$F[EXONS_B_COL],$F[EXONS_E_COL],$F[PROTEIN_ID];
		
	my @exon_b=split(/,/,$F[EXONS_B_COL]);
	my @exon_e=split(/,/,$F[EXONS_E_COL]);

	for(my $i=0; $i<scalar(@exon_b); $i++){
		print 	$F[CHR_COL],
			$exon_b[$i],$exon_e[$i],
			$F[NAME_COL],$F[PROTEIN_ID] if $exon_b[$i] >= $F[CDS_B_COL] and $exon_e[$i] <= $F[CDS_E_COL];
	}
}
