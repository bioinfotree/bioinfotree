include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

ENS_VERSION ?= 42
MART_ARCHIVE?=www
ENS_ANNOTE_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
BIN_DIR ?= $(BIOINFO_ROOT)/task/annotations/local/bin
SPECIES ?= hsapiens
MYSQL_MART ?= mysql -BCAN -u anonymous -h martdb.ensembl.org -P 3316 ensembl_mart_$(ENS_VERSION)
MYSQL_ENS  ?= mysql -BCAN -u anonymous -h ensembldb.ensembl.org ensembl_go_$(ENS_VERSION)
MARTSERVICE_URL = http://$(MART_ARCHIVE).ensembl.org/biomart/martservice
EBI_MULTISPECIES_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/GO/ebi/$(EBI_VERSION)
MIN_GO_SIZE_FOR_NON_REDUNDANT = 7

all: ensg_go.inclusive.gz


extern $(EBI_MULTISPECIES_DIR)/go_tree as GO_TREE
extern $(EBI_MULTISPECIES_DIR)/go_term as GO_TERM

ensg_go.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_go.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| bawk '$$2' \
	| gzip >$@
	rm $@_query.xml

ensg_go.noIEA.gz: ensg_go.gz
	bawk '$$3!="IEA"' $< | gzip >$@

ensg_go.noIEA.biological_process.gz ensg_go.noIEA.molecular_function.gz ensg_go.noIEA.cellular_component.gz: ensg_go.noIEA.%.gz: ensg_go.noIEA.gz $(GO_TERM)
	zcat $< \
	| translate -a -v -e NA <(cut -f 1,3 $^2) 2 \             * aggiunge l'ontologia molecular_function, cellular_component o biological_process, accetto NA per problemi di versione ensembl/ebi
	| bawk '$$3=="$*"' | cut -f -2,4- | gzip > $@

ensg_go.noIEA.biological_process.inclusive.gz ensg_go.noIEA.molecular_function.inclusive.gz ensg_go.noIEA.cellular_component.inclusive.gz ensg_go.inclusive.gz ensg_go.noIEA.inclusive.gz: ensg_go%inclusive.gz: ensg_go%gz $(GO_TREE) $(GO_TERM)
	zcat $< \
	| filter_1col -v 2 <(bawk '$$4==1 {print $$1}' $^3) \       * removing non translable molecular_function cellular_component biological_process *
	| cut -f 1,2 \
	| translate -v -e NA -a -j -d -n <(cut -f 1,2 $^2 | sort | uniq) 2 \     *NA are caused by ensembl and ebi version mismatch*
	| awk '{print $$1 "\t" $$2 "\n" $$1 "\t" $$3}' \
	| bsort -S 20% | uniq \
	| gzip > $@

.DOC: ensg_go.*.inclusive.gz 
	is inclusive since GO_TREE is inclusive

.META: ensg_go.inclusive.gz ensg_go.noIEA.inclusive.gz
	1	ENSG
	2	GO

%.inclusive.negative.gz: %.inclusive.gz
	bawk '{print $$GO,$$ENSG}' $< | go_negative | gzip >$@

.META: *.inclusive.negative.gz
	1	GO
	2	ENSG


ensg_go.noIEA.inclusive.nonredundant.gz: ensg_go.noIEA.inclusive.gz
	zcat $< | translate -a -k <(bawk '{print $$2}' $< | symbol_count | bawk '$$2>=$(MIN_GO_SIZE_FOR_NON_REDUNDANT)') 2 \
	| translate -a -v -e NA <(cut -f 1,3 $(GO_TERM)) 2 \             * aggiunge l'ontologia molecular_function, cellular_component o biological_process, accetto NA per problemi di versione ensembl/ebi
	| bawk '{print $$3 ";" $$1,$$2,$$4}' \
	| find_best -r 1 3 \
	| tr ";" "\t" | select_columns 2 1 3 4 | gzip > $@

.META: ensg_go.noIEA.inclusive.nonredundant.gz
	1	gene_id		ENSG00000092758
	2	ontology	biological_process
	3	GO		GO:0030574
	4	GO_size		70

ensg_go.noIEA.inclusive.nonredundant.biological_process.gz ensg_go.noIEA.inclusive.nonredundant.cellular_component.gz ensg_go.noIEA.inclusive.nonredundant.molecular_function.gz: ensg_go.noIEA.inclusive.nonredundant.%.gz: ensg_go.noIEA.inclusive.nonredundant.gz
	bawk '$$ontology=="$*" {print $$gene_id,$$GO}' $< | gzip > $@

.META: ensg_go.noIEA.inclusive.nonredundant.*.gz ensg_go.nr*.gz
	1	gene
	2	GO


ensg_go.nrCC.gz: ensg_go.noIEA.inclusive.nonredundant.cellular_component.gz
	ln -s $< $@
ensg_go.nrBP.gz: ensg_go.noIEA.inclusive.nonredundant.biological_process.gz
	ln -s $< $@
ensg_go.nrMF.gz: ensg_go.noIEA.inclusive.nonredundant.molecular_function.gz
	ln -s $< $@

go_region: ensg_go.riannot $(ENS_ANNOTE_DIR)/coords_gene
	join3_pl -i -u -2 5 <(sort -k 1,1 $<) <(grep -F ENS $(word 2,$^) | sort -k 5,5) \
	| awk 'BEGIN{OFS="\t"}{print $$3,$$4,$$5,$$6,$$1,$$2}' \
	| sort -k 1,1 -k 2,2n -k 3,3n > $@

#per avere anche la provenienza dell'annotazione:
# cat ensg_go | join3_pl - -1 2 -2 2 -i -u -- go_tree

%.ENS_family_go: % ensg_go.riannot
	cat $* | $(BIN_DIR)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp $(DISPLAY_GENES) > $@
	rm $@.tmp

#%.family_go: % ensg_go.riannot
#	$(BIN_DIR)/join_annotation.pl -a ensg_go.riannot -n $(DISPLAY_GENES) < $< > $@ 


ENSG_EntrezGene:
	echo "SELECT DISTINCT gene_stable_id,dbprimary_id FROM $(SPECIES)_gene_ensembl__xref_entrezgene__dm WHERE dbprimary_id IS NOT NULL;" \
	| $(MYSQL_MART) \
	| sort -k1,1 \
	> $@

