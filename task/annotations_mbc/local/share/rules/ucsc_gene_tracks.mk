ALL_GENE_TRACKS ?= gene_tracks_not_defined

.PHONY: all.gene_tracks

all.gene_tracks: $(ALL_GENE_TRACKS)

.META: %_genes
	1	id		NR_024540
	2	chr		1
	3	strand		-1
	4	b		4224
	5	e		19233
	6	coding_b	19233
	7	coding_e	19233
	8	exon_number	11
	9	exon_starts	4224,4832,5658,6469,6720,7095,7468,7777,8130,14600,19183
	10	exon_ends	4692,4901,5810,6628,6918,7231,7605,7924,8229,14754,19233

define get_track_standard
	echo "SELECT name, chrom, strand, txStart, txEnd, cdsStart, cdsEnd, exonCount, exonStarts, exonEnds FROM $(1)" \
	| $(call mysql) \
	| bsort -S10 | uniq \
	| $(call convert_strand) \
	| id2count -b 1 -g '#' > $@        *disambiguate id of items that map in many places*
endef

define get_track_no_tx
	query='SELECT DISTINCT qName, tName, strand, tStart, tEnd, tStart, tEnd, blockCount, tStarts, blockSizes FROM $(1);'; \
	$(call get_track,"$$query") \
	| perl -lane '@G=split(/,/,$$F[8]); @H=split(/,/,$$F[9]); for($$i=0; $$i<scalar(@G); $$i++){$$H[$$i]=$$G[$$i]+$$H[$$i]} $$F[9]=join(",",@H); print join("\t",@F)' \
	| $(call convert_strand) >$@
endef


define get_track
	echo $(1) \
	| $(call mysql) > $@
endef

define mysql
	$(MYSQL_CMD) $(UCSC_DATABASE) \
	| sed -r 's/chr//g; s/,\t/\t/g; s/,$$//' \
	| bsort -k2,2 -k4,4n
endef

define convert_strand
	bawk '{sub(/^\+$$/,"+1",$$3);sub(/^-$$/,"-1",$$3);print}'
endef

#     * remove the "chr" string and some "," at the end of fields *
#     * remove strange chromosomes                                *	

known_genes:
	$(call get_track_standard,knownGene)

refseq_genes:
	$(call get_track_standard,refGene)

aceview_genes:
	$(call get_track_standard,acembly)

other_refseq_genes:
	$(call get_track_standard,xenoRefGene)

mgc_genes:
	$(call get_track_standard,mgcGenes)

nscan_genes:
	$(call get_track_standard,nscanGene)

sgp_genes:
	$(call get_track_standard,sgpGene)

geneid_genes:
	$(call get_track_standard,geneid)

genscan_genes:
	$(call get_track_standard,genscan)

flybase_genes:
	$(call get_track_standard,flyBaseGene)

exoniphy_genes:
	$(call get_track_standard,exoniphy)

ensembl_genes:
	$(call get_track_standard,ensGene)

gencodeCompV17_genes:
	$(call get_track_standard,wgEncodeGencodeCompV17)

ensembl_transcript2gene.map:
	query='SELECT DISTINCT name, name2 FROM ensGene WHERE name is not NULL and name2 is not NULL;'; \
	$(call get_track,"$$query") > $@


ccds_genes:
	$(call get_track_standard,ccdsGene)

mirna_genes:
	query="SELECT DISTINCT name, chrom, strand, chromStart, chromEnd, chromStart, chromEnd, '1', chromStart ,chromEnd FROM miRNA;"\
	$(call get_track,"$$query") > $@

genetrap_genes:
	$(call get_track_no_tx,igcc)

nia_genes:
	$(call get_track_no_tx,nia)

transmap_genes: 
	$(call get_track_no_tx,transMapAlnUcscGenes)

affyU133Plus2_genes:
	$(call get_track_no_tx, affyU133Plus2)

affyU133Plus2_genes-entrez.map: affyU133Plus2_genes
	echo 'SELECT DISTINCT affy.value, entrez.value FROM knownToU133Plus2 as affy JOIN knownToLocusLink as entrez ON (affy.name = entrez.name)' | $(MYSQL_CMD) $(UCSC_DATABASE) >$@

aceview-cdnas.gz: aceview_genes
	bawk '{if($$4<$$6){o=$$4}else{o=$$6}; $$4-=o; $$5-=o; $$6-=o; $$7-=o; \    * relative coords  *
	       if($$7==0) { \
	         print $$1,$$5,"","","",""; \                                      * no coding region *
	       } else { \
	         if($$6==0){b5=""}else{b5=0}; if($$5==$$7){e3=""}else{e3=$$5}; \   * UTR coords       *
	         print $$1,$$5,$$6,$$7,b5,e3; \
	       }}' $< \
	| gzip >$@

gene_symbol-refseq.map.gz:
	$(MYSQL_CMD) $(UCSC_DATABASE) <<<"SELECT geneName, name FROM refFlat" \
	| gzip >$@

name-refseq.map:
	$(MYSQL_CMD) $(UCSC_DATABASE) <<< "SELECT geneName, name FROM refFlat"\ > $@


known_genes.xref.map.gz:
	wget -O $@ ftp://hgdownload.cse.ucsc.edu/goldenPath/mm9/database/kgXref.txt.gz

.META: known_genes.xref.map.gz
	1	kgID
	2	mRNA
	3	spID
	4	spDisplayID
	5	geneSymbol
	6	refseq
	7	protAcc
	8	description

