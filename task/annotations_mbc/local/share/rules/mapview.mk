# Copyright 2009-2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES     ?= hsapiens
VERSION     ?= 36.3

SPECIES_MAP := $(TASK_ROOT)/local/share/species-mapview.map
BASE_URL    := ftp://ftp.ncbi.nlm.nih.gov/genomes/MapView/$(shell translate $(SPECIES_MAP) 1 <<<$(SPECIES))/sequence/BUILD.$(VERSION)/initial_release/

import gene-structures


ALL += seq_gene.md.gz \
       transcript_structures.gz transcript-gene.map.gz \
       exons.gz exon-transcript.map.gz


seq_gene.md.gz:
	wget -O- $(BASE_URL)/$@ \
	| zcat \
	| unhead \
	| gzip >$@

.META:	seq_gene.md.gz
	1	tax_id	9606
	2	chromosome	1
	3	chr_start	127
	4	chr_stop	2318
	5	chr_orient	+
	6	contig	NW_001838563.2
	7	ctg_start	3562
	8	ctg_stop	5753
	9	ctg_orient	-
	10	feature_name	LOC100131754
	11	feature_id	GeneID:100131754
	12	feature_type	GENE
	13	group_label	HuRef
	14	transcript	-
	15	evidence_code	protein;;

transcript_structures.gz: seq_gene.md.gz
	zcat $< \
	| bsort -k14,14 \
	| mapview-transcript_structures \
	| gzip >$@

.META: transcript_structures.gz
	FASTA
	> transcript_ID chromosome coding_start coding_stop strand
	exon_ID exon_start exon_stop

transcript-gene.map.gz: seq_gene.md.gz
	zcat $< \
	| bawk '($$13=="reference" || $$13=="Primary_Assembly") && $$14!="-" {sub(/GeneID:/,"",$$11);print $$14,$$11}' \
	| bsort -u \
	| gzip >$@

.META: transcript-gene.map.gz
	1  transcript_ID  NM_000014.4
	2  gene_ID        2

exons.gz: transcript_structures.gz
	zcat $< \
	| bawk '$$1~/^>/ {chr=$$2; strand=$$5} $$1!~/^>/ {print $$1,chr,$$2,$$3,strand}' \
	| gzip >$@

.META: exons.gz
	1  exon_ID  NM_000014.4_1 (generated internally, not by NCBI)
	2  chr      12
	3  start    9220303
	4  stop     9220418
	5  strand   -1

exon-transcript.map.gz: exons.gz
	bawk '{tr=$$1; sub(/_[0-9]+$$/,"",tr); print $$1,tr}' $< \
	| gzip >$@

.META: exon-transcript.map.gz
	1  exon_ID        NM_000014.4_1
	2  transcript_ID  NM_000014.4
