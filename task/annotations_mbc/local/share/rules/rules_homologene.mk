include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk
HOMOLOGENE_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/homologene
GENE_INFO = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/$(VERSION_INFO)/gene_info.gz
SPECIES1_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES1)/$(VERSION)
SPECIES2_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES2)/$(VERSION)
NCBI_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/20081019
TAX_HUMAN = 9606
TAX_MOUSE = 10090
TAX_RAT = 10116
TAX_ZEBRA = 7955
TAX_DMELANOGASTER=7227

homologene.data: 
	wget ftp://ftp.ncbi.nih.gov/pub/HomoloGene/$(BUILD)/$@

.META: homologene.data
	1	HID	(HomoloGene group id)
	2	TaxonomyID
	3	GeneID
	4	GeneSymbol
	5	ProteinGi
	6	ProteinAccession

human_rat_cluster_with_entrez.sort: homologene.data
	perl -ane 'print "$$F[0]\t$$F[1]\t$$F[2]\t$$F[3]\n" if ($$F[1] == $(TAX_HUMAN) || $$F[1] == $(TAX_RAT))' $< \
	| sort -k 1,1n -k 3,3n > $@
	perl -ane 'print if ($$F[1] == $(TAX_HUMAN))' $@ > human
	perl -ane 'print if ($$F[1] == $(TAX_RAT))' $@ > rat
	join human rat > human_rat
	perl -ane 'print "$$F[5]\t$$F[2]\t$$F[3]\n"' human_rat > entrez_rat_human_corresponding

human_mouse_cluster_with_entrez.sort: homologene.data
	perl -ane 'print "$$F[0]\t$$F[1]\t$$F[2]\t$$F[3]\n" if ($$F[1] == $(TAX_HUMAN) || $$F[1] == $(TAX_RAT))' $< \
	| sort -k 1,1n -k 3,3n > $@
	perl -ane 'print if ($$F[1] == $(TAX_HUMAN))' $@ > human
	perl -ane 'print if ($$F[1] == $(TAX_MOUSE))' $@ > rat
	join human rat > human_rat
	perl -ane 'print "$$F[5]\t$$F[2]\t$$F[3]\n"' human_rat > entrez_rat_human_corresponding

human_zebra_cluster_with_entrez.sort: homologene.data
	perl -ane 'print "$$F[0]\t$$F[1]\t$$F[2]\t$$F[3]\n" if ($$F[1] == $(TAX_HUMAN) || $$F[1] == $(TAX_ZEBRA))' $< \
	| sort -k 1,1n -k 3,3n > $@
	perl -ane 'print if ($$F[1] == $(TAX_HUMAN))' $@ > human
	perl -ane 'print if ($$F[1] == $(TAX_ZEBRA))' $@ > zebra
	join human zebra > human_zebra
	perl -ane 'print "$$F[5]\t$$F[2]\t$$F[3]\n"' human_zebra > entrez_zebra_human_corresponding

mouse_zebra_cluster_with_entrez.sort: homologene.data
	perl -ane 'print "$$F[0]\t$$F[1]\t$$F[2]\t$$F[3]\n" if ($$F[1] == $(TAX_MOUSE) || $$F[1] == $(TAX_ZEBRA))' $< \
	| sort -k 1,1n -k 3,3n > $@
	perl -ane 'print if ($$F[1] == $(TAX_MOUSE))' $@ > human
	perl -ane 'print if ($$F[1] == $(TAX_ZEBRA))' $@ > zebra
	join human zebra > human_zebra
	perl -ane 'print "$$F[5]\t$$F[2]\t$$F[3]\n"' human_zebra > entrez_zebra_human_corresponding

human_to_zebra_entrez: human_zebra_cluster_with_entrez.sort
	sort -k1,1n -k 2,2n -k 3,3n $< | homologinize -T new.tmp > $@.tmp
	sort -k1,1n $@.tmp > $@
	rm -f new.tmp_* $@.tmp

mouse_to_zebra_entrez: mouse_zebra_cluster_with_entrez.sort
	sort -k1,1n -k 2,2n -k 3,3n $< | homologinize -T new.tmp > $@.tmp
	sort -k1,1n $@.tmp > $@
	rm -f new.tmp_* $@.tmp
	
human_mouse_cluster_with_entrel.sort: homologene.data
	perl -ane 'print "$$F[0]\t$$F[1]\t$$F[2]\t$$F[3]\n" if ($$F[1] == $(TAX_HUMAN) || $$F[1] == $(TAX_MOUSE))' $< \
	| sort -k 2,2n -k 3,3n > $@

human_to_mouse_entrez: human_mouse_cluster_with_entrez.sort 
	sort -k1,1n -k 2,2n -k 3,3n $< | homologinize -T new.tmp > $@.tmp
	sort -k1,1n $@.tmp > $@
	rm -f new.tmp_* $@.tmp

.META: human_to_mouse_entrez
	1	cluster	3
	2	human_id	9606
	3	human_entrez	34
	4	human_gsymbol	ACADM
	5	mouse_id	10090
	6	mouse_entrez	11364
	7	mouse_gsymbol	Acadm

Entrez_Hs_gene_with_Mm_orth_uniq: human_mouse_cluster_with_entrez.sort
	Hs_gene_with_Mm_orth $< $(TAX_HUMAN) $(TAX_MOUSE) | sort -n | uniq > $@

ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous: $(SPECIES1_DIR)/gene-homologous.map.gz $(SPECIES1_DIR)/gene-entrez.map.gz $(SPECIES2_DIR)/gene-entrez.map.gz
	zcat $< | bawk '$$6=="$(SPECIES_HOMOL)"' | translate -a -d -j -k <(zcat $^2 |bawk '{print $$0}' | sort | uniq) 2 \
	| translate -a -d -j -k <(zcat $^3 | bawk '{print $$0}' | sort | uniq) 5 > $@

ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous_only_entrez: ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous
	cut -f 3,6 $< | sort -k1,1 -k2,2 | uniq > $@

ENSEMBL_entrez_Hs: ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous
	perl -ane 'print "$$F[2]\n"' $< | sort | uniq > $@

Entrez_Hs_gene_with_Mm_orth_uniq_1_link: Entrez_Hs_gene_with_Mm_orth_uniq ENSEMBL_entrez_Hs
	cat $< $^2 | sort -n | uniq -c | perl -ane 'print "$$F[1]\n" if ($$F[0]==1)' > $@

human_entrez_to_add_from_ensembl: ENSEMBL_entrez_Hs Entrez_Hs_gene_with_Mm_orth_uniq_1_link
	cat $< $^2 | sort -n | uniq -c | perl -ane 'print "$$F[1]\n" if ($$F[0]==2)' > $@

homologene-ensembl.data: Entrez_Hs_gene_with_Mm_orth_uniq homologene.data human_entrez_to_add_from_ensembl ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous_only_entrez
	zcat $(GENE_INFO) | perl -ane 'print "$$F[1]\t$$F[2]\n" if ($$F[0] == $(TAX_HUMAN) || $$F[0] == $(TAX_MOUSE))' > gene.info.tmp
	join_homologene_ensembl $< $^2 $(TAX_HUMAN) $(TAX_MOUSE) $^3 $^4 gene.info.tmp > $@
	rm gene.info.tmp

homologene-ensembl.data_complete_union: homologene.data ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous_only_entrez $(NCBI_DIR)/gene_history.gz
	zcat $^3 | perl -ane 'print if ($$F[0] == $(TAX_HUMAN) || $$F[0] == $(TAX_MOUSE))' > gene_history.tmp
	convert_homologene $< $(TAX1) $(TAX_MOUSE) > human_mouse_clusters
	cat human_mouse_clusters $^2 | sort -nk1,1 -nk2,2 | uniq > homologene-ensembl.data_complete_union.tmp
	zcat $(GENE_INFO) | perl -ane 'print "$$F[1]\t$$F[2]\t$$F[0]\n" if ($$F[0] == $(TAX_HUMAN) || $$F[0] == $(TAX_MOUSE))' > gene.info.tmp
	construct_orth homologene-ensembl.data_complete_union.tmp gene.info.tmp $(TAX1) $(TAX_MOUSE) gene_history.tmp > $@

#	cat human_mouse_clusters $^2 | sort -nk1,1 -nk2,2 | uniq -c | perl -ane 'print "$$F[1]\t$$F[2]\n" if ($$F[0] == 1)' > ENS_Homol.tmp
#	cat ENS_Homol.tmp $^2 | sort -nk1,1 -nk2,2 | uniq -c | perl -ane 'print "$$F[1]\t$$F[2]\n" if ($$F[0] == 2)' > ENS-to-add.tmp
	

check_entrez: homologene.data_02
	check_homologene_cluster $(SPECIES) $(ENTREZ) $<


mouse_human: homologene.data
	bawk '$$TaxonomyID==9606' $< | translate -a -d -j -k <(bawk '$$TaxonomyID==10090' $<) 1 > $@

droso_human: homologene.data
	bawk '$$TaxonomyID==$(TAX_HUMAN)' $< | translate -a -d -j -k <(bawk '$$TaxonomyID==$(TAX_DMELANOGASTER)' $<) 1 > $@

.META: mouse_human droso_human
	1	HID	(HomoloGene group id)
	2	TaxonomyID_other
	3	GeneID_other
	4	GeneSymbol_other
	5	ProteinGi_other
	6	ProteinAccession_other
	7	TaxonomyID_human
	8	GeneID_human
	9	GeneSymbol_human
	10	ProteinGi_human
	11	ProteinAccession_human

%_human.cluser_size: %_human
	translate -a <(cut -f 1 $< | symbol_count) 1 < $< > $@

.META: *.cluser_size
	1	HID	(HomoloGene group id)
	2	HID_size
	3	TaxonomyID_other
	4	GeneID_other
	5	GeneSymbol_other
	6	ProteinGi_other
	7	ProteinAccession_other
	8	TaxonomyID_human
	9	GeneID_human
	10	GeneSymbol_human
	11	ProteinGi_human
	12	ProteinAccession_human

