URL_PREFIX = http://tables.pseudogene.org/dump.cgi?table=
pg.gz:
	wget -O >(unhead | gzip > $@) $(URL_PREFIX)$(SPECIES)$(VERSION)
	
.META:	pg.gz
	1	ID
	2	Chromosome
	3	Start Coordinate
	4	Stop Coordinate
	5	Strand
	6	Parent Protein
	7	Protein Start
	8	Protein Stop
	9	Parent Gene
	10	Fraction
	11	Num Insertions
	12	Num Deletions
	13	Num Shifts
	14	Num Stops
	15	E Value
	16	Identity
	17	PolyA
	18	Disablements
	19	Exons
	20	Introns
	21	Class
	22	Sequence
	23	Link

pg.exons.gz: pg.gz
	bawk '{print $$ID, $$Chromosome, $$Exons}' pg.gz | expandsets -s'],' 3 | sed 's/, /\t/' |tr -d '[,] ' \
	| bsort -k 2,2 -k3,3n \
	| gzip > $@

.META: pg.exons.gz
	1	id
	2	chr
	3	b
	4	e

