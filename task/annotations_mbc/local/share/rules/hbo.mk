HTTP_PREFIX_ANNOTATION=http://compbio.charite.de/hudson/job/hpo.annotations.monthly/$(VERSION_ANNOTATION)/artifact/annotation
HTTP_PREFIX_ONTOLOGY=http://compbio.charite.de/hudson/job/hpo/$(VERSION_ONTOLOGY)/artifact/hp
ENTREZ2ENSEMBL = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/genes/hsapiens/$(ENTREZ2ENSEMBL_VERSION)/entrez-ensembl.map.gz

hp.obo.gz: 
	wget -O >(gzip > $@) $(HTTP_PREFIX_ONTOLOGY)/hp.obo

hp.tree.gz: hp.obo.gz
	zcat $< \
	| perl -ne 'chomp;\\
		unless(m/^id:/ or m/^alt_id:/ or m/^is_a:/){next};\\                    *prendo solo le righe che mi interessano, cosi` butto anche via l'header
		s/^id:\s/\n/;\\								*tutti i blocchi iniziano con id:, aggiungo un \n per dire che il blocco precedente e` finito
		s/\s+!.*$$//;\\								*dopo il ! ho dei commenti o descrizioni*
		s/^alt_id:\s/;/;\\							*posso avere molti sinonimi, li incollo all'id principale con ;*
		s/^is_a:\s(.*)$$/\@$$1/;\\						*ecco l'indicazione del parent, separo con @, possono essercene piu` di uno e solo il primo diventera` \t*
		print; \\
		END{print "\n"}' \
	| unhead \									*tolgo la prima riga vuota spuria*
	| sed 's/@/\t/'\								*solo il primo, gli altri li espando*
	| bawk 'NF>=2'\									*tolgo la radice e termini "obsolete" che non sono figli di niente e fanno casino*
	| expandsets -s ';' 1\
	| expandsets -s '@' 2\
	| gzip >$@

.META: hp.tree.gz
	1	child	HP:0000002
	2	parent	HP:0001507

hp.ancestors.gz: hp.tree.gz
	zcat $< | dag2ancestors -s | gzip >$@

entrez2hp.gz: ALL_SOURCES_ALL_FREQUENCIES_diseases_to_genes_to_phenotypes.raw.gz _cancergenes_to_phenotype.raw.gz 
	(zcat $^1 | unhead | cut -f 3,4; \
	 zcat $^2 | unhead | cut -f 3,4 ) \
	| bsort | uniq | gzip > $@

entrez2hp-inclusive.gz: entrez2hp.gz hp.ancestors.gz
	zcat $< | translate -a -d -j <(\
		zcat $^2 | expandsets 2 \
		| bawk '{print $$1,$$2}'\                      *sono figli di se stessi gia` in hp.ancestors.gz*
	) 2 \
	| cut -f 1,3 | gzip > $@

ensembl2hp-inclusive.gz: entrez2hp-inclusive.gz $(ENTREZ2ENSEMBL)
	zcat $< | translate -a -v -e NA -k -d -j <(zcat $^2) 1 |bawk '$$2!="NA" {print $$2,$$3}' \
	| bsort | uniq | gzip > $@

hp.owl.gz: 
	wget -O >(gzip > $@) $(HTTP_PREFIX_ONTOLOGY)/hp.owl

%.raw.gz:
	wget -O >(gzip > $@) $(HTTP_PREFIX_ANNOTATION)/$*.txt

genes_to_diseases.gz:
	wget -O >(gzip > $@) $(HTTP_PREFIX)/genes_to_diseases.txt

diseases_to_genes.gz: 
	wget -O >(gzip > $@) $(HTTP_PREFIX)/diseases_to_genes.txt


########################
#
#	note contenute in 
#
#	http://compbio.charite.de/hudson/job/hpo.annotations.monthly/67/artifact/annotation/external_data.txt
#
## URLs to files needed for gene2phenotype
#
#
#ftp://anonymous:sebastian.koehler%40charite.de@ftp.omim.org/OMIM/mim2gene.txt
#ftp://anonymous:sebastian.koehler%40charite.de@ftp.omim.org/OMIM/genemap
#ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz

######
#
## URLs to files provided by ORPHANET (http://www.orphadata.org/)
#
## Diseases and cross-referencing 
#http://www.orphadata.org/data/xml/en_product1.xml

# Diseases with their associated genes
# http://www.orphadata.org/data/xml/en_product6.xml
#
#
# ### HPO-data
# http://compbio.charite.de/hudson/job/hpo.annotations/lastStableBuild/artifact/misc/phenotype_annotation.tab
# http://compbio.charite.de/hudson/job/hpo/lastSuccessfulBuild/artifact/hp/hp.obo
