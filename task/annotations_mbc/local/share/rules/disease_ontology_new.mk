tree.gz:
	echo 'suppressMessages(library(DO.db)); write.table(as.data.frame(DOANCESTOR), file="",  row.names = FALSE, col.names=FALSE, quote = FALSE, sep="\t")' | Rscript --vanilla /dev/stdin | gzip > $@

.META: tree.gz
	1	DO
	2	DO_ANCESTOR

term.gz:
	echo 'suppressMessages(library(DO.db)); write.table(as.data.frame(DOTERM), file="",  row.names = FALSE, col.names=FALSE, quote = FALSE, sep="\t")' | Rscript --vanilla /dev/stdin \
	| cut -f 2- | gzip > $@


