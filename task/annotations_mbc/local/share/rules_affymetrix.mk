#require: bmake

#vodka annotation download
V_PLATFORM=GPL1261

$(V_PLATFORM).annot.gz:
	wget ftp://ftp.ncbi.nih.gov/pub/geo/DATA/annotation/platforms/$@
#

ALL+=probe-entrez.map

include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

#http://www.affymetrix.com/Auth/analysis/downloads/na32/ivt/HG-U133A.na32.annot.csv.zip
#annot.csv.zip: 
#	@ echo "#wget -O $@ https://www.affymetrix.com/Auth/analysis/downloads/$(ANNOT_VERSION)/ivt/$(VERSION).csv.zip"
#	@ echo "It doesn't work, you must be logged"

annot.csv: annot.csv.zip
	unzip $<
	rm -f 3prime-IVT.AFFX_README.NetAffx-CSV-Files.txt
	mv $(VERSION).csv $@
	touch $@
	@ echo touch becouse uzipped file has a old date

#vodka added expandsets 2 to match mouse74_2 format
probe-entrez.map: annot.csv $(NCBI_DIR)/gene_history.gz $(NCBI_DIR)/genes.gz
	zcat $(word 2,$^) > $@.tmp.gene_history
	zcat $(word 3,$^) > $@.tmp.gene_info
	affymetrix_annotation $< $@.tmp.gene_history $@.tmp.gene_info $@.tmp $(TAXONOMY_ID)
	unhead -n 1 $@.tmp \
	| sed 's/\t/@/' | tr "\t" ";" | sed 's/@/\t/' | expandsets 2 > $@ 
	cat $@.tmp | sed 's/\t/@/' | tr "\t" ";" | sed 's/@/\t/' | perl -ane 'next if ($$F[1] =~ /\-/); print "$$F[0]\t$$F[1]\n"'> $@_entrez_annoted
	rm $@.tmp*

probe-refseq.map: annot.csv
	perl -lne 'm|^"(\d+)"|; $$id=$$1; while(m|RefSeq|){s|([^\s,"]+)\s*//\s*RefSeq||; print "$$id\t$$1";}' $< > $@

gene_present: probe-entrez.map_entrez_annoted
	cut -f 2 $< | expandsets 1 | bsort | uniq > $@
#cut -f 2 $< | tr ";" "\n" | bsort -k1,1n | uniq > $@

.META: probe-entrez.map
	1	ProbeSetID
	2	EntrezGene

probe-ensembl.map: annot.csv
	grep -v '^#' $< \
	| sed 's/","/\t/g; s/^"//; s| /// |;|g' \
	| cut -f 1,18 | expandsets 2 | bawk '$$2!="---"' > $@

probe-entrez.map_entrez_retired: probe-entrez.map

probe-entrez.map_entrez_annoted: probe-entrez.map


#by m@n

.PHONY:gene
gene: gene_$(NA)_expandset

gene_$(NA)_expandset: annot.csv
	awk 'BEGIN{FS="\",\"" ; OFS="\t"}{ if($$0 ~ /^#/){next}else{print $$1,$$15,$$19} }' $< | sed 's/"//' | \
	awk 'BEGIN{FS="\t" ; OFS = "\t"}{ \
		if($$0 !~ /\/\/\//){ \
			print $$0 \
		}else{ \
			split($$2, gene, " /// ") ; split($$3, entrez, " /// ") ; \
			if(length(gene)!=length(entrez)){ \
				print $$1 > "error.probset" \
			}else{ \
				for(i=1 ; i<= length(gene) ; i++){print $$1,gene[i],entrez[i]} \
			} \
		} \
	}' > $@

#still with "---" and with multi-annotated probeset with entrez joined by ";"
gene_$(NA)_expandset_cleaned: gene_$(NA)_expandset
	cat $< | sed 's/\t/@/2' | collapsesets 2 | grep -v ";" | grep -v "\-\-\-" | sed 's/@/\t/' > $@

probe-entrez.map_entrez_annoted_expanded: probe-entrez.map_entrez_annoted
	expandsets 2 < $< > $@

gene_expression_matrix:
	perl /home/bioinfotree/prj/retta/local/src/expression_by_gene_multi_aassociation.pl -t probe-entrez.map_entrez_annoted_expanded /home/bioinfotree/task/micro_array/dataset/affymetrix/dmelanogaster/drosgenome1/expression_MAS5.row > $@
