#!/usr/bin/env python
from __future__ import with_statement, division

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from math import log
#from subprocess import Popen, PIPE
#from collections import defaultdict

def main():
	usage = format_usage('''
		%prog < MATRIX
MATRIX:
	1	A normalizzed frequency
	2	C normalizzed frequency
	3	G normalizzed frequency
	4	T normalizzed frequency
	''')
	parser = OptionParser(usage=usage)
	
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	entropy = [0,0,0,0]
	sites = 0
	for line in stdin:
		p = [ float(i) for i in safe_rstrip(line).split('\t')]
		#tokens = line.rstrip().split('\t')
		assert len(p) == 4
		sites += 1
		for i in (0,1,2,3):
			if p[i] > 0:
				entropy[i] += p[i]*log(p[i])

	print - sum(entropy) # sum over the different characters considered as indipendent systems


if __name__ == '__main__':
	main()

