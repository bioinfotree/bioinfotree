#!/bin/bash

set -o pipefail

if  [ "$1" == "" -o "$2" == "" ]; then
	echo "$0 REGIONS SNP_REDUCED ">&2;
	exit -1;
fi;

if ! [ -f $1 -a -f $2 ] ; then
	echo -e "usage: $0 REGIONS SNP_REDUCED \n regular file only, not stdin, pipe, or process substitution.">&2;
	exit -1;
fi;

#intersection non ha il -l ma le regioni prive di snp vengono ricuperate dal join
intersection  -- $1 $2 | cut -f 8 | symbol_count | sort -k 1,1 | join3_pl -- <(awk '{print $4,$2,$3}' $1 | symbol_length | sort -k 1,1) - | awk '{if(!$4){$4=0} print $1,$2,$4,$4/$2}'
