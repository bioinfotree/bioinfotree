#include <assert.h>
#include "mask.h"
#include <stdlib.h>

int mask_init(mask_t* const mask, unsigned int size)
{
	if (size == 0)
		return 0;
	
	mask->space = (unsigned char*)calloc(size, sizeof(char));
	if (!mask->space)
		return 0;
	
	mask->size = size;
	return 1;
}

void mask_free(mask_t* const mask)
{
	free(mask->space);
}

void mask_set(mask_t* const mask, const unsigned int start, const unsigned int stop, const char label)
{
#ifndef NDEBUG
	assert(start < mask->size);
	assert(stop <= mask->size);
	assert(label <= 8);
#endif
	
	const char pattern = 1 << (label-1);
	unsigned int i;
	for (i = start; i < stop; i++)
		mask->space[i] |= pattern;
}

int mask_iterator_init(mask_iterator_t* const it, const mask_t* const mask)
{
	it->mask = mask;
	it->pos = 0;
}

void mask_iterator_free(mask_iterator_t* const it)
{	
}

int mask_iterator_has_more(const mask_iterator_t* const it)
{
	return it->pos < it->mask->size;
}

void mask_iterator_advance(mask_iterator_t* const it)
{
#ifndef NDEBUG
	assert(mask_iterator_has_more(it));
#endif
	
	const unsigned char* const space = it->mask->space;
	const unsigned int size = it->mask->size;
	
	unsigned char pattern = 1 << 7;
	char label = 8;
	while (pattern != 0 && (space[it->pos] & pattern) == 0)
	{
		pattern >>= 1;
		label--;
	}
	
	it->span_start = it->pos++;
	it->span_label = label;
	if (pattern == 0)
	{
		while (it->pos < size && space[it->pos] == 0)
			it->pos++;
	}
	else
	{
		const unsigned char full = 0xff >> (8 - label);
		while (it->pos < size && (space[it->pos] & pattern) != 0 && space[it->pos] <= full)
			it->pos++;
	}
	
	it->span_stop = it->pos;
}

unsigned int mask_iterator_span_start(const mask_iterator_t* const it)
{
	return it->span_start;
}

unsigned int mask_iterator_span_stop(const mask_iterator_t* const it)
{
	return it->span_stop;
}

char mask_iterator_span_label(const mask_iterator_t* const it)
{
	return it->span_label;
}
