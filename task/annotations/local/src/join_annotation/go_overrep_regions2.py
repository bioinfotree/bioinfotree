#!/usr/bin/env python
from __future__ import with_statement

from itertools import groupby
from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import exit, stdin, stderr
#from vfork.io.colreader import Reader
from collections import defaultdict

class Hyper(object):
	def __init__(self, exename):
		self.exe = Popen(exename, stdin=PIPE, stdout=PIPE, close_fds=True, shell=False)
	
	def close(self):
		self.exe.stdin.close()
		self.exe.wait()
	
	def calc(self, N, M, n, x):
		print >>self.exe.stdin, '%d\t%d\t%d\t%d' % (N, M, n, x)
		self.exe.stdin.flush()
		
		tokens = self.exe.stdout.readline().split(None)
		assert len(tokens) == 5, repr(tokens)
		return float(tokens[4])

def bonferroni_correction(family_regions,go_descr):
	return len(family_regions)*len(go_descr)

def build_command(go_region_path):
	return 'intersection - ' + go_region_path + '| cut -f 8,9,11,12 | sort -k 2,2 | uniq';

def read_descriptions(filename):
	with file(filename, 'r') as fd:
		go_descr = {}
		
		for line in fd:
			tokens = line.rstrip().split('\t', 1)
			assert len(tokens) == 2
			go_descr[tokens[0]] = tokens[1]
			
	return go_descr

def conn_comp(pairs): # prende una lista, i.e. gli edge, e restituisce una lsita di set, i.e. le componenti connesse
	conn_comp = {}
	for a,b in pairs:
		conn_a = conn_comp.get(a, None)
		conn_b = conn_comp.get(b, None)
		
		if conn_a == conn_b and conn_a is not None:
			continue

		if conn_a is not None and conn_b is not None:
			for node, c in conn_comp.iteritems():
				if c == conn_b:
					conn_comp[node] = conn_a
		elif conn_a is not None:
				conn_comp[b] = conn_a
		elif conn_b is not None:
				conn_comp[a] = conn_b
		else:
			conn_comp[a]=a
			conn_comp[b]=a

	retval = defaultdict(set) 
	for node,c in conn_comp.iteritems():
		if node[0]=='g':
			retval[c].add(node)
	return retval.itervalues()
	
def update(region_gene, family, family_key_x, gene_keys, family_n):
	for c in conn_comp(region_gene):
		family_n[family].append(c)
		keys = set()
		for g in c:
			for k in gene_keys[g]:
				keys.add(k)
				# non ho ancora letto tutto, quindi gene_keys e` incompleto
				# ma lo assumo completo per quanto riguarda i geni
				# associati alla famiglia che ho gia` letto tutta
		for k in keys:
			family_key_x[family][k] += 1

def my_append(list,el):
	present = 0
	for l in list:
		if el == l:
			present = 1
			break
	if not present:
		list.append(el)

def compute_universe(all_conn_comp, family_conn_comp):
	universe = []
	for c1 in all_conn_comp:
		to_append = True
		for c2 in family_conn_comp:
			if c1 != c2 and len( c1 & c2 ) != 0:
				to_append = False
				break
		if to_append:
			universe.append(c1)
	return universe
	
def compute_M_keys(universe, gene_keys):
	M_keys = defaultdict( lambda: 0 )
	for c in universe:
		keys = set()
		for g in c:
			for k in gene_keys[g]:
				keys.add(k)
		for k in keys:
			M_keys[k] += 1
	return M_keys

def main():
	parser = OptionParser(usage='''%prog GO_REGION GO_DESCR_MAP <REGION_REGID_GROUPID

GO_REGION:
chr	b	e	strand	gene	GO

GO_DESCR_MAP:
GO:0000001	mitochondrion inheritance
GO:0000002	mitochondrial genome maintenance
GO:0000003	reproduction

STDIN:
chr	b	e	region_id	family_id

The Bonferroni correction is calculated as tot number of GO in go_descr * number of families whit at least 1 GO association.

The starting point of the program is:
	intersection -  go_region_path | cut -f 8,9,11,12 | sort -k 2,2 | uniq

''')
	parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=1e-4, help='do not report items with a p-value larger than CUTOFF', metavar='CUTOFF')
	parser.add_option('-p', '--dump-params', dest='params_file', help='dump hypergeometric parameters to FILE', metavar='FILE')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	elif options.cutoff < 0:
		exit('Invalid cutoff value.')
	
	go_descr = read_descriptions(args[1])
	
	#setup external process
	hyper = Hyper('ipergeo_file')
	proc = Popen(['/bin/bash', '-c', build_command(args[0])], shell=False, stdout=PIPE)

	region_gene     = set()
	family_n        = defaultdict( lambda: [] ) # conterra`, per ogni famiglia, la lista delle sue componenti connesse (lista di set di geni)
	gene_keys       = defaultdict( set )
	family_key_x    = defaultdict( lambda: defaultdict( lambda: 0))
	family_last 	= None
	all_conn_comp	= []


	########## leggo i dati e faccio i conteggi
	for line in proc.stdout:
		region, family, gene, key = line.rstrip().split('\t')

		gene = "g"+gene
		if region[0] == 'g':
			exit("region intentifiers cannot begin with 'g'");
		
		########## gestisco il cambio di famiglia
		if family_last is not None and family_last > family:
			exit("Disorder found")
		if family is not None and family_last != family:
			update(region_gene, family_last, family_key_x, gene_keys, family_n)
			region_gene = set()
		##########

		family_last = family
		region_gene.add( (region,gene) )
		gene_keys[gene].add(key)
	# ho finito di leggere

	# faccio i calcoli per l'ultima famiglia
	if(len(region_gene) > 0):
		update(region_gene, family, family_key_x, gene_keys, family_n)
		region_gene = set()
	##########
	bonferroni = bonferroni_correction(family_n, go_descr)
	#print '%d\t%d\t%d' % (len(family_n), len(go_descr), bonferroni)

	for set_c in family_n.itervalues():
		for c in set_c:
			my_append(all_conn_comp, c)
	
	for family, key_count in family_key_x.iteritems():
		
		#elimino dall'universo del set le con_con comp vietate
		universe = compute_universe(all_conn_comp, family_n[family])
		N = len(universe)
		
		M_keys = compute_M_keys(universe, gene_keys)
		
		n = len(family_n[family])
		for key, x in key_count.iteritems():
			M = M_keys[key]
			if n <= (N - M) + 1:
				#print (N, M, n, x)
				pvalue = hyper.calc(N, M, n, x)
			else:
				print >>stderr, "WARNING: n > (N - M) + 1 for %s\t%s\t%d\t%d\t%d\t%d\t%s" % (family, key, N, M, n, x, go_descr[key])
				continue
			bonferroni_pvalue = pvalue * bonferroni
			if pvalue < options.cutoff:
				print '%s\t%s\t%d\t%d\t%d\t%d\t%e\t%e\t%s' % (family, key, N, M, n, x, pvalue, bonferroni_pvalue, go_descr[key])



if __name__ == '__main__':
	main()
