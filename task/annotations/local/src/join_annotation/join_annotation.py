#!/usr/bin/env python
from __future__ import with_statement
from __future__ import division

from itertools import groupby
from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import exit, stdin, stderr, maxint
from vfork.io.colreader import Reader
from random import shuffle
from math import log, floor, fabs, exp, pow 
from collections import defaultdict

class Hyper(object):
	def __init__(self, exename):
		self.exe = Popen(exename, stdin=PIPE, stdout=PIPE, close_fds=True, shell=False)
	
	def close(self):
		self.exe.stdin.close()
		self.exe.wait()
	
	def calc(self, N, M, n, x):
		print >>self.exe.stdin, '%d\t%d\t%d\t%d' % (N, M, n, x)
		self.exe.stdin.flush()
		
		tokens = self.exe.stdout.readline().split(None)
		assert len(tokens) == 5, repr(tokens)
		return float(tokens[4])

def read_families(fd):
	reader = Reader(fd, '0s,1s', False)
	last_family_id = None
	while True:
		tokens = reader.readline()
		if tokens is None:
			break
		
		if last_family_id is not None and tokens[0] < last_family_id:
			exit('Disorder detected at line %d of column 1 of gene-family file.' % reader.lineno())
		
		last_family_id = tokens[0]
		yield tokens

def group_families(fd):
	families = []
	gene_universe = set()
	
	for family_id, genes_iter in groupby(read_families(fd), lambda tokens: tokens[0]):
		genes = set(g[1] for g in genes_iter)
		families.append( (family_id, genes) )
		gene_universe |= genes
	
	return families, gene_universe

def read_ens_go_map(filename, gene_universe):
	with file(filename, 'r') as fd:
		reader = Reader(fd, '0s,1s', False)
		ens_go = {}
		go_count = {}
		
		while True:
			tokens = reader.readline()
			if tokens is None:
				break
			
			if gene_universe is not None and tokens[0] not in gene_universe:
				continue
			
			ens_list = ens_go.get(tokens[0], [])
			if tokens[1] in ens_list:
				exit('Duplicate gene-GO association for: %s %s.' % (tokens[0], tokens[1]))

			ens_list.append(tokens[1])
			ens_go[tokens[0]] = ens_list
			
			go_count[tokens[1]] = go_count.get(tokens[1], 0) + 1
	
	return ens_go, go_count

def read_descriptions(filename):
	with file(filename, 'r') as fd:
		go_descr = {}
		
		for line in fd:
			tokens = line.rstrip().split('\t', 1)
			assert len(tokens) == 2
			go_descr[tokens[0]] = tokens[1]
			
	return go_descr

def assign_go(families, ens_go, gene_universe):
	print ens_go[100]
	def assign_words(genes):
		res = []
		for gene in genes:
			try:
				res.append((gene, ens_go[gene]))
			except KeyError:
				if gene in gene_universe:
					gene_universe.remove(gene)
		return res
	
	return [ (family_id, assign_words(genes)) for family_id, genes in families ]

def build_go_ens_map(family_ens_go):
	go_ens = {}
	for ens, gos in family_ens_go:
		for go in gos:
			ens_set = go_ens.get(go, set())
			if ens in ens_set:
				exit('Duplicate GO-gene association for: %s %s' % go, ens)
			ens_set.add(ens)
			go_ens[go] = ens_set
	
	return go_ens

def bonferroni_correction(families,go_count):
	#return len(families)*len(go_count)
	return sum(len(build_go_ens_map(family_ens_go)) for family_id, family_ens_go in families)

def compute_pvalues(families, N, go_count, go_descr, hyper, bonferroni, options, random=False):
	ret_val = []
	for family_id, family_ens_go in families:
		n = len(family_ens_go)
		go_ens = build_go_ens_map(family_ens_go)
		for go, ens_set in go_ens.iteritems():
			M = go_count[go]
			x = len(ens_set)

			if options.params_file is not None:
				print >>options.params_file, '%d\t%d\t%d\t%d' % (N, M, n, x)
			
			if n > N - M + 1:
				print >>stderr, '[WARNING] skipping hypergeometric computation'
				continue
			pvalue = hyper.calc(N, M, n, x)
			if not random:
				bonferroni_pvalue = pvalue * bonferroni
				ret_val.append( (family_id, go, N, M, n, x, pvalue, bonferroni_pvalue, go_descr[go],  ','.join(ens_set)) )	
			else:
				ret_val.append( (family_id, pvalue) )
	return ret_val

def update_histo_pvalues(histo_pvalues, best_pvalues, dp):
	for p in best_pvalues:
		log_p = log(p,10)
		x = fabs(log_p)
		x = floor(x/dp)
		histo_pvalues[x] += 1

def randomizze(families):
	a = []
	b = []
	for family_id, family_ens_go in families:
		for g_go in family_ens_go:
			a.append(family_id)
			b.append(g_go)
	shuffle(a)
	
	tmp = defaultdict(list)
	for k,v in zip(a,b):
		tmp[k].append(v)
	ret_val =[]
	for k,v in tmp.iteritems():
		ret_val.append( (k,v) )
	return ret_val

def find_best_pvalues(a):
	tmp = defaultdict(list)
	for k,v in a:
		tmp[k].append(v)
	return [ min(vals) for vals in tmp.itervalues() ]

def main():
	parser = OptionParser(usage='''%prog ENS_GO_MAP GO_DESCR_MAP <ENS_FAMILIES
ENS_FAMILIES: family_id	ENSG''')
	parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=maxint, help='do not report items with a *corrected* p-value larger than CUTOFF', metavar='CUTOFF')
	parser.add_option('-f', '--histo_file', dest='histo_file', default=None, help='file to store randomization histogram', metavar='CUTOFF')
	parser.add_option('-r', '--randomizations_rounds', type=int, dest='randomizzations', default=None, help='number of randomizzation to perform', metavar='NUMBER')
	parser.add_option('-p', '--dump-params', dest='params_file', help='dump hypergeometric parameters to FILE', metavar='FILE')
	parser.add_option('-u', '--universe', default=False, dest='universe', action='store_true', help='use all genes in GO as the universe')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	elif options.cutoff < 0:
		exit('Invalid cutoff value.')
	
	if options.histo_file is not None and options.randomizzations is None:
		exit('Pelase indicate a number of randomizzation (-r) to perform or omit -h option')
	if options.histo_file is None and options.randomizzations is not None:
		exit('Pelase indicate a file to store the randomizzation hisogram (-h) or omit -r option')
	
	families, gene_universe = group_families(stdin)
	ens_go, go_count = read_ens_go_map(args[0], gene_universe if not options.universe else None)

	go_descr = read_descriptions(args[1])
	families = assign_go(families, ens_go, gene_universe)
	bonferroni = bonferroni_correction(families, go_count)
	
	#N = len(gene_universe)
	N = len(ens_go)
	hyper = Hyper('ipergeo')
	if options.params_file is not None:
		options.params_file = file(options.params_file, 'w')

	results = compute_pvalues(families, N, go_count, go_descr, hyper, bonferroni, options)
	for r in results:
		if options.cutoff is None or r[7] < options.cutoff:
			print '%s\t%s\t%d\t%d\t%d\t%d\t%e\t%e\t%s\t%s' % r

	#randomizzation
	if options.histo_file is None:
		return

	histo_fd = file(options.histo_file,'w')
	histo_pvalues = defaultdict( lambda: 0 )
	maxlogp = 100
	minlogp = 0
	nbin = 1000
	dp = (maxlogp - minlogp)/nbin

	round = 1
	while round <= options.randomizzations:
		families = randomizze(families)
		tmp_results = compute_pvalues(families, N, go_count, go_descr, hyper, bonferroni, options, True)
		best_pvalues = find_best_pvalues(tmp_results)
		update_histo_pvalues(histo_pvalues, best_pvalues, dp)
		if round%100==0 or round == options.randomizzations:
			for i in xrange(0, nbin):
				print >>histo_fd, '%e\t%u' % (pow(10,-i*dp), histo_pvalues[i])
		round+=1
		
	if options.params_file is not None:
		options.params_file.close()
	hyper.close()

if __name__ == '__main__':
	main()
