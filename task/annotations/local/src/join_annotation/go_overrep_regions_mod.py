#!/usr/bin/env python
from __future__ import with_statement

from itertools import groupby
from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import exit, stdin, stderr
#from vfork.io.colreader import Reader
from collections import defaultdict

class Hyper(object):
	def __init__(self, exename):
		self.exe = Popen(exename, stdin=PIPE, stdout=PIPE, close_fds=True, shell=False)
	
	def close(self):
		self.exe.stdin.close()
		self.exe.wait()
	
	def calc(self, N, M, n, x):
		print >>self.exe.stdin, '%d\t%d\t%d\t%d' % (N, M, n, x)
		self.exe.stdin.flush()
		
		tokens = self.exe.stdout.readline().split(None)
		assert len(tokens) == 5, repr(tokens)
		return float(tokens[4])

def bonferroni_correction(family_regions,go_descr):
	return len(family_regions)*len(go_descr)

def build_command(go_region_path):
	return 'intersection - ' + go_region_path + '| cut -f 8,9,12 | sort | uniq';

def read_descriptions(filename):
	with file(filename, 'r') as fd:
		go_descr = {}
		
		for line in fd:
			tokens = line.rstrip().split('\t', 1)
			assert len(tokens) == 2
			go_descr[tokens[0]] = tokens[1]
			
	return go_descr

def main():
	parser = OptionParser(usage='''%prog GO_REGION GO_DESCR_MAP <REGION_REGID_GROUPID

GO_REGION:
chr	b	e	strand	gene	GO

GO_DESCR_MAP:
GO:0000001	mitochondrion inheritance
GO:0000002	mitochondrial genome maintenance
GO:0000003	reproduction

STDIN:
chr	b	e	region_id	family_id

The Bonferroni correction is calculated as tot number of GO in go_descr * number of families whit at least 1 GO association.

The starting point of the program is:
	intersection -  go_region_path + | cut -f 8,9,12 | sort | uniq
the uniq assert that redundant association of the same GO key to the same region are removed.
''')
	parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='do not report items with a *corrected* p-value larger than CUTOFF', metavar='CUTOFF')
	parser.add_option('-p', '--dump-params', dest='params_file', help='dump hypergeometric parameters to FILE', metavar='FILE')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	elif options.cutoff < 0:
		exit('Invalid cutoff value.')
	
	go_descr = read_descriptions(args[1])
	
	#setup external process
	hyper = Hyper('ipergeo_file')
	proc = Popen(['/bin/bash', '-c', build_command(args[0])], shell=False, stdout=PIPE)
	

	#read intersection output
	regions = set()
	family_regions = defaultdict(set)
	key_count = defaultdict(int)
	family_key_count = defaultdict(lambda: defaultdict(dict))

	for line in proc.stdout:
		region, family, key = line.rstrip().split('\t')
		
		regions.add(region)
		family_regions[family].add(region)
		key_count[key] += 1
		family_key_count[family][key] = family_key_count[family].setdefault(key, 0) + 1
	

	bonferroni = bonferroni_correction(family_regions, go_descr)
	#print '%d\t%d\t%d' % (len(family_regions), len(go_descr), bonferroni)
	
	
	N = len(regions)

	for family, keys in family_key_count.iteritems():
		n = len(family_regions[family])

		for key, x in keys.iteritems():
			M = key_count[key]
			if n <= (N - M) + 1:
				pvalue = hyper.calc(N, M, n, x)
			else:
				print >>stderr, "WARNING: n > (N - M) + 1 for %s\t%s\t%d\t%d\t%d\t%d\t%s" % (family, key, N, M, n, x, go_descr[key])
				continue
			bonferroni_pvalue = pvalue * bonferroni
			if bonferroni_pvalue < options.cutoff:
				print '%s\t%s\t%d\t%d\t%d\t%d\t%e\t%e\t%s' % (family, key, N, M, n, x, pvalue, bonferroni_pvalue, go_descr[key])
	

if __name__ == '__main__':
	main()
