#!/usr/bin/env python
#
# Copyright 2010 Gabriele Sales <grbsales@gmail.com>

from cStringIO import StringIO
from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import stdin, stdout
from tempfile import TemporaryFile
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter
from vfork.sequence import reverse_complement
from vfork.util import exit, format_usage
import re


def load_structures(fd, chr_filter):
    structures = []

    for header, lines in MultipleBlockStreamingReader(fd, join_lines=False):
        tokens = header.split('\t')
        if len(tokens) != 3:
            exit('Malformed header: ' + header)

        gene_id, chr, strand = tokens
        try:
            strand = int(strand)
        except ValueError:
            exit('Invalid strand for gene "%s": %s' % (gene_id, strand))

        if chr_filter is not None and chr_filter.search(chr) is None:
            continue

        exons = []
        for line in lines:
            try:
                exon = [ int(c) for c in line.split('\t') ]
                if len(exon) != 2 or exon[0] >= exon[1]: raise ValueError
                exons.append(tuple(exon))
            except ValueError:
                exit('Invalid coordinates for gene "%s": %s' % (gene_id, line))

        structures.append((chr, exons[0][0], gene_id, strand, exons))

    return structures

def iter_gene_blocks(fd):
    gene = strand = seq = None

    for header, exon in MultipleBlockStreamingReader(fd, join_lines=True):
        tokens = header.split(':')
        if len(tokens) > 1:
            if gene is not None:
                yield gene, strand, seq.getvalue()

            gene = tokens[0]
            strand = int(tokens[1])
            seq = StringIO()

        seq.write(exon)

    if gene is not None:
        yield gene, strand, seq.getvalue()


def main():
    parser = OptionParser(usage=format_usage('''
	Usage: %prog GENOME_DIR <GENE_STRUCTURES >GENE_FA

	Extracts (spliced) gene sequences from chromosomes and
	from the description of gene structures.

	Genes are defined, here, as the union of their transcripts.
    '''))
    parser.add_option('-f', '--chr-filter', dest='chr_filter', default=None, help='process only chromosomes matching the provided regular expression', metavar='RX')
    options, args = parser.parse_args()
    if len(args) != 1:
        exit('Unexpected argument number.')

    if options.chr_filter is None:
        chr_filter = None
    else:
        try:
            chr_filter = re.compile(options.chr_filter)
        except re.error:
            exit('Malformed chromosome filter: ' + options.chr_filter)

    structures = load_structures(stdin, chr_filter)
    structures.sort()

    with TemporaryFile() as coord_fd:
        for chr, _, gene_id, strand, exons in structures:
            for idx, (start, stop) in enumerate(exons):
                if idx == 0:
                    label = '%s:%s' % (gene_id, strand)
                else:
                    label = gene_id

                print >>coord_fd, '%s\t%s\t%d\t%d' % (label, chr, start, stop)

        coord_fd.flush()
        coord_fd.seek(0)

        proc = Popen(['seqlist2fasta', args[0]], stdin=coord_fd, stdout=PIPE, close_fds=True)
        w = MultipleBlockWriter(stdout)
        for gene, strand, seq in iter_gene_blocks(proc.stdout):
            w.write_header(gene)
            if strand < 0: seq = reverse_complement(seq)
            w.write_sequence(seq)

        w.flush()
        if proc.wait() != 0:
            exit('Error retrieving exon sequences via seqlist2fasta.')


if __name__ == '__main__':
    main()
