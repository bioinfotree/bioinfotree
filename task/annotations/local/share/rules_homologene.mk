include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk
HOMOLOGENE_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/homologene
GENE_INFO = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/$(VERSION_INFO)/gene_info.gz
SPECIES1_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES1)/$(VERSION)
SPECIES2_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES2)/$(VERSION)
NCBI_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/20081019

homologene.data: 
	wget ftp://ftp.ncbi.nih.gov/pub/HomoloGene/$(BUILD)/$@

.META: homologene.data
	1 HID (HomoloGene group id)
	2 Taxonomy ID
	3 Gene ID
	4 Gene Symbol
	5 Protein gi
	6 Protein accession

human_mouse_cluster_with_entrez.sort: homologene.data
	perl -ane 'print "$$F[0]\t$$F[1]\t$$F[2]\t$$F[3]\n" if ($$F[1] == $(TAX1) || $$F[1] == $(TAX2))' $< \
	| sort -k 2,2n -k 3,3n > $@

Entrez_Hs_gene_with_Mm_orth_uniq: human_mouse_cluster_with_entrez.sort
	Hs_gene_with_Mm_orth $< $(TAX1) $(TAX2) | sort -n | uniq > $@

ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous: $(SPECIES1_DIR)/gene-homologous.map.gz $(SPECIES1_DIR)/gene-entrez.map.gz $(SPECIES2_DIR)/gene-entrez.map.gz
	zcat $< | bawk '$$6=="$(SPECIES_HOMOL)"' | translate -a -d -j -k <(zcat $^2 |bawk '{print $$0}' | sort | uniq) 2 \
	| translate -a -d -j -k <(zcat $^3 | bawk '{print $$0}' | sort | uniq) 5 > $@

ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous_only_entrez: ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous
	cut -f 3,6 $< | sort -k1,1 -k2,2 | uniq > $@

ENSEMBL_entrez_Hs: ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous
	perl -ane 'print "$$F[2]\n"' $< | sort | uniq > $@

Entrez_Hs_gene_with_Mm_orth_uniq_1_link: Entrez_Hs_gene_with_Mm_orth_uniq ENSEMBL_entrez_Hs
	cat $< $^2 | sort -n | uniq -c | perl -ane 'print "$$F[1]\n" if ($$F[0]==1)' > $@

human_entrez_to_add_from_ensembl: ENSEMBL_entrez_Hs Entrez_Hs_gene_with_Mm_orth_uniq_1_link
	cat $< $^2 | sort -n | uniq -c | perl -ane 'print "$$F[1]\n" if ($$F[0]==2)' > $@

homologene-ensembl.data: Entrez_Hs_gene_with_Mm_orth_uniq homologene.data human_entrez_to_add_from_ensembl ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous_only_entrez
	zcat $(GENE_INFO) | perl -ane 'print "$$F[1]\t$$F[2]\n" if ($$F[0] == $(TAX1) || $$F[0] == $(TAX2))' > gene.info.tmp
	join_homologene_ensembl $< $^2 $(TAX1) $(TAX2) $^3 $^4 gene.info.tmp > $@
	rm gene.info.tmp

homologene-ensembl.data_complete_union: homologene.data ENSMUSP_ENSP_Entrez_ENSEMBL_orthologous_only_entrez $(NCBI_DIR)/gene_history.gz
	zcat $^3 | perl -ane 'print if ($$F[0] == $(TAX1) || $$F[0] == $(TAX2))' > gene_history.tmp
	convert_homologene $< $(TAX1) $(TAX2) > human_mouse_clusters
	cat human_mouse_clusters $^2 | sort -nk1,1 -nk2,2 | uniq > homologene-ensembl.data_complete_union.tmp
	zcat $(GENE_INFO) | perl -ane 'print "$$F[1]\t$$F[2]\t$$F[0]\n" if ($$F[0] == $(TAX1) || $$F[0] == $(TAX2))' > gene.info.tmp
	construct_orth homologene-ensembl.data_complete_union.tmp gene.info.tmp $(TAX1) $(TAX2) gene_history.tmp > $@

#	cat human_mouse_clusters $^2 | sort -nk1,1 -nk2,2 | uniq -c | perl -ane 'print "$$F[1]\t$$F[2]\n" if ($$F[0] == 1)' > ENS_Homol.tmp
#	cat ENS_Homol.tmp $^2 | sort -nk1,1 -nk2,2 | uniq -c | perl -ane 'print "$$F[1]\t$$F[2]\n" if ($$F[0] == 2)' > ENS-to-add.tmp
	

check_entrez:	homologene.data_02
	check_homologene_cluster $(SPECIES) $(ENTREZ) $<
