include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

ALL += inclusive_annotation


MYSQL ?= mysql -u ugo

go_daily-termdb-tables.tar.gz: 
	wget -O $@.tmp http://archive.geneontology.org/latest-termdb/$@
	mv $@.tmp $@

all: go.term go.alt go.obs go.anc

go.alt:	go.term
go.obs:	go.term
go.anc:	go.term

go_daily-termdb-tables: go_daily-termdb-tables.tar.gz
	rm -rf  go_daily-termdb-tables
	tar -xvzf go_daily-termdb-tables.tar.gz

go.term: go_daily-termdb-tables
	cat go_daily-termdb-tables/*.sql | $(MYSQL) ipg
	mysqlimport -u ugo -L ipg go_daily-termdb-tables/*.txt
	go_tables -d ipg
