gene-readable.map.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_readable_75.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip > $@
	rm $@_query.xml

.META: gene-readable.map.gz
	1  gene_id      ENSG00000208234
	2  gene_name    AC019043.8
	3  description  cippo lippo
	4  biotype      scRNA_pseudogene
	5  status       NOVEL
