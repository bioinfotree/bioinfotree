# Copyright 2010,2014 Gabriele Sales <gbrsales@gmail.com>

VERSION ?= 2010_08

url.mk:
	( \
	  echo -n 'BASE_URL:='; \
	  uniprot_url_helper $(VERSION); \
	) >$@

include url.mk


define download
wget -O- '$1' \
| gunzip \
| pixz -9e -p6 >$@
endef


idmapping.xz:
	$(call download,$(BASE_URL)/knowledgebase/idmapping/idmapping.dat.gz)

.META: idmapping.xz
	1  uniprot_id  Q197F8
	2  db          EMBL
	3  db_id       DQ643392


idmapping_selected.xz:
	$(call download,$(BASE_URL)/knowledgebase/idmapping/idmapping_selected.tab.gz)

.META: idmapping_selected.xz
	1   UniProtKB_AC
	2   UniProtKB_ID
	3   GeneID (EntrezGene)
	4   RefSeq
	5   GI
	6   PDB
	7   GO
	8   IPI
	9   UniRef100
	10  UniRef90
	11  UniRef50
	12  UniParc
	13  PIR
	14  NCBI_taxon
	15  MIM
	16  UniGene
	17  PubMed
	18  EMBL
	19  EMBL_CDS
	20  Ensembl
	21  Ensembl_TRS
	22  Ensembl_PRO
	23  Additional PubMed


sprot.xml.xz:
	$(call download,$(BASE_URL)/knowledgebase/complete/uniprot_sprot.xml.gz)

trembl.xml.xz:
	$(call download,$(BASE_URL)/knowledgebase/complete/uniprot_trembl.xml.gz)


ALL   += idmapping.xz sprot.xml.xz trembl.xml.xz
CLEAN += url.mk idmapping_selected.xz
