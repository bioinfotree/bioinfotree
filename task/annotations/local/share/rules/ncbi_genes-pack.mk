# Copyright 2008 Molineris Ivan <ivan.molineris@gmail.com>
# Copyright 2008-2009,2013,2016 Gabriele Sales <gbrsales@gmail.com>

NCBI_URL ?= ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/
QUIET    ?= no


define check_date
	#@set -e; \
	#date="$$(ftp_mdate '$(NCBI_URL)/$*')"; \
	#if [[ $$date -gt $(VERSION) ]]; then \
#		echo "Version mismatch: remote data is more recent than this dataset." >&2; \
#		exit 1; \
#	fi
endef

ifeq ($(QUIET),yes)
WGET_OPTS := --no-verbose
else
WGET_OPTS :=
endif



GZ    := gene2accession.gz gene2go.gz gene2pubmed.gz \
         gene2refseq.gz gene_group.gz gene_history.gz \
         gene_info.gz gene_refseq_uniprotkb_collab.gz

$(GZ:.gz=.xz): %.xz:
	$(call check_date)
	wget -O- $(WGET_OPTS) '$(NCBI_URL)/$*.gz' \
	| zcat \
	| unhead \
	| xz -9 >$@


PLAIN := gene2sts gene2unigene go_process.xml

$(addsuffix .xz,$(PLAIN)): %.xz:
	$(call check_date)
	wget -O- $(WGET_OPTS) '$(NCBI_URL)/$*' \
	| unhead \
	| xz -9 >$@

gene2ensembl.gz:
	$(call check_date)
	wget -O $@ '$(NCBI_URL)/gene2ensembl.gz'

mim2gene.xz:
	$(call check_date)
	wget -O- $(WGET_OPTS) '$(NCBI_URL)/mim2gene_medgen' \
	| unhead \
	| xz -9 >$@


.META: gene2accession.xz
	1   tax_id                                   9
	2   gene_id                                  1246500
	3   status                                   -
	4   rna_nucleotide_accession         -
	5   rna_nucleotide_gi                        -
	6   protein_accession                AAD12597.1
	7   protein_gi                               3282737
	8   genomic_nucleotide_accession     AF041837.1
	9   genomic_nucleotide_gi                    3282736
	10  start_position_on_the_genomic_accession  -
	11  end_position_on_the_genomic_accession    -
	12  orientation                              ?
	13  assembly                                 -

.META: gene2go.xz
	1  tax_id     3702
	2  gene_id    814629
	3  go_id      GO:0003676
	4  evidence   IEA
	5  qualifier  -
	6  go_term    nucleic acid binding
	7  pubmed     -
	8  category   Function

.META: gene2pubmed.xz
	1  tax_id     9
	2  gene_id    1246500
	3  pubmed_id  9873079

.META: gene2refseq.xz
	1   tax_id                                   9
	2   gene_id                                  1246500
	3   status                                   PROVISIONAL
	4   rna_nucleotide_accession         -
	5   rna_nucleotide_gi                        -
	6   protein_accession                NP_047184.1
	7   protein_gi                               10954455
	8   genomic_nucleotide_accession     NC_001911.1
	9   genomic_nucleotide_gi                    10954454
	10  start_position_on_the_genomic_accession  348
	11  end_position_on_the_genomic_accession    1190
	12  orientation                              -
	13  assembly                                 -

.META: gene_group.xz
	1  tax_id         7425
	2  gene_id        100116762
	3  relationship   Related functional gene
	4  other_tax_id   7425
	5  other_gene_id  100122024

.META: gene_history.xz
	1  tax_id                9
	2  gene_id               -
	3  discontinued_gene_id  1246494
	4  discontinued_symbol   repA1
	5  discontinue_date      20031113

.META: gene_info.xz
	1   tax_id                                 7
	2   gene_id                                5692769
	3   symbol                                 NEWENTRY
	4   locus_tag                              -
	5   synonyms                               -
	6   db_xrefs                               -
	7   chromosome                             -
	8   map_location                           -
	9   description                            Record to support submission of GeneRIFs [...]
	10  type_of_gene                           other
	11  symbol_from_nomenclature_authority     -
	12  full_name_from_nomenclature_authority  -
	13  nomenclature_status                    -
	14  other_designations                     -
	15  modification_date                      20071023

.META: gene_refseq_uniprotkb_collab.xz
	1  ncbi_protein_accession       AP_000064
	2  uniprotkb_protein_accession  Q65958

.META: gene2sts.xz
	1  unists   5320
	2  gene_id  65536

.META: gene2unigene.xz
	1  gene_id          1268433
	2  unigene_cluster  Aga.201

.META: mim2gene.xz
	1  mim_number  100300
	2  gene_id     100188340
	3  type        gene
	4  source      GeneMap
	5  medgen_cui  C3149220


ALL += $(GZ:.gz=.xz) $(addsuffix .xz,$(PLAIN)) mim2gene.xz

.META: gene2ensembl.gz
	1	tax_id
	2	GeneID
	3	Ensembl_gene_identifier
	4	RNA_nucleotide_accession
	5	Ensembl_rna_identifier
	6	protein_accession
	7	Ensembl_protein_identifier

hsapiens.entrez2refseq_rna_nucleotide_accession.map.xz: gene2refseq.xz
	bawk '$$tax_id==9606 && $$rna_nucleotide_accession!="-" {print $$gene_id,$$rna_nucleotide_accession}' $< | perl -pe 's/\.\d+$$//' | xz > $@

hsapiens.entrez2refseq_rna_nucleotide_accession2esng.map.gz: gene2ensembl.gz hsapiens.entrez2refseq_rna_nucleotide_accession.map.xz
	bawk '$$tax_id==9606 {print $$GeneID,$$Ensembl_gene_identifier}' $< | translate -a -d -j -i <(xzcat $^2) 1 | gzip > $@

hsapiens.entrez2refseq_rna_nucleotide_accession2symbol.map.xz: hsapiens.entrez2refseq_rna_nucleotide_accession.map.xz gene_info.xz
	xzcat $< | translate -a -r <(xzcat $^2 | cut -f 2,3) 1 | xz > $@
