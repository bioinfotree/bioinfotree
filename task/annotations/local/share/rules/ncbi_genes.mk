# Copyright 2009-2010 Paolo Martini <paolo.cavei@gmail.com>
# Copyright 2010,2013 Gabriele Sales <gbrsales@gmail.com>

PACK_DIR    := $(TASK_ROOT)/dataset/ncbi/genes/pack/$(VERSION)
SPECIES_MAP := $(TASK_ROOT)/local/share/species-ncbi_taxon.map
TAXON_ID    ?= $$(translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")

extern $(PACK_DIR)/gene_info.xz   as GENE_INFO
extern $(PACK_DIR)/gene2refseq.xz as GENE2REFSEQ


genes.xz: $(GENE_INFO)
	bawk '$$tax_id=="'$(TAXON_ID)'"' $< \
	| cut -f 2- \
	| pixz -9e >$@

.META: genes.xz
	1   gene_id                                5692769
	2   symbol                                 NEWENTRY
	3   locus_tag                              -
	4   synonyms                               -
	5   db_xrefs                               -
	6   chromosome                             -
	7   map_location                           -
	8   description                            Record to support submission of GeneRIFs [...]
	9   type_of_gene                           other
	10  symbol_from_nomenclature_authority     -
	11  full_name_from_nomenclature_authority  -
	12  nomenclature_status                    -
	13  other_designations                     -
	14  modification_date                      20071023


entrez-refseq.map.xz: $(GENE2REFSEQ)
	bawk -v T=$(TAXON_ID) '$$tax_id==T {if($$4!="-"){print $$2,$$4;} if($$6!="-"){print $$2,$$6;} if($$8!="-"){print $$2,$$8}}' $< \
	| tr "." "\t" \
	| bsort -u \
	| pixz -9e > $@

.META: entrez-refseq.map.xz
	1	gene_id		1
	2	refseq_id	NP_570602
	3	version		2


entrez-ensembl.map.xz: genes.xz
	bawk '$$db_xrefs~/Ensembl/ {print $$gene_id, $$db_xrefs}' $< \
	| perl -lane '$$,="\t"; @m = m/Ensembl:([^\|\s]+)/g; for(@m){print $$F[0],$$_}' \
	| bsort -k1,1n -u \
	| pixz -9e >$@

.META: entrez-ensembl.map.xz
	1	entrez_gene_id
	2	ensembl_gene_id


ALL   += genes.xz
CLEAN += entrez-ensembl.map.xz entrez-refseq.map.xz


ifdef ENSEMBL_VERSION
extern $(TASK_ROOT)/dataset/ensembl/$(SPECIES)/$(ENSEMBL_VERSION)/gene-entrez.map.gz as ENSG2ENTREZ

CLEAN += entrez-ensembl.inclusive-map.xz

entrez-ensembl.inclusive-map.xz: entrez-ensembl.map.xz $(ENSG2ENTREZ)
	( xzcat $<; \
	  zcat $^2 | select_columns 2 1 ) \
	| bsort -nu \
	| pixz -9e > $@
endif
