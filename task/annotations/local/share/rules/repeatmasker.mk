# Copyright 2014 Davide Risso <risso.davide@gmail.com>
# Copyright 2014-2015 Gabriele Sales <gbrsales@gmail.com>

URL     ?= http://www.repeatmasker.org/genomes/mm10/RepeatMasker-rm403-db20130422/mm10.fa.out.gz
SEQ_DIR ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/mmusculus/73_nrm


repbase.out.gz:
	wget -O $@ $(URL)

repbase.gtf.xz: repbase.out.gz
	!threads
	zcat $< \
	| unhead -n 3 \
	| gawk -F " " ' $$5 !~ "random" {sub(/chr/, "", $$5); sub(/C/, "-", $$9); sub(/M/, "MT", $$5); \
	                print $$5 "\t" $$6 "\t" $$7 "\t" $$10 "\t" $$9 "\t" $$11 "\t" ($$2+$$3+$$4)/3}' \
	| bawk '{printf("%s\t%d\t%d\t%s\t%s\t%s\t%f\tRB%.7d\tRB%.7d\n", $$1, $$2, $$3, $$4, $$5, $$6, $$7, NR, NR)}' \
	| tab2gff feature:exon strand:5 family:6 gene_id:8 transcript_id:9 repeat:4 divergence:7 \
	| pixz -9 -p $$THREADNUM >$@


repbase.map.xz: repbase.gtf.xz
	!threads
	bawk '{split($$9, a, /"/); if($$7==".") $$7="+"; print a[8], $$1, $$4, $$5, $$7, a[2], a[4], a[6]}' $< \
	| pixz -9 -p $$THREADNUM >$@

.META: repbase.map.xz
	1  id          RB0000001
	2  chr         1
	3  start       3000001
	4  stop        3000097
	5  strand      -
	6  class       L1_Mus3
	7  family      LINE/L1
	8  divergence  4.033330


repbase.fa: repbase.map.xz
	bawk '$$2 ~ "^[1-9XYM]" && $$2 !~ "_"' $< \
	| seqlist2fasta $(SEQ_DIR) >$@


ALL          += repbase.gtf.xz
CLEAN        += repbase.map.xz repbase.fa
INTERMEDIATE += repbase.out.gz
