# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>

# Certain genomic regions encode different, overlapping genes.
# In particular, the same sequence may encode one gene in one
# direction and another in the anti-sense.
# These rules build a list of all the regions where such an
# overlap occurs. The output regions are merged, so that any
# two rows refer to disjoint ranges.


ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/64

extern $(ANNOTATION_DIR)/exons.gz as EXONS
extern $(ANNOTATION_DIR)/exon-transcript.map.gz as EXON_TRANSCRIPT_MAP
extern $(ANNOTATION_DIR)/transcript-gene.map.gz as TRANSCRIPT_GENE_MAP


coords: $(EXONS)
	zcat $< \
	| select_columns 2 3 4 1 \
	| bsort -k1,1 -k2,2n >$@

regions.gz: coords $(EXON_TRANSCRIPT_MAP) $(TRANSCRIPT_GENE_MAP)
	intersection $< $< \
	| bawk '$$8!=$$9 {print $$1,$$2,$$3,$$8,$$9}' \
	| translate -d <(zcat $^2) 4 5 \
	| expandsets 4 5 \
	| translate <(zcat $^3) 4 5 \
	| bawk '$$4!=$$5' \
	| bsort -u -k1,1 -k2,2n \
	| region_union --allow-duplicates --region-id-glue=';' \
	| bawk '{ n = split($$4, genes, ";"); \   * make gene names printed by *
	          delete geneset; \               * region_union unique        *
	          for (i = 1; i <= n; i++) { geneset[genes[i]] = 1; } \
	          first = 1; \
	          for (g in geneset) { \
	            if (first) { labels = g; first = 0 } \
	            else       { labels = labels ";" g } \
	          } \
	          print $$1,$$2,$$3,labels }' \
	| gzip -9 >$@

.META: regions.gz
	1  chr    1
	2  start  14362
	3  stop   14409
	4  genes  ENSG00000227232;ENSG00000223972


ALL          += regions.gz
INTERMEDIATE += coords
