#!/usr/bin/env python
from codecs import EncodedFile
from optparse import OptionParser
from sys import stdin
from vfork.util import exit
from xml.sax import parse
from xml.sax.handler import ContentHandler

class Flattener(ContentHandler):
	def __init__(self, fields):
		self.fields = fields
	
	def startDocument(self):
		self.level = 0
	
	def startElement(self, name, attrs):
		self.level += 1
		if self.level == 2:
			self.cols = [''] * len(self.fields)
		elif self.level == 3:
			try:
				self.field_idx = self.fields.index(name)
				self.content = ''
			except ValueError:
				self.field_idx = None
	
	def endElement(self, name):
		self.level -= 1
		if self.level == 1:
			print '\t'.join(self.cols).encode('utf8')
		elif self.level == 2 and self.field_idx is not None:
			self.cols[self.field_idx] = self.content.replace('\n', ' ')
	
	def characters(self, content):
		if self.level == 3 and self.field_idx is not None:
			if '\t' in content:
				exit('Found tab in content.')
			self.content += content

def main():
	parser = OptionParser(usage='%prog FIELDS... <XML')
	options, args = parser.parse_args()

	if len(args) == 0:
		exit('Unexpected argument number.')
	
	parse(EncodedFile(stdin, 'utf8'), Flattener(args))

if __name__ == '__main__':
	main()
