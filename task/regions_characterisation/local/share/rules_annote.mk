###############################################
############### ANNOTATION ####################
###############################################

GO_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/GO/$(SPECIES)/$(GO_ENS_VERSION)
GO_CUT_BONFERRONI ?= 0.05


################ VETTORINI ####################

# A. annotazione sulle regioni del genoma ben allineate con un simbolo 

%.parsed3.annote: %.parsed3 $(addprefix $(ANNOTATION_DIR)/coords_nonredundant.,$(ALL_CHR))
	repeat_group_pipe 'echo ">$$1"; cut -f -3' 4 < $< \
	| repeat_fasta_pipe -n '\
		echo ">$$HEADER"; sort -k 1,1 -k 2,2n \
		| repeat_group_pipe '\'' \
			intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$1 \
	                | annote_vettorini $(NORMALIZE_VETTORINI) -l "$(COORDS_NONREDUNDANT)" \
		'\'' 1; \
	' > $@

%.parsed3.annote.bool: %.parsed3.annote
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; awk "BEGIN{OFS=\"\t\"} {for (i=4; i<=NF; i++) {if (\$$i>0) {\$$i=1}} print}" \
	' < $< > $@

%.parsed3.annote.sum: %.parsed3.annote.bool
	grep -v '>' $< | cut -f 4- | sum_column -n > $@

%.parsed3.annote.bool.overrepr: %.parsed3.annote.bool %.parsed3.annote.sum
	repeat_fasta_pipe -n "\
		echo -ne \"\$$HEADER\t\"; \
		cut -f 4- | sum_column -c | $(BIN_DIR_ANNOTATION)/annot_binomial.pl $(word 2,$^) \
	" < $< > $@

%.annote.bool.overrepr.Bonferroni: %.annote.bool.overrepr
	correzione=$$(($$(wc -l < $<) * 7)); \
	perl -lane "\$$\=\"\n\"; \$$,=\"\t\"; my \$$c=shift @F; @F = map {\$$_= \$$_ * $$correzione; \$$_=sprintf (\"\%.2e\",\$$_) } @F; print \$$c,@F" < $< > $@
	# per ogni label (COORDS_NONREDUNDANT): pvalue Bonferroni corretto, segno - se coda sx, segno + se coda dx

%.parsed3.annote.bool.overrepr.Bonferroni.db: %.parsed3.annote.bool.overrepr.Bonferroni
	$(BIN_DIR)/best_annotations_vettorini $(VETTORINI_CUTOFF) $< > $@

# B. annotazione sulle regioni di partenza 

regions.annote: $(REGIONS_LIST) $(addprefix $(ANNOTATION_DIR)/coords_nonredundant.,$(ALL_CHR))
	cut -f 2- $< \
	| sort -k 1,1 -k 2,2n \
	| repeat_group_pipe '\
		intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$1 \
		| annote_vettorini $(NORMALIZE_VETTORINI) -l "$(COORDS_NONREDUNDANT)" \
	' 1 > $@

regions.annote.bool: regions.annote
	awk 'BEGIN{OFS="\t"} {for (i=4; i<=NF; i++) {if ($$i>0) {$$i=1}} print}' \
	< $< > $@

regions.background.annote.sum: $(BACKGROUND)
	sort -k 1,1 -k 2,2n $< \
	| repeat_group_pipe '\
		intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$1 \
		| annote_vettorini -n -l "$(COORDS_NONREDUNDANT)" \
	' 1 \
	| cut -f 4- | sum_column -n > $@

regions.annote.bool.overrepr: regions.annote.bool regions.background.annote.sum
	echo -ne "regions\t" > $@;
	cut -f 4- $< | sum_column -c \
	| $(BIN_DIR_ANNOTATION)/annot_binomial.pl $(word 2,$^) >> $@

#regions.annote.bool.overrepr.Bonferroni: regions.annote.bool.overrepr
#	correzione=$$(($$(wc -l < $<) * 7)); \
#	perl -lane "\$$\=\"\n\"; \$$,=\"\t\"; @F = map {\$$_= \$$_ * $$correzione; \$$_=sprintf (\"\%.2e\",\$$_) } @F; print @F" < $< > $@
#	# per ogni label (COORDS_NONREDUNDANT): pvalue Bonferroni corretto con segno - se coda sx, segno + se coda dx

###############################################
#
#	Anomalie cromosomi
#

%.parsed3.chr_distrib: %.parsed3
	sort -k 4,4n -k 1,1 $< | awk '{print $$4 "\t" $$1}' | uniq -c | awk '{print $$2 "\t" $$3 "\t" $$1}' > $@

%.parsed3.chr_distrib.overrepr: %.parsed3
	sort -k 4,4n $<  | $(BIN_DIR)/chr_stats_family.pl \
	| Ipergeo_file \
	| awk 'BEGIN{OFS="\t"}{print $$7,$$8,$$1,$$2,$$3,$$4,$$5*$$6}'> $@
	# formato output:
	# 1. pattern_id
	# 2. chr
	# 3. N (numero totale di regioni del genoma allineate con un pattern, in realta` `wc -l %.parsed3`, ovvero numero di coppie pattern-regione)
	# 4. M (numero di coppie pattern-regione in cui la regione e` sul cromosoma chr)
	# 5. n (numero di regioni allineate a pattern_id) 
	# 6. x (numero di regioni allineate a pattern_id e che stanno sul cromosoma chr)
	# 7. pvalue corretto Bonferroni

%.parsed3.chr_distrib.norm: %.parsed3.chr_distrib $(ANNOTATION_DIR)/chr_nomask_len
	

################### GO ########################

# A. annotazione sulle regioni del genoma ben allineate con un simbolo 

%.parsed3.annote_gene: %.parsed3 $(ANNOTATION_DIR)/coords_gene 
	sort -S10% -k 4,4 -k 1,1 -k 2,2n $< \
	| repeat_group_pipe '\
		echo ">$$1"; \
		sort -k 1,1 -k 2,2n \
		| $(BIN_DIR_ANNOTATION)/region_nearest_genes.pl -a $(word 2,$^) \
	' 4 > $@
	# formato output:
	# >pattern_id
	# 1. pattern_id
	# 2. gene_chr
	# 3. gene_b
	# 4. gene_e
	# 5. strand (?)
	# 6. ENSG / OTTHUMG
	# 7. b/i/a
	# 8. distance

%.parsed3.isect_only.annote_gene: %.parsed3.annote_gene
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		awk '\''{FS="\t"} $$7=="i"'\'' \
	' < $< > $@

%.parsed3.annote_gene.family: %.parsed3.annote_gene
	grep -v '^>' $< | cut -f 1,6 \
        | sed -r 's/,?OTTHUMG[0123456789]+//g' | awk '$$2' | sort -k2,2 | uniq > $@
	# formato output:
	# 1. pattern_id / family_id
	# 2. ENSG


# B. annotazione sulle regioni di partenza 

regions.isect_only.annote_gene: regions.annote_gene 
	awk '{FS="\t"} $$7=="i"' $< > $@

regions.annote_gene: $(REGIONS_LIST) $(ANNOTATION_DIR)/coords_gene 
	cut -f 2- $< | sort -k 1,1 -k 2,2n \
	| $(BIN_DIR_ANNOTATION)/region_nearest_genes.pl -a $(word 2,$^) > $@
	# formato output:
	# 1. regions_id (chr_b_e)
	# 2. gene_chr
	# 3. gene_b
	# 4. gene_e
	# 5. strand (?)
	# 6. ENSG / OTTHUMG
	# 7. b/i/a
	# 8. distance

%.annote_gene.family: %.annote_gene
	append_each_row -b 'regions' < $< \
        | sed -r 's/,?OTTHUMG[0123456789]+//g' | cut -f 1,7 \
	| awk '$$2' | sort -k2,2 | uniq > $@

##

%.annote_gene.family.go: %.annote_gene.family $(GO_ANNOTATION_DIR)/ensg_go.riannot $(GO_ANNOTATION_DIR)/keywords_term_type_level $(GO_ANNOTATION_DIR)/go_description
	$(BIN_DIR_ANNOTATION)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp -g $< \
	| Ipergeo_file | awk -F "\t" 'BEGIN{OFS="\t"}{print $$6, $$7, $$5, $$1, $$2, $$3, $$4, $$8}' \
	| sort -k2,2 \
	| join3_pl -1 1 -2 2 -i -u -- $(word 3,$^) - \
	| join3_pl -i -u -- $(word 4,$^) - \
	| sort_pl 6 \
	| awk -F "\t" 'BEGIN{OFS="\t"}{print $$5,$$1,$$2,$$3,$$4,$$6,$$7,$$8,$$9,$$10,$$11}' > $@
	rm -f $@.tmp
	# formato output: 
	# 1. pattern_id
	# 2. GO:.....
	# 3. description
	# 4. GO_term_type (biological_process, cellular_component, molecular_function)
	# 5. livello
	# 6. pvalue
	# 7. numero di geni totali
	# 8. numero di geni totali annotati alla parola
	# 9. numero di geni del pattern
	# 10. numero di geni del pattern associati alla parola
	# 11. elenco dei geni del pattern associati alla parola


.INTERMEDIATE: gene.coords
gene.coords: $(ANNOTATION_DIR)/coords_gene
	sort -S40% -k 1,1 -k2,2n -k3,3n $< > $@

.INTERMEDIATE: symbols.coords
symbols.coords: regions.wublast.0.99.conn_comp.commty2.conn_comp.consensus_matrix.out.patterns.genome.wublast.0.8.parsed3
	sort -S40% -k 1,1 -k2,2n -k3,3n $< > $@

symbols.gene: symbols.coords gene.coords
	( \
		$(BIN_DIR_ANNOTATION)/neighbor_genes $(word 2,$^) < $< | cut -f 1,3,7,8; \
		intersection $^ | awk 'BEGIN{OFS="\t"} {print $$8, $$10, "i", "0"}'; \
	) | sort -S40% -k1,1 > $@


symbols.go: symbols.gene $(GO_ANNOTATION_DIR)/ensg_go.riannot $(GO_ANNOTATION_DIR)/go_description_full
	cut -f 1,2 $< | sort | uniq \
	| $(BIN_DIR_ANNOTATION)/join_annotation -u -c $(GO_CUT_BONFERRONI) $(word 2,$^) $(word 3,$^) > $@

symbols.go.reduced: symbols.go $(GO_ANNOTATION_DIR)/go_tree
	sort -k1,1 -k9,9 symbols.go | awk '$$8<0.05' \
	| $(BIN_DIR_ANNOTATION)/go_reduce $(word 2,$^) \
	| grep 'biological_process' \
	| find_best 1 10 > $@

################## PFAM #######################

%.annote_gene.family.pfam: %.annote_gene.family $(PFAM_ANNOTATION_DIR)/gene_pfam $(PFAM_ANNOTATION_DIR)/pfam_acc_desc
	$(BIN_DIR_ANNOTATION)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp -g  $< \
	| Ipergeo_file | awk -F "\t" 'BEGIN{OFS="\t"}{print $$6, $$7, $$5, $$1, $$2, $$3, $$4, $$8}' \
	| sort -k2,2 \
	| join3_pl -1 1 -2 2 -i -u -- $(word 3,$^) - \
	| sort_pl 4 \
	| awk -F "\t" 'BEGIN{OFS="\t"}{print $$3,$$1,$$2,$$4,$$5,$$6,$$7,$$8,$$9}' > $@
	rm -f $@.tmp
	# formato output: 
	# 1. pattern_id
	# 2. PF...
	# 3. description
	# 4. pvalue
	# 5. numero di geni totali
	# 6. numero di geni totali annotati alla famiglia
	# 7. numero di geni del pattern
	# 8. numero di geni del pattern associati alla famiglia
	# 9. elenco dei geni del pattern associati alla famiglia


################ BEST PATTERNS ################

%.go.best: %.go
	cut -f -10 $< \
	| find_best_multi -r -s -n -m 1 6 > $@
	# formato output: 
	# 1. pattern_id
	# 2. GO:.....
	# 3. description
	# 4. GO_term_type (biological_process, cellular_component, molecular_function)
	# 5. livello
	# 6. best pvalue
	# 7. numero di geni totali
	# 8. numero di geni totali annotati alla parola
	# 9. numero di geni del pattern
	# 10. numero di geni del pattern associati alla parola

%.pfam.best: %.pfam
	cut -f -8 $< \
	| find_best_multi -r -s -n -m 1 4 > $@
	# formato output: 
	# 1. pattern_id
	# 2. PF...
	# 3. description
	# 4. best pvalue
	# 5. numero di geni totali
	# 6. numero di geni totali annotati alla famiglia
	# 7. numero di geni del pattern
	# 8. numero di geni del pattern associati alla famiglia

