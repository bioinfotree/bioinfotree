#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";


my $usage = " $0 qualcosa.conn_comp < qualcosa.score
	-id if ids in file qualcosa.conn_comp don't contain chr,b,e
	-ccomp if groups are connected components (e.g. no links between nodes from 2 different groups)";


my $id_only   = undef;
my $is_conn_comp = undef;
GetOptions (
	'id_only|id'  => \$id_only,
	'ccomp|ccomp' => \$is_conn_comp
) or die ($usage);

my $group_file = shift @ARGV;
die $usage if !$group_file;

open FH,$group_file or die("Can't open file ($group_file)");


my $group_id=undef;
my %group=();
my @all_group_id=();
my %n=();
while (my $line = <FH>) {
	chomp $line;
	my @F = split /\t/,$line;
	
	$group_id = shift @F;

	for(@F){
		$group{$_} = $group_id;
	}
	$n{$group_id} = scalar @F;
	push @all_group_id, $group_id;
}


my %edges=();
while(<>){
	chomp;
	my @F=split;
	if ($id_only) {
		$F[0] =~ s/:.*//g;
		$F[1] =~ s/:.*//g;
	}

	next if (!defined $group{$F[0]} or !defined $group{$F[1]});

	if ($is_conn_comp) {
		die ("ERROR: edge between nodes from different connected components!!") if ($group{$F[0]} ne $group{$F[1]});
	} else {
		next if ($group{$F[0]} ne $group{$F[1]}); # considero solo i links all'interno del gruppo
	}

	$edges{$group{$F[0]}}=0 if (!defined $edges{$group{$F[0]}});
	$edges{$group{$F[0]}}++;
}

for(@all_group_id){
	my $n_clique = ($n{$_} / 2) * ($n{$_} - 1);
	my $ratio = $edges{$_} / $n_clique;
	print $_, $n{$_}, $edges{$_}, $n_clique, $ratio;
}
