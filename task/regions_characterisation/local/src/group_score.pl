#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

use constant ID1 => 0;
use constant ID2 => 1;

$,="\t";
$\="\n";

my $usage = "cat regions.wublast.0.99.score | $0 -c regions.wublast.0.99.conn_comp.cliques.conn_comp [-id] [-ccomp]
	-id if ids in group_file don't contain chr,b,e";

my $group_file = undef;
my $id_only = undef;
GetOptions (
	'group_file|c=s' => \$group_file,
	'id_only|id'     => \$id_only,
) or die ($usage);
die $usage if !defined $group_file;

open GROUP_FILE,$group_file or die "Can't open file $group_file";


my %network = ();
while (<>) {
	chomp;
	my @F = split /\t/,$_;
	if ($id_only) {
		$F[ID1] =~ s/:.*//g;
		$F[ID2] =~ s/:.*//g;
	}
	$network{$F[ID1]}{$F[ID2]}=\@F;
	$network{$F[ID2]}{$F[ID1]}=\@F;
}


while (<GROUP_FILE>) {
	chomp;
	my @F = split /\t/,$_;
	print shift @F; # group_id
	for (my $i=0; $i<scalar @F; $i++){
		for (my $j=$i + 1; $j<scalar @F; $j++){
			print @{$network{$F[$i]}{$F[$j]}} if (defined $network{$F[$i]} and defined $network{$F[$i]}{$F[$j]});
		}
	}
}
