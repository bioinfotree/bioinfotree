#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my $coords_filename = shift @ARGV;
$coords_filename or die('ERROR: no cords file given');
open FH,$coords_filename or die("can't open file ($coords_filename)");

my %coords;
my $start_align_coords=0;
my $pre_id=undef;
while(<FH>){
	chomp;
	my ($id,$discard,@F)=split;
	$id=~s/_\d+//;
	
	$start_align_coords=0 if defined($pre_id) and $id != $pre_id;
	$pre_id=$id;
	
	my $l=$F[1] - $F[0];
	my @tmp=($start_align_coords, $start_align_coords + $l, $F[0]);
	$start_align_coords += $l + 1; # +1 per l'hyphen
	push @{$coords{$id}},\@tmp;
}

#my @aux=@{$coords{3}};
#for(@aux){print STDERR @{$_}};
#die;

while(<>){
	chomp;
	my @F=split;
	die('ERROR: undefined id ('.$F[0].')') if !defined($coords{$F[0]});
	
	my @exons = @{$coords{$F[0]}};
	my $offset = &my_shift($F[1],\@exons);
	$F[1] += $offset;
	$F[2] += $offset;
	
	my $last_idx=scalar(@exons);
	$last_idx--;
	print @F;
	die("ERROR: $F[2] > gene length") if $F[2] > $exons[$last_idx][2] + $exons[$last_idx][1] - $exons[$last_idx][0] ;
	
}

sub my_shift
{
	my $c = shift;
	my $exons_ref = shift;
	my @exons = @{$exons_ref};
	for(@exons){
		my ($start_exon_align, $stop_exon_align, $offset) = @{$_};
		next if( $c > $stop_exon_align );
		
		return $offset - $start_exon_align;
	}
	die("ERROR: coordinate out of gene");
}
