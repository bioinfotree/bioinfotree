SEQ_DIR1   ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/39
SEQ_DIR2   ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/39
SPECIES1   ?= hsapiens
SPECIES2   ?= hsapiens
ALIGN_TYPE ?= wublast

megablast_DEPS = \$$(SEQ_DIR1)/$${seq1}.fa.splitted \$$(SEQ_DIR2)/$${seq2}.fa.nsq
chaos_DEPS     = \$$(SEQ_DIR1)/$${seq1}.fa \$$(SEQ_DIR2)/$${seq2}.fa
wublast_DEPS   = \$$(SEQ_DIR1)/$${seq1}.fa.splitted \$$(SEQ_DIR2)/$${seq2}.fa.splitted.xns

BIN_DIR_PARENT := $(BIOINFO_ROOT)/task/alignments/local/bin

.DELETE_ON_ERROR:
.SECONDARY:
.PHONY: all clean \
        all.$(ALIGN_TYPE) clean.$(ALIGN_TYPE)

all : all.$(ALIGN_TYPE)
clean : clean.$(ALIGN_TYPE)
distclean : clean
	rm -f seqs.combo sequences-1.list sequences-2.list

include rules-algo.mk
include sequences.combo

sequences-1.list : makefile rules.mk $(SEQ_DIR1)/makefile
	grep '^ALL_DNA\s*.*=' $($(addsuffix $*,SEQ_DIR1))/makefile | sed 's|^ALL_DNA\s*.*=\s*||' | sed 's|\.fa||g' >$@

sequences-2.list : makefile rules.mk $(SEQ_DIR2)/makefile
	grep '^ALL_DNA\s*.*=' $($(addsuffix $*,SEQ_DIR2))/makefile | sed 's|^ALL_DNA\s*.*=\s*||' | sed 's|\.fa||g' >$@

sequences.combo: sequences-1.list sequences-2.list
	( \
		targets=""; \
		pdb_targets=""; \
		if [ $(SEQ_DIR1) == $(SEQ_DIR2) ]; then \
			seqs=( $$(cat sequences-1.list) ); \
			for ((i=0; i<$${#seqs[*]}; i=$$i+1)); do \
				for ((j=$$i; j<$${#seqs[*]}; j=$$j+1)); do \
					seq1=$${seqs[$$i]}; \
					seq2=$${seqs[$$j]}; \
					target="$(SPECIES1)_$${seq1}_$(SPECIES1)_$${seq2}.$(ALIGN_TYPE).gz"; \
					targets="$$targets $$target"; \
					pdb_targets="$$pdb_targets $${target%%.gz}.pdb.gz"; \
					echo -n "$$target : "; \
					echo "$($(ALIGN_TYPE)_DEPS)"; \
					echo -e "\t\$$(call $(ALIGN_TYPE)_single_run)"; \
				done; \
			done; \
		else \
			for seq1 in $$(cat sequences-1.list); do \
				for seq2 in $$(cat sequences-2.list); do \
					target="$(SPECIES1)_$${seq1}_$(SPECIES2)_$${seq2}.$(ALIGN_TYPE).gz"; \
					targets="$$targets $$target"; \
					pdb_targets="$$pdb_targets $${target%%.gz}.pdb.gz"; \
					echo -n "$$target : "; \
					echo "$($(ALIGN_TYPE)_DEPS)"; \
					echo -e "\t\$$(call $(ALIGN_TYPE)_single_run)"; \
				done; \
			done; \
		fi; \
		echo "all.$(ALIGN_TYPE) : $$targets"; \
		echo "clean.$(ALIGN_TYPE) :"; \
		echo -e "\trm -f $$targets"; \
		echo -e "\trm -f $$(echo $$targets | sed 's|.gz|.log|g')";\
		echo "all.pdb : $$pdb_targets"; \
		echo "clean.pdb :"; \
		echo -e "\trm -f $$pdb_targets"; \
	) >$@

%.fa %.fa.splitted %.fa.nsq %.fa.xns:
	cd $$(dirname $@) && $(MAKE) $$(basename $@)

%.$(ALIGN_TYPE).pdb.gz: %.$(ALIGN_TYPE).gz
	set -o pipefail; \
	set -e; \
	pdb_opt=""; \
	if [ "$(SPECIES1)" == "$(SPECIES2)" ]; then \
		pdb_opt="$$pdb_opt -s"; \
	fi; \
	zcat $< \
	| $(BIN_DIR)/per_database $$pdb_opts \
	| gzip >$@
