#!/bin/bash
exec awk 'BEGIN{OFS="\t"} {if ($9<$10) strand="+"; else {strand="-"; tmp=$9; $9=$10; $10=tmp}; print $1, $7, $8, strand, $2, $9, $10, $4, $12, $3, $11,"","" }'
