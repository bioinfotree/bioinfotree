from optparse import OptionParser
from pyPgSQL import PgSQL as pgsql
from sys import exit, stderr, stdout
from vfork.config.mapconfig import Configuration
import re

def openDatabaseConnections(conf, srcDatabase, destDatabase):
	srcConf = conf.as_dict()
	srcConf['database'] = srcDatabase
	srcConnection = pgsql.connect(**srcConf)
	
	destConf = conf.as_dict()
	destConf['database'] = destDatabase
	destConnection = pgsql.connect(**destConf)
	
	return srcConnection, destConnection

def getMethodId(connection, methodName):
	cursor = connection.cursor()
	try:
		cursor.execute('select id from runs where method=%s', methodName)
		row = cursor.fetchone()
		if row is None:
			print >>stderr, "Unknown method '%s'." % methodName
			exit(1)
		else:
			return row[0]
	finally:
		cursor.close()

def listTables(connection, filter):
	if filter is not None:
		filter = re.compile(filter, re.I)
	
	cursor = connection.cursor()
	try:
		cursor.execute("select tablename from pg_tables where schemaname='public'")
		tableList = [ r[0] for r in cursor.fetchall() ]
		if filter is not None:
			tableList = [ n for n in tableList if filter.search(n) is not None ]
		return tableList
			
	finally:
		cursor.close()

def tableCopy(srcConnection, srcTable, destConnection, methodId, verbose):
	srcCursor = srcConnection.cursor()
	destCursor = destConnection.cursor()
	
	try:
		if verbose:
			srcCursor.execute('select count(*) from %s' % srcTable)
			rowNum, = srcCursor.fetchone()
			
			rowIdx = -1
			lastProgress = 0
			
			stdout.write(' 0%')
			stdout.flush()
		
		srcCursor.execute('select qname, qstart, qend, qstrand, tname, tstart, tend, score from %s' % srcTable)
		while True:
			row = srcCursor.fetchone()
			if row is None:
				break
			
			if verbose:
				rowIdx += 1
				progress = rowIdx * 100 / rowNum
				
				if progress > lastProgress:
					lastProgress = progress	
					stdout.write('\b\b\b%2d%%' % progress)
					stdout.flush()
			
			row = list(row)
			row[0] = row[0][3:]
			row[4] = row[4][3:]
			if len(row[0]) > 2 or len(row[4]) > 2:
				continue
			
			qstart, qstop = row[1:3]
			tstart, tstop = row[5:7]
			row.append(max(qstop-qstart, tstop-tstart))
			
			row.append(methodId)
			
			query = '''
				insert into alignments (qchr, qstart, qstop, qstrand, tchr, tstart, tstop, score, length, run_id)
					   values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
			'''
			destCursor.execute(query, *row)
	
		destConnection.commit()
		
		if verbose:
			stdout.write('\b\b\b')
	
	finally:
		srcCursor.close()
		destCursor.close()

if __name__ == '__main__':
	parser = OptionParser(usage='%prog [options] SOURCE_DB DEST_DB METHOD')
	parser.add_option('-c', '--conf', dest='conf', default='db.conf', help='read database configuration from FILE', metavar='FILE')
	parser.add_option('-f', '--filter', dest='filter', help='a regular expression to FILTER source table names', metavar='FILTER')
	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', help='verbose output')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		print >>stderr, 'Wrong argument number.'
		exit(1)
	
	conf = Configuration().load(options.conf)
	srcConnection, destConnection = openDatabaseConnections(conf, *args[:2])
	
	try:
		methodId = getMethodId(destConnection, args[2])
		
		tableList = listTables(srcConnection, options.filter)
		for tableName in tableList:
			if options.verbose:
				stdout.write('copying %-30s ' % tableName)
				stdout.flush()
			
			tableCopy(srcConnection, tableName, destConnection, methodId, options.verbose)
			
			if options.verbose:
				stdout.write('[DONE]\n')
	
	finally:
		srcConnection.close()
		destConnection.close()
