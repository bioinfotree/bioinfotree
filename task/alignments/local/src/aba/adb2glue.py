#!/usr/bin/env python
from __future__ import with_statement
from optparse import OptionParser
from sys import stdin
from vfork.alignment.adb import iter_alignments_simple
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.util import exit, format_usage

def load_sequence_serials(filename):
	serial = 1
	serials = {}
	
	with file(filename, 'r') as fd:
		for header, sequence in MultipleBlockStreamingReader(fd):
			serials[header] = serial
			serial += 1
	
	return serials

def main():
	usage = format_usage('''
		%prog SEQUENCES <ADB

		SEQUENCES is a multi-FASTA holding the aligned sequences.
	''')
	parser = OptionParser(usage=usage)

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	sequence_serials = load_sequence_serials(args[0])
	
	for alignment in iter_alignments_simple(stdin):
		gaps = [ (g[0], g[1], 'query') for g in alignment.query_gaps ] + \
		       [ (g[0], g[1], 'target') for g in alignment.target_gaps ]
		gaps.sort()
		gaps.append((alignment.length, 0, 'end'))
		gaps.reverse()

		offset = query_offset = 0
		if alignment.strand == '+':
			target_offset = 0
			target_direction = 1
		else:
			target_offset = alignment.target_stop - alignment.target_start - 1
			target_direction = -1
		
		query_serial = sequence_serials[alignment.query_label]
		target_serial = sequence_serials[alignment.target_label]
		if alignment.strand == '-':
			target_serial *= -1
		
		while len(gaps):
			gap = gaps.pop()

			while offset < gap[0]:
				print '%10d %d; \t%10d %d;' % (alignment.query_start+query_offset+1, query_serial, alignment.target_start+target_offset+1, target_serial)
				offset += 1
				query_offset += 1
				target_offset += target_direction

			offset += gap[1]
			if gap[2] == 'target':
				query_offset += gap[1]
			else:
				target_offset += gap[1] * target_direction


if __name__ == '__main__':
	main()

