#!/usr/bin/env python
from optparse import OptionParser
from os import stat
from os.path import isfile, join
from sys import exit, stdin, stdout
from vfork.alignment.adb import iter_alignments_simple
from vfork.alignment.base import AlignedSequences
from vfork.fasta.reader import SingleBlockReader, MultipleBlockReader
from vfork.io.util import format_usage

def same_file(a, b):
	a_info = stat(a)
	b_info = stat(b)
	return a_info.st_ino == b_info.st_ino

def sequence_path(dirname, name):
	path = join(dirname, '%s.fa' % name)
	if not isfile(path):
		path = join(dirname, 'chr%s.fa' % name)
	return path

def main():
	usage = format_usage('''
		%prog [OPTIONS] QUERY_SPEC TARGET_SPEC <PDB
		
		QUERY_SPEC and TARGET_SPEC are the directories where the
		FASTA files for query and target sequences can be found.
		If the script runs in single file mode (-s/--single-file)
		they should instead specify two multi-FASTA files.
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-s', '--single-file', dest='single_file', action='store_true', default='False', help='enable single file mode')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	if options.single_file:
		query_sequence_provider = MultipleBlockReader(args[0])
		if same_file(args[0], args[1]):
			target_sequence_provider = query_sequence_provider
		else:
			target_sequence_provider = MultipleBlockReader(args[1])
	else:
		query_sequence = target_sequence = None

	for alignment in iter_alignments_simple(stdin):
		if options.single_file:
			query_sequence = query_sequence_provider[alignment.query_label]
			target_sequence = target_sequence_provider[alignment.target_label]
		else:
			if query_sequence is None or query_sequence.header != alignment.query_label:
				query_sequence = SingleBlockReader(sequence_path(args[0], alignment.query_label))
			if target_sequence is None or target_sequence.header != alignment.target_label:
				target_sequence = SingleBlockReader(sequence_path(args[1], alignment.target_label))
		
		aseq = AlignedSequences(alignment, query_sequence, target_sequence)
		aseq.display(stdout, 80)

if __name__ == '__main__':
	main()
