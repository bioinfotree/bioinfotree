from database import Database
from os import chdir, mkdir, system
from os.path import dirname, isdir, isfile, join
from sys import argv, exit, stderr
from vfork.config.mapconfig import Configuration

def print_error(msg):
	print >>stderr, 'ERROR:', msg
	exit(1)

def changeWorkingDir():
    dirName = dirname(argv[0])
    if len(dirName):
        chdir(dirName)

def getMethods(conf, db):
	methodNames = [ n for n in (n.strip() for n in conf['compara-methods'].split(',')) if len(n) ]
	if len(methodNames) == 0:
		return {}
	
	methodMap = db.fetchMethods()
	aux = {}
	for methodName in methodNames:
		if not methodName in methodMap:
			print_error('no such method "%s"' % methodName)
		else:
			aux[methodName] = methodMap[methodName]
	
	return aux

if __name__ == '__main__':
	changeWorkingDir()
	
	conf = Configuration()
	conf.load('batch.conf')
	
	db = Database(conf['database-host'], conf['database-user'], conf['database-password'], conf['compara-db'])
	
	try:
		invMethodMap = getMethods(conf, db)
		speciesMap = db.fetchSpecies()
		methodSpeciesSetSeen = set()
		sequenceDatabasePath = join(conf['compara-flatfiles-dir'], 'genomic_align_block.txt.table.gz')
		
		for methodName, methodId in invMethodMap.iteritems():
			methodPath = methodName.lower()
			if not isdir(methodPath):
				mkdir(methodPath)
			
			for querySpeciesId, querySpeciesName in speciesMap.iteritems():
				mangledQuerySpeciesName = querySpeciesName.lower().replace(' ', '_')
				targetSpeciesMap = db.fetchTargetSpecies(querySpeciesId, methodId)
				
				for targetSpeciesId, methodSpeciesSetId in targetSpeciesMap.iteritems():
					if methodSpeciesSetId in methodSpeciesSetSeen:
						continue
					
					targetSpeciesName = speciesMap[targetSpeciesId]
					mangledTargetSpeciesName = targetSpeciesName.lower().replace(' ', '_')
					targetSpeciesPrefix = join(methodPath, '%s-%s' % (mangledQuerySpeciesName, mangledTargetSpeciesName))
					targetSpeciesPath = targetSpeciesPrefix + '-lengths.txt'
					methodSpeciesSetSeen.add(methodSpeciesSetId)
					
					print '%s alignments between %s and %s' % (methodName, querySpeciesName, targetSpeciesName)
					if isfile(targetSpeciesPath):
						print '\t* file already present; skipping'
						continue
					
					print '\t* fetching'
					res = system("set -e; gzcat %s | awk '$2==%d {print $5}' | sort -n | uniq -c > %s" % (sequenceDatabasePath, methodSpeciesSetId, targetSpeciesPath))
					if res != 0:
						print_error('fetch pipeline failed')
					
					print '\t* plotting'
					res = system("'%s' -batchinput -batchoutput %s %s < graph.m >/dev/null" % (conf['mathematica-kernel'], targetSpeciesPath, targetSpeciesPrefix))
					if res != 0:
						print_error('mathematica failed')
					
	finally:
		db.close()
