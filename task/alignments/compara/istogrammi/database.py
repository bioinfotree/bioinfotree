import MySQLdb

class Database(object):
	def __init__(self, host, user, password, database):
		'''Opens a connection to the Ensembl-like database.
		
		   @param host: the host name of the database server.
		   @param user: the user name.
		   @param password: the password (may be empty).
		   @param database: the database name.
		'''
		
		self.host = host
		self.user = user
		self.database = database
		
		self.connection = MySQLdb.connect(host=host, user=user, passwd=password, db=database)
		self.cursor = self.connection.cursor()
	
	def close(self):
		'''Closes the connection, releasing all the associated resources.
		'''
		self.cursor.close()
		self.connection.close()
	
	def fetchSpecies(self):
		'''Retrieves informations about species.
		
		   @return: a B{dict} of (id, name) pairs.
		'''
		self.cursor.execute('select genome_db_id, name from genome_db')
		return dict(self.cursor.fetchall())
	
	def fetchMethods(self):
		'''Retrieves informations about the methods used to compute alignments.
		
		   @return: a B{dict} of (name, id) pairs.
 		'''
		self.cursor.execute('select type, method_link_id from method_link')
		return dict(self.cursor.fetchall())
	
	def fetchTargetSpecies(self, querySpecies, method):
		'''Computes the list of all species that have been aligned with I{querySpecies}.
		
		   @param querySpecies: the id of the query species.
		   @param method: the id of an alignment method.
		   @return: a B{dict} of (speciesId, methodSpeciesSetId) pairs.
		'''
		# find auto-alignments and whose between two different organisms
		query = '''
			select mlss2.method_link_species_set_id, count(*) as count
			from method_link_species_set as mlss1, method_link_species_set as mlss2
			where
				mlss1.method_link_id = %s and
				mlss1.genome_db_id = %s and
				mlss2.method_link_species_set_id = mlss1.method_link_species_set_id
			group by mlss2.method_link_species_set_id
			having count = 1 or count = 2
		'''
		self.cursor.execute(query, (method, querySpecies))
		rows = self.cursor.fetchall()
		
		aux = {}
		for row in rows:
			methodLinkId = row[0]
			if row[1] == 1:
				aux[querySpecies] = methodLinkId
			else:
				query = '''
					select genome_db_id
					from method_link_species_set
					where
						method_link_species_set_id = %s and
						genome_db_id != %s
				'''
				self.cursor.execute(query, (methodLinkId, querySpecies))
				for row in self.cursor:
					aux[row[0]] = methodLinkId
		
		return aux
