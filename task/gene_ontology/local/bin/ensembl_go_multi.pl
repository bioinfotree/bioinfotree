#! /usr/bin/perl -wd

use strict;
use warnings;
use DBI;
use Getopt::Long;

my $usage="
cat ENSG_file | ensembl_go_multi.pl -v 40
";

########################################
#definizione dei capostipiti delle ontologie

	#mysql> select term2term.term2_id from term, term2term where  term.id=term2term.term1_id and term.is_root=1;
	#+----------+
	#| term2_id |
	#+----------+
	#|       11 |
	#|       12 |
	#|       42 |
	#|      296 |
	#|     1468 |
	#|     3396 |
	#+----------+
	
	#mysql> select term.id, term.name from term where id IN(11,12,13,42,296,1468,3396);
	#+------+--------------------+
	#| id   | name               |
	#+------+--------------------+
	#|   11 | biological_process |
	#|   12 | obsolete_function  |
	#|   13 | top                |
	#|   42 | obsolete_process   |
	#|  296 | obsolete_component |
	#| 1468 | molecular_function |
	#| 3396 | cellular_component |
	#+------+--------------------+
	#7 rows in set (0.05 sec)
########################################
	
	
	#query utilii
	#
	#
	#
	#SELECT term2_id, count( term2_id ) , min( distance )
	#FROM graph_path
	#WHERE term1_id = '3396'
	#GROUP BY term2_id
	#LIMIT 0 , 30
	#
	#


my %ontology_table = (
	'biological_process'=>'hsapiens_go_biol_process__go_biol_process__main',
	'cellular_component'=>'hsapiens_go_cell_location__go_cell_location__main',
	'molecular_function'=>'hsapiens_go_mol_function__go_mol_function__main'
);

my $ontology = "'".join("','",keys %ontology_table)."'";
	
#my $input_level=-1;
my $ensembl_version=undef;

GetOptions (
	'version|v=i' => \$ensembl_version,
) or die($usage);
die($usage) if (!$ensembl_version);

my $db = DBI->connect( "dbi:mysql:ensembl_mart_$ensembl_version:ensembldb.ensembl.org", "anonymous", "", {
	PrintError => 1 } ) || die "Can't connect to mySQL database: $DBI::errstr\n";


MAIN:{
	my $sql="SELECT distinct term.id, term.name FROM ensembl_go_$ensembl_version.term 
		WHERE id IN 
			(SELECT term2term.term2_id FROM ensembl_go_$ensembl_version.term, ensembl_go_$ensembl_version.term2term 
			WHERE term.id=term2term.term1_id AND term.is_root=1)
		AND term.name in ($ontology);";
	my $st=$db->prepare($sql);
	$st->execute();
	my $R_data = $st->fetchall_arrayref();
	my %ontology_founder_id =(); 
	for(@$R_data){
		$ontology_founder_id{@$_[1]}=@$_[0];
	}

	foreach my $input_ontology_name (keys %ontology_founder_id) {
		my $input_ontology_id=$ontology_founder_id{$input_ontology_name} or die("ERROR: ontology $input_ontology_name not found\n");
		my $input_ontology_table=$ontology_table{$input_ontology_name} or die("ERROR: ontology $input_ontology_name not found\n");

		while (my $input_gene=<>) {
			chomp($input_gene);
				
			my $words_of_gene_ref = &find_all_keywords($input_ontology_table,$input_gene);
			my @words_of_gene=keys(%$words_of_gene_ref);

			next if (!@words_of_gene);


#			my $words_at_level_ref = &filter_keywords_for_level(\@words_of_gene, $input_level, $input_ontology_id);
#			my @words_at_level=keys(%$words_at_level_ref);


#			my $parent_words_ref=&find_parent_words(\@words_at_level);
			my $parent_words_ref=&find_parent_words(\@words_of_gene);
			my @parent_words = keys(%$parent_words_ref);
			if(scalar @parent_words > 0 ){
				print $input_gene,"\t",$input_ontology_name,"\t";
				print join("\n$input_gene\t$input_ontology_name\t",@parent_words),"\n";
			}			


#			my $children_words_ref=&find_children_words(\@words_at_level);
#			my @children_words = keys(%$children_words_ref);


#			my $related_genes_ref=&find_all_genes(\@children_words);
#			if(scalar(@$related_genes_ref) > 0 ){
#				print $input_gene,"\t",join("\n$input_gene\t",@$related_genes_ref),"\n";
#			}			
			
		}
	}

}



sub find_all_keywords{
	my $f_usage="&find_all_keywords('hsapiens_go_cell_location__go_cell_location__main')";
	my $table = shift @_ or die($f_usage);
	my $gene = shift @_ or die($f_usage);

#	my $st = $db->prepare("select DISTINCT gene_stable_id, dbprimary_id, description FROM $table WHERE dbprimary_id is not NULL;");
	my $sql = "select DISTINCT gene_stable_id, dbprimary_id FROM $table WHERE dbprimary_id is not NULL AND gene_stable_id=?;";
	my $st = $db->prepare($sql);
#	my $st = $db->prepare("select DISTINCT gene_stable_id, dbprimary_id, description FROM $table WHERE dbprimary_id is not NULL AND gene_stable_id='$gene';");
	$st->execute($gene);
	$st->execute();
#	my $st = $db->prepare("select DISTINCT dbprimary_id, description from $table WHERE gene_stable_id=? and dbprimary_id is not NULL;");
#	$st->execute($ENSG) or die $db->errstr;
	
	my $R_data = $st->fetchall_arrayref();
	my %words_of_gene;
	for(@$R_data){
		#print @$_[0],"\t",@$_[1],"\n";
		$words_of_gene{@$_[1]}=@$_[0];
	}
	return \%words_of_gene;
}



sub filter_keywords_for_level{
	my $f_usage='$words_ref=filter_keywords_for_level(\@word,$level,$ontology_founder_id)';
	my $words_ref=shift(@_) or die($f_usage);
	my $level=shift(@_);
		$level >=0 or die($f_usage);
	my $ontology_founder_id=shift(@_) or die($f_usage);
	my $in= join("','",@$words_ref);
	#my $sql="select ensembl_go_$ensembl_version.graph_path.term2_id 
	#		from ensembl_go_$ensembl_version.graph_path, ensembl_go_$ensembl_version.term 
	#		where ensembl_go_$ensembl_version.graph_path.term2_id=ensembl_go_$ensembl_version.term.id". #inpongo che il termine figlio 
	#"			and ensembl_go_$ensembl_version.term.acc IN('$in')".			#sia un termine associato al gene
	#"			and ensembl_go_$ensembl_version.graph_path.term1_id=?".		#e che il padre sia il capostipite
	#"			and distance=?";					#e che la distana sia il livello

	my $sql="SELECT distinct 
		ensembl_go_$ensembl_version.graph_path.term2_id,
		ensembl_go_$ensembl_version.term.name 
	FROM ensembl_go_$ensembl_version.graph_path
	LEFT JOIN ensembl_go_$ensembl_version.term 
		ON ensembl_go_$ensembl_version.graph_path.term2_id=ensembl_go_$ensembl_version.term.id 
	WHERE
		ensembl_go_$ensembl_version.term.acc IN('$in') 
			and
		ensembl_go_$ensembl_version.graph_path.term1_id=?
	GROUP BY
		term2_id
	HAVING min(ensembl_go_$ensembl_version.graph_path.distance)=?;";
		
	my $st=$db->prepare($sql);
	
	$st->execute($ontology_founder_id,$level);
	$st->execute();
	my $R_data = $st->fetchall_arrayref();

	my %words_at_level;
	for(@$R_data){
		$words_at_level{@$_[0]}=@$_[1];
	}
	return \%words_at_level;
}



sub find_children_words{
	my $f_usage="\$words_id_array_ref=find_children_words(\@words)";
	my $aux=shift(@_) or die($f_usage);
	my $in=join("','",@$aux);
	my $st=$db->prepare("
		select DISTINCT ensembl_go_$ensembl_version.term.acc, ensembl_go_$ensembl_version.term.name 
		from ensembl_go_$ensembl_version.graph_path
		LEFT JOIN ensembl_go_$ensembl_version.term on ensembl_go_$ensembl_version.term.id=ensembl_go_$ensembl_version.graph_path.term2_id 
		where ensembl_go_$ensembl_version.graph_path.term1_id IN ('$in')
	");
	$st->execute();
	my $R_data=$st->fetchall_arrayref();
	my %out;
	for(@$R_data){
		$out{@$_[0]}=@$_[1];
	}
	return \%out;
}



sub find_parent_words{
	my $f_usage="\$words_id_array_ref=find_parent_words(\@words)";
	my $aux=shift(@_) or die($f_usage);
	my $in=join("','",@$aux);
	my $st=$db->prepare("
		select DISTINCT ensembl_go_$ensembl_version.term.acc, ensembl_go_$ensembl_version.term.name 
		from ensembl_go_$ensembl_version.graph_path
		LEFT JOIN ensembl_go_$ensembl_version.term on ensembl_go_$ensembl_version.term.id=ensembl_go_$ensembl_version.graph_path.term1_id 
		where ensembl_go_$ensembl_version.graph_path.term2_id IN ('$in')
	");
	print "select DISTINCT ensembl_go_$ensembl_version.term.acc, ensembl_go_$ensembl_version.term.name\nfrom ensembl_go_$ensembl_version.graph_path\n";
	print "LEFT JOIN ensembl_go_$ensembl_version.term on ensembl_go_$ensembl_version.term.id=ensembl_go_$ensembl_version.graph_path.term1_id\nwhere ensembl_go_$ensembl_version.graph_path.term2_id IN ('$in');";
	$st->execute();
	my $R_data=$st->fetchall_arrayref();
	my %out;
	for(@$R_data){
		$out{@$_[0]}=@$_[1];
	}
	return \%out;
}



sub find_all_genes{
	my $f_usage="\$related_genes_ref=find_all_genes(\$children_words_ref);\n";	
	my $aux=shift(@_) or die($f_usage);
	my $in=join("','",@$aux);
	my $st = $db->prepare(
		"SELECT DISTINCT gene_stable_id 
		FROM hsapiens_gene_ensembl__xref_go__dm 
		WHERE dbprimary_id IN ('$in')"
	);
	$st->execute();
	my $R_data = $st->fetchall_arrayref();
	my @related_genes;
	for(@$R_data){
		push(@related_genes, @$_[0]);
	}
	return \@related_genes;
}

