#!/usr/bin/perl 
use warnings;
use strict;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "go_groups.pl -f ENSG_ontology_file -b \$(BIN_DIR) [-c pvalue_cutoff] [-d go_description_file] gene_groups_file\n";
my $go_file = undef;
my $descr_file = undef;
my $pvalue_cutoff = undef;
my $bin_dir = undef;

GetOptions (
	'GO_file|f=s' => \$go_file,
	'bin_dir|b=s' => \$bin_dir,
	'cutoff|c=f' => \$pvalue_cutoff,
	'descr|d=s' => \$descr_file
) or die($usage);

$bin_dir or die ("Can't open directory BIN_DIR\n$usage");


if (!defined $pvalue_cutoff){
	$pvalue_cutoff = 1;
}


my %go_description=();
if (defined $descr_file){
	open DESCR, $descr_file or die("Can't open file ($descr_file)\n");
	while (<DESCR>){
		chomp;
		my ($go_word,$go_descr) = split(/\t/,$_);
		$go_description{$go_word} = $go_descr;
	}
}


open GO_FILE , $go_file or die("Can't open file ($go_file)\n");

my %words=();
my %gene_words=();
while(<GO_FILE>){
	chomp;
	my ($gene, $go_word) = split;
	if (!defined $words{$go_word}) {
		my @tmp=($gene);
		$words{$go_word} = \@tmp;
	} else {
		push @{$words{$go_word}},$gene;
	}
	if (!defined  $gene_words{$gene}) {
		my @tmp2=($go_word);
		$gene_words{$gene} = \@tmp2;
	} else {
		push @{$gene_words{$gene}},$go_word;
	}
}


my %group_words=();
my $n = 0;
while(my $gene = <>){
	chomp $gene;
	if (defined $gene_words{$gene}) {
		$n++;
		my @gene_words = @{$gene_words{$gene}};
		foreach my $go_word (@gene_words) {
			if (!defined $group_words{$go_word}) {
				my @tmp=($gene);
				$group_words{$go_word} = \@tmp;
			} else {
				push @{$group_words{$go_word}},$gene;
			}
		}
	}
}


my $N = scalar keys %gene_words;
my %M = ();
my %x = ();
foreach my $go_word (keys %group_words) {
	$M{$go_word}=scalar @{$words{$go_word}};
	$x{$go_word}=scalar @{$group_words{$go_word}};
}
	 


if (defined $descr_file){
	foreach my $go_word (keys %group_words) {
		my $tmp = `$bin_dir/Ipergeo $N $M{$go_word} $n $x{$go_word}`;
		chomp $tmp;
		my ($par, $pvalue)=split /Pvalue\s*:\s*/,$tmp;
		if ($pvalue <= $pvalue_cutoff) {
			print $go_word,$pvalue,$go_description{$go_word};
	#		print $go_word,$tmp,$go_description{$go_word};
		}
	}

} else {
	foreach my $go_word (keys %group_words) {
		my $tmp = `./Ipergeo $N $M{$go_word} $n $x{$go_word}`;
		chomp $tmp;
		my ($par, $pvalue)=split /Pvalue\s*:\s*/,$tmp;
		if ($pvalue <= $pvalue_cutoff) {
			print $go_word,$pvalue;
	#		print $go_word,$tmp;
		}
	}
}



